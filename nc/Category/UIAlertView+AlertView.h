//
//  UIAlertView+AlertView.h
//  nc
//
//  Created by docy admin on 7/14/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (AlertView)

+ (void)alertViewWithTitle:(NSString *)title;
// 需要代理
+ (UIAlertView *)alertViewWithTitle:(NSString *)title target:(id)target;
+ (UIAlertView *)alertViewWithTitle:(NSString *)title subTitle:(NSString *)subTitle target:(id)target;
@end
