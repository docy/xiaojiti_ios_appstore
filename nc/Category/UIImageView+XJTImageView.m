//
//  UIImageView+XJTImageView.m
//  nc
//
//  Created by docy admin on 15/11/3.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "UIImageView+XJTImageView.h"

@implementation UIImageView (XJTImageView)

+ (UIImageView *)imageViewWithImage:(NSString *)imageName{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    imageView.frame = [[UIScreen mainScreen] bounds];
    return imageView;
}

+ (void)hiddenImageView:(UITapGestureRecognizer *)tap{
    UIImageView *imageView = (UIImageView *)tap.view;
    [UIView animateWithDuration:0.5 animations:^{
        imageView.alpha = 0;
    } completion:^(BOOL finished) {
        [imageView removeFromSuperview];
    }];
    
}

+ (void)imageViewWithBgImage:(NSString *)imageName{
    AppDelegate *deleagte = APP_DELEGATE;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    imageView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenImageView:)];
    imageView.userInteractionEnabled = YES;
    [imageView addGestureRecognizer:tap];
    imageView.alpha = 0;
    [deleagte.window addSubview:imageView];
    
    [UIView animateWithDuration:0.5 animations:^{
        imageView.alpha = 1.0;
    } completion:^(BOOL finished) {
    }];
}

@end
