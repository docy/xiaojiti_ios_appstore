//
//  NSString+XJTString.h
//  nc
//
//  Created by docy admin on 15/8/26.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (XJTString)

// 删除空格
+ (NSString *)stringThrowOffBlankWithString:(NSString *)string;

// 删除换行
+ (NSString *)stringThrowOffLinesWithString:(NSString *)string;

// 删除空格 换行
+ (NSString *)stringThrowOffBlankAndLinesWithString:(NSString *)string;

// 获取集体类别
+ (NSString *)stringGetCatoryWithNumber:(NSInteger)number;

+ (void)stringGetErrorCodeWithCode:(NSNumber *)code;

@end
