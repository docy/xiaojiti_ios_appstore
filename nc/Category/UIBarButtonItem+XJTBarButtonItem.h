//
//  UIBarButtonItem+XJTBarButtonItem.h
//  nc
//
//  Created by docy admin on 6/4/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (XJTBarButtonItem)

+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action;
+ (UIBarButtonItem *)itemWithTitle:(NSString *)title target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)itemLeftItemWithTarget:(id)target action:(SEL)action;
@end
