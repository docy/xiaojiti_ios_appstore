//
//  UITabBarController+CustomTabBarController.m
//  nc
//
//  Created by docy admin on 6/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "UITabBarController+CustomTabBarController.h"

#import "XJTNavigationController.h"
#import "LeftViewController.h"
#import "AddressBookTableViewController.h"
//#import "AddressBookViewController.h"

#import "MessageNotifyViewController.h"
#import "PersonalProfileViewController.h"
#import "UserProfileViewController.h"

@implementation UITabBarController (CustomTabBarController)
+ (UITabBarController *)creatTabBarController
{
    // 创建tabBar  XJTNavigationController
    LeftViewController *left = [LeftViewController new];
    XJTNavigationController *groupVC = [[XJTNavigationController alloc] initWithRootViewController:left];
    
    left.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"小组" image:[UIImage imageNamed:@"toolbar_group"] selectedImage:[UIImage imageNamed:@"selected_group"]];
    
    XJTNavigationController *addressBookVC = [[XJTNavigationController alloc] initWithRootViewController:[AddressBookTableViewController new]];
    addressBookVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"通讯录" image:[UIImage imageNamed:@"toolbar_address"] selectedImage:[UIImage imageNamed:@"selected_address"]];
    
    XJTNavigationController *notifyVC = [[XJTNavigationController alloc] initWithRootViewController:[MessageNotifyViewController new]];
    notifyVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"关注" image:[UIImage imageNamed:@"Focus_normal"] selectedImage:[UIImage imageNamed:@"Focus_selected"]];
    
    XJTNavigationController *setVC = [[XJTNavigationController alloc] initWithRootViewController:[PersonalProfileViewController new]];
    setVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"设置" image:[UIImage imageNamed:@"toolbar_set"] selectedImage:[UIImage imageNamed:@"selected_set"]];
    
    UITabBarController *tab = [[UITabBarController alloc] init];
    tab.viewControllers = @[groupVC,addressBookVC,notifyVC,setVC];
    tab.tabBar.tintColor = [UIColor colorWithRed:72/255.0 green:193/255.0 blue:168/255.0 alpha:1];
    
    return tab;
}


@end
