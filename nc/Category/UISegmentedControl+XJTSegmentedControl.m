//
//  UISegmentedControl+XJTSegmentedControl.m
//  nc
//
//  Created by docy admin on 15/9/23.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "UISegmentedControl+XJTSegmentedControl.h"

@implementation UISegmentedControl (XJTSegmentedControl)

+ (UISegmentedControl *)segmentedControlWithItems:(NSArray *)items target:(id)target action:(SEL)action
{
    UISegmentedControl *seg = [[UISegmentedControl alloc] initWithItems:items];

    seg.selectedSegmentIndex = 0;
    [seg setWidth:ScreenWidth*0.2 forSegmentAtIndex:0];
    [seg setWidth:ScreenWidth*0.2 forSegmentAtIndex:1];
    [seg addTarget:target action:action forControlEvents:UIControlEventValueChanged];
//    seg.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1];
    seg.tintColor = [UIColor whiteColor];
    return seg;
}

@end
