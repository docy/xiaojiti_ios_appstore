//
//  UIButton+XJTButton.h
//  nc
//
//  Created by docy admin on 6/4/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (XJTButton)

- (instancetype)setNormalImage:(NSString *)normal andSelectedImage:(NSString *)selected andFrame:(CGRect)rect addTarget:(id)target action:(SEL)action;

- (instancetype)setNormalTitle:(NSString *)normalTitle andSelectesTitle:(NSString *)selectedTitle andFrame:(CGRect)rect addTarget:(id)target action:(SEL)action;

+ (instancetype)buttonWithFrame:(CGRect)frame backgroundColor:(UIColor *)color cornerRadius:(CGFloat)cornerRadius;

+ (instancetype)buttonWithFrame:(CGRect)frame backgroundImage:(UIImage *)image target:(id)target action:(SEL)action;

@end
