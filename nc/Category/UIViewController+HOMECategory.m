//
//  UIViewController+HOMECategory.m
//  HomeToolsTest
//
//  Created by mahaomeng on 15/11/24.
//  Copyright © 2015年 Mahaomeng. All rights reserved.
//

#import "UIViewController+HOMECategory.h"
#import "AFHTTPSessionManager.h"
#import <objc/runtime.h>

static const void *reRequestBlockKey = &reRequestBlockKey;
static const void *connectFlagKey =&connectFlagKey;

@implementation UIViewController (HOMECategory)

@dynamic connectFlag;
@dynamic reRequestBlock;

-(void)startReachability
{
    AFNetworkReachabilityManager *reach = [AFNetworkReachabilityManager sharedManager];
    __weak typeof(self) myself = self;
    [reach setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown: // 未知网络
                if (myself.reRequestBlock &&myself.connectFlag.integerValue ==status) {
                    myself.reRequestBlock();
                }
                NSLog(@"未知网络");
                break;
                
            case AFNetworkReachabilityStatusNotReachable: // 没有网络(断网)
                NSLog(@"没有网络(断网)");
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN: // 手机自带网络
                if (myself.reRequestBlock &&myself.connectFlag.integerValue ==status) {
                    myself.reRequestBlock();
                }
                NSLog(@"手机自带网络");
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi: // WIFI
                if (myself.reRequestBlock &&myself.connectFlag.integerValue ==status) {
                    myself.reRequestBlock();
                }
                NSLog(@"WIFI");
                break;
        }
        myself.connectFlag = [NSString stringWithFormat:@"%lu", status];
        NSLog(@"%@", myself.connectFlag);
    }];
    [reach startMonitoring];
}

-(void)stopReachability
{
    AFNetworkReachabilityManager *reach = [AFNetworkReachabilityManager sharedManager];
    [reach stopMonitoring];
}

-(void)GETHttpRequest:(NSString *)urlStr parameters:(id)para success:(HOMECategoryHttpRequestSuccessBlock)success failure:(HOMECategoryHttpRequestFailureBlock)failure
{
    __weak typeof(self) myself = self;
    self.reRequestBlock = ^(){
        [myself stopReachability];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:urlStr parameters:para success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            id obj = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            success(obj);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [myself startReachability];
            });
            failure(error.localizedDescription);
        }];
    };
    if (self.reRequestBlock) {
        self.reRequestBlock();
    }
}

-(void)POSTHttpRequest:(NSString *)urlStr parameters:(id)para success:(HOMECategoryHttpRequestSuccessBlock)success failure:(HOMECategoryHttpRequestFailureBlock)failure
{
    __weak typeof(self) myself = self;
    self.reRequestBlock = ^(){
        [myself stopReachability];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager POST:urlStr parameters:para success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            id obj = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            success(obj);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [myself startReachability];
            });
            failure(error.localizedDescription);
        }];
    };
    if (self.reRequestBlock) {
        self.reRequestBlock();
    }
}

-(void)setReRequestBlock:(HOMECategoryReConnectNetWorkBlock)reRequestBlock
{
    objc_setAssociatedObject(self, reRequestBlockKey, reRequestBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(HOMECategoryReConnectNetWorkBlock)reRequestBlock
{
    return objc_getAssociatedObject(self, reRequestBlockKey);
}

-(void)setConnectFlag:(NSString *)connectFlag
{
    objc_setAssociatedObject(self, connectFlagKey, connectFlag, OBJC_ASSOCIATION_COPY_NONATOMIC);
   
}

-(NSString *)connectFlag
{
    return objc_getAssociatedObject(self, connectFlagKey);

}



@end
