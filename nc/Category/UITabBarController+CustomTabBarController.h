//
//  UITabBarController+CustomTabBarController.h
//  nc
//
//  Created by docy admin on 6/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (CustomTabBarController)<UIGestureRecognizerDelegate>

+ (UITabBarController *)creatTabBarController;

@end
