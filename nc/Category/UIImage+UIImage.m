//
//  UIImage+UIImage.m
//  nc
//
//  Created by docy admin on 16/3/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "UIImage+UIImage.h"

@implementation UIImage (UIImage)


+ (UIImage *)stretchableWithImageName:(NSString *)imageName{
    
    UIImage *image = [UIImage imageNamed:imageName];
    
    // 左端盖宽度
    NSInteger leftCapWidth = image.size.width * 0.5f;
    // 顶端盖高度
    NSInteger topCapHeight = image.size.height * 0.5f;
    // 重新赋值
    return [image stretchableImageWithLeftCapWidth:leftCapWidth topCapHeight:topCapHeight];
}

+ (UIImage *)resizableWithImageName:(NSString *)imageName{
    
    UIImage *image = [UIImage imageNamed:imageName];
    
    CGFloat top = 25; // 顶端盖高度
    CGFloat bottom = 25 ; // 底端盖高度
    CGFloat left = 10; // 左端盖宽度
    CGFloat right = 10; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
    // 伸缩后重新赋值
    return [image resizableImageWithCapInsets:insets];
}

+ (UIImage *)resizableWithImageName:(NSString *)imageName mode:(NSInteger)mode{
    
//    UIImageResizingModeStretch：拉伸模式，通过拉伸UIEdgeInsets指定的矩形区域来填充图片
//    UIImageResizingModeTile：平铺模式，通过重复显示UIEdgeInsets指定的矩形区域来填充图片
    
    
    UIImage *image = [UIImage imageNamed:imageName];
    
    CGFloat top = 25; // 顶端盖高度
    CGFloat bottom = 25 ; // 底端盖高度
    CGFloat left = 10; // 左端盖宽度
    CGFloat right = 10; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
    // 指定为拉伸模式，伸缩后重新赋值
    return [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
}

@end
