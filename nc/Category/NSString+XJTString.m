//
//  NSString+XJTString.m
//  nc
//
//  Created by docy admin on 15/8/26.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "NSString+XJTString.h"
#import "ToolOfClass.h"

@implementation NSString (XJTString)

+ (NSString *)stringThrowOffBlankWithString:(NSString *)string{
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@" "];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    return string;
}

+ (NSString *)stringThrowOffLinesWithString:(NSString *)string{
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@"\n"];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    return string;
}

+ (NSString *)stringThrowOffBlankAndLinesWithString:(NSString *)string{
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@" "];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    
    for (NSInteger i = 0; ; i++) {
        NSRange range = [string rangeOfString:@"\n"];
        if (range.location!=NSNotFound) {
            string = [string stringByReplacingCharactersInRange:range withString:@""];
        } else {
            break;
        }
    }
    return string;
}

+ (NSString *)stringGetCatoryWithNumber:(NSInteger)number{
    NSString *category;
    switch (number) {
        case 1:
            category = NSLocalized(@"company_category_1");
            break;
        case 2:
            category = NSLocalized(@"company_category_2");
            break;
        case 3:
            category = NSLocalized(@"company_category_3");
            break;
        case 4:
            category = NSLocalized(@"company_category_4");
            break;
        case 5:
            category = NSLocalized(@"company_category_5");
            break;
        case 6:
            category = NSLocalized(@"company_category_6");
            break;
        default:
            category = NSLocalized(@"company_category_7");
            break;
    }
    return category;
}

+ (void)stringGetErrorCodeWithCode:(NSNumber *)code{
    
    if ([code.stringValue isEqualToString:@"402001"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"postLogoutVC" object:nil];
    }
    
    AppDelegate *delegate = APP_DELEGATE;
//    [delegate.errorCodeDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//        NSLog(@"key = %@ and obj = %@", key, obj);
//        if ([key isEqualToString:code]) {
//            [ToolOfClass showMessage:@"hahh"];
//        }
//    }];

    for (NSString *key in [delegate.errorCodeDict allKeys]) {
        if ([key isEqualToString:code.stringValue]) {
            NSDictionary *dict = delegate.errorCodeDict[key];
            [ToolOfClass showMessage:isEnglish?dict[@"en"]:dict[@"zh"]];
            
            break;
        }
    }
}
@end
