//
//  UIViewController+HOMECategory.h
//  HomeToolsTest
//
//  Created by mahaomeng on 15/11/24.
//  Copyright © 2015年 Mahaomeng. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HOMECategoryHttpRequestSuccessBlock)(id obj);
typedef void(^HOMECategoryHttpRequestFailureBlock)(NSString *localizedDescription);
typedef void(^HOMECategoryReConnectNetWorkBlock)(void);

@interface UIViewController (HOMECategory)

@property (nonatomic, copy) HOMECategoryReConnectNetWorkBlock reRequestBlock;
@property (nonatomic, copy) NSString *connectFlag;

/**
 *  UIViewController+HOMECategory:发起GET网络请求
 *
 *  @param urlStr  请求地址
 *  @param para    参数
 *  @param success 请求成功时的block（obj-json已转化完毕）
 *  @param failure 请求失败时的block（返回失败的localizedDescription）
 */
-(void)GETHttpRequest:(NSString *)urlStr parameters:(id)para success:(HOMECategoryHttpRequestSuccessBlock)success failure:(HOMECategoryHttpRequestFailureBlock)failure;
/**
 *  UIViewController+HOMECategory:发起POST网络请求
 *
 *  @param urlStr  请求地址
 *  @param para    参数
 *  @param success 请求成功时的block（obj-json已转化完毕）
 *  @param failure 请求失败时的block（返回失败的localizedDescription）
 */
-(void)POSTHttpRequest:(NSString *)urlStr parameters:(id)para success:(HOMECategoryHttpRequestSuccessBlock)success failure:(HOMECategoryHttpRequestFailureBlock)failure;

@end
