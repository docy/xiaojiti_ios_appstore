//
//  UISegmentedControl+XJTSegmentedControl.h
//  nc
//
//  Created by docy admin on 15/9/23.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegmentedControl (XJTSegmentedControl)

+ (UISegmentedControl *)segmentedControlWithItems:(NSArray *)items target:(id)target action:(SEL)action;

@end
