//
//  UIButton+XJTButton.m
//  nc
//
//  Created by docy admin on 6/4/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "UIButton+XJTButton.h"

@implementation UIButton (XJTButton)

- (instancetype)setNormalImage:(NSString *)normal andSelectedImage:(NSString *)selected andFrame:(CGRect)rect addTarget:(id)target action:(SEL)action{
    [self setBackgroundImage:[UIImage imageNamed:normal] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:selected] forState:UIControlStateSelected];
    self.frame = rect;
    self.layer.cornerRadius = self.frame.size.width/2.0;
    self.layer.masksToBounds = YES;
    
    [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
//    self.selected = NO;

    return self;
}

- (instancetype)setNormalTitle:(NSString *)normalTitle andSelectesTitle:(NSString *)selectedTitle andFrame:(CGRect)rect addTarget:(id)target action:(SEL)action{
    [self setTitle:normalTitle forState:UIControlStateNormal];
    [self setTitle:selectedTitle forState:UIControlStateSelected];
    [self setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    self.selected = NO;
    self.frame = rect;
    [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return self;
}

+ (instancetype)buttonWithFrame:(CGRect)frame backgroundColor:(UIColor *)color cornerRadius:(CGFloat)cornerRadius{
    UIButton *button = [[UIButton alloc] init];
    button.frame = frame;
    button.backgroundColor = color;
    button.layer.cornerRadius = cornerRadius;
    button.clipsToBounds = YES;
    return button;
}

+ (instancetype)buttonWithFrame:(CGRect)frame backgroundImage:(UIImage *)image target:(id)target action:(SEL)action{
    UIButton *button = [[UIButton alloc] init];
    button.frame = frame;
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}

@end
