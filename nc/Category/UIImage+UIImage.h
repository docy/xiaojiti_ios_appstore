//
//  UIImage+UIImage.h
//  nc
//
//  Created by docy admin on 16/3/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage)

+ (UIImage *)stretchableWithImageName:(NSString *)imageName;

@end
