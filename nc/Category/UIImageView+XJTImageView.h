//
//  UIImageView+XJTImageView.h
//  nc
//
//  Created by docy admin on 15/11/3.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (XJTImageView)

+ (UIImageView *)imageViewWithImage:(NSString *)imageName;

+ (void)imageViewWithBgImage:(NSString *)imageName;

@end
