//
//  AudioSession.m
//  AVAudioRecorder
//
//  Created by guanxf on 15/12/25.
//  Copyright © 2015年 cmjstudio. All rights reserved.
//

#import "AudioSession.h"
#define kRecordAudioFile @"audioName.aac"//@"myRecord.caf"
#define kAudioFile @"audioName.m4a"//@"myRecord.caf"

static AudioSession * audioSession;

@interface AudioSession ()<AVAudioRecorderDelegate,AVAudioPlayerDelegate>{
    int RecDuration;
}

@end


@implementation AudioSession

+(id)ShareAAudioSession{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        audioSession=[[AudioSession alloc] init];
    });
    return audioSession;
}

/**
 *  设置音频会话
 */
-(void)setAudioSession{
    AVAudioSession *AudioSession=[AVAudioSession sharedInstance];
    //设置为播放和录音状态，以便可以在录制完之后播放录音
    [AudioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [AudioSession setActive:YES error:nil];
}

- (void)recordClick{
    if (![self.audioRecorder isRecording]) {
        [self.audioRecorder record];//首次使用应用时如果调用record方法会询问用户是否允许使用麦克风
        self.timer.fireDate=[NSDate distantPast];
    }
}

/**
 *  取得录音文件保存路径
 *
 *  @return 录音文件路径
 */
-(NSURL *)getSavePath{
    NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    urlStr=[urlStr stringByAppendingPathComponent:kRecordAudioFile];
//    NSLog(@"file path:%@",urlStr);
    NSURL *url=[NSURL fileURLWithPath:urlStr];
    return url;
}

-(NSURL *)getSaveAudioPath{
    NSString *urlStr=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    urlStr=[urlStr stringByAppendingPathComponent:kAudioFile];
//    NSLog(@"file path:%@",urlStr);
    NSURL *url=[NSURL fileURLWithPath:urlStr];
    return url;
}

/**
 *  取得录音文件设置
 *
 *  @return 录音设置
 */
-(NSDictionary *)getAudioSetting{
    NSMutableDictionary *dicM=[NSMutableDictionary dictionary];
    //设置录音格式
    [dicM setObject:@(kAudioFormatMPEG4AAC) forKey:AVFormatIDKey];
    //设置录音采样率(Hz) 如：AVSampleRateKey==8000/44100/96000（影响音频的质量）//设置录音采样率，8000是电话采样率，对于一般录音已经够了
    [dicM setObject:@(44100) forKey:AVSampleRateKey];
    //设置通道,这里采用单声道
    [dicM setObject:@(1) forKey:AVNumberOfChannelsKey];
    //每个采样点位数,分为8、16、24、32
    [dicM setObject:@(16) forKey:AVLinearPCMBitDepthKey];
    //是否使用浮点数采样
    [dicM setObject:@(YES) forKey:AVLinearPCMIsFloatKey];
    //录音的质量
    [dicM setValue:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
    
    //....其他设置等
    return dicM;
}

/**
 *  获得录音机对象
 *
 *  @return 录音机对象
 */
-(AVAudioRecorder *)audioRecorder{
//    _audioRecorder=nil;
    if (!_audioRecorder) {
        //创建录音文件保存路径
        NSURL *url=[self getSavePath];
        //创建录音格式设置
        NSDictionary *setting=[self getAudioSetting];
        //创建录音机
        NSError *error=nil;
        _audioRecorder=[[AVAudioRecorder alloc]initWithURL:url settings:setting error:&error];
        _audioRecorder.delegate=self;
        _audioRecorder.meteringEnabled=YES;//如果要监控声波则必须设置为YES
        RecDuration=0;
        [self setAudioSession];
        if (error) {
            NSLog(@"创建录音对象时发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioRecorder;
}

/**
 *  创建播放器
 *
 *  @return 播放器
 */
-(AVAudioPlayer *)audioPlayer{
//    _audioPlayer=nil;
    if (!_audioPlayer) {
        NSURL *url=[self getSaveAudioPath];
        NSError *error=nil;
        _audioPlayer=[[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
        _audioPlayer.numberOfLoops=0;
        _audioPlayer.delegate=self;
        [_audioPlayer prepareToPlay];
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        if (error) {
            NSLog(@"创建播放器过程中发生错误，错误信息：%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioPlayer;
}

/**
 *  录音声波监控定制器
 *
 *  @return 定时器
 */
-(NSTimer *)timer{
    if (!_timer) {
        _timer=[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(audioPowerChange) userInfo:nil repeats:YES];
    }
    return _timer;
}

-(NSTimer *)timer2{
    if (!_timer2) {
        _timer2=[NSTimer scheduledTimerWithTimeInterval:0.333 target:self selector:@selector(audioPowerChange2) userInfo:nil repeats:YES];
    }
    return _timer2;
}

/**
 *  录音声波状态设置
 */
static int count=0;
-(void)audioPowerChange{
    RecDuration++;
    [self.audioRecorder updateMeters];//更新测量值
    float power= [self.audioRecorder averagePowerForChannel:0];//取得第一个通道的音频，注意音频强度范围时-160到0
    CGFloat progress=(1.0/160.0)*(power+160.0);
    if ([self.delegate respondsToSelector:@selector(AudioRecorderProgress:andRecDuration:)]) {
        [self.delegate AudioRecorderProgress:progress andRecDuration:RecDuration];
    }
//    [self.audioPower setProgress:progress];
}

-(void)audioPowerChange2{
    //    [self.audioPower setProgress:progress];
//    _audioPlayerPro.progress = _audioPlayer.currentTime/_audioPlayer.duration;
//    NSLog(@"------:::::%f/%f",_audioPlayer.currentTime,_audioPlayer.duration);
    float power=[_audioPlayer averagePowerForChannel:0];
//    NSLog(@"------:::::%f",power);
    
    CGFloat progress=_audioPlayer.currentTime;///_audioPlayer.duration;//(1.0/160.0)*(power+160.0);
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"SSS"];
    NSString *date =  [formatter stringFromDate:[NSDate date]];
    NSString *timeLocal = [[NSString alloc] initWithFormat:@"%@", date];
    progress=[timeLocal intValue]/333;
    
//    NSLog(@"------:::::%f",progress);
    if ([self.delegate respondsToSelector:@selector(AudioPlayerProgress:andDuration:)]) {
        [self.delegate AudioPlayerProgress:progress andDuration:_audioPlayer.duration];
    }
//    
//    if (_audioPlayer.currentTime==0.0) {
//        if (_audioPlayer.duration>0.0) {
//            count++;
//            if (count>1) {
//                self.timer2.fireDate=[NSDate distantPast];
//                count=0;
//            }
//        }
//    }
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
    NSLog(@"录音完成!");
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    self.timer2.fireDate=[NSDate distantFuture];
    if ([self.delegate respondsToSelector:@selector(audioPlayerDidFinishPlaying)]) {
        [self.delegate audioPlayerDidFinishPlaying];
    }
    NSLog(@"播放完成!");
}

@end
