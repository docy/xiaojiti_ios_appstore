//
//  XJTSearchBar.m
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "XJTSearchBar.h"
@implementation XJTSearchBar

+ (instancetype)searchBar
{
    return [[self alloc] init];
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor]
        ;
        // 左侧图片
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"composedingwei"]];
        imageView.contentMode = UIViewContentModeCenter;
        self.leftView = imageView;
        self.leftViewMode = UITextFieldViewModeAlways;
        
        // 右侧按钮
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"取消" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.rightView = button;
        self.rightViewMode = UITextFieldViewModeWhileEditing;
        
        
        // 设置清除按钮
        self.clearButtonMode = UITextFieldViewModeAlways;
        
        // 设置字体大小
        self.font = [UIFont systemFontOfSize:15];
        // 设置键盘右下角按钮的样式
        self.returnKeyType = UIReturnKeyGo;
        self.enablesReturnKeyAutomatically = YES;

    }
    return self;
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds
{
    CGFloat leftViewX = 0;
    CGFloat leftViewY = 0;
    CGFloat leftViewW = bounds.size.height ;
    CGFloat leftViewH = bounds.size.height;
    return CGRectMake(leftViewX, leftViewY, leftViewW, leftViewH);
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds
{
    CGFloat rightViewW = bounds.size.height;
    CGFloat rightViewX = bounds.size.width-rightViewW;
    CGFloat rightViewY = 0;
    CGFloat rightViewH = bounds.size.height;
    return CGRectMake(rightViewX, rightViewY, rightViewW, rightViewH);
}

- (CGRect)clearButtonRectForBounds:(CGRect)bounds
{
    CGFloat clearButtonW = bounds.size.height;
    CGFloat clearButtonX = bounds.size.width-2*clearButtonW ;
    CGFloat clearButtonY = 0;
    CGFloat clearButtonH = bounds.size.height;
    return CGRectMake(clearButtonX, clearButtonY, clearButtonW, clearButtonH);
}

@end
