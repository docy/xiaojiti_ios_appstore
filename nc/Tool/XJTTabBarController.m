//
//  XJTTabBarController.m
//  nc
//
//  Created by docy admin on 15/10/8.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "XJTTabBarController.h"

#import "XJTNavigationController.h"
#import "LeftViewController.h"
#import "AddressBookTableViewController.h"
//#import "AddressBookViewController.h"
#import "PrivateGroupViewController.h"

#import "MessageNotifyViewController.h"
#import "PersonalProfileViewController.h"
#import "UserProfileViewController.h"

#import "CompanySetViewController.h"
@interface XJTTabBarController ()

@end

@implementation XJTTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SwitchCompanySetTabIndex) name:@"SwitchCurrentCompany" object:nil];
    
    
    LeftViewController *left = [LeftViewController new];
    XJTNavigationController *groupVC = [[XJTNavigationController alloc] initWithRootViewController:left];
    
    left.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalized(@"tabBar_0") image:[UIImage imageNamed:@"group_normal"] selectedImage:[UIImage imageNamed:@"selected_group"]];
    
    XJTNavigationController *addressBookVC = [[XJTNavigationController alloc] initWithRootViewController:[PrivateGroupViewController new]];
    addressBookVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalized(@"tabBar_1") image:[UIImage imageNamed:@"normal_private chat"] selectedImage:[UIImage imageNamed:@"selected_private chat"]];
    
    XJTNavigationController *notifyVC = [[XJTNavigationController alloc] initWithRootViewController:[MessageNotifyViewController new]];
    notifyVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalized(@"tabBar_2") image:[UIImage imageNamed:@"Focus_normal"] selectedImage:[UIImage imageNamed:@"Focus_selected"]];
    
    XJTNavigationController *setVC = [[XJTNavigationController alloc] initWithRootViewController:[CompanySetViewController new]];
    setVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalized(@"tabBar_3") image:[UIImage imageNamed:@"options"] selectedImage:[UIImage imageNamed:@"options_selected"]];
    
//    UITabBarController *tab = [[UITabBarController alloc] init];
    self.viewControllers = @[groupVC,addressBookVC,notifyVC,setVC];
    self.tabBar.tintColor = [UIColor colorWithRed:72/255.0 green:193/255.0 blue:168/255.0 alpha:1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.selectedIndex = 0;
    
}

- (void)SwitchCompanySetTabIndex{
    self.selectedIndex = 0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
