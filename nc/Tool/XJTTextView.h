//
//  XJTTextView.h
//  nc
//
//  Created by docy admin on 16/3/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XJTTextView : UITextView


@property (nonatomic, copy) NSString *placeholder;

@property (nonatomic, assign) BOOL hidePlaceholder;

@end
