//
//  AudioSession.h
//  AVAudioRecorder
//
//  Created by guanxf on 15/12/25.
//  Copyright © 2015年 cmjstudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol AudioSessionDelegate <NSObject>

-(void)AudioRecorderProgress:(float)progress andRecDuration:(int)duration;
-(void)AudioPlayerProgress:(float)progress andDuration:(float)duration;
-(void)audioPlayerDidFinishPlaying;

@end

@interface AudioSession : NSObject

@property (nonatomic,strong) AVAudioRecorder *audioRecorder;//音频录音机
@property (nonatomic,strong) AVAudioPlayer *audioPlayer;//音频播放器，用于播放录音文件
@property (nonatomic,strong) NSTimer *timer;//录音声波监控（注意这里暂时不对播放进行监控）
@property (nonatomic,strong) NSTimer *timer2;//录音声波监控（注意这里暂时不对播放进行监控）

+(id)ShareAAudioSession;

/**
 *  设置音频会话
 */
-(void)setAudioSession;

- (void)recordClick;

-(NSURL*)getSavePath;

@property (nonatomic,weak) id<AudioSessionDelegate> delegate;

@end
