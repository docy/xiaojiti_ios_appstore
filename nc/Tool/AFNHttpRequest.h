//
//  AFNHttpRequest.h
//  nc
//
//  Created by docy admin on 15/10/8.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFNHttpRequest : NSObject

+ (void)httpRequestResetUnreadCountWithGroupId:(NSNumber *)groupId;
+ (void)httpRequestSetDataWithUrl:(NSString *)url parameters:(NSMutableDictionary *)parameter;
+ (void)httpRequestGetVersionWith;
@end
