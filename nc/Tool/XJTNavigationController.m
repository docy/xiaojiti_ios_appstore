//
//  XJTNavigationController.m
//  nc
//
//  Created by docy admin on 6/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "XJTNavigationController.h"
#import "UIColor+XJTColor.h"
@interface XJTNavigationController ()

@end

@implementation XJTNavigationController

+ (void)initialize
{
    
    [self setUpNavigationBarTheme];
    [self setUpUIBarButtonItemTheme];
    
}

+ (void)setUpUIBarButtonItemTheme
{
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    
    // 设置标题属性
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSFontAttributeName] = [UIFont fontWithName:@"Heiti SC" size:17.0];
//    textAttrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
    if (item.enabled==NO) {
        textAttrs[NSForegroundColorAttributeName] = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];
    } else {
        textAttrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
    }
    [item setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    
}



+ (void)setUpNavigationBarTheme
{
    UINavigationBar *navBar = [UINavigationBar appearance];
    [navBar setBackgroundImage:[UIImage imageNamed:@"bg_navigation bar"] forBarMetrics:UIBarMetricsDefault];

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    // 设置标题属性
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSFontAttributeName] = [UIFont fontWithName:@"Heiti SC" size:17.0];
    textAttrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
    
    [navBar setTitleTextAttributes:textAttrs];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
