//
//  AFNHttpRequest.m
//  nc
//
//  Created by docy admin on 15/10/8.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "AFNHttpRequest.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "UIAlertView+AlertView.h"

@implementation AFNHttpRequest


+ (void)httpRequestResetUnreadCountWithGroupId:(NSNumber *)groupId{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager POST:[NSString stringWithFormat:ResetUnread,groupId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 发送失败
            
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

+ (void)httpRequestSetDataWithUrl:(NSString *)url parameters:(NSMutableDictionary *)parameter{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:url parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] intValue] == 200) {
            [ToolOfClass showMessage:NSLocalized(@"AFNHttpRequest_set_success")];
        } else {
            
//            [ToolOfClass showMessage:@"设置失败!"];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [ToolOfClass showMessage:NSLocalized(@"AFNHttpRequest_set_fail")];
    }];

}

+ (void)httpRequestGetVersionWith{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:Versions parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] intValue] == 200) {
            NSString *version = responseObject[@"data"][@"ios"];
            if (![version isEqualToString:CURRENT_VERSION]) {
               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"update_title") message:NSLocalized(@"update_message") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"update_update"), nil];
                [alert show];
            }
            
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}


@end
