//
//  ToolOfClass.h
//  nc
//
//  Created by docy admin on 7/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIView;
@class UIImage;
// 工具类，提供类方法（对象方法）
@interface ToolOfClass : NSObject
// 时间转换器
//+ (NSString *)toolGetLocalDateFormateUTCDate:(NSString *)utcDateString isGetUpAmCueerntDate:(BOOL)isAmDate isGroupListDate:(BOOL)isGroupListDate;
// 获取星期天时间显示格式
+ (NSString *)toolGetLocalWeekDateFormateWithUTCDate:(NSString *)utcDateString;
// 获取所有时间显示格式（上下午，昨天，年月日）
+ (NSString *)toolGetLocalAllDateFormateWithUTCDate:(NSString *)utcDateString;
// 获取所有时间显示格式（上下午，昨天，月日+时间）
+ (NSString *)toolGetLocalAllDetailDateFormateWithUTCDate:(NSString *)utcDateString;
// 获取上下午时间
+ (NSString *)toolGetLocalAmDateFormateWithUTCDate:(NSString *)utcDateString;

// 获取日期及上下午时间
+ (NSString *)toolGetLocalAllAmDateFormateWithUTCDate:(NSString *)utcDateString;

// 获取clientID（自定义）
+ (NSString *)toolSetUpClientIdWithGroupId:(NSNumber *)groupId;
// 获取一天中的某一个时间段
+ (NSDate *)toolGetUpCustomDateWithHour:(NSInteger)hour;
+ (BOOL)isBetweenFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate withCurrentDate:(NSDate *)currentDate withIsToday:(BOOL)isToday;

// 获取图片主题的title（时间戳）
+ (NSString *)toolGetTopic_title_Image;

// 获取上传文件的路径 ,参数一：请求网址 ，参数二：文件名 参数三：要上传的文件
+ (void)toolUploadFilePathWithHttpString:(NSString *)httpStr fileName:(NSString *)fileName file:(UIImage *)image setAvatarOrLogoPath:(NSString *)setAvatarOrLogoPath;

// 菊花转动页面
- (void)startAnimationInView:(UIView *)view;
- (void)stopAnimation;

+(void)showMessage:(NSString *)message;
// 获取authToken
+ (NSString *)authToken;

//+ (BOOL)isNetWorkReachable;
+ (AFNetWorkNotReachableView *)showAFNetWorkNotReachableView;

// 验证码时间刷新
+ (void)toolStartTimeSecondsCountDown:(UIButton *)sender;

+ (void)toolStartTimeSecondsCountDown_uilabel:(UILabel *)sender;

//文件大小转换 KB、MB、GB
+(NSString *) convertFileSize:(long long)size ;

//string 转码
+ (NSString *)replaceUnicode:(NSString *)unicodeStr ;

//手机正则判断
+ (BOOL)isMobileNumber:(NSString *)mobileNum;

//判断中英混合的的字符串长度
+ (int)convertToInt:(NSString*)strtemp;

@end
