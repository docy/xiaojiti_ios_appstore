//
//  XJTTextView.m
//  nc
//
//  Created by docy admin on 16/3/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "XJTTextView.h"


@interface XJTTextView ()

@property (nonatomic, weak) UILabel *placeholderLabel;

@end

@implementation XJTTextView



- (instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.font = [UIFont systemFontOfSize:17];
        self.textColor = CustomColor(51, 51, 51);
        self.alwaysBounceVertical = YES;
    }
    return self;
}

- (void)setFont:(UIFont *)font{
    [super setFont:font];
    self.placeholderLabel.font = font;
    [self.placeholderLabel sizeToFit];
}

- (UILabel *)placeholderLabel{
    if (_placeholderLabel==nil) {
        UILabel *label = [[UILabel alloc] init];
        label.textColor = [UIColor lightGrayColor];
        [self addSubview:label];
        _placeholderLabel = label;
    }
    return _placeholderLabel;
}

- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    self.placeholderLabel.text = placeholder;
    [self.placeholderLabel sizeToFit];
}

- (void)setHidePlaceholder:(BOOL)hidePlaceholder{
    _hidePlaceholder = hidePlaceholder;
    self.placeholderLabel.hidden = hidePlaceholder;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGRect point = self.placeholderLabel.frame;
    point.origin = CGPointMake(5, 8);
    self.placeholderLabel.frame = point;
}

@end
