//
//  XJTTitleButton.m
//  nc
//
//  Created by docy admin on 6/4/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "XJTTitleButton.h"
#define XJTTitleButtonImageW 20

@implementation XJTTitleButton

+ (instancetype)titleButton
{
    return [[self alloc] init];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.adjustsImageWhenHighlighted = NO;
        self.titleLabel.font = [UIFont fontWithName:@"Heiti SC" size:17.0];
        self.imageView.contentMode = UIViewContentModeCenter;
        [self setImage:[UIImage imageNamed:@"navigation_groupdata "] forState:UIControlStateNormal];
    }
    return self;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleX = 0;
    CGFloat titleY = 0;
    CGFloat titleW = contentRect.size.width - XJTTitleButtonImageW;
    CGFloat titleH = contentRect.size.height;
    
    return CGRectMake(titleX, titleY, titleW, titleH);
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = XJTTitleButtonImageW;
    CGFloat imageX = contentRect.size.width - imageW;
    CGFloat imageH = XJTTitleButtonImageW;
    CGFloat imageY = (contentRect.size.height-imageH)/2.0;
    return CGRectMake(imageX, imageY, imageW, imageH);
}


@end
