//
//  transferPowerViewController.h
//  
//
//  Created by guanxf on 15/10/24.
//
//

#import <UIKit/UIKit.h>

typedef void(^succeedBlock)(NSNumber *);

@interface transferPowerViewController : UIViewController

@property (nonatomic ,retain) NSNumber *groupId; //公司id

@property (nonatomic, strong) succeedBlock succeedBlock;


@end
