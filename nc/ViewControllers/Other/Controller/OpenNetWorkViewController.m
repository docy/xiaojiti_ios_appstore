//
//  OpenNetWorkViewController.m
//  nc
//
//  Created by docy admin on 15/9/15.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "OpenNetWorkViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

@interface OpenNetWorkViewController ()

@property (weak, nonatomic) IBOutlet UILabel *Localized_des0;
@property (weak, nonatomic) IBOutlet UILabel *Localized_des1;
@property (weak, nonatomic) IBOutlet UILabel *Localized_des2;


@end

@implementation OpenNetWorkViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        self.navigationItem.title = NSLocalized(@"netWork_unUse_title");
        
        

        
        
        
        
        self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onOpenNetWorkViewBackItemClick)];
    }
    return self;
}
- (void)onOpenNetWorkViewBackItemClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.Localized_des0.text = NSLocalized(@"netWork_unUse_des0");
    self.Localized_des1.attributedText = [[NSAttributedString alloc] initWithString:NSLocalized(@"netWork_unUse_des1")];
    self.Localized_des2.attributedText = [[NSAttributedString alloc] initWithString:NSLocalized(@"netWork_unUse_des2")];
//    self.Localized_des0.text = NSLocalized(@"netWork_unUse_des0");
//    self.Localized_des1.text = NSLocalized(@"netWork_unUse_des1");
//    self.Localized_des2.text = NSLocalized(@"netWork_unUse_des2");
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
