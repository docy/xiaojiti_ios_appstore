//
//  CompanyListViewController.m
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import "CompanyListViewController.h"
#import "CompanyNameViewController.h"
#import "CompanyModel.h"
#import "CompanyModel.h"
#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "CompanyTableViewCell.h"
#import "CompanyListTVCell.h"
#import "ToolOfClass.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "CreatGroupViewController.h"
#import "RegisterCreatCompanyViewController.h"
#import "CompanySetViewController.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "PersonalProfileViewController.h"
#import "UISegmentedControl+XJTSegmentedControl.h"
#import "OpenNetWorkViewController.h"
#import "companyInfoViewController.h"
#import "KxMenu.h"
#import "MJRefresh.h"
#import "UIColor+Hex.h"

@interface CompanyListViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>{
    NSNumber *_companyId; // 公司ID，设置当前公司用
    NetWorkBadView *badView;
}

@property (nonatomic, retain) NSMutableArray *companyList;      // 已加入公司列表
//@property (nonatomic, retain) NSMutableArray *companyCanJoinList;      // 未加入且能公司列表
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@property (nonatomic, assign) BOOL isRankList; // 是否是我的排行榜列表，默认NO
@property (nonatomic, retain) NSMutableArray *rankList;      // 排行榜列表

@property (nonatomic, assign) BOOL KxMenu_isShow;   //是否显示菜单view

@end

@implementation CompanyListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if ([DEFAULTS boolForKey:@"firstLaunch"]) {
        [UIImageView imageViewWithBgImage:isEnglish?@"Newbie guide_1 English version":@"Newbie guide_1"];
    }
    _companyList=[[NSMutableArray alloc] init];
    // 设置基本属性
    [self setUpCompanyNameViewData];
    
    //添加下拉刷新
    [self setUpTableViewMJRefresh];
    
    //断网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    //联网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
    badView.contentLabel.text = NSLocalized(@"NetWorkBadView_des");
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    badView.frame=CGRectMake(0, 0, ScreenWidth, 44);
    badView.hidden=YES;
    [self.view addSubview:badView];
}

- (void)setUpTableViewMJRefresh{
//    __block CompanyListViewController *weakSelf = self;
//    [self.tableView addFooterWithCallback:^{
////        [weakSelf setUsersCompanyList];
//    }];
    [self.tableView addHeaderWithCallback:^{
        if (_isRankList) {
            [self setRankingList];
        }else{
            [self setUsersCompanyList];
        }
    }];
}

- (void)ViewRefreshAgainInBadNetWork{
    //    NSLog(@"%s",object_getClassName(self));
    badView.hidden=NO;
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)ViewInGoodNetWork{
    badView.hidden=YES;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self setUsersCompanyList];
    [self setRankingList];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

// 设置基本属性
- (void)setUpCompanyNameViewData
{
//    self.navigationItem.title = @"集体列表";
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithIcon:@"navigation_search" target:self action:@selector(searchCompanyList)];
    UIButton *rightItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightItem setImage:[UIImage imageNamed:@"navigation__more"] forState:UIControlStateNormal];
    CGSize size = [[rightItem currentImage] size];
    rightItem.frame = CGRectMake(0, 0, size.width, size.height);
    [rightItem addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightItem];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.navigationItem.titleView = [UISegmentedControl segmentedControlWithItems:@[NSLocalized(@"company_list_title_my"),NSLocalized(@"company_list_title_rink")] target:self action:@selector(onSegmentControClick:)];
}

- (void)onSegmentControClick:(UISegmentedControl *)seg{
    if (_KxMenu_isShow) {
        _KxMenu_isShow=NO;
        [KxMenu dismissMenu];
    }
    switch (seg.selectedSegmentIndex) {
        case 0:
            self.isRankList = NO;
            [self.tableView reloadData];
            break;
        case 1:
            
            self.isRankList = YES;
            if (_rankList==nil) {
                _rankList = [NSMutableArray array];
                
                [self setRankingList];
            } else {
                [self.tableView reloadData];
            }
            break;
        default:
            break;
    }
}

- (void)setRankingList{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSString *path = [canJoinCompanyList stringByAppendingString:@"?limit=10&order[userCount]=desc"];
    
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {

            [_rankList removeAllObjects];
            for (NSDictionary *dict in responseObject[@"data"]) {
                CompanyModel *model = [[CompanyModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [_rankList addObject:model];
            }
        } else{
            
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
        if (self.isRankList) {
            [self.tableView reloadData];
            [self.tableView headerEndRefreshing];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

// 获取用户的公司列表
- (void)setUsersCompanyList
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    [manager GET:userCompanyListPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [self.companyList removeAllObjects];
            for (NSDictionary *dict in responseObject[@"data"]) {
                CompanyModel *model = [[CompanyModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
               
                if ([self.companyName isEqualToString:model.name]) {
                    model.isCurrentCpy = YES;
                } else {
                    model.isCurrentCpy = NO;
                }
                [self.companyList addObject:model];
            }
            
            if (!self.isRankList) {
                [self.tableView reloadData];
                [self.tableView headerEndRefreshing];
            }
        }
//        else if ([responseObject[@"code"] isEqualToNumber:@3]){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//            
//            [self.tableView headerEndRefreshing];
//        }
        else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            [self.tableView headerEndRefreshing];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
        [self.tableView headerEndRefreshing];
    }];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (iPhone6Plus) {
        return  450;
    }
    if (iPhone6) {
        return  390;
    }
//    return 568.0/2;
    return 330;//105;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.isRankList?self.rankList.count:self.companyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellId";
    CompanyListTVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CompanyListTVCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    CompanyModel *model = self.isRankList?self.rankList[indexPath.row]:self.companyList[indexPath.row];
    
    if (model.name==nil) {  //如果数据为空显示空cell   若没有公司 显示两个空cell
        return cell;
    }
    cell.companyName.text = model.name;
    if (self.isRankList) {
        cell.NumberL.hidden=NO;
        cell.compListImage.hidden=NO;
        cell.NumberL.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        cell.joinBtn.hidden = NO;
        NSString * strr=[NSString stringWithFormat:@"%@",model.topicCount==nil?@"0":model.topicCount]; //防止前期创建的集体没有topicCount参数
        cell.creatorLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",strr] rangeString:strr rangColor:CustomColor(255, 17, 46)];//NSLocalized(@"company_list_subTopicCount"),
        cell.userCountLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.userCount] rangeString:[NSString stringWithFormat:@"%@",model.userCount] rangColor:[UIColor colorWithHex:0x4A4A4A]];//CustomColor(76, 182, 72)];//NSLocalized(@"company_list_userCount"),
        [cell.MinImage setImage:[UIImage imageNamed:@"topic_icon"]];
        cell.addressLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.city==NULL?NSLocalized(@"company_list_city_default"):model.city] rangeString:[NSString stringWithFormat:@"%@",model.city] rangColor:CustomColor(74, 144, 226)];//,NSLocalized(@"company_list_city")
        //        cell.userCountLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"地点  %@",model.city] rangeString:[NSString stringWithFormat:@"%@",model.city] rangColor:CustomColor(74, 144, 226)];
    } else {
        cell.joinBtn.hidden = YES;
        cell.NumberL.hidden=YES;
        cell.compListImage.hidden=YES;
        [cell.MinImage setImage:[UIImage imageNamed:@"news"]];
        if ([model.unread isEqualToNumber:@0]) {
            cell.creatorLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.unread] rangeString:[NSString stringWithFormat:@"%@",model.unread] rangColor:[UIColor colorWithHex:0x4A4A4A]];//CustomColor(153, 153, 153)];//NSLocalized(@"company_list_newMessage")
        } else {
            cell.creatorLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.unread] rangeString:[NSString stringWithFormat:@"%@",model.unread] rangColor:CustomColor(255, 17, 46)];//,NSLocalized(@"company_list_newMessage")
        }
        cell.userCountLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.userCount] rangeString:[NSString stringWithFormat:@"%@",model.userCount] rangColor:[UIColor colorWithHex:0x4A4A4A]];//CustomColor(76, 182, 72)];//NSLocalized(@"company_list_userCount"),
        if (!model.city) {
            cell.addressLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",NSLocalized(@"company_list_city_default")] rangeString:NSLocalized(@"company_list_city_default") rangColor:[UIColor colorWithHex:0x4A4A4A]];//CustomColor(74, 144, 226)];//NSLocalized(@"company_list_city"),
        } else {
            cell.addressLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.city] rangeString:[NSString stringWithFormat:@"%@",model.city] rangColor:[UIColor colorWithHex:0x4A4A4A]];//CustomColor(74, 144, 226)];//,NSLocalized(@"company_list_city")
        }
    }
    [cell.joinBtn setTitle:NSLocalized(@"company_list_joinBtn") forState:UIControlStateNormal];
    cell.joinBtn.tag=1200+indexPath.row;
    [cell.joinBtn addTarget:self action:@selector(rankJoinCompanyBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    if (model.desc==nil) {
        cell.descriptionLabel.text=NSLocalized(@"company_list_des_default");
    } else {
        cell.descriptionLabel.text=model.desc;
    }
    if (model.logoUrlOrigin!=nil) {
        [cell.companyImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logoUrlOrigin]] placeholderImage:nil];
    }
    
    
//    cell.settinButClick.tag=1200+indexPath.row;
//    [cell.settinButClick addTarget:self action:@selector(pustCompanySetViewController:) forControlEvents:UIControlEventTouchUpInside];
//    normal_set
//    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
//    if ([num isEqualToNumber:model.creator]) {  //若是公司创建者 则隐藏退出button 显示转让button
//        [cell.settinButClick setImage:[UIImage imageNamed:@"set"] forState:UIControlStateNormal];
//    }else{
//        [cell.settinButClick setImage:[UIImage imageNamed:@"normal_set"] forState:UIControlStateNormal];
//    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CompanyModel *model = self.isRankList?self.rankList[indexPath.row]:self.companyList[indexPath.row];
    
    if (self.isRankList) {
        companyInfoViewController * comvc=[[companyInfoViewController alloc] initWithNibName:@"companyInfoViewController" bundle:nil];
        comvc.model = model;
        [self.navigationController pushViewController:comvc animated:YES];
        return;
    }
    
    _companyId = model.id;
    AppDelegate *deleagte = APP_DELEGATE;
    
//    deleagte.tempCopId = model.id;
    deleagte.tempCopModel = model;
    [self setUserCurrentCompany];
//    [deleagte socketStatusIsConnect];
    //若切换公司 应断开socket 重新链接下
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
}

- (void)rankJoinCompanyBtn:(UIButton *)sender{
    CompanyModel *model = self.rankList[sender.tag-1200];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSString *path = [addCompany stringByAppendingFormat:@"%@",model.id];
    
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 加入公司成功
            _companyId = model.id;
            AppDelegate *deleagte = APP_DELEGATE;
            deleagte.tempCopModel = model;
            
            [self.rankList removeObject:model];
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:sender.tag-1200 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [self setUserCurrentCompany];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

// cell的选择按钮点击事件
//- (void)onSetCurrentCompanyBtnClick:(UIButton *)sender
//{
//    if (sender.selected) {
//        return;
//    }
//    
//    sender.selected = !sender.selected;
//    for (NSInteger i = 0; i < self.companyList.count; i++) {
//        CompanyModel *model = self.companyList[i];
//        if (sender.tag-800==i) {
//            model.isCurrentCpy = YES;
//        } else {
//            model.isCurrentCpy = NO;
//        }
//        
//    }
//    [self.tableView reloadData];
//    
//    CompanyModel *model = self.companyList[sender.tag-800];
//    _companyId = model.id;
//    //  发送设置为当前公司
//    [self setUserCurrentCompany];
//}
// 设置当前公司
- (void)setUserCurrentCompany
{
//    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
//    [SVProgressHUD showWithStatus:@"正在载入集体..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info_group = [[NSUserDefaults alloc] initWithSuiteName:@"group.docy.co"];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];

    NSString *str = [NSString stringWithFormat:setCurrentCompany,_companyId];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            [info setObject:_companyId forKey:@"currentCompany"];
            [info_group setObject:_companyId forKey:@"currentCompany"];
            [info_group synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
            double delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [SVProgressHUD dismiss];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
            });
        }else{
//            [SVProgressHUD dismiss];
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"company_list_joinFailure") message:nil delegate:nil cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
        [alert show];
    }];
}

//创建新公司
- (IBAction)addButtonClick:(id)sender {
//    [self onLeftViewAddGroupButtonClick];
}

//搜索公司
- (void)searchCompanyList{
    if (_KxMenu_isShow) {
        _KxMenu_isShow=NO;
        [KxMenu dismissMenu];
    }
    CompanyNameViewController * compNameListVC=[[CompanyNameViewController alloc] initWithNibName:@"CompanyNameViewController" bundle:nil];
    [self.navigationController pushViewController:compNameListVC animated:YES];
}

- (void)showMenu:(UIButton *)sender{
    CGRect rect = sender.frame;
    rect.origin.y = 0;

    if (_KxMenu_isShow) {
        _KxMenu_isShow=NO;
        [KxMenu dismissMenu];
    }else{
        NSArray *menuItems =
        @[
          [KxMenuItem menuItem:NSLocalized(@"company_list_title_right_creat")
                         image:[UIImage imageNamed:@"add collective_List"]
                        target:self
                        action:@selector(pushMenuItemCreatGroup:)],
          [KxMenuItem menuItem:NSLocalized(@"company_list_title_right_codeJoin")
                         image:[UIImage imageNamed:@"Invite_List"]
                        target:self
                        action:@selector(pushMenuItemCodeJoin:)],
          [KxMenuItem menuItem:NSLocalized(@"company_list_title_right_set")
                         image:[UIImage imageNamed:@"set_List"]
                        target:self
                        action:@selector(pushMenuItemSet:)],
          
          [KxMenuItem menuItem:NSLocalized(@"company_list_title_right_logout")
                         image:[UIImage imageNamed:@"logout_List"]
                        target:self
                        action:@selector(pushMenuItemLogOut:)],
          ];
        
        [KxMenu setTitleFont:[UIFont fontWithName:@"Heiti SC" size:15.0]];
        [KxMenu showMenuInView:self.view
                      fromRect:rect
                     menuItems:menuItems];
        _KxMenu_isShow=YES;
    }

    
}

- (void)pushMenuItemCreatGroup:(id)sender{
    RegisterCreatCompanyViewController *creatCompanyVC = [[RegisterCreatCompanyViewController alloc] init];
    creatCompanyVC.hidesBottomBarWhenPushed = YES;
    _KxMenu_isShow=NO;
    [self.navigationController pushViewController:creatCompanyVC animated:YES];
}

- (void)pushMenuItemCodeJoin:(id)sender{
    RegisterInviteToJoinCompanyViewController *joinCompanyVC = [[RegisterInviteToJoinCompanyViewController alloc] init];
    _KxMenu_isShow=NO;
    [self.navigationController pushViewController:joinCompanyVC animated:YES];
    
}

- (void)pushMenuItemSet:(id)sender{
    PersonalProfileViewController *userView = [[PersonalProfileViewController alloc] init];
    _KxMenu_isShow=NO;
    [self.navigationController pushViewController:userView animated:YES];
}

- (void)pushMenuItemLogOut:(id)sender
{
    UIAlertView * aview=[[UIAlertView alloc] initWithTitle:NSLocalized(@"company_list_logout_title") message:NSLocalized(@"company_list_logout_mess") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"HOME_alert_sure"), nil];
    [aview show];
}

//退出
- (IBAction)logoutButton:(id)sender {
//    UIAlertView * aview=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"您将要退出登录状态，是否确认退出？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
//    [aview show];
}

//进入公司详情页面
-(void)pustCompanySetViewController:(UIButton*)button{

//    CompanyModel *model = self.companyList[button.tag-1200];
//    CompanySetViewController* compSetListVC=[[CompanySetViewController alloc] initWithNibName:@"CompanySetViewController" bundle:nil];
//    compSetListVC.companyId=model.id;
//    compSetListVC.model=model;
//    [self.navigationController pushViewController:compSetListVC animated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        return;
    }else{
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        [manager POST:logoutPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"message==%@",responseObject[@"message"]);
            if ([responseObject[@"code"] isEqualToNumber:@200]) { // 退出程序
                [info setObject:@"YES" forKey:@"isLogout"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postLoginVC" object:nil];
            } else {
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
    }
}

@end
