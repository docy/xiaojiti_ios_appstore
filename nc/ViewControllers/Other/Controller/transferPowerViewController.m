//
//  transferPowerViewController.m
//  
//
//  Created by guanxf on 15/10/24.
//
//

#import "transferPowerViewController.h"
#import "ListDataTableViewCell.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import <AFNetworking/AFNetworking.h>
#import "UserModel.h"
#import "ToolOfClass.h"
#import "UIImageView+WebCache.h"

@interface transferPowerViewController () <UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *groupUsers;   //公司成员列表
@property (nonatomic, retain) NSNumber *userId;
@property (nonatomic ,retain) UIAlertView *alert;

@end

@implementation transferPowerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpGroupConveyGroupViewData];
}

- (void)setUpGroupConveyGroupViewData{
    self.navigationItem.title = NSLocalized(@"company_convey_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpGroupConveyGroupViewBack)];
    self.groupUsers = [NSMutableArray array];
    [self getUpGroupConveyGroupViewUserListData];
}

- (void)setUpGroupConveyGroupViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpGroupConveyGroupViewUserListData
{
    [self.groupUsers removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:GetUserList,self.groupId];
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] intValue] == 200) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                if (![model.id isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
                    [self.groupUsers addObject:model];
                }
            }
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"CellId";
    ListDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell==nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ListDataTableViewCell" owner:self options:nil] lastObject];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserModel *model = self.groupUsers[indexPath.row];
    cell.nameLabel.text = model.nickName;
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.location==NSNotFound) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [cell.iconImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
    [cell.iconImageView setTranslatesAutoresizingMaskIntoConstraints:YES];
//    cell.iconImageView.frame=CGRectMake(10, 5, 30, 30);
    cell.countbtn.tag = indexPath.row;
    cell.countbtn.hidden=YES;
    [cell.countbtn addTarget:self action:@selector(conveyToUser:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return NSLocalized(@"company_convey_hTitle");
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView reloadData];
    ListDataTableViewCell *cell = (ListDataTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.countbtn.selected = YES;
    UserModel *model = self.groupUsers[indexPath.row];
    self.userId = model.id;
    
    if (self.alert==nil) {
        self.alert = [UIAlertView alertViewWithTitle:NSLocalized(@"company_convey_title") subTitle:[NSString stringWithFormat:@"%@%@",[cell.nameLabel text],NSLocalized(@"company_convey_alertMess")] target:self];
    }
    [self.alert setMessage:[NSString stringWithFormat:@"%@%@",[cell.nameLabel text],NSLocalized(@"company_convey_alertMess")]];
    [self.alert show];
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ListDataTableViewCell *cell = (ListDataTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.countbtn.selected = NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==1) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        parameter[@"newAdminId"]  = self.userId;
        NSString *path = [NSString stringWithFormat:companyOwnerTransfer,self.groupId];
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] intValue] == 200) {
                [ToolOfClass showMessage:NSLocalized(@"company_convey_success")];
                if (self.succeedBlock) {
                    self.succeedBlock(self.userId);
                }
                [self.navigationController popViewControllerAnimated:YES];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:error.userInfo[@"message"]];
        }];
    }
}



- (void)conveyToUser:(UIButton *)sender{
    
    [self.tableView reloadData];
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    
}


@end

