//
//  CompanySetViewController.h
//  
//
//  Created by guanxf on 15/10/23.
//
//

#import <UIKit/UIKit.h>
#import "CompanyModel.h"

@interface CompanySetViewController : UIViewController

@property(nonatomic, retain) NSNumber * companyId;
@property(nonatomic, retain) CompanyModel * model;


@end
