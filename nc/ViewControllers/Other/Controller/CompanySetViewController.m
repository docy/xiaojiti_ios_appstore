//
//  CompanySetViewController.m
//  
//
//  Created by guanxf on 15/10/23.
//
//

#import "CompanySetViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "CompanyModel.h"
#import "AddAddressBookViewController.h"
#import "AddBookViewController.h"
#import "ToolOfClass.h"
//#import "ExitCompanyViewController.h"
#import "AllGroupViewController.h"
#import "UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "CompanyInviteCodeViewController.h"
#import "UIAlertView+AlertView.h"
#import "transferPowerViewController.h"
#import "OpenNetWorkViewController.h"
#import "NSString+XJTString.h"

@interface CompanySetViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NetWorkBadView *badView;
}
@property (nonatomic, assign) BOOL cannotDisband;

@property (nonatomic, copy) NSString *logoName; // 公司logo名

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *companyNameLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *descTV;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;

@property (weak, nonatomic) IBOutlet UILabel *userCount;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;

@property (weak, nonatomic) IBOutlet UIButton *exitAndTransf;
@property (weak, nonatomic) IBOutlet UIButton *dismissCompanyBut;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@property (weak, nonatomic) IBOutlet UISwitch *simiJItiSW;


@property (weak, nonatomic) IBOutlet UILabel *Localized_name;
@property (weak, nonatomic) IBOutlet UILabel *Localized_des;
@property (weak, nonatomic) IBOutlet UILabel *Localized_member;
@property (weak, nonatomic) IBOutlet UILabel *Localized_master;
@property (weak, nonatomic) IBOutlet UILabel *Localized_code;
@property (weak, nonatomic) IBOutlet UILabel *Localized_city;
@property (weak, nonatomic) IBOutlet UILabel *Localized_category;
@property (weak, nonatomic) IBOutlet UILabel *Localized_allgroup;
@property (weak, nonatomic) IBOutlet UILabel *Localized_idPricate;


@end

@implementation CompanySetViewController

-(void)viewWillAppear:(BOOL)animated{
    [self.descriptionLabel resignFirstResponder];
    [self.companyNameLabel resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.Localized_name.text = NSLocalized(@"company_set_name");
    self.Localized_des.text = NSLocalized(@"company_set_des");
    self.Localized_member.text = NSLocalized(@"company_set_member");
    self.Localized_master.text = NSLocalized(@"company_set_master");
    self.Localized_code.text = NSLocalized(@"company_set_code");
    self.Localized_city.text = NSLocalized(@"company_set_city");
    self.Localized_category.text = NSLocalized(@"company_set_category");
    self.Localized_allgroup.text = NSLocalized(@"company_set_allGroup");
    self.Localized_idPricate.text = NSLocalized(@"company_set_isPrivate");
    [self.exitButton setTitle:NSLocalized(@"company_set_logout") forState:UIControlStateNormal];
    [self.exitAndTransf setTitle:NSLocalized(@"company_set_convey") forState:UIControlStateNormal];
    [self.dismissCompanyBut setTitle:NSLocalized(@"company_set_disband") forState:UIControlStateNormal];
    
    AppDelegate *deleagte = APP_DELEGATE;
    //    deleagte.tempCopId = model.id;
    _model = deleagte.tempCopModel;
    _companyId = deleagte.tempCopModel.id;
    
    [self RegisterCreatCompanyViewSetUpData];
//    [self setUsersCompanyList];
    [self updataViewData];
    
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
    
    //断网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    //联网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
    badView.contentLabel.text = NSLocalized(@"NetWorkBadView_des");
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    badView.frame=CGRectMake(0, 0, ScreenWidth, 44);
    badView.hidden=YES;
    [self.view addSubview:badView];
}

- (void)LeftViewRefreshAgainInBadNetWork{
    //    NSLog(@"%s",object_getClassName(self));
    badView.hidden=NO;
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)LeftViewInGoodNetWork{
    badView.hidden=YES;
}

-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [self.descriptionLabel resignFirstResponder];
    [self.companyNameLabel resignFirstResponder];
}

// 设置基本属性
- (void)RegisterCreatCompanyViewSetUpData
{
//    切换公司通知
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCompanyModel) name:@"SwitchCurrentCompany" object:nil];
    self.navigationItem.title = NSLocalized(@"company_set_title");
    float hight=172;
    if (iPhone6) {
        hight=190;
    }else if (iPhone6Plus){
        hight=200;
    }
    
//    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(RegisterCreatCompanyViewBackBtnClick)];
//    UIBarButtonItem * butitem = [UIBarButtonItem itemWithTitle:@"完成" target:self action:@selector(onUpdataButtonClick)]; //alloc] initWithTitle:@"修改" style:UIBarButtonItemStyleDone target:self action:@selector(onUpdataButtonClick)];
    //    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:getImageBeforeRegisterPath,@"company"]] placeholderImage:nil];
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
//    self.scrollView.contentSize=CGSizeMake(ScreenWidth, ScreenHeight+200);
    self.scrollView.contentSize=CGSizeMake(ScreenWidth, ScreenHeight+100);
    
}

- (void)getCompanyModel{
    // 解决集体头像问题
    self.logoImageView.image = nil;
    AppDelegate *deleagte = APP_DELEGATE;
    _model = deleagte.tempCopModel;
    _companyId = deleagte.tempCopModel.id;
    [self updataViewData];
}

-(void)setButtonShowAndHidden{
    UIBarButtonItem * butitem = [UIBarButtonItem itemWithTitle:NSLocalized(@"company_set_title_rightTitle") target:self action:@selector(onUpdataButtonClick)];
    
    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    if ([num isEqualToNumber:_model.creator]) {  //若是公司创建者 则隐藏退出button 显示转让button
        _exitButton.hidden=YES;
        _dismissCompanyBut.hidden= NO;
        
        self.companyNameLabel.enabled=YES;
        self.descriptionLabel.editable=YES;
        self.simiJItiSW.enabled = YES;
        self.cameraButton.hidden=NO;
        self.navigationItem.rightBarButtonItem=butitem;
    }else{
        _exitButton.hidden=NO;
        _dismissCompanyBut.hidden=YES;
        
        self.simiJItiSW.enabled = NO;
        self.companyNameLabel.enabled=NO;
        self.descriptionLabel.editable=NO;
        self.cameraButton.hidden=YES;
        self.navigationItem.rightBarButtonItem =nil;
    }
}

// 返回按钮
- (void)RegisterCreatCompanyViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 获取公司info
- (void)setUsersCompanyList
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    NSString * urlstr=[NSString stringWithFormat:GetCompanyInfo,[_companyId intValue]];
    [manager GET:urlstr parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [_model setValuesForKeysWithDictionary:responseObject[@"data"]];
            [self updataViewData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

-(void)updataViewData{
    [self.bgImage sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:_model.logoUrlOrigin]] placeholderImage:nil];
    self.companyNameLabel.text=_model.name;
    self.descriptionLabel.text=_model.desc;
    _userCount.text=[NSString stringWithFormat:@"%@%@",_model.userCount,NSLocalized(@"company_set_member_Count")];
    _creatorLabel.text=_model.creatorNick;
    _cityLabel.text = _model.city;
    _simiJItiSW.on = _model.isPrivate.intValue;
//    NSString *category;
//    switch (_model.category.intValue) {
//        case 1:
//            category = @"会议";
//            break;
//        case 2:
//            category = @"股票";
//            break;
//        case 3:
//            category = @"旅游";
//            break;
//        case 4:
//            category = @"教育";
//            break;
//        case 5:
//            category = @"户外";
//            break;
//        case 6:
//            category = @"其他";
//            break;
//        default:
//            category = @"通用";
//            break;
//    }
    _categoryLabel.text = [NSString stringGetCatoryWithNumber:_model.category.integerValue];
    if (_model.desc!=nil) {
        _descTV.hidden=YES;
    }
    
    [self setButtonShowAndHidden]; //设置button显示
}

- (IBAction)chanshengyaoqingmaClick:(id)sender {
    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    if ([num isEqualToNumber:_model.creator]) {  //若是公司创建者 则隐藏退出button 显示转让button
        [self pushAddFriend];
    }else{
        [ToolOfClass showMessage:NSLocalized(@"company_set_code_failure")];
    }
}

#pragma mark textFieldDidBeginEditing

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSInteger size=0;
    int height=173+150;
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)sender {
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}


- (IBAction)touchView:(id)sender {
    [self.view endEditing:YES];
    [_descriptionLabel resignFirstResponder];
    [_companyNameLabel resignFirstResponder];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _descTV.hidden=YES;
    NSInteger size=0;
    int height=173+150+70;
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length==0) {
        _descTV.hidden=NO;
    }
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)cameraButClick:(id)sender {
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
    
}

#pragma mark 修改公司信息
- (void)onUpdataButtonClick{

    //1.信息是否变更
//    if (self.companyNameTextField.text.length==0 || self.phoneTextField.text.length==0) {
//        [ToolOfClass showMessage:@"输入集体名称或集体电话"];
//        return;
//    }
    
    NSData *imagedata=nil;
    NSInteger changeNum=0;  //公司图片、名字、描述 改变个数  其中至少有一个改变才能提交修改
    
    NSData * Iimagedata = UIImageJPEGRepresentation(self.logoImageView.image, ImageYaSuo);
    if (Iimagedata.length>0) {
        changeNum++;
        imagedata=Iimagedata;
        if (self.logoName==nil) { // 上传默认图片
            self.logoName = @"companyAvatar.png";
        }
    }
    
    if (![_model.name isEqualToString:self.companyNameLabel.text]) {
        if (_companyNameLabel.text.length>20) {
            UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"company_set_name_tooLong") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_know") otherButtonTitles:nil, nil];
            [uiav show];
        }
        changeNum++;
    }
    
    if (![_model.desc isEqualToString:self.descriptionLabel.text]) {
        if (_descriptionLabel.text.length>60) {
            UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"company_set_name_tooLong") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_know") otherButtonTitles:nil, nil];
            [uiav show];
        }
        changeNum++;
    }
    if (!(_model.isPrivate.intValue==_simiJItiSW.on)) {
        changeNum++;
    }
    if (changeNum==0) {   //没有修改数据
        UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"company_set_remoke_not") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_know") otherButtonTitles:nil, nil];
        [uiav show];
        return;
    }
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:NSLocalized(@"HOME_show_sendMess")];
    
    // 修改公司
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"name"] = self.companyNameLabel.text;
    parameter[@"desc"] = self.descriptionLabel.text;
    parameter[@"isPrivate"] = @(self.simiJItiSW.on);
//    if (Iimagedata.length>0) {
//        parameter[@"logo"] =Iimagedata;
//    }
    NSString* url=[NSString stringWithFormat:UpdataCompanyInfo,_companyId];
    [manager POST:url parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (self.logoName!=nil) { // 上传默认图片
            if (imagedata.length>0) {
              [formData appendPartWithFileData:imagedata name:@"logo" fileName:self.logoName mimeType:@"image/png"];
            }
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue]==200) {
            [ToolOfClass showMessage:NSLocalized(@"HOME_show_remoke_success")];
            _model.isPrivate = @(self.simiJItiSW.on);
            [SVProgressHUD dismiss];
            
            if (Iimagedata.length>0) {
                self.bgImage.image = self.logoImageView.image;
                self.logoImageView.image = nil;
            }
            if (![_model.name isEqualToString:self.companyNameLabel.text]) {
                _model.name = self.companyNameLabel.text;
            }
            
            if (![_model.desc isEqualToString:self.descriptionLabel.text]) {
                _model.desc = self.descriptionLabel.text;
            }
            if (!(_model.isPrivate.intValue==_simiJItiSW.on)) {
                _model.isPrivate = @(_simiJItiSW.on);
            }

            [self.navigationController popViewControllerAnimated:YES];
            //修改成功
        } else {
            [SVProgressHUD dismiss];
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            NSLog(@"responseObject:::%@",responseObject[@"message"]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark --UIImagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.logoImageView.image = info[UIImagePickerControllerEditedImage];
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    self.logoName = [fileName copy];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//邀请好友点击事件
-(void)pushAddFriend{
    [self getyaoqingma];
}

-(void)getyaoqingma{
    __block NSString * inviteCode;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSLog(@"xuanZhongNum -------is :%d",[[info objectForKey:@"currentCompany"] intValue]);
    parameter[@"target"]=@"13000000000";//xuanZhongNum;//
    NSString * URLstr=[NSString stringWithFormat:CompanyInviteCode,[_model.id intValue]];
    
    [manager POST:URLstr parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Dic is %@",responseObject);
//        if ([responseObject[@"code"] isEqualToNumber:@1]) {
//            //            NSLog(@"error is %@",[self replaceUnicode:responseObject[@"error"]]);
//            [UIAlertView alertViewWithTitle:responseObject[@"error"]];
//        }else
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            inviteCode =[responseObject[@"data"] objectForKey:@"code"];
            
            CompanyInviteCodeViewController *phoneAB = [[CompanyInviteCodeViewController alloc] init];
            phoneAB.hidesBottomBarWhenPushed = YES;
            phoneAB.navigationItem.title = NSLocalized(@"company_set_code_get_PhoneItem");
            phoneAB.code=inviteCode;
//            phoneAB.Fname=friendName;
//            phoneAB.FriendPhoneNum=xuanZhongNum;
            [self.navigationController pushViewController:phoneAB animated:YES];
            
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark 所有小组
- (IBAction)allGroupsButClick:(id)sender {
    AllGroupViewController * allgvc=[[AllGroupViewController alloc] initWithNibName:@"AllGroupViewController" bundle:nil];
    allgvc.groupId=_model.id;
    allgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:allgvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark --退出,转让，解散相关
//转让点击事件
- (IBAction)onGroupDisbandGroupViewConveyBtnClick:(id)sender {
    [self setUsersCompanyList]; //转让前更新界面
    if (self.cannotDisband==YES) {
        [UIAlertView alertViewWithTitle:NSLocalized(@"HOME_alert_cueTitle") subTitle:NSLocalized(@"company_set_convey_notAdmin") target:nil];

    } else {
        
        if ([_model.userCount intValue]<=1) {
            [UIAlertView alertViewWithTitle:NSLocalized(@"HOME_alert_cueTitle") subTitle:NSLocalized(@"company_set_convey_onlyOne") target:nil];
            return;
        }
        transferPowerViewController *convey = [[transferPowerViewController alloc] init];
        convey.groupId = _model.id;
        convey.succeedBlock = ^(NSNumber *userId){
            self.cannotDisband = YES;
            [self setUsersCompanyList];
            
        };
//        [SVProgressHUD dismiss];
        [self.navigationController pushViewController:convey animated:YES];
    }
    
}
//解散点击事件
- (IBAction)onGroupDisbandGroupViewDisbandBtnClick:(id)sender {
    if (self.cannotDisband==YES) {
        [UIAlertView alertViewWithTitle:NSLocalized(@"HOME_alert_cueTitle") subTitle:NSLocalized(@"company_set_disband_notAdmin") target:nil];
    } else {
        [UIAlertView alertViewWithTitle:NSLocalized(@"company_set_disband_alertTitle") subTitle:NSLocalized(@"company_set_disband_alertMess") target:self];
    }
}
//退出点击事件
- (IBAction)exitButtonClick:(id)sender {
    UIAlertView * alertV=[[UIAlertView alloc] initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"company_set_logout_sure") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"HOME_alert_sure"), nil];
    alertV.tag=1000;
    [alertV show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:NSLocalized(@"company_set_dealing")];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        NSString *path =  [NSString stringWithFormat:delectCompanyPath,_model.id];
        NSString * mess=NSLocalized(@"company_set_disband");
        if (alertView.tag==1000) {  //退出
            path = [NSString stringWithFormat:leaveCompanyPath,_model.id];
            mess=NSLocalized(@"company_set_logout");
        }
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [SVProgressHUD dismiss];
            if ([responseObject[@"code"] intValue] == 200) {
                
                [ToolOfClass showMessage:[NSString stringWithFormat:@"%@%@",mess,NSLocalized(@"company_set_success")]];
                if ([mess isEqualToString:NSLocalized(@"company_set_disband")]) {
                    _exitButton.hidden=NO;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                }else{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                }
                
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
//                UIAlertView * alertV=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:responseObject[@"message"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
//                [alertV show];
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:[NSString stringWithFormat:@"%@%@",mess,NSLocalized(@"company_set_failure")]];
            
            [SVProgressHUD dismiss];
        }];
        
    }
}

@end
