//
//  AllGroupViewController.m
//  
//
//  Created by guanxf on 15/10/24.
//
//

#import "AllGroupViewController.h"
#import "ListDataTableViewCell.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import <AFNetworking/AFNetworking.h>
#import "UserModel.h"
#import "ToolOfClass.h"
#import "UIImageView+WebCache.h"
#import "GroupModel.h"
#import "UserJionedGroupTableViewCell.h"
#import "GroupDetailViewController.h"
#import "MainThemeViewController.h"

@interface AllGroupViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, retain) NSMutableArray *groups;   //公司小组列表
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *navView;

@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController; //系统文档控制器
@property (nonatomic, strong) UIImage *fileImage; //系统文档控制器

@end

@implementation AllGroupViewController

- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.groups =[[NSMutableArray alloc] init];
    if (_isShearPush) {
        self.navigationItem.title = NSLocalized(@"group_all_shareTo");
    }else{
        self.navigationItem.title = NSLocalized(@"group_all_title");
    }

    [self getUpGroupsListData];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(AllGroupViewBack)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    NSURL *fileURL= nil;
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path = [[NSString stringWithFormat:@"%@/Inbox",documentDir] stringByAppendingPathComponent:[_userStr lastPathComponent]];
    fileURL = [NSURL fileURLWithPath:path];
    
    [self setupDocumentControllerWithURL:fileURL];
    NSInteger iconCount = [self.docInteractionController.icons count];
    if (iconCount > 0)
    {
       self.fileImage = [self.docInteractionController.icons objectAtIndex:iconCount - 1];
    }
    
    NSString *fileURLString = [self.docInteractionController.URL path];
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURLString error:nil];
    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] intValue];
    NSString *fileSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize
                                                           countStyle:NSByteCountFormatterCountStyleFile];
//    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", fileSizeStr, self.docInteractionController.UTI];
//    UILongPressGestureRecognizer *longPressGesture =
//    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
//    [cell.imageView addGestureRecognizer:longPressGesture];
//    cell.imageView.userInteractionEnabled = YES;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)AllGroupViewBack{
    if (_isShearPush) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)getUpGroupsListData
{
    [self.groups removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:allCompanyGroups,self.groupId];
    
    if (_isShearPush) {
        path = [NSString stringWithFormat:userGroupsPath];
    }
    
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] intValue] == 200) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                
                if ([dict[@"category"] intValue] == 1) {
                    
                    GroupModel *model = [[GroupModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    [self.groups addObject:model];
                }
            }
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groups.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *celId = @"cellId";
    UserJionedGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:celId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"UserJionedGroupTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CGRect rect = cell.statusLabel.frame;
    rect.size.height = 0.5;
    cell.statusLabel.frame = rect;
    
    if (indexPath.row==self.groups.count-1) {
        cell.statusLabel.hidden = YES;
    } else {
        cell.statusLabel.hidden = NO;
    }
    cell.lockIgView.hidden = YES;
    GroupModel *model = self.groups[indexPath.row];
    cell.groupName.text = model.name;
    [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
    cell.groupUserCount.text = [NSString stringWithFormat:@"%@%@",model.userCount,NSLocalized(@"group_all_user")];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!_isShearPush) {
        // 不允许查看小组的资料，2016200
//        GroupDetailViewController *detail = [[GroupDetailViewController alloc] initWithNibName:@"GroupDetailViewController" bundle:[NSBundle mainBundle]];
//        GroupModel *model = self.groups[indexPath.row];
//        detail.groupId = model.id;
//        detail.SuperViewStr = @"chakan";
//        [self.navigationController pushViewController:detail animated:YES];
        
    }else{
//        QLPreviewController *previewController = [[QLPreviewController alloc] init];
//        previewController.dataSource = self;
//        previewController.delegate = self;
//        
//        // start previewing the document at the current section index
//        previewController.currentPreviewItemIndex = indexPath.row;
//        [[self navigationController] pushViewController:previewController animated:YES];
        
        MainThemeViewController * mainvc=[[MainThemeViewController alloc] initWithNibName:@"MainThemeViewController" bundle:nil];
        mainvc.image=_fileImage;
        mainvc.FileCopyPath=_userStr;
        mainvc.isFileCreate=YES;
        GroupModel *model = self.groups[indexPath.row];
        mainvc.groupId = model.id;
        mainvc.titleString=[_userStr lastPathComponent];
        [[self navigationController] pushViewController:mainvc animated:YES];
    }
}
- (IBAction)cancelButtonClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


#pragma mark - UIDocumentInteractionControllerDelegate

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
    return self;
}


#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    //    NSInteger numToPreview = 0;
    //
    //	numToPreview = [self.dirArray count];
    //
    //    return numToPreview;
    return 1;//[self.dirArray count];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    [previewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"click.png"] forBarMetrics:UIBarMetricsDefaultPrompt];
    
    NSURL *fileURL = nil;
//    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path = [[NSString stringWithFormat:@"%@/Inbox",documentDir] stringByAppendingPathComponent:@"fileName.txt"];
    fileURL = [NSURL fileURLWithPath:path];
    return fileURL;
}

-(id)fileImage{
    if (!_fileImage) {
        _fileImage=[[UIImage alloc] init];
    }
    return _fileImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
