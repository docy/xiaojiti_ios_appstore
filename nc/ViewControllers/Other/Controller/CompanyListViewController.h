//
//  CompanyListViewController.h
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import <UIKit/UIKit.h>

@interface CompanyListViewController : UIViewController

@property (nonatomic, copy) NSString *companyName; // 公司名称

@end
