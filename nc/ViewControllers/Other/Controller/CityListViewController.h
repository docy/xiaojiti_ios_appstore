//
//  CityListViewController.h
//  nc
//
//  Created by docy admin on 15/12/3.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CityListViewControllerDelegate <NSObject>

- (void)cityListViewControllerSetCity:(NSString *)city;

@end

@interface CityListViewController : UITableViewController

@property (nonatomic, strong) id<CityListViewControllerDelegate> delegate;

@end
