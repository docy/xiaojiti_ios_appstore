//
//  ListDataTableViewCell.h
//  
//
//  Created by guanxf on 15/10/25.
//
//

#import <UIKit/UIKit.h>

@interface ListDataTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *countbtn;

@end
