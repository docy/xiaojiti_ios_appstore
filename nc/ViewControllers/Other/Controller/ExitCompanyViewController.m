//
//  ExitCompanyViewController.m
//  
//
//  Created by guanxf on 15/10/24.
//
//

#import "ExitCompanyViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import "CompanyModel.h"
#import "transferPowerViewController.h"
#import "ToolOfClass.h"

@interface ExitCompanyViewController ()<UIAlertViewDelegate>

@property (nonatomic, assign) BOOL cannotDisband;

@end

@implementation ExitCompanyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self RegisterCreatCompanyViewSetUpData];
    _userCountLabel.text=[NSString stringWithFormat:@"%ld人",_userCount];
    self.namelabel.text = self.model.name;
    [self setButtonShowAndHidden];
}
// 设置基本属性
- (void)RegisterCreatCompanyViewSetUpData
{
    float hight=172;
    if (iPhone6) {
        hight=190;
    }else if (iPhone6Plus){
        hight=200;
    }
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.title = @"集体设置";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(RegisterCreatCompanyViewBackBtnClick)];
    //    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:getImageBeforeRegisterPath,@"company"]] placeholderImage:nil];
    
}

-(void)setButtonShowAndHidden{
    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    if ([num isEqualToNumber:_model.creator]) {  //若是公司创建者 则隐藏退出button 显示转让button
        _exitButton.hidden=YES;
        _dismissCompanyBut.hidden= NO;
    }else{
        _exitButton.hidden=NO;
        _dismissCompanyBut.hidden=YES;
    }
}

// 返回按钮
- (void)RegisterCreatCompanyViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

//转让点击事件
- (IBAction)onGroupDisbandGroupViewConveyBtnClick:(id)sender {
    
    if (self.cannotDisband==YES) {
        [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"你不是admin，无权转让集体admin身份!" target:nil];
    } else {
        
        if (_userCount<=1) {
            [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"这个集体就你一人儿，你还想转给谁，赶紧邀请几个伙伴吧。" target:nil];
            return;
        }
        
        transferPowerViewController *convey = [[transferPowerViewController alloc] init];
        convey.groupId = self.groupId;
        convey.succeedBlock = ^(NSNumber *userId){
            NSLog(@"转让成功");
        };
        [self.navigationController pushViewController:convey animated:YES];
    }
    
}
//解散点击事件
- (IBAction)onGroupDisbandGroupViewDisbandBtnClick:(id)sender {
    if (self.cannotDisband==YES) {
        [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"你不是Admin，无权解散集体!" target:nil];
    } else {
        [UIAlertView alertViewWithTitle:@"解散集体" subTitle:@"解散集体之后你将和好友失去联系，确定解散集体?" target:self];
    }
    
}
//退出点击事件
- (IBAction)exitButtonClick:(id)sender {
    UIAlertView * alertV=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"是否真的要退出该集体" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertV.tag=1000;
    [alertV show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        NSString *path =  [NSString stringWithFormat:delectCompanyPath,self.groupId];
        NSString * mess=@"解散";
        if (alertView.tag==1000) {  //退出
            path = [NSString stringWithFormat:leaveCompanyPath,self.groupId];
            mess=@"退出";
        }
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] intValue] == 200) {
                [ToolOfClass showMessage:[NSString stringWithFormat:@"%@成功",mess]];
                if ([mess isEqualToString:@"解散"]) {
                    _exitButton.hidden=NO;
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }else{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
//                UIAlertView * alertV=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:responseObject[@"message"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
//                [alertV show];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:[NSString stringWithFormat:@"%@失败",mess]];
        }];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
