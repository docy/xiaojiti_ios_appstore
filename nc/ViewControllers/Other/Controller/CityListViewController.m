//
//  CityListViewController.m
//  nc
//
//  Created by docy admin on 15/12/3.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import "CityListViewController.h"

@interface CityListViewController ()

@property (nonatomic, retain) NSArray *cityList;
@property (nonatomic, retain) NSArray *cityKeys;

@end

@implementation CityListViewController

- (instancetype)init{
    if (self = [super init]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:isEnglish?@"CityList_en":@"CityList" ofType:@"plist"];
        
        NSDictionary *listDic = [NSDictionary dictionaryWithContentsOfFile:path];

        _cityKeys = [[NSArray arrayWithArray:[listDic allKeys]] sortedArrayUsingSelector:@selector(compare:)];
        NSMutableArray *city = [NSMutableArray array];
        for (NSString *item in _cityKeys) {
            [city addObject:listDic[item]];
        }
        _cityList = city;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalized(@"city_list_title");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _cityKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cityList[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cityListId = @"cityListId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityListId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cityListId];
    }
    
    cell.textLabel.text = self.cityList[indexPath.section][indexPath.row];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return _cityKeys[section];
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return _cityKeys;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(cityListViewControllerSetCity:)]) {
        [self.delegate cityListViewControllerSetCity:_cityList[indexPath.section][indexPath.row]];
        
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
