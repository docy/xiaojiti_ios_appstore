//
//  CompanyListTVCell.m
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import "CompanyListTVCell.h"




@implementation CompanyListTVCell


- (void)awakeFromNib {
    // Initialization code
}

-(FXLabel*)companyName{
    _companyName.shadowColor = nil;
//    _companyName.shadowColor = [UIColor blackColor];
//    _companyName.shadowBlur=2.0f;
    return _companyName;
}

-(UILabel *)creatorLabel{
    _creatorLabel.shadowColor = nil;
//    _creatorLabel.shadowColor = [UIColor blackColor];
//    _creatorLabel.shadowBlur=2.0f;
    return _creatorLabel;
}

-(UILabel*)userCountLabel{
    _userCountLabel.shadowColor = nil;
//    _userCountLabel.shadowColor = [UIColor blackColor];
//    _userCountLabel.shadowBlur=2.0f;
    return _userCountLabel;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSMutableAttributedString *)stringWithString:(NSString *)string rangeString:(NSString *)rangeString rangColor:(UIColor *)color{
    NSMutableAttributedString *attributsStr = [[NSMutableAttributedString alloc] initWithString:string];
    NSRange range = [string rangeOfString:rangeString];
    [attributsStr addAttribute:NSForegroundColorAttributeName value:color range:range];
    return attributsStr;
}

@end
