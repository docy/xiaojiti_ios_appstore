//
//  AllGroupViewController.h
//  
//
//  Created by guanxf on 15/10/24.
//
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>

@interface AllGroupViewController : UIViewController<QLPreviewControllerDataSource,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate>

@property (nonatomic, retain) NSNumber *groupId;   //公司id

@property (nonatomic, assign) BOOL isShearPush;     //是否是通过分享文件进入该页面

@property (nonatomic, strong) NSString *userStr;   //文件分享进来的路径

@end
