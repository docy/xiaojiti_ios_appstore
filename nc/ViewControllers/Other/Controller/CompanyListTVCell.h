//
//  CompanyListTVCell.h
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import <UIKit/UIKit.h>
#import "FXLabel.h"


@interface CompanyListTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *companyImageView;
@property (weak, nonatomic) IBOutlet FXLabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *userCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *settinButClick;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *joinBtn;
@property (weak, nonatomic) IBOutlet UIImageView *MinImage;
@property (weak, nonatomic) IBOutlet UILabel *NumberL;
@property (weak, nonatomic) IBOutlet UIImageView *compListImage;

- (NSMutableAttributedString *)stringWithString:(NSString *)string rangeString:(NSString *)rangeString rangColor:(UIColor *)color;

@end
