//
//  NetWorkBadView.h
//  nc
//
//  Created by docy admin on 15/9/15.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface NetWorkBadView : UIView

@property (weak, nonatomic) IBOutlet UIButton *OpenNetWorkBtn;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;


@end
