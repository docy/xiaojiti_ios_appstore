//
//  AFNetWorkNotReachableView.h
//  nc
//
//  Created by docy admin on 15/9/14.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFNetWorkNotReachableView : UIView

@property (weak, nonatomic) IBOutlet UIButton *loadAgainBtn;

@end
