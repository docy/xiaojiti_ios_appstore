//
//  usernamePW_ViewController.h
//  nc
//
//  Created by guanxf on 16/3/18.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^updataUserName)(NSString*);

@interface usernamePW_ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

@property (nonatomic, copy) NSString *itemTitle; 
@property (nonatomic, strong) updataUserName updataUserName;// 界面更新
@property (nonatomic, assign) BOOL isPush; //设置下是否是 push的VC

@end
