//
//  RegisterViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "RegisterViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "CompanyNameViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "SeconRegisterViewController.h"
#import "ToolOfClass.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "SVProgressHUD.h"
#import "UIColor+Hex.h"

@interface RegisterViewController () <UITextFieldDelegate>

//@property (nonatomic, copy) NSString *identifyCode; // 存储验证码
@property (weak, nonatomic) IBOutlet UIButton *zhuceButton;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
//    [super viewDidLoad];
    
    [self setUpRegisterViewData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardwillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)keyboardwillAppear:(NSNotification *)notification
{
    CGRect currentFrame = self.view.frame;
//    NSLog(@"currentFrame.origin.y: %f",currentFrame.origin.y);
    if (currentFrame.origin.y!=0.00) {
        return;
    }
    //    CGFloat change = [self keyboardEndingFrameHeight:[notification userInfo]];
    currentFrame.origin.y = -60 ;
    self.view.frame = currentFrame;
}

-(void)keyboardWillDisappear:(NSNotification *)notification
{
    CGRect currentFrame = self.view.frame;
    //    CGFloat change = [self keyboardEndingFrameHeight:[notification userInfo]];
    currentFrame.origin.y = 0.0;//currentFrame.origin.y + change ;
    self.view.frame = currentFrame;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    self.navigationController.navigationBar.hidden=YES;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.phoneLabel.text=_phoneString;
}

- (void)setUpRegisterViewData
{
    //    self.navigationItem.title = @"注册";
//    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onRegisterViewBackButtonClick)];
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onRegisterViewBackButtonClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onRegisterViewBackButtonClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onRegisterViewBackButtonClick)];
    
    self.nameLabel.placeholder = NSLocalized(@"regist_namePH");
    self.phoneLabel.placeholder = NSLocalized(@"regist_phonePH");
    self.identifyCodeLabel.placeholder = NSLocalized(@"regist_codePH");
    self.passwordLabel.placeholder = NSLocalized(@"regist_passwordPH");
    
    self.daojishiLabel.text = NSLocalized(@"regist_getCode");
//    [self.identifyCodeBtn setTitle:NSLocalized(@"regist_getCode") forState:UIControlStateNormal];
    [self.Localized_regist setTitle:NSLocalized(@"regist_registBtn") forState:UIControlStateNormal];
    self.navigationItem.title = NSLocalized(@"regist_title");
//    self.identifyCode = [NSString string];
    
    self.nameLabel.delegate = self;
//    self.emailLabel.delegate = self;
    self.phoneLabel.delegate = self;
    self.identifyCodeLabel.delegate = self;
    self.passwordLabel.delegate = self;
    _zhuceButton.layer.borderWidth=0.5f;
    _zhuceButton.layer.borderColor=[[UIColor whiteColor] CGColor];
    UIColor * colorr=[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0];
    [self.nameLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];//[UIColor colorWithHex:0x48C1A8]
    [self.phoneLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    [self.identifyCodeLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    
}

// 返回按钮的点击事件
- (IBAction)onRegisterViewBackButtonClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 获取验证码
- (IBAction)getIdentifyCodeBtnClick:(id)sender {
    
    if (self.phoneLabel.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"regist_phone_unBlank")];
    } else {
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:NSLocalized(@"info_forgetPW_sendRequest")];
        _identifyCodeBtn.enabled=NO;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"phone"] = self.phoneLabel.text;

        [manager POST:getidentifyCodePath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"code"] intValue] == 200) {
                [ToolOfClass toolStartTimeSecondsCountDown_uilabel:self.daojishiLabel];
                NSLog(@"***************邀请码为:%@",responseObject[@"data"][@"code"]);
                _identifyCodeBtn.enabled=NO;
                _identifyCodeBtn.hidden = YES;
                double delayInSeconds = 30.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    _identifyCodeBtn.enabled=YES;
                    _identifyCodeBtn.hidden = NO;
                });
//                [ToolOfClass toolStartTimeSecondsCountDown:self.identifyCodeBtn];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                _identifyCodeBtn.enabled=YES;
                _identifyCodeBtn.hidden = NO;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}


// 注册点击事件
- (IBAction)onNextButtonClick:(id)sender {
    
    if (self.nameLabel.text.length==0||self.phoneLabel.text.length == 0||self.passwordLabel.text.length==0)
    {
        [ToolOfClass showMessage:NSLocalized(@"regist_unBlank")];
    } else if (self.identifyCodeLabel.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"regist_code_unBlank")];
    } else {// 确保用户名，电话，邮箱已经输入信息
        // 检测输入信息是否正确
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"name"] = self.nameLabel.text;
//        parameter[@"email"] = self.emailLabel.text;
        parameter[@"phone"] = self.phoneLabel.text;
        parameter[@"code"] = self.identifyCodeLabel.text;
        parameter[@"password"] = self.passwordLabel.text;
        
        [manager POST:registCheck parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

            if ([responseObject[@"code"] isEqualToNumber:@200]) { // 输入注册信息可用
                PersonalMessageModel *model = [[PersonalMessageModel alloc] init];
                model.name = self.nameLabel.text;
//                model.email = self.emailLabel.text;
                model.phone = self.phoneLabel.text;
                model.password = self.passwordLabel.text;

                // 跳转至个人资料界面
                NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
                [info setObject:self.nameLabel.text forKey:@"name"];
//                SeconRegisterViewController *secondVC = [[SeconRegisterViewController alloc] initWithNibName:@"SeconRegisterViewController" bundle:[NSBundle mainBundle]];
//                secondVC.personalModel = model;
//                [self.navigationController pushViewController:secondVC animated:YES];
                [self onSeconRegisterRegister];
                
            } else { // 输入信息错误
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (void)onSeconRegisterRegister {
    PersonalMessageModel *model = [[PersonalMessageModel alloc] init];
    model.name = self.nameLabel.text;
//    model.email = self.emailLabel.text;
    model.phone = self.phoneLabel.text;
    model.password = self.passwordLabel.text;

    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    [info setObject:model.name forKey:@"name"];
    [info setObject:model.password forKey:@"password"];
    
    // 发送注册请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    
    parameter[@"name"] = model.name;
    parameter[@"nickName"] = model.name;
    parameter[@"email"] = model.email;
    parameter[@"password"] = model.password;
    parameter[@"phone"] = model.phone;
    parameter[@"sex"] = @1;
    NSData *imagedata;
//    if (self.avartarName==nil) { // 上传默认图片
//        self.avartarName = @"userAvatar.png";
//        parameter[@"logo"] = self.avatarPath;
//    }
//    imagedata = UIImageJPEGRepresentation(self.personalIconImageView.image, ImageYaSuo);
    [manager POST:registerPath parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
//        [formData appendPartWithFileData:imagedata name:@"avatar" fileName:self.avartarName mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@200])
        {
            
            [ToolOfClass showMessage:NSLocalized(@"regist_success")];
            // 注册成功(加入公司或创建公司)
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            //            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            // 发送登录请求
            AFHTTPRequestOperationManager *loginManager = [AFHTTPRequestOperationManager manager];
            loginManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"account"] = model.name;
            parameters[@"password"] = model.password;
            
            [loginManager POST:LoginPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] isEqualToNumber:@200]) {
                    NSDictionary *data = responseObject[@"data"];
                    // 存储token,id值
                    [info setObject:data[@"authToken"] forKey:@"authToken"];
                    [info setObject:data[@"id"] forKey:@"id"];
                    [info setObject:@"NO" forKey:@"isLogout"];
                    [info setObject:data[@"nickName"] forKey:@"nickName"];
                    [info setObject:data[@"avatar"] forKey:@"avatar"];
                    
                    if (![data[@"currentCompany"] isEqual:[NSNull null]]) {
                        [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                    }

                    SeconRegisterViewController * srvc=[[SeconRegisterViewController alloc] initWithNibName:@"SeconRegisterViewController" bundle:nil];
                    [self.navigationController pushViewController:srvc animated:YES];
                    [self.navigationController setNavigationBarHidden:NO];
                    //显示输入邀请码加入
//                    RegisterInviteToJoinCompanyViewController *inviteVC = [[RegisterInviteToJoinCompanyViewController alloc] init];
//                    inviteVC.isRegisterViewPush=YES;
//                    [self.navigationController pushViewController:inviteVC animated:YES];
                    
                } else {
//                    [ToolOfClass showMessage:responseObject[@"message"]];
                    [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            }];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"regist_failure")];
    }];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.phoneLabel resignFirstResponder];
    [self.nameLabel resignFirstResponder];
    [self.emailLabel resignFirstResponder];
    [self.passwordLabel resignFirstResponder];
    [self.identifyCodeLabel resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag==1111) {
        _phoneImg.image=[UIImage imageNamed:@"icon-phone"];
    }else if (textField.tag==1112) {
        _codeImg.image=[UIImage imageNamed:@"icon-code"];
    }else if (textField.tag==1113) {
        _userNameimg.image=[UIImage imageNamed:@"icon-user name"];
    }else if (textField.tag==1114) {
        _passWordimg.image=[UIImage imageNamed:@"icon-password"];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag==1111) {
        _phoneImg.image=[UIImage imageNamed:@"icon-phone gray"];
    }else if (textField.tag==1112) {
        _codeImg.image=[UIImage imageNamed:@"icon-code gray"];
    }else if (textField.tag==1113) {
        _userNameimg.image=[UIImage imageNamed:@"icon-user name gray"];
    }else if (textField.tag==1114) {
        _passWordimg.image=[UIImage imageNamed:@"icon-password gray"];
    }
}

//-(void)startTimeSecondsCountDown{
//    __block int timeout = 30; //倒计时时间
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
//    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
//    dispatch_source_set_event_handler(_timer, ^{
//        if(timeout<=0){ //倒计时结束，关闭
//            dispatch_source_cancel(_timer);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                [self.identifyCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
//                self.identifyCodeBtn.userInteractionEnabled = YES;
//            });
//        }else{
//            int seconds = timeout % 60;
//            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                //NSLog(@"____%@",strTime);
//                [UIView beginAnimations:nil context:nil];
//                [UIView setAnimationDuration:1];
//                [self.identifyCodeBtn setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
//                [UIView commitAnimations];
//                self.identifyCodeBtn.userInteractionEnabled = NO;
//            });
//            timeout--;
//        }
//    });
//    dispatch_resume(_timer);
//}

@end
