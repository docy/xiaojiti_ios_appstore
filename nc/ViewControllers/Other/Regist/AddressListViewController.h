//
//  AddressListViewController.h
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupModel.h"
@interface AddressListViewController : UIViewController
- (IBAction)addressListButton:(id)sender;
- (IBAction)refuseButton:(id)sender;

@property (nonatomic, strong) GroupModel *groupModel; // 用于界面传值

@end
