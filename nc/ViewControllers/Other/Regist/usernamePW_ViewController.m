//
//  usernamePW_ViewController.m
//  nc
//
//  Created by guanxf on 16/3/18.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "usernamePW_ViewController.h"
#import "UIAlertView+AlertView.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
@interface usernamePW_ViewController ()
@property (weak, nonatomic) IBOutlet UIView *rootview;
@property (weak, nonatomic) IBOutlet UIButton *dissButton;

@end

@implementation usernamePW_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=_itemTitle;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_isPush) {
        CGRect newframe=_rootview.frame;
        newframe.origin.y=0;
        _rootview.frame=newframe;
        _dissButton.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)lookPW_ButtonClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    self.password.secureTextEntry = !self.password.secureTextEntry;
}

- (IBAction)dismissVC_Click:(id)sender {
    if (_isPush) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:^{
        
        }];
    }
}

- (IBAction)onZhuCeClick:(id)sender {
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSString *password = self.password.text;
    NSString *username = self.username.text;
    
    if (username.length==0) {
        [UIAlertView alertViewWithTitle:@"用户名不能为空哦！"];//NSLocalized(@"info_reworkPW_needOld")];
    } else if (password.length == 0) {
        [UIAlertView alertViewWithTitle:@"密码不能为空哦！"];//NSLocalized(@"info_reworkPW_needNew")];
    } else if ([password isEqualToString:username]) {
        [UIAlertView alertViewWithTitle:@"用户名和密码不能相同哦！"];//NSLocalized(@"info_reworkPW_notSame")];
    } else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        parameter[@"password"] = password;
        parameter[@"name"] = username;
        [manager POST:UpdateUserInfo parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"UpdateUserInfo :%@",responseObject);
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                
                [ToolOfClass showMessage:NSLocalized(@"HOME_show_remoke_success")];
                [info setObject:password forKey:@"password"];
                [info setObject:username forKey:@"name"];

                if (_isPush) {
                    self.updataUserName(username);
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                }
                
                
            } else {
                //                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:NSLocalized(@"info_reworkPW_failure")];
        }];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
