//
//  RegisterViewController.h
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 注册视图控制器

@interface RegisterViewController : UIViewController

- (IBAction)getIdentifyCodeBtnClick:(id)sender;

- (IBAction)onNextButtonClick:(id)sender;

@property (nonatomic,strong) NSString * phoneString;   //预添加手机号

@property (weak, nonatomic) IBOutlet UITextField *nameLabel;

@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordLabel;
@property (weak, nonatomic) IBOutlet UITextField *identifyCodeLabel;  // 验证码
@property (weak, nonatomic) IBOutlet UIButton *identifyCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *daojishiLabel;

@property (weak, nonatomic) IBOutlet UIImageView *phoneImg;
@property (weak, nonatomic) IBOutlet UIImageView *codeImg;
@property (weak, nonatomic) IBOutlet UIImageView *userNameimg;
@property (weak, nonatomic) IBOutlet UIImageView *passWordimg;

@property (weak, nonatomic) IBOutlet UIButton *Localized_regist;



@end
