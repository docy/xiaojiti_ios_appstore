//
//  RegisterCreatCompanyViewController.m
//  xjt
//
//  Created by docy admin on 7/31/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "RegisterCreatCompanyViewController.h"

#import <AFNetworking/AFNetworking.h>
//#import "UITabBarController+CustomTabBarController.h"
#import "XJTTabBarController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import "APIHeader.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "CompanyModel.h"
#import "CityListViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "UIButton+XJTButton.h"
#import "SVProgressHUD.h"

@interface RegisterCreatCompanyViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,CityListViewControllerDelegate,CLLocationManagerDelegate>

@property (nonatomic, copy) NSString *logoName; // 公司logo名
@property (nonatomic, assign) NSInteger categoryType; // 分类
@property (nonatomic, copy) NSString *companyPath; // 公司logo名
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISwitch *switchBar;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UILabel *cityAddressLabel;

@property (weak, nonatomic) IBOutlet UILabel *companyTypeLabel;
- (IBAction)selectedCityAddress;

- (IBAction)seletedCompanyType;

@property (nonatomic, retain) NSArray *pickerData;
@property (weak, nonatomic) IBOutlet UITextField *categoryTF;

@property (weak, nonatomic) IBOutlet UILabel *Localized_name;
@property (weak, nonatomic) IBOutlet UILabel *Localized_des;
@property (weak, nonatomic) IBOutlet UILabel *Localized_city;
@property (weak, nonatomic) IBOutlet UILabel *Localized_category;
@property (weak, nonatomic) IBOutlet UILabel *Localized_isPrivate;
@property (weak, nonatomic) IBOutlet UILabel *Localized_descPH;

@property (nonatomic , strong) CLLocationManager *locationManager; // 定位
@property (nonatomic , weak) UIView *bgView; // 分类的灰色背景
@property (nonatomic , assign) BOOL isCategoryTF; // 判断是否是分类调用键盘
@end

@implementation RegisterCreatCompanyViewController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self RegisterCreatCompanyViewSetUpData];
    [self getUpRegisterCreatCompanyViewRandomLogo];
    [self locate];
    
    self.Localized_name.text = NSLocalized(@"company_creat_name");
    self.Localized_des.text = NSLocalized(@"company_creat_des");
    self.Localized_city.text = NSLocalized(@"company_creat_city");
    self.Localized_category.text = NSLocalized(@"company_creat_category");
    self.Localized_isPrivate.text = NSLocalized(@"company_creat_isPrivate");
    self.companyNameTextField.placeholder = NSLocalized(@"company_creat_name_placeH");
    self.Localized_descPH.text = NSLocalized(@"company_creat_des_placeH");
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                  selector:@selector(keyboardWillShow:)
                                                      name:UIKeyboardWillShowNotification
                                                    object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification{
    if (self.isCategoryTF) {
        CGFloat duration = [notification.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
        AppDelegate *delegate = APP_DELEGATE;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        view.alpha = 0.0;
        view.backgroundColor = [UIColor grayColor];
        [delegate.window addSubview:view];
        self.bgView = view;
        
        [UIView animateWithDuration:duration animations:^{
            view.alpha = 0.5;
        }];
    }
    
    
}
- (void)keyboardWillHide:(NSNotification *)notification{
    
    if (self.isCategoryTF) {
        CGFloat duration = [notification.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
        [UIView animateWithDuration:duration animations:^{
            self.bgView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.bgView removeFromSuperview];
            self.isCategoryTF = NO;
        }];
    }
}
- (void)locate{
    _locationManager=[[CLLocationManager alloc] init];
    _locationManager.delegate=self;
    _locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    _locationManager.distanceFilter=10;

    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8) {
        [_locationManager requestWhenInUseAuthorization];//使用程序其间允许访问位置数据（iOS8定位需要）
    }
    [_locationManager startUpdatingLocation];//开启定位
}

// 设置基本属性
- (void)RegisterCreatCompanyViewSetUpData
{
//    self.pickerView.delegate = self;
//    self.pickerView.dataSource = self;
    self.pickerView.hidden = YES;
    self.pickerData = @[NSLocalized(@"company_category_1"),NSLocalized(@"company_category_2"),NSLocalized(@"company_category_3"),NSLocalized(@"company_category_4"),NSLocalized(@"company_category_5"),NSLocalized(@"company_category_6")];

    self.pickerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.scrollView.contentSize=CGSizeMake(ScreenWidth, ScreenHeight);
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.title = NSLocalized(@"company_creat_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(RegisterCreatCompanyViewBackBtnClick)];
    self.navigationItem.rightBarButtonItem=[UIBarButtonItem itemWithTitle:NSLocalized(@"company_creat_title_rightTitle") target:self action:@selector(sendCreateMessageButtonClick)];
    
    self.categoryTF.delegate = self;
    self.categoryTF.tag = 100;
    UIPickerView *picker = [[UIPickerView alloc] init];
    picker.backgroundColor = [UIColor whiteColor];
    picker.dataSource = self;
    picker.delegate = self;
    self.categoryTF.inputView = picker;
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    toolBar.barTintColor = CustomColor(72, 193, 168);
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(selecteCatory)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:NSLocalized(@"company_creat_title_rightTitle") forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 50, 30);
    [button addTarget:self action:@selector(selecteCatory) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithCustomView:button];
    toolBar.items = @[item1,item2];
    self.categoryTF.inputAccessoryView = toolBar;
    
}

- (void)selecteCatory{
    
    [self.categoryTF resignFirstResponder];
    if (self.categoryTF.text.length==0) {
        [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
    }
}

-(void)sendCreateMessageButtonClick{
    [self RegisterCreatCompanyViewCreatCompanyBtnClick:nil];
}

// 返回按钮
- (void)RegisterCreatCompanyViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpRegisterCreatCompanyViewRandomLogo
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:randomLogo] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"1100=＝%@",responseObject);
        if ([responseObject[@"code"] intValue] == 200) {
            self.companyPath = responseObject[@"data"][@"logo"];
            [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:responseObject[@"data"][@"logo"]]] placeholderImage:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 选择图片
- (IBAction)RegisterCreatCompanyViewSelectLogoBtnClick:(id)sender {
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
}

// 创建公司
- (IBAction)RegisterCreatCompanyViewCreatCompanyBtnClick:(id)sender {
    
    if (self.companyNameTextField.text.length==0 || self.phoneTextField.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"company_creat_mess_notAll")];
        return;
    }
    
    if (self.cityAddressLabel.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"company_creat_chooseCity")];
        return;
    }
    if (self.categoryTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"company_creat_chooseCategory")];
        return;
    }
    
//    if (_companyNameTextField.text.length>20) {
//        UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"集体名字太长了，不要超过20字哦！" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
//        [uiav show];
//        return;
//    }
//    if (_phoneTextField.text.length>60) {
//        UIAlertView * uiav=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"集体描述太长了，不要超过60字哦！" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
//        [uiav show];
//        return;
//    }

    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.view.userInteractionEnabled = NO;
//    AppDelegate *delegate = APP_DELEGATE;
//    delegate.window.userInteractionEnabled = NO;
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:NSLocalized(@"company_creat_creating")];
    // 创建公司
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"name"] = self.companyNameTextField.text;
    parameter[@"desc"] = self.phoneTextField.text;
    parameter[@"isPrivate"] = @(self.switchBar.on?1:0);
   /* const Categories = {
        0: '通用', 没有通用
        1: '会议',
        2: '股票',
        3: '旅游',
        4: '教育',
        5: '户外',
        6: '其他'
    };
    */
    parameter[@"category"] = @(_categoryType+1);
    parameter[@"city"] = self.cityAddressLabel.text;
    
    NSData *imagedata;
    if (self.logoName==nil) { // 上传默认图片
        self.logoName = @"companyAvatar.png";
        parameter[@"logoUrl"] = self.companyPath;
    }
    imagedata = UIImageJPEGRepresentation(self.logoImageView.image, 1.0);
    [manager POST:creatCompanyPath parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imagedata name:@"logoUrl" fileName:self.logoName mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [SVProgressHUD dismiss];
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.view.userInteractionEnabled = YES;
        
//        delegate.window.userInteractionEnabled = YES;
        if ([responseObject[@"code"] intValue]==200) {
            
            CompanyModel *model = [[CompanyModel alloc] init];
            [model setValuesForKeysWithDictionary:responseObject[@"data"]];
            model.creator = USER_ID;
            model.userCount = @1;
            AppDelegate *deleagte = APP_DELEGATE;
            deleagte.tempCopModel = model;
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            // 设置当前公司
            [self RegisterCreatCompanyViewSetUserCurrentCompany:model.id];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.view.userInteractionEnabled = YES;
//        delegate.window.userInteractionEnabled = YES;
        [SVProgressHUD showWithStatus:NSLocalized(@"company_creat_creatFailure")];
        [SVProgressHUD dismissWithDelay:0.3];
    }];
}

#pragma mark --UIImagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.logoImageView.image = info[UIImagePickerControllerEditedImage];
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    self.logoName = [fileName copy];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 设置为当前公司
- (void)RegisterCreatCompanyViewSetUserCurrentCompany:(NSNumber *)companyId
{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [info setObject:companyId forKey:@"currentCompany"];
    NSString *str = [NSString stringWithFormat:setCurrentCompany,companyId];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
            [info setObject:companyId forKey:@"currentCompany"];
            [info setValue:@"YES" forKey:@"isHaveCurrent"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
            [ToolOfClass showMessage:NSLocalized(@"company_creat_creatSuccess")];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        [ToolOfClass showMessage:NSLocalized(@"company_creat_creatFailure")];
        
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.companyNameTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
}

#pragma mark textFieldDidBeginEditing

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSInteger size=0;
    int height=173+150;
    
    if (textField.tag == 100) { // 分类标签
        height=height+150;
        self.isCategoryTF = YES;
    } else {
        self.isCategoryTF = NO;
    }
    
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
    //    [self.tableview setContentOffset:CGPointMake(0, 70) animated:YES];
    //    [self.themeTextFiled becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)sender {
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
#define STRING_MAX 20
    if ((textField.text.length - range.length + string.length) > STRING_MAX)
    {
        NSString *substring = [string substringToIndex:STRING_MAX - (textField.text.length - range.length)];
        NSMutableString *lastString = [textField.text mutableCopy];
        [lastString replaceCharactersInRange:range withString:substring];
        textField.text = [lastString copy];
        return NO;
    }
    else
    {
        return YES;
    }
}

- (IBAction)touchView:(id)sender {
    [self.view endEditing:YES];
    
    if (!self.pickerView.hidden) {
        [UIView animateWithDuration:0.5 animations:^{
            self.pickerView.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.pickerView.hidden = YES;
        }];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _descTV.hidden=YES;
    NSInteger size=0;
    int height=173+150+70;
    size=ScreenHeight-height;//-44*(textField.tag-100+1);
    if (size<300) {
        [self.scrollView setContentOffset:CGPointMake(0, 300-size) animated:YES];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length==0) {
        _descTV.hidden=NO;
    }
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    #define TEXT_MAX 60
    if ((textView.text.length - range.length + text.length) > TEXT_MAX)
    {
        NSString *substring = [text substringToIndex:TEXT_MAX - (textView.text.length - range.length)];
        NSMutableString *lastString = [textView.text mutableCopy];
        [lastString replaceCharactersInRange:range withString:substring];
        textView.text = [lastString copy];
        return NO;
    }
    else
    {
        return YES;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.pickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.pickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
//    self.companyTypeLabel.text = self.pickerData[row];
    self.categoryTF.text = self.pickerData[row];
    self.categoryType = row;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30;
}

#pragma mark 选择城市
- (IBAction)selectedCityAddress {
    [self.view endEditing:YES];
    CityListViewController *cityList = [[CityListViewController alloc] init];
    cityList.delegate = self;
    [self.navigationController pushViewController:cityList animated:YES];
}

#pragma mark 选择城市后的代理
- (void)cityListViewControllerSetCity:(NSString *)city{
    self.cityAddressLabel.text = city;
}

#pragma mark 选择分类
- (IBAction)seletedCompanyType {
    
    if (self.pickerView.hidden) {
        self.pickerView.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^{
            self.pickerView.alpha = 1.0;
        } completion:^(BOOL finished) {
//            self.pickerView.hidden = NO;
            if (self.companyTypeLabel.text.length==0) {
                [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
            }
        }];
        
        
    } else {
        [UIView animateWithDuration:0.5 animations:^{
            self.pickerView.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.pickerView.hidden = YES;
        }];
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //此处locations存储了持续更新的位置坐标值，取最后一个值为最新位置，如果不想让其持续更新位置，则在此方法中获取到一个值之后让locationManager stopUpdatingLocation
    CLLocation *currentLocation = [locations lastObject];
    // 获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *array, NSError *error)
     {
         if (array.count > 0)
         {
             CLPlacemark *placemark = [array objectAtIndex:0];
             //NSLog(@%@,placemark.name);//具体位置
             //获取城市
             NSString *city = placemark.locality;
             if (!city) {
                 //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                 city = placemark.administrativeArea;
             }
             NSString * cityName = city;
             NSLog(@"定位完成:%@",cityName);
             self.cityAddressLabel.text = cityName;
             //系统会一直更新数据，直到选择停止更新，因为我们只需要获得一次经纬度即可，所以获取之后就停止更新
             [manager stopUpdatingLocation];
         }else if (error == nil && [array count] == 0)
         {
             NSLog(@"No results were returned.");
         }else if (error != nil)
         {
             NSLog(@"An error occurred = %@", error);
         }
     }];
    
}

@end
