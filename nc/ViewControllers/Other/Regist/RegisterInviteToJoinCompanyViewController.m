//
//  RegisterInviteToJoinCompanyViewController.m
//  xjt
//
//  Created by docy admin on 7/31/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "RegisterInviteToJoinCompanyViewController.h"
#import "RegisterCreatCompanyViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+AlertView.h"
#import "ToolOfClass.h"
//#import "UITabBarController+CustomTabBarController.h"
#import "XJTTabBarController.h"
#import "APIHeader.h"
#import "CHDigitInput.h"

@interface RegisterInviteToJoinCompanyViewController ()<UITextFieldDelegate>
{
    CHDigitInput *digitInput;
}
@property (weak, nonatomic) IBOutlet UITextField *textFiled;
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property (weak, nonatomic) IBOutlet UIButton *tiaoguoButton;
@property (weak, nonatomic) IBOutlet UIButton *join_btn;

-(void)didBeginEditing:(id)sender;
-(void)didEndEditing:(id)sender;
-(void)textDidChange:(id)sender;
-(void)valueChanged:(id)sender;


@end

@implementation RegisterInviteToJoinCompanyViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationItem.title= NSLocalized(@"company_codeJoin_title");
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = nil;//[UIBarButtonItem itemWithTarget:self action:@selector(InviteToJoinCompanyViewBackBtnClick)];
    
    if (self.isRegisterViewPush==NO) {
        self.hiddenView.hidden = YES;
        _tiaoguoButton.hidden=YES;
//        _alertLabel.hidden=YES;
         [self.navigationItem setHidesBackButton:NO];
    } else {
        self.hiddenView.hidden = NO;
        _tiaoguoButton.hidden=NO;
//        _alertLabel.hidden=NO;
        [self.navigationItem setHidesBackButton:YES];
    }
    self.alertLabel.text = NSLocalized(@"company_codeJoin_desc");
    [self.join_btn setTitle:NSLocalized(@"company_codeJoin_joinBtn") forState:UIControlStateNormal];
    [self.tiaoguoButton setTitle:NSLocalized(@"regist_code_jumpBtn") forState:UIControlStateNormal];
    
    [self initCHDigitInt];
    
}
// 返回按钮(回到登录界面)
- (void)InviteToJoinCompanyViewBackBtnClick
{
    if (self.isRegisterViewPush) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void) initCHDigitInt{
    digitInput = [[CHDigitInput alloc] initWithNumberOfDigits:6];
    
    
    digitInput.digitOverlayImage = [UIImage imageNamed:@"digitOverlay"];
    digitInput.digitBackgroundImage = [UIImage imageNamed:@"digitControlBG"];
    
    //digitInput.backgroundView = digitBGView;
    
    digitInput.placeHolderCharacter = @"0";
    
    // we are using an overlayimage, so make the bg color clear color
    
    digitInput.digitViewBackgroundColor = [UIColor clearColor];
    digitInput.digitViewHighlightedBackgroundColor = [UIColor clearColor];
    
    digitInput.digitViewTextColor = [UIColor whiteColor];
    digitInput.digitViewHighlightedTextColor = [UIColor orangeColor];
    
    
    // we changed the default settings, so call redrawControl
    [digitInput redrawControl];
    
    [self.view addSubview:digitInput];
    
    // adding the target,actions for available events
    [digitInput addTarget:self action:@selector(didBeginEditing:) forControlEvents:UIControlEventEditingDidBegin];
    [digitInput addTarget:self action:@selector(didEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    [digitInput addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [digitInput addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    
}

// dismissing the keyboard
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [digitInput resignFirstResponder];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    digitInput.frame = CGRectMake(10, 90, self.view.frame.size.width-20, 55);
}

/////////// recating on demo events ///////////
-(void)didBeginEditing:(id)sender
{
//    CHDigitInput *input = (CHDigitInput *)sender;
//    NSLog(@"did begin editing %i",input.value);
}

-(void)didEndEditing:(id)sender
{
//    CHDigitInput *input = (CHDigitInput *)sender;
//    NSLog(@"did end editing %i",input.value);
}

-(void)textDidChange:(id)sender
{
//    CHDigitInput *input = (CHDigitInput *)sender;
//    NSLog(@"text did change %i",input.value);
}

-(void)valueChanged:(id)sender
{
//    CHDigitInput *input = (CHDigitInput *)sender;
//    NSLog(@"value changed %i",input.value);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
// 加入公司
- (IBAction)InviteToJoinCompanyViewToJoinCompany:(id)sender {
    
    NSString *invateCode = [[[[[self.yaoqingma1.text stringByAppendingString:self.yaoqingma2.text] stringByAppendingString:self.yaoqingma3.text] stringByAppendingString:self.yaoqingma4.text] stringByAppendingString:self.yaoqingma5.text] stringByAppendingString:self.yaoqingma6.text];
    invateCode=_textFiled.text;
//    if (invateCode.length!=6) {
//        UIAlertView * alv=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"邀请码为6位,请重新输入！" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
//        [alv show];
//        return;
//    }

    invateCode=[NSString stringWithFormat:@"%ld",digitInput.value];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:JoinCompanyInv,[invateCode stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 加入公司成功
            
            // 设置为用户当前公司
            NSNumber * num=responseObject[@"data"][@"companyId"];
            [self InviteToJoinCompanyViewSetUserCurrentCompany:num];
        }else{
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
// 创建公司
- (IBAction)InviteToJoinCompanyViewCreatCompanyBtnClick:(id)sender {
    RegisterCreatCompanyViewController *creatCompanyVC = [[RegisterCreatCompanyViewController  alloc] init];
    [self.navigationController pushViewController:creatCompanyVC animated:YES];
}
// 设置为当前公司
- (void)InviteToJoinCompanyViewSetUserCurrentCompany:(NSNumber *)companyId
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [info setObject:companyId forKey:@"currentCompany"];
    NSString *str = [NSString stringWithFormat:setCurrentCompany,companyId];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            [info setObject:companyId forKey:@"currentCompany"];
            [ToolOfClass showMessage:NSLocalized(@"company_codeJoin_joinSuccess")];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
            
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIAlertView alertViewWithTitle:NSLocalized(@"company_codeJoin_joinFailure")];
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark - uitextfiledDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"%s : %@***",__func__,string);
    if (textField.text.length>5) {
        if(![string isEqualToString:@""]){
            [UIAlertView alertViewWithTitle:NSLocalized(@"company_codeJoin_codeCount")];
        }else{
            return YES;  //允许删除字符
        }
        return NO;
    }
    /*
//    double delayInSeconds = 0.3;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    
//    if (string.length==0) {
//        if (textField.tag==1) {
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [_yaoqingma1 becomeFirstResponder];
//            });
//        }else if (textField.tag==2) {
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [_yaoqingma1 becomeFirstResponder];
//            });
//        }else if (textField.tag==3) {
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [_yaoqingma2 becomeFirstResponder];
//            });
//        }else if (textField.tag==4) {
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [_yaoqingma3 becomeFirstResponder];
//            });
//        }else if (textField.tag==5) {
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [_yaoqingma4 becomeFirstResponder];
//            });
//        }else if (textField.tag==6) {
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [_yaoqingma5 becomeFirstResponder];
//            });
//        }
//        if (textField.text.length>=1) {
//            textField.text=@"";
//            return YES;
//        }
//    }
//    
//    
//    if (textField.tag==1) {
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [_yaoqingma2 becomeFirstResponder];
//        });
//    }else if (textField.tag==2) {
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [_yaoqingma3 becomeFirstResponder];
//        });
//    }else if (textField.tag==3) {
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [_yaoqingma4 becomeFirstResponder];
//        });
//    }else if (textField.tag==4) {
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [_yaoqingma5 becomeFirstResponder];
//        });
//    }else if (textField.tag==5) {
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [_yaoqingma6 becomeFirstResponder];
//        });
//    }else if (textField.tag==6) {
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [_yaoqingma6 becomeFirstResponder];
//        });
//    }
//    
//    if (textField.text.length>=1) {
//        textField.text=@"";
//        return YES;
//    }
    */
    return YES;
}
- (IBAction)skipButtonClick:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
