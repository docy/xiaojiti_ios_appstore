//
//  RegisterInviteToJoinCompanyViewController.h
//  xjt
//
//  Created by docy admin on 7/31/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 输入邀请码加入公司
@interface RegisterInviteToJoinCompanyViewController : UIViewController



@property (weak, nonatomic) IBOutlet UITextField *yaoqingma1;
@property (weak, nonatomic) IBOutlet UITextField *yaoqingma2;
@property (weak, nonatomic) IBOutlet UITextField *yaoqingma3;
@property (weak, nonatomic) IBOutlet UITextField *yaoqingma4;
@property (weak, nonatomic) IBOutlet UITextField *yaoqingma5;
@property (weak, nonatomic) IBOutlet UITextField *yaoqingma6;

@property (weak, nonatomic) IBOutlet UIView *hiddenView;


- (IBAction)InviteToJoinCompanyViewToJoinCompany:(id)sender;

- (IBAction)InviteToJoinCompanyViewCreatCompanyBtnClick:(id)sender;

@property (nonatomic, assign) BOOL isRegisterViewPush; // 注册页面push

@end
