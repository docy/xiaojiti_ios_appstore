//
//  SeconRegisterViewController.h
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalMessageModel.h"
// 注册（填写更多资料）视图控制器

@interface SeconRegisterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *personalIconImageView; // 个人头像
@property (weak, nonatomic) IBOutlet UIButton *manBtn;
@property (weak, nonatomic) IBOutlet UIButton *womanBtn;

- (IBAction)selectedPersonalIconBtnClick:(id)sender;
- (IBAction)onSeconRegisterRegisterButtonClick:(id)sender;
- (IBAction)onSelectPersonalSexBtnClick:(id)sender;

@property (nonatomic, retain) PersonalMessageModel *personalModel; // 注册时页面传值

@property (weak, nonatomic) IBOutlet UILabel *Localized_avator;
@property (weak, nonatomic) IBOutlet UILabel *Localized_sex;
@property (weak, nonatomic) IBOutlet UIButton *Localized_next;


@property (nonatomic, retain) NSString *avatar; // 头像

@end
