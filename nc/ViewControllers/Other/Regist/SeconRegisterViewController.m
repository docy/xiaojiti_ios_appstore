//
//  SeconRegisterViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SeconRegisterViewController.h"
#import "RegisterInviteToJoinCompanyViewController.h"

#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "ToolOfClass.h"

@interface SeconRegisterViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>

@property (nonatomic, copy) NSString *avartarName; // 用于记录选择自定义头像的图片名
@property (nonatomic, assign) NSInteger sex; // 用于记录性别（1男，0女）
@property (nonatomic, copy) NSString *avatarPath;
@end

@implementation SeconRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalized(@"regist_info_title");
    self.navigationItem.backBarButtonItem = nil;//[UIBarButtonItem itemWithTarget:self action:@selector(onSeconRegisterBackButtonClick)];
    self.navigationItem.leftBarButtonItem = nil;//[UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onSeconRegisterBackButtonClick)];
    [self.navigationItem setHidesBackButton:YES];
    
    
    self.Localized_avator.text = NSLocalized(@"regist_info_avator");
    self.Localized_sex.text = NSLocalized(@"regist_info_sex");
    [self.Localized_next setTitle:NSLocalized(@"regist_info_nextBtn") forState:UIControlStateNormal];
    
    self.sex = -1;
    [self.personalIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:self.avatar]] placeholderImage:nil];
//    [self getUpSeconRegisterViewRandomLogo];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

// 返回按钮的点击事件
- (void)onSeconRegisterBackButtonClick
{
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 获取注册前的图片
- (void)getUpSeconRegisterViewRandomLogo
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:getRandomLogoPath,@"user"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            self.avatarPath = responseObject[@"data"][@"logo"];
            [self.personalIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:responseObject[@"data"][@"logo"]]] placeholderImage:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark 按钮的点击事件
// 选择个人头像
- (IBAction)selectedPersonalIconBtnClick:(id)sender {
//    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
//    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    pickerVC.allowsEditing = YES;
//    pickerVC.delegate = self;
//    [self presentViewController:pickerVC animated:YES completion:nil];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
    [actionSheet showInView:self.view];
    
}
// 下一步按钮
- (IBAction)onSeconRegisterRegisterButtonClick:(id)sender {
    if (self.sex==-1) {
        [ToolOfClass showMessage:NSLocalized(@"regist_info_sex_seleted")];
        return ;
    }
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    parameter[@"sex"] = [NSNumber numberWithInteger:self.sex];

    [manager POST:UpdateUserInfo parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            //显示输入邀请码加入
            RegisterInviteToJoinCompanyViewController *inviteVC = [[RegisterInviteToJoinCompanyViewController alloc] init];
            inviteVC.isRegisterViewPush=YES;
            [self.navigationController pushViewController:inviteVC animated:YES];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

-(void)lodZhuci:(id)sender{
    if (self.sex==-1) {
        [ToolOfClass showMessage:NSLocalized(@"regist_info_sex_seleted")];
        return ;
    }
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    [info setObject:self.personalModel.name forKey:@"name"];
    [info setObject:self.personalModel.password forKey:@"password"];
    
    // 发送注册请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    
    parameter[@"name"] = self.personalModel.name;
    parameter[@"nickName"] = self.personalModel.name;
    parameter[@"email"] = self.personalModel.email;
    parameter[@"password"] = self.personalModel.password;
    parameter[@"phone"] = self.personalModel.phone;
    parameter[@"sex"] = @1; //默认1男
    NSData *imagedata;
    if (self.avartarName==nil) { // 上传默认图片
        self.avartarName = @"userAvatar.png";
        parameter[@"logo"] = self.avatarPath;
    }
    imagedata = UIImageJPEGRepresentation(self.personalIconImageView.image, ImageYaSuo);
    [manager POST:registerPath parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imagedata name:@"avatar" fileName:self.avartarName mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200])
        {
            [ToolOfClass showMessage:NSLocalized(@"regist_success")];
            // 注册成功(加入公司或创建公司)
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            //            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            // 发送登录请求
            AFHTTPRequestOperationManager *loginManager = [AFHTTPRequestOperationManager manager];
            loginManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"account"] = self.personalModel.name;
            parameters[@"password"] = self.personalModel.password;
            
            [loginManager POST:LoginPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] isEqualToNumber:@200]) {
                    NSDictionary *data = responseObject[@"data"];
                    // 存储token,id值
                    [info setObject:data[@"authToken"] forKey:@"authToken"];
                    [info setObject:data[@"id"] forKey:@"id"];
                    [info setObject:@"NO" forKey:@"isLogout"];
                    [info setObject:data[@"nickName"] forKey:@"nickName"];
                    [info setObject:data[@"avatar"] forKey:@"avatar"];
                    
                    if (![data[@"currentCompany"] isEqual:[NSNull null]]) {
                        [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                    }
                    
                    RegisterInviteToJoinCompanyViewController *inviteVC = [[RegisterInviteToJoinCompanyViewController alloc] init];
                    inviteVC.isRegisterViewPush=YES;
                    [self.navigationController pushViewController:inviteVC animated:YES];
                    
                } else {
//                    [ToolOfClass showMessage:responseObject[@"message"]];
                    [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            }];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"regist_failure")];
    }];
}

// 选择性别
- (IBAction)onSelectPersonalSexBtnClick:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (button.tag==99) {
        if (!button.selected) {
            button.selected = YES;
            self.womanBtn.selected = NO;
        }
        self.sex = 1;
    } else {
        if (!button.selected) {
            button.selected = YES;
            self.manBtn.selected = NO;
        }
        self.sex = 0;
    }
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    self.personalIconImageView.image = info[UIImagePickerControllerEditedImage];
//    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
//    NSString *fileName = [url lastPathComponent];
//    self.avartarName = [fileName copy];
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (picker.sourceType==UIImagePickerControllerSourceTypePhotoLibrary) {
        self.personalIconImageView.image = info[UIImagePickerControllerEditedImage];
        NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
        NSString *fileName = [url lastPathComponent];
        // 上传图片文件
        [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.personalIconImageView.image setAvatarOrLogoPath:setAvatarPath];
        //
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        
        self.personalIconImageView.image = info[UIImagePickerControllerOriginalImage];
        NSString *fileName = @"origin.jpg";
        [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.personalIconImageView.image setAvatarOrLogoPath:setAvatarPath];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex == 2) {
        return;
    }
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
    } else if (buttonIndex==1){
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
}

@end
