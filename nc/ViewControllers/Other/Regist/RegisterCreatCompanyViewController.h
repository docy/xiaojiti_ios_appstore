//
//  RegisterCreatCompanyViewController.h
//  xjt
//
//  Created by docy admin on 7/31/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterCreatCompanyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UITextField *companyNameTextField;
@property (weak, nonatomic) IBOutlet UITextView *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *zhiweiTextField;


- (IBAction)RegisterCreatCompanyViewSelectLogoBtnClick:(id)sender;

- (IBAction)RegisterCreatCompanyViewCreatCompanyBtnClick:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *descTV;


//@property (nonatomic, assign) BOOL isPersonalViewPush; // 标示从个人资料push

@end
