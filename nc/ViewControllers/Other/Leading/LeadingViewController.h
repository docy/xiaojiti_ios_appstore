//
//  LeadingViewController.h
//  nc
//
//  Created by docy admin on 6/6/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 引导页视图控制器

@interface LeadingViewController : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollerView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageView;


@end
