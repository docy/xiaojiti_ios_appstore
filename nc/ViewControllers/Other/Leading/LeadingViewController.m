//
//  LeadingViewController.m
//  nc
//
//  Created by docy admin on 6/6/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LeadingViewController.h"
#import "LoginViewController.h"
#import "Login_ViewController.h"
#import "XJTNavigationController.h"
#import "CompanyListViewController.h"
#import "UIButton+XJTButton.h"

#define kImageCount 1
@interface LeadingViewController ()

//@property (nonatomic, retain) NSMutableArray *imageData;

@end

@implementation LeadingViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageView.hidden = YES;
    
//    [self setUpScrollerView];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:isEnglish?@"Guide page-L":@"Guide page"]];
    imageView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    imageView.userInteractionEnabled = YES;
    [self.scrollerView addSubview:imageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onEnterButtonClick)];
    [imageView addGestureRecognizer:tap];
}

- (void)setUpScrollerView
{
//    self.scrollerView.delegate = self;

    self.scrollerView.contentSize = CGSizeMake(kImageCount*ScreenWidth, 0);
    NSArray *imageArr = @[@"Guide page",@"Guide page-L"];
    for (NSInteger i = 0; i < kImageCount; i++) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:isEnglish?imageArr[i+1]:imageArr[i]]];
        imageView.frame = CGRectMake(i*ScreenWidth, 0, ScreenWidth, ScreenHeight);
        [self.scrollerView addSubview:imageView];
        
        if (i == kImageCount - 1) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(0, 0, ScreenWidth*0.5, 60);
            button.frame = imageView.frame;
            [button addTarget:self action:@selector(onEnterButtonClick) forControlEvents:UIControlEventTouchUpInside];
//            button.layer.borderColor = [UIColor whiteColor].CGColor;
//            button.layer.borderWidth = 0.5;
            [self.scrollerView addSubview:button];
            imageView.userInteractionEnabled = YES;
        }

    }
    // 设置pageView
//    self.pageView.numberOfPages = kImageCount;
//    self.pageView.currentPage = 0;
}

- (void)onEnterButtonClick
{
    // 还应该判断是否是登录状态，是否是版本更新（即已安装，只是更新新版本）
    [UIApplication sharedApplication].keyWindow.rootViewController = [[XJTNavigationController alloc] initWithRootViewController:[[Login_ViewController alloc] init]];
}

// 减速结束时调用
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    NSInteger currentPage = self.scrollerView.contentOffset.x/ScreenWidth;
//    self.pageView.currentPage = currentPage;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
