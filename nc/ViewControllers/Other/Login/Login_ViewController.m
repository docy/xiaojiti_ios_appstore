//
//  Login_ViewController.m
//  nc
//
//  Created by guanxf on 16/1/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "Login_ViewController.h"
#import "RegisterViewController.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import <AFNetworking/AFNetworking.h>
//#import "UITabBarController+CustomTabBarController.h"
#import "XJTTabBarController.h"
#import "PersonalMessageModel.h"
#import "ToolOfClass.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import <Socket_IO_Client_Swift/Socket_IO_Client_Swift-Swift.h>
#import "ResetPasswordViewController.h"
#import "UIColor+Hex.h"
#import "SeconRegisterViewController.h"
#import "WXApi.h"

@interface Login_ViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *animview;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;

@property (weak, nonatomic) IBOutlet UIButton *Button1;
@property (weak, nonatomic) IBOutlet UIButton *Button2;


@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *ForgetPWButton;


@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *identifyTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *identifyBtn;
@property (weak, nonatomic) IBOutlet UILabel *daojishiLabel;
@property (weak, nonatomic) IBOutlet UIButton *resetPW_button;

@property (weak, nonatomic) IBOutlet UITextField *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordLabel;
@property (weak, nonatomic) IBOutlet UITextField *identifyCodeLabel;  // 验证码
@property (weak, nonatomic) IBOutlet UIButton *identifyCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *daojishiLabel2;

@property (weak, nonatomic) IBOutlet UIImageView *phoneImg2;
@property (weak, nonatomic) IBOutlet UIImageView *codeImg2;
@property (weak, nonatomic) IBOutlet UIImageView *userNameimg;
@property (weak, nonatomic) IBOutlet UIImageView *passWordimg2;
@property (weak, nonatomic) IBOutlet UIButton *zhuceButton;
@property (weak, nonatomic) IBOutlet UIButton *zhuceBackButton;

@property (weak, nonatomic) IBOutlet UIView *zhuceview;


@property (weak, nonatomic) IBOutlet UILabel *contryCode;
@property (weak, nonatomic) IBOutlet UIImageView *contryImg;
@property (weak, nonatomic) IBOutlet CountryPicker *countryPicker;
@property (weak, nonatomic) IBOutlet UIView *countryPicView;

@property (weak, nonatomic) IBOutlet UIButton *NSLocalized_wxBtn_1; // view1中的
@property (weak, nonatomic) IBOutlet UIButton *NSLocalized_wxBtn_2; // view2中的
@property (weak, nonatomic) IBOutlet UIButton *donebutton;



@end

@implementation Login_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 多语言支持
    // 主界面
    [self.Button1 setTitle:NSLocalized(@"login_new_regist") forState:UIControlStateNormal];
    [self.Button2 setTitle:NSLocalized(@"login_new_login") forState:UIControlStateNormal];
    // view1
    [self.nextButton setTitle:NSLocalized(@"login_new_view1_nextBtn") forState:UIControlStateNormal];
    [self.NSLocalized_wxBtn_1 setImage:[UIImage imageNamed:isEnglish?@"wechat":@"WeChat landing"] forState:UIControlStateNormal];
    self.phoneNumField.placeholder = NSLocalized(@"login_new_view1_phonePH");
    
    // view2
    [self.ForgetPWButton setTitle:NSLocalized(@"login_new_view2_foregetPass") forState:UIControlStateNormal];
    [self.NSLocalized_wxBtn_2 setImage:[UIImage imageNamed:isEnglish?@"wechat":@"WeChat landing"] forState:UIControlStateNormal];
    self.phoneTextField.placeholder = NSLocalized(@"login_new_view2_phonePH");
    self.passwordTextField.placeholder = NSLocalized(@"login_new_view2_passwordPH");
    
    // view3
    self.phoneTF.placeholder = NSLocalized(@"login_new_view3_phonePH");
    self.identifyTF.placeholder = NSLocalized(@"login_new_view3_codePH");
    self.passwordTF.placeholder = NSLocalized(@"login_new_view3_passwordPH");
    self.daojishiLabel.text = NSLocalized(@"login_new_view3_getCode");
    
    //countryPicview
    [self.donebutton setTitle:NSLocalized(@"HOME_alert_sure") forState:UIControlStateNormal];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    // Do any additional setup after loading the view from its nib.

    CGRect frame=CGRectMake(0, CGRectGetMaxY(_animview.frame)+10, ScreenWidth, CGRectGetHeight(_scrollview.frame)-0);
    self.scrollview.frame=frame;
    
    self.scrollview.contentSize=CGSizeMake(ScreenWidth*3, CGRectGetHeight(_scrollview.frame));

    UIColor * colorr=[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0];
    [_passwordTextField setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    [_phoneNumField setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    [_phoneTextField setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    //    [textField setValue:[UIFont boldSystemFontOfSize:16] forKeyPath:@"_placeholderLabel.font"];
    
    [_phoneTF setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    [_identifyTF setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    [_passwordTF setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    
//    UIColor * fcolor=[UIColor colorWithHex:0xE5E5E5 alpha:0.45f];
//    _resetPW_button.layer.borderWidth=0.5f;
//    _resetPW_button.layer.borderColor=[fcolor CGColor];
//    _loginButton.layer.borderWidth=0.5f;
//    _loginButton.layer.borderColor=[fcolor CGColor];
    
    CGRect frame2=CGRectMake(0, ScreenHeight+100, ScreenWidth, CGRectGetHeight(_scrollview.frame));
    self.zhuceview.frame=frame2;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardwillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    
    self.nameLabel.placeholder = NSLocalized(@"regist_namePH");
    self.phoneLabel.placeholder = NSLocalized(@"regist_phonePH");
    self.identifyCodeLabel.placeholder = NSLocalized(@"regist_codePH");
    self.passwordLabel.placeholder = NSLocalized(@"regist_passwordPH");
    self.daojishiLabel2.text = NSLocalized(@"regist_getCode");
    //    [self.identifyCodeBtn setTitle:NSLocalized(@"regist_getCode") forState:UIControlStateNormal];
//    [self.Localized_regist setTitle:NSLocalized(@"regist_registBtn") forState:UIControlStateNormal];
//    self.navigationItem.title = NSLocalized(@"regist_title");
    //    self.identifyCode = [NSString string];
    
    //    self.emailLabel.delegate = self;
    self.nameLabel.delegate = self;
    self.phoneLabel.delegate = self;
    self.identifyCodeLabel.delegate = self;
    self.passwordLabel.delegate = self;
//    _zhuceButton.layer.borderWidth=0.5f;
//    _zhuceButton.layer.borderColor=[[UIColor whiteColor] CGColor];
    [self.nameLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];//[UIColor colorWithHex:0x48C1A8]
    [self.phoneLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];//[UIColor colorWithHex:0x48C1A8]
    [self.identifyCodeLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordLabel setValue:colorr forKeyPath:@"_placeholderLabel.textColor"];
    
    _contryCode.text=@"+86";
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/CN"];
    _contryImg.image=[UIImage imageNamed:imagePath];
    
    if ([WXApi isWXAppInstalled]) {
        _NSLocalized_wxBtn_1.hidden=NO;
        _NSLocalized_wxBtn_2.hidden=NO;
    }else{
        _NSLocalized_wxBtn_1.hidden=YES;
        _NSLocalized_wxBtn_2.hidden=YES;
    }
    
}

// 返回按钮的点击事件
- (IBAction)onRegisterViewBackButtonClick:(id)sender
{
    _zhuceBackButton.hidden=YES;
    self.phoneLabel.text=[NSString stringWithFormat:@"(%@) %@",_contryCode.text,_phoneNumField.text];//_phoneNumField.text;//
    CGRect frame=_zhuceview.frame;
    CGRect frame2=_scrollview.frame;
    frame.origin.y=CGRectGetMinY(frame2);
    frame2.origin.y=CGRectGetMinY(_zhuceview.frame);
    _Button1.hidden=NO;
    _Button2.hidden=NO;
    _animview.hidden=NO;
    [UIView animateWithDuration:0.5 animations:^{
        _Button1.alpha=1.0f;
        _Button2.alpha=1.0f;
        _animview.alpha=1.0f;
        _zhuceview.frame=frame;
        _scrollview.frame=frame2;
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)identifyCodeViewChage{
    self.loginButton.enabled=YES;
    _zhuceBackButton.hidden=NO;
    self.phoneLabel.text=[NSString stringWithFormat:@"(%@) %@",_contryCode.text,_phoneNumField.text];//_phoneNumField.text;
    CGRect frame=_zhuceview.frame;
    CGRect frame2=_scrollview.frame;
    frame.origin.y=CGRectGetMinY(frame2);
    frame2.origin.y=CGRectGetMinY(_zhuceview.frame);
    [UIView animateWithDuration:0.5 animations:^{
        _Button1.alpha=0.0f;
        _Button2.alpha=0.0f;
        _animview.alpha=0.0f;
        _zhuceview.frame=frame;
        _scrollview.frame=frame2;
    } completion:^(BOOL finished) {
        _Button1.hidden=YES;
        _Button2.hidden=YES;
        _animview.hidden=YES;
    }];
}

-(CGFloat)keyboardEndingFrameHeight:(NSDictionary *)userInfo//计算键盘的高度
{
    CGRect keyboardEndingUncorrectedFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue];
    CGRect keyboardEndingFrame = [self.view convertRect:keyboardEndingUncorrectedFrame fromView:nil];
    return keyboardEndingFrame.size.height;
}

-(void)keyboardwillAppear:(NSNotification *)notification
{
    CGRect currentFrame = self.view.frame;
    if (currentFrame.origin.y!=0.00) {
        return;
    }
//    CGFloat change = [self keyboardEndingFrameHeight:[notification userInfo]];
    currentFrame.origin.y = -80 ;
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame = currentFrame;
    } completion:^(BOOL finished) {
        
    }];
    //弹出键盘时 国家view消失
    [self dismissCountryview:nil];
}

-(void)keyboardWillDisappear:(NSNotification *)notification
{
    CGRect currentFrame = self.view.frame;
//    CGFloat change = [self keyboardEndingFrameHeight:[notification userInfo]];
    currentFrame.origin.y = 0.0;//currentFrame.origin.y + change ;
    [UIView animateWithDuration:0.3f animations:^{
        self.view.frame = currentFrame;
    } completion:^(BOOL finished) {
        
    }];
    self.view.frame = currentFrame;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
    _scrollview.hidden=YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
}
-(void)viewDidAppear:(BOOL)animated{
    CGRect frame1=CGRectMake(0, 0, ScreenWidth, CGRectGetHeight(_scrollview.frame));
    CGRect frame2=CGRectMake(ScreenWidth, 0, ScreenWidth, CGRectGetHeight(_scrollview.frame));
    CGRect frame3=CGRectMake(ScreenWidth*2, 0, ScreenWidth, CGRectGetHeight(_scrollview.frame));
    _view1.frame=frame1;
    _view2.frame=frame2;
    _view3.frame=frame3;
    CGRect frame=_animview.frame;
    frame.origin.x=CGRectGetMinX(_Button2.frame);
    _animview.frame =frame;
    [_scrollview setContentOffset:CGPointMake(ScreenWidth,0) animated:NO];
    _scrollview.hidden=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//服务器端设置当前用户ID对应的百度Token
-(void)pushToken{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    if ([info objectForKey:@"deviceToken"]==nil) {
        return;
    }
    parameter[@"pushToken"]=[info objectForKey:@"deviceToken"];//[BPush getChannelId];
    parameter[@"os"]=@"ios";
    
    [manager POST:SetPushToken parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (![responseObject[@"code"] isEqualToNumber:@200]) {
            NSLog(@"error is %@",responseObject[@"error"]);
        }else if ([responseObject[@"code"] isEqualToNumber:@200]) {
            NSLog(@"Dic is %@",responseObject);
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error is %@",error);
    }];
}

// 注册新用户
- (IBAction)onButton1Click:(id)sender {
    CGRect frame=_animview.frame;
    frame.origin.x=CGRectGetMinX(_Button1.frame);
    [UIView animateWithDuration:0.3 animations:^{
        _animview.frame =frame;
        [_scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    } completion:^(BOOL finished) {
        
    }];
}
// 注册新用户
- (IBAction)onButton2Click:(id)sender {
    CGRect frame=_animview.frame;
    frame.origin.x=CGRectGetMinX(_Button2.frame);
    [UIView animateWithDuration:0.3 animations:^{
        _animview.frame =frame;
        [_scrollview setContentOffset:CGPointMake(ScreenWidth,0) animated:YES];
    } completion:^(BOOL finished) {
        
    }];
}

// 注册新用户 下一步
- (IBAction)onRegisterNewUserButtonClick:(id)sender {
    [self.phoneNumField resignFirstResponder];
    
    [self getIdentifyCodeBtnClick:sender];
    
//    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
//    registerVC.phoneString=_phoneNumField.text;
//    self.navigationController.navigationBarHidden=YES;
//    [self.navigationController pushViewController:registerVC animated:YES];
}

// 登录按钮点击事件
- (IBAction)LoginButtonClick:(id)sender {
    //    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    //    [SVProgressHUD showWithStatus:@"正在登录..."];
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
    if (self.phoneTextField.text.length==0||self.passwordTextField.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"login_new_scarcity")];
    } else {
        self.loginButton.enabled=NO;
        [self LoginViewLoginRequestWithAccount:self.phoneTextField.text passWord:self.passwordTextField.text];
    }
}

- (IBAction)onLoginResetPasswordBtnClick:(id)sender {
    //    self.loginButton.enabled=YES;pjnmn
    [_scrollview setContentOffset:CGPointMake(ScreenWidth*2,0) animated:YES];
//    ResetPasswordViewController *resetPass = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:[NSBundle mainBundle]];
//    [self.navigationController pushViewController:resetPass animated:YES];
    
}

- (void)LoginViewLoginRequestWithAccount:(NSString *)account passWord:(NSString *)password{
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = account;
    parameter[@"password"] = password;
    
    [manager POST:LoginPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200] ) {
            
            self.loginButton.enabled=YES;
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [info setObject:data[@"id"] forKey:@"id"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [info setObject:data[@"nickName"] forKey:@"nickName"];
            [info setObject:data[@"avatar"] forKey:@"avatar"];
            
            NSUserDefaults *info_group = [[NSUserDefaults alloc] initWithSuiteName:@"group.docy.co"];
            // 存储用户信息
            [info_group setObject:data[@"authToken"] forKey:@"authToken"];
            [info_group setObject:data[@"id"] forKey:@"id"];
            [info_group setObject:@"NO" forKey:@"isLogout"];
            [info_group setObject:data[@"nickName"] forKey:@"nickName"];
            [info_group setObject:data[@"avatar"] forKey:@"avatar"];
            
            NSString * str=data[@"currentCompany"];
            if (![str isEqual:[NSNull null]]) {
                [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                [info_group setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogout"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            }
            [info setObject:data[@"name"] forKey:@"name"];
            
            [info_group setObject:data[@"name"] forKey:@"name"];
            [info_group synchronize];
            
            // 发送socket连接通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newUserLogin" object:nil];
            
            [self pushToken];
            
        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.loginButton.enabled=YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Login error :%@",error);
        //        [ToolOfClass showMessage:@"登录失败"];
        self.loginButton.enabled=YES;
    }];
}

//登陆失败通过IP地址重新登陆
- (void)LoginViewLoginRequest_IP_WithAccount:(NSString *)account passWord:(NSString *)password{
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    [info setObject:account forKey:@"name"];
    [info setObject:password forKey:@"password"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = account;
    parameter[@"password"] = password;
    
    [manager POST:@"http://docy.datacanvas.io/v2/users/login" parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200] ) {
            
            self.loginButton.enabled=YES;
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [info setObject:data[@"id"] forKey:@"id"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [info setObject:data[@"nickName"] forKey:@"nickName"];
            [info setObject:data[@"avatar"] forKey:@"avatar"];
            
            NSUserDefaults *info_group = [[NSUserDefaults alloc] initWithSuiteName:@"group.share.co"];
            // 存储用户信息
            [info_group setObject:data[@"authToken"] forKey:@"authToken"];
            [info_group setObject:data[@"id"] forKey:@"id"];
            [info_group setObject:@"NO" forKey:@"isLogout"];
            [info_group setObject:data[@"nickName"] forKey:@"nickName"];
            [info_group setObject:data[@"avatar"] forKey:@"avatar"];
            
            
            NSString * str=data[@"currentCompany"];
            if (![str isEqual:[NSNull null]]) {
                [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                [info_group setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogout"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            }
            [info setObject:data[@"name"] forKey:@"name"];
            
            [info_group setObject:data[@"name"] forKey:@"name"];
            [info_group synchronize];
            
            // 发送socket连接通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newUserLogin" object:nil];
            
            [self pushToken];
            
        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.loginButton.enabled=YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Login error :%@",error);
        //        [ToolOfClass showMessage:@"登录失败"];
        self.loginButton.enabled=YES;
    }];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.phoneTextField resignFirstResponder];
    [self.phoneNumField resignFirstResponder];
    [self.phoneTF resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.identifyTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
    
    [self.phoneLabel resignFirstResponder];
    [self.nameLabel resignFirstResponder];
    [self.passwordLabel resignFirstResponder];
    [self.identifyCodeLabel resignFirstResponder];
    
//    if (_countryPicker.hidden==NO) {
//        _countryPicker.hidden=YES;
//    }
    
    CGRect frame=_countryPicView.frame;
    frame.origin.y=800;
    [UIView animateWithDuration:0.3 animations:^{
        _countryPicView.frame =frame;
    } completion:^(BOOL finished) {
        
    }];
    
}


- (IBAction)dismissCountryview:(id)sender {
    CGRect frame=_countryPicView.frame;
    frame.origin.y=800;
    [UIView animateWithDuration:0.3 animations:^{
        _countryPicView.frame =frame;
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)WXLogin {
    //此处调用授权的方法,你可以把下面的platformName 替换成 UMShareToSina,UMShareToTencent等
    
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        //          获取微博用户名、uid、token等
        if (response.responseCode == UMSResponseCodeSuccess) {
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToWechatSession];
//            NSLog(@"\n username is :%@,\nuid is :%@,\ntoken is :%@ \niconUrl is :%@",snsAccount.userName,snsAccount.openId,snsAccount.accessToken,snsAccount.iconURL);
            [self LoginViewLoginRequest_WEIXINWithOpenid:snsAccount.openId andName:snsAccount.userName andAvatar:snsAccount.iconURL andAccess_token:snsAccount.accessToken];
        }else if (response.responseCode == UMSResponseCodeCancel) {
            //用户取消操作
        }
        
    });
    
}

- (void)LoginViewLoginRequest_WEIXINWithOpenid:(NSString*)openid andName:(NSString *)name andAvatar:(NSString *)avatar andAccess_token:(NSString*)access_token{
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"openid"] = openid;
    parameter[@"name"] = name;
    parameter[@"avatar"] = avatar;
    parameter[@"access_token"] = access_token;
    [manager POST:WEIXIN_LoginPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200] ) {
            
            self.loginButton.enabled=YES;
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [info setObject:data[@"id"] forKey:@"id"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [info setObject:data[@"nickName"] forKey:@"nickName"];
            [info setObject:data[@"avatar"] forKey:@"avatar"];
            
            NSUserDefaults *info_group = [[NSUserDefaults alloc] initWithSuiteName:@"group.share.co"];
            // 存储用户信息
            [info_group setObject:data[@"authToken"] forKey:@"authToken"];
            [info_group setObject:data[@"id"] forKey:@"id"];
            [info_group setObject:@"NO" forKey:@"isLogout"];
            [info_group setObject:data[@"nickName"] forKey:@"nickName"];
            [info_group setObject:data[@"avatar"] forKey:@"avatar"];
            
            NSString * str=data[@"currentCompany"];
            if (![str isEqual:[NSNull null]]) {
                [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                [info_group setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogout"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isWeiXinLogin"];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            }
            [info setObject:data[@"name"] forKey:@"name"];
            
            [info_group setObject:data[@"name"] forKey:@"name"];
            [info_group synchronize];
            
            
            // 发送socket连接通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            BOOL changed=data[@"info"][@"changed"];
            if ([data[@"origin"]isEqualToString:@"weixin"]&&!changed) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"WXLoginSuccessful" object:nil]; //提示用户注册小集体 用户名密码
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newUserLogin" object:nil];
            
            [self pushToken];
            
        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.loginButton.enabled=YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Login error :%@",error);
        [ToolOfClass showMessage:NSLocalized(@"login_new_failureWX")];
        self.loginButton.enabled=YES;
    }];
}

#pragma mark - scrollviewDelegate
//-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
//    if (scrollView.contentOffset.x==ScreenWidth) {
//        [self onButton2Click:nil];
//    }else{
//        [self onButton1Click:nil];
//    }
//}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x==ScreenWidth) {
        [self onButton2Click:nil];
    }else if (scrollView.contentOffset.x==ScreenWidth*2) {
//        [self onButton2Click:nil];
    }else{
        [self onButton1Click:nil];
    }
}

#pragma mark - textFiledDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag==111) {
//        _nextButton.hidden=NO;
    }else if(textField.tag==101&&_phoneTextField.text.length!=0){
        _loginButton.hidden=NO;
        _ForgetPWButton.hidden=YES;
    }
//    NSLog(@"textField.tag :%ld",textField.tag);
    if (textField.tag==1011) {
        _phoneImg.image=[UIImage imageNamed:@"icon-phone"];
    }else if (textField.tag==1012) {
        _codeImg.image=[UIImage imageNamed:@"icon-code"];
    }else if (textField.tag==1013) {
        _passWordimg.image=[UIImage imageNamed:@"icon-password"];
    }
    
    if (textField.tag==1111) {
        _phoneImg2.image=[UIImage imageNamed:@"icon-phone"];
    }else if (textField.tag==1112) {
        _codeImg2.image=[UIImage imageNamed:@"icon-code"];
    }else if (textField.tag==1113) {
        _userNameimg.image=[UIImage imageNamed:@"icon-user name"];
    }else if (textField.tag==1114) {
        _passWordimg2.image=[UIImage imageNamed:@"icon-password"];
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag==111&&textField.text.length==0) {
//        _nextButton.hidden=YES;
    }else if(textField.tag==101&&(_phoneTextField.text.length==0||_passwordTextField.text.length==0)){
        _loginButton.hidden=YES;
        _ForgetPWButton.hidden=NO;
    }

    if (textField.tag==1011) {
        _phoneImg.image=[UIImage imageNamed:@"icon-phone gray"];
    }else if (textField.tag==1012) {
        _codeImg.image=[UIImage imageNamed:@"icon-code gray"];
    }else if (textField.tag==1013) {
        _passWordimg.image=[UIImage imageNamed:@"icon-password gray"];
    }
    
    if (textField.tag==1111) {
        _phoneImg2.image=[UIImage imageNamed:@"icon-phone gray"];
    }else if (textField.tag==1112) {
        _codeImg2.image=[UIImage imageNamed:@"icon-code gray"];
    }else if (textField.tag==1113) {
        _userNameimg.image=[UIImage imageNamed:@"icon-user name gray"];
    }else if (textField.tag==1114) {
        _passWordimg2.image=[UIImage imageNamed:@"icon-password gray"];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag==111) {
        BOOL isMobNum=[ToolOfClass isMobileNumber:textField.text];
        if ((textField.text.length==9)||textField.text.length==10||textField.text.length==11||textField.text.length==12) {
            if ((textField.text.length==12&&![string isEqualToString:@""])||(textField.text.length==11&&![string isEqualToString:@""])||(textField.text.length==10&&[string isEqualToString:@""])||(textField.text.length==9&&[string isEqualToString:@""])) {
                _nextButton.hidden=YES;
            }else{
                _nextButton.hidden=NO;
            }
        }else{
            _nextButton.hidden=YES;
        }
    }
    return YES;
}

- (IBAction)onGetUpidentifyBtnClick:(id)sender {
    
    if (self.phoneTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"login_new_input_phone")];
    } else {
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:NSLocalized(@"login_new_sending")];
        _identifyBtn.enabled=NO;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"phone"] = self.phoneTF.text;
        parameter[@"countryCode"] = self.contryCode.text;
        [manager POST:ResetPasswordSms parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"code"] intValue] == 200) {
                NSLog(@"***************邀请码为:%@",responseObject[@"data"][@"code"]);
                //                [self startTimeSecondsCountDown];
                [ToolOfClass toolStartTimeSecondsCountDown_uilabel:self.daojishiLabel];
                double delayInSeconds = 120.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    _identifyBtn.enabled=YES;
                });
                //                [ToolOfClass toolStartTimeSecondsCountDown:self.identifyCodeBtn];
            } else {
                //                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                _identifyBtn.enabled=YES;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            _identifyBtn.enabled=YES;
        }];
    }
}

- (IBAction)onResetPassTiJiaoBtnClick:(id)sender {
    
    if (self.identifyTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"login_new_input_code")];
    } else if (self.passwordTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"login_new_input_passW")];
    } else {
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:NSLocalized(@"login_new_sending")];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        
        parameter[@"phone"] = self.phoneTF.text;
        parameter[@"code"] = self.identifyTF.text;
        parameter[@"password"] = self.passwordTF.text;
        
        [manager POST:ResetPassword parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"code"] intValue] == 200) {
                [ToolOfClass showMessage:NSLocalized(@"login_new_reset_success")];
                [info setObject:self.passwordTF.text forKey:@"password"];
                
                [self onButton2Click:nil];
                //修改成功自动登录
                _phoneTextField.text=_phoneTF.text;
                _passwordTextField.text=_passwordTF.text;
                
                [self LoginButtonClick:nil];
                
//                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
//              [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (IBAction)onSetUpSecureTextEntryBtnClicl:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    self.passwordTF.secureTextEntry = !self.passwordTF.secureTextEntry;
}

// 获取验证码
- (IBAction)getIdentifyCodeBtnClick:(id)sender {
    
    if (self.phoneNumField.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"regist_phone_unBlank")];
    } else {
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:NSLocalized(@"info_forgetPW_sendRequest")];
        _identifyCodeBtn.enabled=NO;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"phone"] = self.phoneNumField.text;
        parameter[@"countryCode"] = self.contryCode.text;
        [manager POST:getidentifyCodePath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"code"] intValue] == 200) {
                [ToolOfClass toolStartTimeSecondsCountDown_uilabel:self.daojishiLabel2];
                NSLog(@"***************邀请码为:%@",responseObject[@"data"][@"code"]);
                if (!_YZMstr) {
                    _YZMstr=[[NSString alloc] init];
                }
                _YZMstr=responseObject[@"data"][@"code"];
                //弹出注册信息填写view
                [self identifyCodeViewChage];
                
                _identifyCodeBtn.enabled=NO;
                double delayInSeconds = 120.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    _identifyCodeBtn.enabled=YES;
                });
                //                [ToolOfClass toolStartTimeSecondsCountDown:self.identifyCodeBtn];
            } else {
                //                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                _identifyCodeBtn.enabled=YES;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}


// 注册点击事件
- (IBAction)onNextButtonClick:(id)sender {
    
    if (self.nameLabel.text.length==0||self.phoneLabel.text.length == 0||self.passwordLabel.text.length==0)
    {
        [ToolOfClass showMessage:NSLocalized(@"regist_unBlank")];
    } else if (self.identifyCodeLabel.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"regist_code_unBlank")];
    } else {// 确保用户名，电话，邮箱已经输入信息
        // 检测输入信息是否正确
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"name"] = self.nameLabel.text;
        //        parameter[@"email"] = self.emailLabel.text;
        parameter[@"phone"] = _phoneNumField.text;
        parameter[@"code"] = self.identifyCodeLabel.text;
        parameter[@"password"] = self.passwordLabel.text;
        parameter[@"lang"] = isEnglish?@"EN":@"CN";
        
        
        [manager POST:registerPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@200]) { // 输入注册信息可用
                PersonalMessageModel *model = [[PersonalMessageModel alloc] init];
                model.name = self.nameLabel.text;
                //                model.email = self.emailLabel.text;
                model.phone = _phoneNumField.text;
                model.password = self.passwordLabel.text;

                AFHTTPRequestOperationManager *loginManager = [AFHTTPRequestOperationManager manager];
                loginManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
                NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                parameters[@"account"] = model.name;
                parameters[@"password"] = model.password;
                
                [loginManager POST:LoginPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    if ([responseObject[@"code"] isEqualToNumber:@200]) {
                        NSDictionary *data = responseObject[@"data"];
                        // 存储token,id值
                        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
                        [info setObject:data[@"authToken"] forKey:@"authToken"];
                        [info setObject:data[@"id"] forKey:@"id"];
                        [info setObject:@"NO" forKey:@"isLogout"];
                        [info setObject:data[@"nickName"] forKey:@"nickName"];
                        [info setObject:data[@"avatar"] forKey:@"avatar"];
                        
                        NSUserDefaults *info_group = [[NSUserDefaults alloc] initWithSuiteName:@"group.share.co"];
                        // 存储用户信息
                        [info_group setObject:data[@"authToken"] forKey:@"authToken"];
                        [info_group setObject:data[@"id"] forKey:@"id"];
                        [info_group setObject:@"NO" forKey:@"isLogout"];
                        [info_group setObject:data[@"nickName"] forKey:@"nickName"];
                        [info_group setObject:data[@"avatar"] forKey:@"avatar"];
                        
                        if (![data[@"currentCompany"] isEqual:[NSNull null]]) {
                            [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                            [info_group setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                        }
                        
                        [info_group synchronize];
                        
                        SeconRegisterViewController * srvc=[[SeconRegisterViewController alloc] initWithNibName:@"SeconRegisterViewController" bundle:nil];
                        srvc.avatar = responseObject[@"data"][@"avatar"];
                        [self.navigationController pushViewController:srvc animated:YES];
                        [self.navigationController setNavigationBarHidden:NO];
                        //显示输入邀请码加入
                        //                    RegisterInviteToJoinCompanyViewController *inviteVC = [[RegisterInviteToJoinCompanyViewController alloc] init];
                        //                    inviteVC.isRegisterViewPush=YES;
                        //                    [self.navigationController pushViewController:inviteVC animated:YES];
                        
                    } else {
                        //                    [ToolOfClass showMessage:responseObject[@"message"]];
                        [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                }];
                
//                // 跳转至个人资料界面
//                NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
//                [info setObject:self.nameLabel.text forKey:@"name"];
//                [self onSeconRegisterRegister];
                
            } else { // 输入信息错误
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (void)onSeconRegisterRegister {
    PersonalMessageModel *model = [[PersonalMessageModel alloc] init];
    model.name = self.nameLabel.text;
    //    model.email = self.emailLabel.text;
    model.phone =_phoneNumField.text;
    model.password = self.passwordLabel.text;
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    [info setObject:model.name forKey:@"name"];
    [info setObject:model.password forKey:@"password"];
    
    // 发送注册请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    
    parameter[@"name"] = model.name;
    parameter[@"nickName"] = model.name;
    parameter[@"email"] = model.email;
    parameter[@"password"] = model.password;
    parameter[@"phone"] = model.phone;
    parameter[@"sex"] = @1;
    NSData *imagedata;
    //    if (self.avartarName==nil) { // 上传默认图片
    //        self.avartarName = @"userAvatar.png";
    //        parameter[@"logo"] = self.avatarPath;
    //    }
    //    imagedata = UIImageJPEGRepresentation(self.personalIconImageView.image, ImageYaSuo);
    [manager POST:registerPath parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        //        [formData appendPartWithFileData:imagedata name:@"avatar" fileName:self.avartarName mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@200])
        {
            
            [ToolOfClass showMessage:NSLocalized(@"regist_success")];
            // 注册成功(加入公司或创建公司)
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            //            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            // 发送登录请求
            AFHTTPRequestOperationManager *loginManager = [AFHTTPRequestOperationManager manager];
            loginManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"account"] = model.name;
            parameters[@"password"] = model.password;
            
            [loginManager POST:LoginPath parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] isEqualToNumber:@200]) {
                    NSDictionary *data = responseObject[@"data"];
                    // 存储token,id值
                    [info setObject:data[@"authToken"] forKey:@"authToken"];
                    [info setObject:data[@"id"] forKey:@"id"];
                    [info setObject:@"NO" forKey:@"isLogout"];
                    [info setObject:data[@"nickName"] forKey:@"nickName"];
                    [info setObject:data[@"avatar"] forKey:@"avatar"];
                    
                    NSUserDefaults *info_group = [[NSUserDefaults alloc] initWithSuiteName:@"group.share.co"];
                    // 存储用户信息
                    [info_group setObject:data[@"authToken"] forKey:@"authToken"];
                    [info_group setObject:data[@"id"] forKey:@"id"];
                    [info_group setObject:@"NO" forKey:@"isLogout"];
                    [info_group setObject:data[@"nickName"] forKey:@"nickName"];
                    [info_group setObject:data[@"avatar"] forKey:@"avatar"];
                    
                    if (![data[@"currentCompany"] isEqual:[NSNull null]]) {
                        [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                        [info_group setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                    }
                    
                    [info_group synchronize];
                    
                    SeconRegisterViewController * srvc=[[SeconRegisterViewController alloc] initWithNibName:@"SeconRegisterViewController" bundle:nil];
                    srvc.avatar = responseObject[@"data"][@"avatar"];
                    [self.navigationController pushViewController:srvc animated:YES];
                    [self.navigationController setNavigationBarHidden:NO];
                    //显示输入邀请码加入
                    //                    RegisterInviteToJoinCompanyViewController *inviteVC = [[RegisterInviteToJoinCompanyViewController alloc] init];
                    //                    inviteVC.isRegisterViewPush=YES;
                    //                    [self.navigationController pushViewController:inviteVC animated:YES];
                    
                } else {
                    //                    [ToolOfClass showMessage:responseObject[@"message"]];
                    [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            }];
        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"regist_failure")];
    }];
}

- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    _contryCode.text=code;
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@",name];
    _contryImg.image=[UIImage imageNamed:imagePath];
}

- (IBAction)showContryView:(id)sender {
    
    //先关闭键盘
    [self touchesBegan:nil withEvent:nil];
    
    CGRect frame=_countryPicView.frame;
    frame.origin.y=ScreenHeight-frame.size.height;//CGRectGetMinY(_scrollview.frame)-52;//
    [UIView animateWithDuration:0.3 animations:^{
        _countryPicView.frame =frame;
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)zidongTianYZMClick:(id)sender {
    _identifyCodeLabel.text=_YZMstr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
