//
//  LoginViewController.h
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMSocial.h"
// 登录视图控制器

@interface LoginViewController : UIViewController <UITextFieldDelegate> 
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;



- (IBAction)onRegisterNewUserButtonClick:(id)sender;
- (IBAction)LoginButtonClick:(id)sender;
- (IBAction)onLoginResetPasswordBtnClick:(id)sender;

@end
