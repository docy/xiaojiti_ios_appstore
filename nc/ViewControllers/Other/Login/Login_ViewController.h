//
//  Login_ViewController.h
//  nc
//
//  Created by guanxf on 16/1/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMSocial.h"
#import "CountryPicker.h"

@interface Login_ViewController : UIViewController<UITextFieldDelegate,CountryPickerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UITextField *phoneNumField;

@property (strong, nonatomic)  NSString * YZMstr; //储存验证码 暗黑

- (IBAction)onRegisterNewUserButtonClick:(id)sender;
- (IBAction)LoginButtonClick:(id)sender;
- (IBAction)onLoginResetPasswordBtnClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *phoneImg;
@property (weak, nonatomic) IBOutlet UIImageView *codeImg;
@property (weak, nonatomic) IBOutlet UIImageView *passWordimg;

@end
