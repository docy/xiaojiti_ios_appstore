//
//  LoginViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import <AFNetworking/AFNetworking.h>
//#import "UITabBarController+CustomTabBarController.h"
#import "XJTTabBarController.h"
#import "PersonalMessageModel.h"
#import "ToolOfClass.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import <Socket_IO_Client_Swift/Socket_IO_Client_Swift-Swift.h>
#import "ResetPasswordViewController.h"

@interface LoginViewController ()


@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetBtn;
@property (weak, nonatomic) IBOutlet UIButton *login_WXBtn;


@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.phoneTextField.placeholder = NSLocalized(@"login_accountPH");
    self.passwordTextField.placeholder = NSLocalized(@"login_passwordPH");
    [self.loginButton setTitle:NSLocalized(@"login_loginBtn") forState:UIControlStateNormal];
    [self.registBtn setTitle:NSLocalized(@"login_regist") forState:UIControlStateNormal];
    [self.forgetBtn setTitle:NSLocalized(@"login_forgetPW") forState:UIControlStateNormal];
    [self.login_WXBtn setTitle:NSLocalized(@"login_login_WXBtn") forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}
//服务器端设置当前用户ID对应的百度Token
-(void)pushToken{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    if ([info objectForKey:@"deviceToken"]==nil) {
        return;
    }
    parameter[@"pushToken"]=[info objectForKey:@"deviceToken"];//[BPush getChannelId];
    parameter[@"os"]=@"ios";

    [manager POST:SetPushToken parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (![responseObject[@"code"] isEqualToNumber:@200]) {
            NSLog(@"error is %@",responseObject[@"error"]);
        }else if ([responseObject[@"code"] isEqualToNumber:@200]) {
            NSLog(@"Dic is %@",responseObject);
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error is %@",error);
    }];
}

// 注册新用户
- (IBAction)onRegisterNewUserButtonClick:(id)sender {
    self.loginButton.enabled=YES;
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

// 登录按钮点击事件
- (IBAction)LoginButtonClick:(id)sender {
//    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
//    [SVProgressHUD showWithStatus:@"正在登录..."];
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
    if (self.phoneTextField.text.length==0||self.passwordTextField.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"login_account_scarcity")];
        
    } else {
        self.loginButton.enabled=NO;
        [self LoginViewLoginRequestWithAccount:self.phoneTextField.text passWord:self.passwordTextField.text];
    }
}

- (IBAction)onLoginResetPasswordBtnClick:(id)sender {
//    self.loginButton.enabled=YES;pjnmn
    ResetPasswordViewController *resetPass = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:[NSBundle mainBundle]];

    [self.navigationController pushViewController:resetPass animated:YES];
    
}

- (void)LoginViewLoginRequestWithAccount:(NSString *)account passWord:(NSString *)password{
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    [info setObject:account forKey:@"name"];
    [info setObject:password forKey:@"password"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = account;
    parameter[@"password"] = password;
    
    [manager POST:LoginPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200] ) {

            self.loginButton.enabled=YES;
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [info setObject:data[@"id"] forKey:@"id"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [info setObject:data[@"nickName"] forKey:@"nickName"];
            [info setObject:data[@"avatar"] forKey:@"avatar"];
            NSString * str=data[@"currentCompany"];
            if (![str isEqual:[NSNull null]]) {
                [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogout"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            }
			[info setObject:data[@"name"] forKey:@"name"];
            // 发送socket连接通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WXLoginSuccessful" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newUserLogin" object:nil];
            
            [self pushToken];
            
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.loginButton.enabled=YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Login error :%@",error);
//        [ToolOfClass showMessage:@"登录失败"];
        self.loginButton.enabled=YES;
    }];
}

//登陆失败通过IP地址重新登陆
- (void)LoginViewLoginRequest_IP_WithAccount:(NSString *)account passWord:(NSString *)password{
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    [info setObject:account forKey:@"name"];
    [info setObject:password forKey:@"password"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = account;
    parameter[@"password"] = password;
    
    [manager POST:@"http://docy.datacanvas.io/v2/users/login" parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200] ) {
            
            self.loginButton.enabled=YES;
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [info setObject:data[@"id"] forKey:@"id"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [info setObject:data[@"nickName"] forKey:@"nickName"];
            [info setObject:data[@"avatar"] forKey:@"avatar"];
            NSString * str=data[@"currentCompany"];
            if (![str isEqual:[NSNull null]]) {
                [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogout"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            }
            [info setObject:data[@"name"] forKey:@"name"];
            // 发送socket连接通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newUserLogin" object:nil];
            
            [self pushToken];
            
        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.loginButton.enabled=YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Login error :%@",error);
        //        [ToolOfClass showMessage:@"登录失败"];
        self.loginButton.enabled=YES;
    }];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.phoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (IBAction)WXLogin {
    //此处调用授权的方法,你可以把下面的platformName 替换成 UMShareToSina,UMShareToTencent等

    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        //          获取微博用户名、uid、token等
        if (response.responseCode == UMSResponseCodeSuccess) {
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToWechatSession];
//            NSLog(@"\n username is :%@,\nuid is :%@,\ntoken is :%@ \niconUrl is :%@",snsAccount.userName,snsAccount.openId,snsAccount.accessToken,snsAccount.iconURL);
            [self LoginViewLoginRequest_WEIXINWithOpenid:snsAccount.openId andName:snsAccount.userName andAvatar:snsAccount.iconURL andAccess_token:snsAccount.accessToken];
        }else if (response.responseCode == UMSResponseCodeCancel) {
            //用户取消操作
        }
        
    });
    
}

- (void)LoginViewLoginRequest_WEIXINWithOpenid:(NSString*)openid andName:(NSString *)name andAvatar:(NSString *)avatar andAccess_token:(NSString*)access_token{
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"openid"] = openid;
    parameter[@"name"] = name;
    parameter[@"avatar"] = avatar;
    parameter[@"access_token"] = access_token;
    [manager POST:WEIXIN_LoginPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200] ) {
            
            self.loginButton.enabled=YES;
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [info setObject:data[@"id"] forKey:@"id"];
            [info setObject:@"NO" forKey:@"isLogout"];
            [info setObject:data[@"nickName"] forKey:@"nickName"];
            [info setObject:data[@"avatar"] forKey:@"avatar"];
            NSString * str=data[@"currentCompany"];
            if (![str isEqual:[NSNull null]]) {
                [info setObject:data[@"currentCompany"] forKey:@"currentCompany"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isLogout"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isHaveCurrent"];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isWeiXinLogin"];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
            }
            [info setObject:data[@"name"] forKey:@"name"];
            // 发送socket连接通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccessful" object:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newUserLogin" object:nil];
            
            [self pushToken];
            
        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.loginButton.enabled=YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Login error :%@",error);
        [ToolOfClass showMessage:NSLocalized(@"login_failureWX")];
        
        self.loginButton.enabled=YES;
    }];
}


@end
