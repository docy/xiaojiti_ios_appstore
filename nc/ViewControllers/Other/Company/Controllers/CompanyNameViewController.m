//
//  CompanyNameViewController.m
//  nc
//
//  Created by docy admin on 6/28/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CompanyNameViewController.h"
#import "CompanyModel.h"
#import "CanJoinCompanyTVCell.h"
#import "UIImageView+WebCache.h"
#import "AFHTTPRequestOperationManager.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "companyInfoViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIColor+Hex.h"
#import "CompanyListTVCell.h"

@interface CompanyNameViewController () <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UISearchControllerDelegate,UISearchBarDelegate,UISearchResultsUpdating>
{
    NSMutableArray *_searchCompanyList; // 搜索公司结果数组
    NSMutableArray *_canJoinCompanyList; // 网络数据公司列表
    int _companyId; // 公司ID
}

@property (nonatomic, strong) UISearchController *searchController; // 实现搜索
@property (strong, nonatomic) IBOutlet UIView *headView;

@end


@implementation CompanyNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置基本属性
    [self setUpCompanyNameViewData];
    
    [self getCanJoinCompanyList];
//    self.tableView.tableHeaderView=self.headView;
}

// 设置基本属性
- (void)setUpCompanyNameViewData
{
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.rightBarButtonItem = nil;//[[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(onVotifyButtonClick)];                //确定
    self.navigationItem.leftBarButtonItem =[UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onCompanyNameViewLeftItemClick)];
    self.navigationItem.backBarButtonItem=nil;
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.0f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    [self.companyNameTextFiled addTarget:self action:@selector(textFieldTextDidChanged:) forControlEvents:UIControlEventEditingChanged];
    self.companyNameTextFiled.delegate = self;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    [self.searchController.searchBar sizeToFit];
    self.navigationItem.titleView = self.searchController.searchBar;

    
    
    _canJoinCompanyList = [NSMutableArray array];
    _searchCompanyList = [NSMutableArray array];
    _companyId = -1;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)onCompanyNameViewLeftItemClick{
    if (self.searchController.active) {
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getCanJoinCompanyList
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    
    [manager GET:canJoinCompanyList parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
            [_canJoinCompanyList removeAllObjects];
            NSArray *data = responseObject[@"data"];
            for (NSDictionary *dict in data) {
                CompanyModel *model = [[CompanyModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [_canJoinCompanyList addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

// 实时监听搜索框的变化
- (void)textFieldTextDidChanged:(UITextField *)textField
{
    [_searchCompanyList removeAllObjects];
    for (CompanyModel *model in _canJoinCompanyList) {
        NSRange range = [model.name rangeOfString:self.companyNameTextFiled.text];
        if (range.location != NSNotFound) {
            [_searchCompanyList addObject:model];
        }
    }
    [self.tableView reloadData];
}

// 确定按钮的点击事件（加入公司，并设为当前公司）
//- (void)onVotifyButtonClick
//{
//    if (self.companyNameTextFiled.text.length!=0) {
//      
//        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
//
//        if (_companyId==-1) { // 创建公司
//            parameter[@"name"] = self.companyNameTextFiled.text;
//            parameter[@"description"] = @"科技集体";
//            parameter[@"accessToken"] = [info objectForKey:@"authToken"];
//            
//            [manager POST:creatCompany parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                if ([responseObject[@"code"] isEqualToNumber:@0]) {// 创建公司成功
//                    NSDictionary *data = responseObject[@"data"];
//                    _companyId = [data[@"id"] intValue];
//                   
//                    // 加入公司
//                    [self joinInCompany];
//                } else { // 创建公司失败
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"集体已经创建，请更改集体名称" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                    [alert show];
//                }
//            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"创建集体失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                [alert show];
//            }];
//        } else { // 选择已经存在的公司加入
//            // 加入公司
//            [self joinInCompany];
//        }
//    } else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"请输入有效集体名称" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}

// 加入公司
- (void)joinInCompany
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSString *path = [addCompany stringByAppendingFormat:@"%d",_companyId];
    
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 加入公司成功
            [self.navigationController popToRootViewControllerAnimated:YES];
            // 设置为用户当前公司
            [self setUserCurrentCompany];
            //设置成功后直接跳转到 新加公司
            // 消除搜索控制器
            if (self.searchController.active) {
                [self.searchController dismissViewControllerAnimated:YES completion:nil];
                [self.searchController.view removeFromSuperview];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

// 设置当前公司
- (void)setUserCurrentCompany
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    
    NSString *str = [NSString stringWithFormat:setCurrentCompany,[NSNumber numberWithInt:_companyId]];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            [info setObject:[NSNumber numberWithInt:_companyId] forKey:@"currentCompany"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"company_list_search_joinFailure") message:nil delegate:nil cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
        [alert show];
    }];
}

//- (void)delayMothed
//{
//    [self.navigationController popToRootViewControllerAnimated:NO];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.companyNameTextFiled.text.length==0) {
        if (self.searchController.active) {
            return _searchCompanyList.count;
        }
        return _canJoinCompanyList.count;
    }
    return _searchCompanyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *cellId = @"cellId";
//    CanJoinCompanyTVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
//    if (!cell) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:@"CanJoinCompanyTVCell" owner:self options:nil] lastObject];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    CompanyModel *model =nil;//_canJoinCompanyList[indexPath.row];
//    
//    if (self.companyNameTextFiled.text.length==0) {
//        model = _canJoinCompanyList[indexPath.row];
//        if (self.searchController.active) {
//            model = _searchCompanyList[indexPath.row];
//        }
//    } else {
//        model = _searchCompanyList[indexPath.row];
//    }
//    
//    cell.companyName.text = model.name;
//    if (model.logoUrlOrigin!=nil) {
//        [cell.companyImg sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logoUrlOrigin]] placeholderImage:nil];
//    }
//    
//    NSString * creatorName=[NSString stringWithFormat:@"创建者：%@  人数：%d",model.creatorName,[model.userCount intValue]];
//    cell.creatorNameLabel.text=creatorName;
//    cell.addCompanyBut.tag=indexPath.row+1000;
//    [cell.addCompanyBut addTarget:self action:@selector(addCompanyButClick:) forControlEvents:UIControlEventTouchUpInside];
//    
//    if (model.desc==nil||[model.desc isEqualToString:@""]) {
//        cell.descriptionLabel.text=@"梦想是什么， 梦想就是一种能让你感到坚持就是幸福的东西。";
//    }else{
//        cell.descriptionLabel.text=model.desc;
//    }
//    
//    return cell;
    
    
    static NSString *cellId = @"cellId";
    CompanyListTVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CompanyListTVCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
//    CompanyModel *model = self.searchController.active?_searchCompanyList[indexPath.row]:_canJoinCompanyList[indexPath.row];
    CompanyModel *model = self.searchController.active?_searchCompanyList[indexPath.row]:_canJoinCompanyList[indexPath.row];
    if (model.name==nil) {  //如果数据为空显示空cell   若没有公司 显示两个空cell
        return cell;
    }
    
//    cell.NumberL.hidden=NO;
//    cell.NumberL.text=[NSString stringWithFormat:@"No.%ld",indexPath.row+1];
    cell.joinBtn.hidden = NO;
    NSString * strr=[NSString stringWithFormat:@"%@",model.topicCount==nil?@"0":model.topicCount]; //防止前期创建的集体没有topicCount参数
    cell.creatorLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",strr] rangeString:strr rangColor:CustomColor(255, 17, 46)];//NSLocalized(@"company_list_subTopicCount"),
    cell.userCountLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.userCount] rangeString:[NSString stringWithFormat:@"%@",model.userCount] rangColor:[UIColor colorWithHex:0x4A4A4A]];//CustomColor(76, 182, 72)];//NSLocalized(@"company_list_userCount"),
    [cell.MinImage setImage:[UIImage imageNamed:@"topic_icon"]];
    cell.addressLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.city==NULL?NSLocalized(@"company_list_city_default"):model.city] rangeString:[NSString stringWithFormat:@"%@",model.city] rangColor:CustomColor(74, 144, 226)];//,NSLocalized(@"company_list_city")
    //        cell.userCountLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"地点  %@",model.city] rangeString:[NSString stringWithFormat:@"%@",model.city] rangColor:CustomColor(74, 144, 226)];
    
    cell.companyName.text = model.name;
    [cell.joinBtn setTitle:NSLocalized(@"company_list_search_joinBtn") forState:UIControlStateNormal];
//    cell.creatorLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.userCount] rangeString:[NSString stringWithFormat:@"%@",model.userCount] rangColor:CustomColor(76, 182, 72)];//,NSLocalized(@"company_list_search_userCount")
//
//    cell.userCountLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",model.city==nil?NSLocalized(@"company_list_city_default"):model.city] rangeString:[NSString stringWithFormat:@"%@",model.city] rangColor:CustomColor(74, 144, 226)];//NSLocalized(@"company_list_search_city"),
//
//    NSString * strr=[NSString stringWithFormat:@"%@",model.topicCount==nil?@"0":model.topicCount]; //防止前期创建的集体没有topicCount参数
//    cell.addressLabel.attributedText = [cell stringWithString:[NSString stringWithFormat:@"%@",strr] rangeString:strr rangColor:CustomColor(255, 17, 46)];
    //NSLocalized(@"company_list_search_subTopicCount"),
    cell.joinBtn.tag = indexPath.row+1000;
    [cell.joinBtn addTarget:self action:@selector(addCompanyButClick:) forControlEvents:UIControlEventTouchUpInside];

    if (model.desc==nil) {
        cell.descriptionLabel.text=NSLocalized(@"company_list_search_des_default");
    } else {
        cell.descriptionLabel.text=model.desc;
    }
    if (model.logoUrlOrigin!=nil) {
        [cell.companyImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logoUrlOrigin]] placeholderImage:nil];
    }
    return cell;
}

-(void)addCompanyButClick:(UIButton*)button{
    CompanyModel *model=nil;
    if (self.companyNameTextFiled.text.length==0) {
        model = _canJoinCompanyList[button.tag-1000];
        if (self.searchController.active) {
            model = _searchCompanyList[button.tag-1000];
        }
    } else {
        model = _searchCompanyList[button.tag-1000];
    }
    _companyId=[model.id intValue];
    
    AppDelegate *deleagte = APP_DELEGATE;
    
    deleagte.tempCopModel = model;
    [self joinInCompany];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CompanyModel *model=nil;
    if (self.searchController.active) {
        model = _searchCompanyList[indexPath.row];
    } else {
        model = _canJoinCompanyList[indexPath.row];
    }
    companyInfoViewController * comvc=[[companyInfoViewController alloc] initWithNibName:@"companyInfoViewController" bundle:nil];
    comvc.model=model;
    if (self.searchController.active) {
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
        [self.searchController.view removeFromSuperview];
    }
    [self.navigationController pushViewController:comvc animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (iPhone6Plus) {
        return  450;
    }
    if (iPhone6) {
        return  390;
    }
    //    return 568.0/2;
    return 330;//105;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return _searchCompanyList.count>0?[NSString stringWithFormat:NSLocalized(@"company_list_search_count"),_searchCompanyList.count]:nil;
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self.companyNameTextFiled resignFirstResponder];
//}

/**
 *  显示输入邀请码加入公司界面
 *
 *  @param sender
 */
//- (IBAction)pushYQMvc:(id)sender {
////    RegisterInviteToJoinCompanyViewController *joinCompanyVC = [[RegisterInviteToJoinCompanyViewController alloc] init];
////    [self.navigationController pushViewController:joinCompanyVC animated:YES];
////    
//}


#pragma mark UISearchControllerDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    
    [_searchCompanyList removeAllObjects];
    for (CompanyModel *model in _canJoinCompanyList) {
        NSRange range = [model.name rangeOfString:searchString];
        if (range.location != NSNotFound) {
            [_searchCompanyList addObject:model];
        }
    }
    [self.tableView reloadData];
}

@end
