//
//  companyInfoViewController.m
//  
//
//  Created by guanxf on 15/10/26.
//
//

#import "companyInfoViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIImageView+WebCache.h"
#import "AFHTTPRequestOperationManager.h"
#import "AllGroupViewController.h"
#import "NSString+XJTString.h"

@interface companyInfoViewController ()
@property (weak, nonatomic) IBOutlet UITextField *companyNameL;
@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *usersCount;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollerView;
@property (weak, nonatomic) IBOutlet UILabel *citylabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupCnLabel;



@property (weak, nonatomic) IBOutlet UILabel *Localized_name;
@property (weak, nonatomic) IBOutlet UILabel *Localized_des;
@property (weak, nonatomic) IBOutlet UILabel *Localized_member;
@property (weak, nonatomic) IBOutlet UILabel *Localized_master;
@property (weak, nonatomic) IBOutlet UILabel *Localized_city;
@property (weak, nonatomic) IBOutlet UILabel *Localized_category;
@property (weak, nonatomic) IBOutlet UILabel *Localized_allgroup;
@property (weak, nonatomic) IBOutlet UIButton *Localized_joinBtn;

@end

@implementation companyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.Localized_name.text = NSLocalized(@"company_set_name");
    self.Localized_des.text = NSLocalized(@"company_set_des");
    self.Localized_member.text = NSLocalized(@"company_set_member");
    self.Localized_master.text = NSLocalized(@"company_set_master");
    self.Localized_city.text = NSLocalized(@"company_set_city");
    self.Localized_category.text = NSLocalized(@"company_set_category");
    self.Localized_allgroup.text = NSLocalized(@"company_set_allGroup");
    [self.Localized_joinBtn setTitle:NSLocalized(@"company_codeJoin_joinBtn") forState:UIControlStateNormal];
    [self setUpGroupConveyGroupViewData];
    self.scrollerView.contentSize = CGSizeMake(0, ScreenHeight);
    _companyNameL.text=_model.name;
    _descriptionText.text=_model.desc==nil?NSLocalized(@"company_set_des_no"):_model.desc;
    _descriptionText.font=[UIFont systemFontOfSize:15];
    _descriptionText.textAlignment = NSTextAlignmentRight;
    _descriptionText.textColor =CustomColor(153, 153, 153);
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:_model.logoUrlOrigin]] placeholderImage:nil];
    
    _usersCount.text=[NSString stringWithFormat:@"%@%@",_model.userCount,NSLocalized(@"company_set_member_Count")];
    _creatorLabel.text=_model.creatorNick;
    _citylabel.text = _model.city;
    _categoryLabel.text = [NSString stringGetCatoryWithNumber:_model.category.integerValue];
    NSString * strr=[NSString stringWithFormat:@"%@",_model.groupCount==nil?@"0":_model.groupCount];
    _groupCnLabel.text = [NSString stringWithFormat:@"%@%@",strr,NSLocalized(@"company_set_allGroup_count")];
}

- (void)setUpGroupConveyGroupViewData{
    
    self.navigationItem.title = NSLocalized(@"company_set_title_info");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpGroupConveyGroupViewBack)];
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
}

-(void)setUpGroupConveyGroupViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)joinCompanyButtonclick:(id)sender {
    [self joinInCompany];
}


// 加入公司
- (void)joinInCompany
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSString *path = [addCompany stringByAppendingFormat:@"%d",[_model.id intValue]];
    
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 加入公司成功
            // 设置为用户当前公司
            [self setUserCurrentCompany];
            //设置成功后直接跳转到 新加公司
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

// 设置当前公司
- (void)setUserCurrentCompany
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    
    NSString *str = [NSString stringWithFormat:setCurrentCompany,[NSNumber numberWithInt:[_model.id intValue]]];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            [info setObject:[NSNumber numberWithInt:[_model.id intValue]] forKey:@"currentCompany"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"company_set_add_failure") message:nil delegate:nil cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
        [alert show];
    }];
}
#pragma mark 所有小组
- (IBAction)showAllGroupsButClick:(id)sender {
    AllGroupViewController * allgvc=[[AllGroupViewController alloc] initWithNibName:@"AllGroupViewController" bundle:nil];
    allgvc.groupId=_model.id;
    [self.navigationController pushViewController:allgvc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
