//
//  CompanyNameViewController.h
//  nc
//
//  Created by docy admin on 6/28/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyNameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *companyNameTextFiled;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
