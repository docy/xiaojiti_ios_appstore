//
//  CanJoinCompanyTVCell.h
//  
//
//  Created by guanxf on 15/10/21.
//
//

#import <UIKit/UIKit.h>

@interface CanJoinCompanyTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UIImageView *companyImg;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *addCompanyBut;
@property (weak, nonatomic) IBOutlet UILabel *creatorNameLabel;

@end
