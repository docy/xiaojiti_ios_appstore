//
//  CompanyModel.h
//  nc
//
//  Created by docy admin on 6/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"

@interface CompanyModel : CommonModel

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSNumber *id;

@property (nonatomic, copy) NSNumber *category;         //分类
@property (nonatomic, copy) NSString *city;             //城市
@property (nonatomic, copy) NSNumber *creator;          //公司创建者id
@property (nonatomic, copy) NSString *creatorName;      //公司创建者
@property (nonatomic, copy) NSString *creatorNick;      //公司创建者昵称
@property (nonatomic, copy) NSString *logoUrlOrigin;    //公司背景图片
@property (nonatomic, copy) NSNumber *userCount;        //公司现有人数
@property (nonatomic, copy) NSNumber *unread;           //未读消息数

@property (nonatomic, copy) NSNumber *groupCount;       //小组个数

@property (nonatomic, copy) NSNumber *isPrivate;        //私密
@property (nonatomic, copy) NSNumber *topicCount;       //子话题个数

@property (nonatomic, assign) BOOL isCurrentCpy;        //标记是否是当前公司

@end
