//
//  MessageNotifyViewController.m
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "MessageNotifyViewController.h"

#import "MessageDetailViewController.h"
#import "MessageViewController.h"

#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "MessageNotifyModel.h"
#import "MessageNotifyCell.h"
#import "MJRefresh.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "SubTopicModel.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+XJTImageView.h"
#import "SubTopicViewCell.h"
#import "OpenNetWorkViewController.h"

@interface MessageNotifyViewController (){
    NetWorkBadView *badView;
}

@property(nonatomic, assign) int page;
@property(nonatomic, retain) UIView * bgImage;
@end

@implementation MessageNotifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteTopicSuccessfully:) name:@"deleteTopicSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpMessageNotifyViewNewFavorite) name:@"newFavorite" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteTopicSuccessfully:) name:@"newUnFavorite" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMessageNotifyViewNewMessage:) name:@"newMessage" object:nil];
    // 切换公司通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpMessageNotifyViewNewFavorite) name:@"SwitchCurrentCompany" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MessageNotifyViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StartingFromTheNotification:) name:@"StartingFromTheNotification" object:nil];
    
    self.tableView =[[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-49-20)];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    [self.view addSubview:_tableView];
    
    // 初始化属性
    [self setUpMessageNotifyViewAttribute];
    [self setUpMessageNotifyViewMJRefresh];
    [self getUpMessageNotifyViewNewFavorite];
    
    
    //断网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    //联网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
    badView.contentLabel.text = NSLocalized(@"NetWorkBadView_des");
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    badView.frame=CGRectMake(0, 0, ScreenWidth, 44);
    badView.hidden=YES;
    [self.view addSubview:badView];
}

- (void)LeftViewRefreshAgainInBadNetWork{
    //    NSLog(@"%s",object_getClassName(self));
    badView.hidden=NO;
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)LeftViewInGoodNetWork{
    badView.hidden=YES;
}

-(void)StartingFromTheNotification:(NSNotification*)notifi{
    [self.tabBarController setSelectedIndex:0];
}

- (void)MessageNotifyViewInGoodNetWork{
    if (self.favoriteData.count==0) {
        [self getUpMessageNotifyViewNewFavorite];
    }
}

// 初始化属性
- (void)setUpMessageNotifyViewAttribute{
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(MessageNotifyViewInGoodNetWork)];
    [self.navigationItem setHidesBackButton:YES];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.title = NSLocalized(@"attention_list_title");
    self.favoriteData = [NSMutableArray array];
    
}

#pragma mark 通知回调

- (void)deleteTopicSuccessfully:(NSNotification *)not{
    for (NSInteger i = 0; i < self.favoriteData.count; i++) {
        SubTopicModel *model = self.favoriteData[i];
        if ([not.object isEqualToNumber:model.id]) {
            [self.favoriteData removeObject:model];
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }  
    }
    if (self.favoriteData.count == 0) {
//        [self.view addSubview:self.bgImage];
        
        self.tableView.backgroundView = self.bgImage;
    }
}

- (void)getMessageNotifyViewNewMessage:(NSNotification *)notification{
    NSArray *array = notification.object;
    NSDictionary * dict=array[0];
    if ([dict[@"type"] isEqualToNumber:@5]) { // 踢人message
        if ([dict[@"userId"] isEqualToNumber:USER_ID]) {
            for (int i=0; i<self.favoriteData.count; i++) {
                SubTopicModel *model = self.favoriteData[i];
                if ([model.groupId isEqualToNumber:dict[@"groupId"]]) {
                    
                    [self.favoriteData removeObject:model];
                    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:i inSection:0];
                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                }
            }
        }
        return;
    }
    
    if ([dict[@"type"] intValue] == 8) {
        for (int i = 0;i < self.favoriteData.count;i++) {
            SubTopicModel *mode=self.favoriteData[i];
            if (mode.id.intValue ==[(NSNumber*)dict[@"info"][@"topicId"] intValue]) { // 刷新message
                NSMutableDictionary *com = [NSMutableDictionary dictionary];
                com[@"createdAt"] = dict[@"updatedAt"];
                com[@"creatorAvatar"] = dict[@"info"][@"creatorAvatar"];
                com[@"creatorName"] = dict[@"info"][@"creatorName"];
                if (dict[@"info"][@"message"]==nil) {
                    com[@"message"] = @"";
                } else {
                     com[@"type"] = @1;
                     com[@"message"] = dict[@"info"][@"message"];
                }
               
                com[@"nickName"] = dict[@"nickName"];
                mode.comments = @[com];
                [self.favoriteData replaceObjectAtIndex:i withObject:mode];
                
                NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第二行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
                
                if (i!=0) {
                    [self.tableView moveRowAtIndexPath:te toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    [self.favoriteData insertObject:mode atIndex:0];
                    [self.favoriteData removeObjectAtIndex:i+1];
                }
                
                break;
            }
        }
    
    }
}

- (void)getUpMessageNotifyViewNewFavorite{
    self.page = 1;
    [self.favoriteData removeAllObjects];

    [self getUpMessageNotifyViewFavoriteData];
}

- (void)setUpMessageNotifyViewMJRefresh{
//    [self.favoriteData removeAllObjects];
    __block MessageNotifyViewController *weakSelf = self;
    [self.tableView addFooterWithCallback:^{
        [weakSelf getUpMessageNotifyViewFavoriteData];
    }];
    [self.tableView addHeaderWithCallback:^{
        weakSelf.page = 1;
        [weakSelf getUpMessageNotifyViewFavoriteData];
    }];
}

- (id)bgImage{
    if (_bgImage == nil) {
        _bgImage = [[[NSBundle mainBundle] loadNibNamed:isEnglish?@"NullBgView_en":@"NullBgView" owner:self options:nil] lastObject];//[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Focus on"]];
//        _bgImage.contentMode=UIViewContentModeScaleAspectFill;
    }
    
    return _bgImage;
}

- (void)getUpMessageNotifyViewFavoriteData{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];

    [manager GET:[NSString stringWithFormat:getFavorites,self.page] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"code:%@",responseObject);
        if ([responseObject[@"code"] intValue] == 200) {
            if (self.page==1) {
                [self.favoriteData removeAllObjects];
            }
            for (NSDictionary *dict in responseObject[@"data"]) {
                SubTopicModel *model = [[SubTopicModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.favoriteData addObject:model];
            }
            
            if (self.favoriteData.count == 0) {
//                [self.view addSubview:self.bgImage];
                self.tableView.backgroundView = self.bgImage;
            } else{
//                [self.bgImage removeFromSuperview];
                self.tableView.backgroundView = nil;
                self.bgImage = nil;
            }
            
            [self.tableView headerEndRefreshing];
            [self.tableView footerEndRefreshing];
            [self.tableView reloadData];
            
            self.page++;
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self.tableView headerEndRefreshing];
        [self.tableView footerEndRefreshing];
        
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.favoriteData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellId";
    SubTopicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SubTopicViewCell" owner:self options:nil] lastObject];
    }
    SubTopicModel *model = self.favoriteData[indexPath.row];
    
    cell.titleLabel.text = model.title;
    
    if ([model.type intValue] == 2) { // 图片子话题
        [cell.iconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
    } else if ([model.type intValue] == 3){
        cell.iconImageView.image = [UIImage imageNamed:@"list_map"];
    } else if ([model.type intValue] == 4){
        cell.iconImageView.image = [UIImage imageNamed:@"list_vote"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"list_topic"];
    }
    
    cell.viewsLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"attention_list_click"),[model.views intValue]];
    cell.favoriteCountLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"attention_list_guanzhu"),[model.favoriteCount intValue]];
    cell.commentCountLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"attention_list_huiFu"),[model.commentCount intValue]];
    
    if (model.comments.count!=0) {
        NSDictionary *comment = model.comments[0];
        
        NSString *text = nil;
        if ([comment[@"type"] isEqualToNumber:@1]) {
            text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        } else {
            text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],NSLocalized(@"attention_list_sendImage")];
        }
        //        text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        NSMutableAttributedString *attributsStr = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange range = [text rangeOfString:[NSString stringWithFormat:@"%@：",comment[@"nickName"]]];
        [attributsStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:range];
        
        //        [attributsStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1] range:range];
        cell.descLabel.attributedText = attributsStr;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:comment[@"createdAt"]];
    } else {
        
        cell.descLabel.text = NSLocalized(@"attention_list_noHuifu");//nil;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.updatedAt];
        
    }
    
//    if ([model.favorited isEqualToNumber:@1]) {
        [cell.FocusImage setImage:[UIImage imageNamed:@"Focus on"]];
//    }else{
//        [cell.FocusImage setImage:[UIImage imageNamed:@"Focus on_normal"]];
//    }
    cell.FocusButton.tag=indexPath.row;
    [cell.FocusButton addTarget:self action:@selector(FocusButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)FocusButtonClick:(UIButton*)button{
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:button.tag inSection:0];
    [self removeFavoriteDataWithIndexPath:indexp];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

#pragma mark 设置编辑的样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark 设置处理编辑情况
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SubTopicModel *model = self.favoriteData[indexPath.row];
    MessageDetailViewController *detailVC = [[MessageDetailViewController alloc] init];
    detailVC.topicId = model.id;
    detailVC.topicType = model.type;
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

// 取消收藏功能
-(void)removeFavoriteDataWithIndexPath:(NSIndexPath *) indexPath{
    self.view.userInteractionEnabled = NO;
    SubTopicModel *model = self.favoriteData[indexPath.row];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObjects:@[[ToolOfClass authToken],@"true"] forKeys:@[@"accessToken",@"unfavorite"]];
    [manager POST:[NSString stringWithFormat:addFavorite,model.id] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] intValue] == 200) {
            
            self.view.userInteractionEnabled = YES;
            [self.favoriteData removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            //                [self.tableView reloadData];
            if (self.favoriteData.count == 0) {
                //                    [self.view addSubview:self.bgImage];
                self.tableView.backgroundView = self.bgImage;
            }
            [self.tableView reloadData];
        } else {
//            [ToolOfClass showMessage:@"取消收藏失败"];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.view.userInteractionEnabled = YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"attention_list_att_failure")];
        self.view.userInteractionEnabled = YES;
    }];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    SubTopicModel *model = self.favoriteData[indexPath.row];
    
    UITableViewRowAction *disCollectAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalized(@"attention_list_unGuanZhu") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self removeFavoriteDataWithIndexPath:indexPath];
    }];
    disCollectAction.backgroundColor = [UIColor colorWithRed:197/255.0 green:196/255.0 blue:201/255.0 alpha:1];
    
    UITableViewRowAction *interAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalized(@"attention_list_enterGroup") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        MessageViewController *messageVC = [[MessageViewController alloc] init];
        messageVC.groupId = model.groupId;
        messageVC.groupName = model.groupName;
        messageVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:messageVC animated:YES];
        
    }];
    interAction.backgroundColor = [UIColor colorWithRed:255/255.0 green:85/255.0 blue:0/255.0 alpha:1];
    
    return @[disCollectAction,interAction];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
