//
//  MessageNotifyViewController.h
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 新消息通知视图控制器
@interface MessageNotifyViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * tableView;

@property (nonatomic, strong) NSMutableArray *favoriteData;

@end
