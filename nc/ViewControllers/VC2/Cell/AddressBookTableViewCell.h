//
//  AddressBookTableViewCell.h
//  nc
//
//  Created by docy admin on 6/24/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 通讯录cell

@interface AddressBookTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *IconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;


@property (weak, nonatomic) IBOutlet UILabel *phoneOrEmailLabel;

@property (weak, nonatomic) IBOutlet UILabel *xianTiaoLabel;

@end
