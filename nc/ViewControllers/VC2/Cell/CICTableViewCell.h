//
//  CICTableViewCell.h
//  xjt
//
//  Created by guanxf on 15/8/17.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CICTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *heardImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *pushCellButton;

@end
