//
//  AddBookViewController.h
//  xjt
//
//  Created by guanxf on 15/8/18.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBookViewController : UIViewController

@property (nonatomic, assign) BOOL isTellPhone;
@property (nonatomic, copy) NSString *navigationItemTitle;


@end
