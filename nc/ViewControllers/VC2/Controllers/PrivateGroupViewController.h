//
//  PrivateGroupViewController.h
//  
//
//  Created by guanxf on 16/3/10.
//
//

#import <UIKit/UIKit.h>

@interface PrivateGroupViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (nonatomic, strong) NSMutableArray *groupData;
@property (nonatomic, strong) NSMutableArray *PrivateGroupData; // 私聊群组

//@property (nonatomic,retain) NSMutableArray *groupDaList; // 可添加群组数组

@end
