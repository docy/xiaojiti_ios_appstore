//
//  AddBookViewController.m
//  xjt
//
//  Created by guanxf on 15/8/18.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "AddBookViewController.h"
#import <AddressBook/AddressBook.h> // 访问通讯录需要的头文件
#import <AddressBookUI/AddressBookUI.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "AddressBookTableViewCell.h"
#import "PeopleModel.h"
#import "UIButton+XJTButton.h"
#import "XJTSearchBar.h"
#import "CompanyInviteCodeViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+AlertView.h"

#import "ToolOfClass.h"

#define rowHeight 56
#define SearchBarHeight 40

@interface AddBookViewController ()< UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSMutableArray *_sectionIndexTitles; // 存储索引
    NSString* xuanZhongNum;   //选中好友的手机号
    NSString* friendName;   //选中好友名字
}

@property (nonatomic ,retain) NSMutableArray *peopleData; // 用来存储联系人(分组)

@end

@implementation AddBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    xuanZhongNum=[[NSString alloc]init];
    friendName=[[NSString alloc]init];
    self.peopleData = [NSMutableArray array];
    self.navigationItem.title = @"手机通讯录";
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(AddressBookTableViewBackBtnClick)];
    // 获取手机通讯录
    [self getAddressBookTableViewAddressBook];
    
}

// 导航栏返回按钮
- (void)AddressBookTableViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
// 获取手机通讯录
- (void)getAddressBookTableViewAddressBook
{
    // 清空数据
    [self.peopleData removeAllObjects];
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, error);
    // 解决授权问题
    
    if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted==YES) {
                [self getUpAddBookViewPersonDataWithAddBook:addressBook];
            }
        });
    } else {
        if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusAuthorized) {
            [self getUpAddBookViewPersonDataWithAddBook:addressBook];
        } else {
            [ToolOfClass showMessage:@"未授权通讯录!"];
        }
    }
    
}
// 获取所有联系人并存储
- (void)getUpAddBookViewPersonDataWithAddBook:(ABAddressBookRef)addressBook
{
    // 获取所有联系人并存储
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex peolpeCount = ABAddressBookGetPersonCount(addressBook);
    
    NSMutableArray *peopleArray = [NSMutableArray array]; // 用来存储联系人
    
    // 获取联系人信息
    for (NSInteger i = 0; i < peolpeCount; i++)
    {
        PeopleModel *model = [[PeopleModel alloc] init];
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        CFStringRef firstName = ABRecordCopyValue(person, kABPersonFirstNameProperty);
        CFStringRef lastName = ABRecordCopyValue(person, kABPersonLastNameProperty);
        CFStringRef fullName = ABRecordCopyCompositeName(person);
        
        // 读取name
        NSString *firstNameStr = (__bridge NSString *)firstName;
        NSString *lastNameStr = (__bridge NSString *)lastName;
        NSString *fullNameStr = (__bridge NSString *)fullName;
        
        if ( fullNameStr.length != 0) {
            firstNameStr = fullNameStr;
        } else if (lastNameStr.length!=0) {
            firstNameStr = [NSString stringWithFormat:@"%@ %@",firstNameStr,lastNameStr];
        }
        model.name = firstNameStr;
        model.rowSelected = NO; // cell默认不被选中
        model.isShowAccessoryView = NO; // cell的accessoryView默认不显示
        // 读取邮箱
        ABMultiValueRef email = ABRecordCopyValue(person, kABPersonEmailProperty);
        NSInteger emailCount = ABMultiValueGetCount(email);
        for (NSInteger i = 0; i < emailCount; i++) {
            model.email = (__bridge NSString *)ABMultiValueCopyValueAtIndex(email, i);
        }
        
        // 读取电话
        ABMultiValueRef tellPhone = ABRecordCopyValue(person, kABPersonPhoneProperty);
        for (NSInteger i = 0; i < ABMultiValueGetCount(tellPhone); i++) {
            model.tellPhone = (__bridge NSString *)ABMultiValueCopyValueAtIndex(tellPhone, i);
        }
        // 读取联系人头像
        NSData *iconData = (__bridge NSData *)ABPersonCopyImageData(person);
        model.iconImage = [UIImage imageWithData:iconData];
        
        [peopleArray addObject:model];
        if (firstName) {
            CFRelease(firstName);
        }
        if (lastName) {
            CFRelease(lastName);
        }
        if (fullName) {
            CFRelease(fullName);
        }
    }
//    CFRelease(allPeople);
//    CFRelease(addressBook);
    
    // Sort data
    UILocalizedIndexedCollation *theCollation = [UILocalizedIndexedCollation currentCollation];
    for (PeopleModel *model in peopleArray) {
        NSInteger sect = [theCollation sectionForObject:model
                                collationStringSelector:@selector(name)];
        model.sectionNumber = sect;
    }
    
    NSInteger highSection = [[theCollation sectionTitles] count];
    NSMutableArray *sectionArrays = [NSMutableArray arrayWithCapacity:highSection];
    
    for (int i=0; i<=highSection; i++) {
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        [sectionArrays addObject:sectionArray];
    }
    
    for (PeopleModel *model in peopleArray) {
        [(NSMutableArray *)[sectionArrays objectAtIndex:model.sectionNumber] addObject:model];
    }
    
    // 排好序的联系人加入数组中
    for (NSMutableArray *sectionArray in sectionArrays) {
        int count = 0; // 统计name为nil的个数
        for (PeopleModel *model in sectionArray) {
            if (model.name.length==0) {
                count++;
            }
        }
        NSArray *sortedSection = [NSArray array];
        //        NSArray *sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(name)];
        if (count >= 2) { // 有2个以上name为nil,
            sortedSection = [NSArray arrayWithArray:sectionArray];
        } else {
            sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(name)];
        }
        [self.peopleData addObject:sortedSection];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.peopleData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.peopleData[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellId";
    AddressBookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AddressBookTableViewCell" owner:self options:nil] lastObject];
    }
    PeopleModel *model = self.peopleData[indexPath.section][indexPath.row];
    cell.nickNameLabel.hidden = YES;
    if (model.iconImage) {
        cell.IconImageView.image = model.iconImage;
    } else {
        cell.IconImageView.image = [UIImage imageNamed:@"default avatar"];
    }
    cell.nickNameLabel.hidden = YES;
    cell.nameLabel.text = model.name;
    cell.nameLabel.font = [UIFont fontWithName:@"Heiti SC" size:15.0];
    
    CGPoint center = cell.nameLabel.center;
    center.y = cell.center.y;
    cell.nameLabel.center = center;
    
    
    cell.phoneOrEmailLabel.text = [self deleteString:model.tellPhone];
    if (indexPath.row==[self.peopleData[indexPath.section] count]-1) {
        cell.xianTiaoLabel.hidden = YES;
    } else {
        cell.xianTiaoLabel.hidden = NO;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    NSString *title = [self.peopleData[section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section]:nil;
    return title;
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
////    return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:_sectionIndexTitles];
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PeopleModel *model =self.peopleData[indexPath.section][indexPath.row];
    xuanZhongNum= [self deleteString:model.tellPhone];
    friendName=model.name;
    UIAlertView * alertV=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"即将邀请'%@'加入，是否要为其产生邀请码？",model.name] message:@"提示：邀请码时效为24小时" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
    [alertV show];
}

//去掉手机号里的“-”
-(NSString*)deleteString:(NSString*)str{
    if (str==nil) {
        return nil;
    }
    NSRange range=[str rangeOfString:@"-"];
    NSMutableString * mstring=[NSMutableString stringWithString:str];
    if (range.length!=0) {
        [mstring deleteCharactersInRange:range];
        NSRange range2=[mstring rangeOfString:@"-"];
        if (range2.length!=0) {
            [mstring deleteCharactersInRange:range2];
        }
    }
    
    return mstring;
}


#pragma mark --UIAlertViewDegeate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        
    }else{
        [self getyaoqingma];
    }
}

//get邀请码
-(void)getyaoqingma{
    __block NSString * inviteCode;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSLog(@"xuanZhongNum -------is :%d",[[info objectForKey:@"currentCompany"] intValue]);
    parameter[@"target"]=xuanZhongNum;//@"15652342769";
    NSString * URLstr=[NSString stringWithFormat:CompanyInviteCode,[[info objectForKey:@"currentCompany"] intValue]];
    
    [manager POST:URLstr parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            inviteCode =[responseObject[@"data"] objectForKey:@"code"];
            
            CompanyInviteCodeViewController *phoneAB = [[CompanyInviteCodeViewController alloc] init];
            phoneAB.hidesBottomBarWhenPushed = YES;
            phoneAB.navigationItem.title = @"手机通讯录";
            phoneAB.code=inviteCode;
            phoneAB.Fname=friendName;
            phoneAB.FriendPhoneNum=xuanZhongNum;
            [self.navigationController pushViewController:phoneAB animated:YES];
            
        } else {
//            [UIAlertView alertViewWithTitle:responseObject[@"error"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

//转码输出
- (NSString *)replaceUnicode:(NSString *)unicodeStr {
    NSString *tempStr1 = [unicodeStr stringByReplacingOccurrencesOfString:@"\\u" withString:@"\\U"];
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 = [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString* returnStr = [NSPropertyListSerialization propertyListFromData:tempData
                                                           mutabilityOption:NSPropertyListImmutable
                                                                     format:NULL
                                                           errorDescription:NULL];
    
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n" withString:@"\n"];
}

//获得公司列表
//-(void)getCompanyList{
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
//    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
//    
//    [manager GET:CompanyInviteCode parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
////        NSLog(@"Dic is %@",responseObject);
//        
//        if ([responseObject[@"code"] isEqualToNumber:@0]) {
//            
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//    }];
//}

@end
