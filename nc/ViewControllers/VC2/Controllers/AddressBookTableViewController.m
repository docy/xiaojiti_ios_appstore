//
//  AddressBookTableViewController.m
//  nc
//
//  Created by docy admin on 6/23/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "AddressBookTableViewController.h"
#import <AddressBook/AddressBook.h> // 访问通讯录需要的头文件
#import <AddressBookUI/AddressBookUI.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "AddressBookTableViewCell.h"
//#import "PeopleModel.h"
#import "UserModel.h"
#import "UIButton+XJTButton.h"
#import "XJTSearchBar.h"
#import "CompanyInviteCodeViewController.h"
#import "AddBookViewController.h"
#import "SDWebImageManager.h"
#import "UIImageView+AFNetworking.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+AlertView.h"
#import "MJRefresh.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "AddAddressBookViewController.h" // 添加联系人
#import "ToolOfClass.h"
#import "UISegmentedControl+XJTSegmentedControl.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "OpenNetWorkViewController.h"
#import "NetWorkBadView.h"

#define rowHeight 56
#define SearchBarHeight 40

@interface AddressBookTableViewController () < UITextFieldDelegate>
{
    NSMutableArray *_sectionIndexTitles; // 存储索引
    NSString * username;
    NetWorkBadView *badView;
//    UIView * yaoqingCell;
//    BOOL isOther;
//    BOOL isAdministrator;
}

@property (nonatomic ,retain) NSMutableArray *peopleData; // 用来存储联系人(分组)
//@property (nonatomic ,retain) NSMutableArray *peopleData2; // 用来存储联系人(分组)
//@property (nonatomic ,retain) UIView * yaoqingCell; // 用来存储联系人(分组)


@end

@implementation AddressBookTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationItem.title = @"联系";
    //segment选项控制
//    isOther=NO;
    
    //是否是管理员 控制是否显示生成邀请码接口 默认yes
//    isAdministrator=YES;
    
    // 设置搜索框等数据
    [self setUpAddressBookTableViewData];
    
    //获取公司通讯录
    [self getCompanyUserList];
    
    [self setUpMessageViewMJRefresh];
    // 切换公司通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCompanyUserList) name:@"SwitchCurrentCompany" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddressBookTableViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StartingFromTheNotification:) name:@"StartingFromTheNotification" object:nil];
    
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.tableView =[[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-49-20)];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    //断网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    //联网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
    badView.contentLabel.text = NSLocalized(@"NetWorkBadView_des");
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    badView.frame=CGRectMake(0, 0, ScreenWidth, 44);
    badView.hidden=YES;
    [self.view addSubview:badView];
}

- (void)LeftViewRefreshAgainInBadNetWork{
    //    NSLog(@"%s",object_getClassName(self));
    badView.hidden=NO;
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)LeftViewInGoodNetWork{
    badView.hidden=YES;
}

-(void)StartingFromTheNotification:(NSNotification*)notifi{
    [self.tabBarController setSelectedIndex:0];
}

- (void)setUpMessageViewMJRefresh{
    [self.peopleData removeAllObjects];
    self.tableView.headerRefreshingText=nil;
    [self.tableView addHeaderWithCallback:^{
        [self getCompanyUserList];
        
    }];
}

- (void)setUpAddressBookTableViewData{
    
    
    self.peopleData = [NSMutableArray array];
//    self.peopleData2 = [NSMutableArray array];
    
//    self.navigationItem.titleView = [UISegmentedControl segmentedControlWithItems:@[@"内部",@"外部"] target:self action:@selector(segmentClick:)];
    
//    self.navigationItem.leftBarButtonItem = nil;
    
//    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(AddressBookTableViewInGoodNetWork)];
//    [self.navigationItem setHidesBackButton:YES];
    
    // 设置tableView的tableHeaderView
    
//    UIView *viewBg = [UIView new];
//    viewBg.bounds = CGRectMake(0, 0, self.view.bounds.size.width, 50);
//    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"内部",@"外部"]];
//    [segment addTarget:self action:@selector(segmentClick:) forControlEvents:UIControlEventValueChanged];
//    segment.tintColor = [UIColor colorWithRed:72/255.0 green:193/255.0 blue:168/255.0 alpha:1];
//    segment.selectedSegmentIndex = 0;
//    [segment setWidth:self.view.bounds.size.width*0.4 forSegmentAtIndex:0];
//    [segment setWidth:self.view.bounds.size.width*0.4 forSegmentAtIndex:1];
    
//    UIView * view2=[UIView new];
//    view2.frame=CGRectMake(0, 0, self.view.bounds.size.width , 40);
    
//    CGPoint center = segment.center;
//    center.x = view2.bounds.size.width/2.0;
//    center.y = view2.bounds.size.height/2.0;
    
//    [view2 addSubview:segment];
//    [view2 setBackgroundColor:[UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1]];
//    segment.center = center;
    
//    self.navigationItem.titleView=view2;
    
//    [viewBg addSubview:view2];
//    self.tableView.hidden=YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    yaoqingCell=[UIView new];
    
//    UIView *view = [UIView new];
//    view.frame=CGRectMake(0, 0, self.view.bounds.size.width,50 );
//    UIImageView * img=[[UIImageView alloc] initWithFrame:CGRectMake(16, 10, 30, 30)];
//    [img setImage:[UIImage imageNamed:@"invitation friends"]];
//    [view addSubview:img];
//    [view setBackgroundColor:[UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1]];
//    
//    UILabel * label=[[UILabel alloc] initWithFrame:CGRectMake(70, 7, 120, 35)];
//    label.text=@"邀请好友";
//    [view addSubview:label];
//    
//    UIButton * button=[[UIButton alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-60, 5, 40, 40)];
//    [button setImage:[UIImage imageNamed:@"Function button"] forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(pushAddFriend) forControlEvents:UIControlEventTouchUpInside];
//    [view addSubview:button];
//    if (isOther) {
//        
////        yaoqingCell.hidden = NO;
//        self.tableView.tableHeaderView = self.yaoqingCell;
//    } else {
//        
//        self.tableView.tableHeaderView = nil;
////        yaoqingCell.hidden = YES;
//    }
////    [viewBg addSubview:yaoqingCell];
    
//    self.yaoqingCell = view;
    
//    self.tableView.tableHeaderView = self.yaoqingCell;
}

//segment点击事件
//-(void)segmentClick:(UISegmentedControl*)seg{
//    if (seg.selectedSegmentIndex==1) {
//        isOther=YES;
////        yaoqingCell.hidden=NO;
//        self.tableView.tableHeaderView = self.yaoqingCell;
////        UIView * newHV= self.tableView.tableHeaderView;
////        newHV.bounds=CGRectMake(0, 0, self.view.bounds.size.width, 50);
////        self.tableView.tableHeaderView=newHV;
//    }else{
//        isOther=NO;
////        self.yaoqingCell.hidden=NO;
////        UIView * newHV= self.tableView.tableHeaderView;
//        if (isAdministrator==YES) {
//            
//            self.tableView.tableHeaderView = self.yaoqingCell;
////            newHV.bounds=CGRectMake(0, 0, self.view.bounds.size.width, 50);
//        }else{
//            
//            self.tableView.tableHeaderView = nil;
////            newHV.bounds=CGRectMake(0, 0, self.view.bounds.size.width, 50);
////            self.yaoqingCell.hidden=YES;
//        }
////        self.tableView.tableHeaderView=newHV;
//    }
//    [self.tableView reloadData];
//}


//邀请好友点击事件
-(void)pushAddFriend{
//    if (isOther) {
//        AddAddressBookViewController *addPreson = [[AddAddressBookViewController alloc] initWithNibName:@"AddAddressBookViewController" bundle:[NSBundle mainBundle]];
//        [self.navigationController pushViewController:addPreson animated:YES];
//    } else {
        AddBookViewController *phoneAB = [[AddBookViewController alloc] init];
        phoneAB.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:phoneAB animated:YES];
//    }
}

//- (void)AddressBookTableViewBackBtnClick{
//    [self.navigationController popViewControllerAnimated:YES];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)AddressBookTableViewInGoodNetWork{
    if (self.peopleData.count==0) {
        [self getCompanyUserList];
    }
}

//获取用户所在公司通讯录
-(void)getCompanyUserList{
    username=[[NSString alloc] init];
    username=[[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString * cid=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentCompany"];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager GET:[NSString stringWithFormat:GetUserList, cid] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            // 清空数据
            [self.peopleData removeAllObjects];
            NSMutableArray *peopleArray = [NSMutableArray array]; // 用来存储联系人
            NSArray * array=responseObject[@"data"];
            
            
//            NetWorkBadView *badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
//            badView.OpenNetWorkBtn.hidden = YES;
//            badView.backgroundColor = [UIColor whiteColor];
//            badView.imageView.image = [UIImage imageNamed:@"xiaojiti_address book"];
//            NSString *text = [NSString stringWithFormat:@"好友 %lu",array.count];
//            NSString *count = [NSString stringWithFormat:@"%lu",array.count];
//            NSRange range = [text rangeOfString:count];
//            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];

//            [str addAttribute:NSForegroundColorAttributeName value:CustomColor(153, 153, 153) range:range];
//            badView.contentLabel.attributedText = str;
//            badView.contentLabel.text = [NSString stringWithFormat:@"%lu",array.count];
//            badView.contentLabel.textColor = CustomColor(153,153,153);
//            self.tableView.tableHeaderView = badView;
            self.navigationItem.title = [NSString stringWithFormat:@"%@ (%lu)",NSLocalized(@"contact_list_title"),array.count];
            
            
            
            for (int i=0; i<array.count; i++) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:array[i]];
                
//                PeopleModel *model = [[PeopleModel alloc] init];
//                NSDictionary * dic=[array objectAtIndex:i];
//                model.name = [dic objectForKey:@"name"];
//                model.id = [dic objectForKey:@"id"];
//                model.nickName = [dic objectForKey:@"nickName"];
//                model.rowSelected = NO; // cell默认不被选中
//                model.isShowAccessoryView = NO; // cell的accessoryView默认不显示
//                model.email =[dic objectForKey:@"email"];
//                model.tellPhone=[dic objectForKey:@"phone"];
//                NSString* headimg=[NSString stringWithFormat:@"%@%@",iconPath,[dic objectForKey:@"avatar"]];
//                model.iconImageUrl=headimg;
                
//                int type=[[dic objectForKey:@"userType"] intValue];
//                if([[dic objectForKey:@"name"] isEqualToString:username]&&type==1){

//                    isAdministrator=NO;
//                    self.tableView.tableHeaderView=nil;
//                }
//                if([[dic objectForKey:@"name"] isEqualToString:username]&&type==0){
                
//                    isAdministrator=YES;
                    
                    
//                    NetWorkBadView *badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
//                    [badView.OpenNetWorkBtn addTarget:self action:@selector(pushAddFriend) forControlEvents:UIControlEventTouchUpInside];
//                    badView.backgroundColor = [UIColor whiteColor];
//                    badView.imageView.image = [UIImage imageNamed:@"xiaojiti_address book"];
//                    badView.contentLabel.text = @"邀请好友";
//                    self.tableView.tableHeaderView = badView;

                    
//                    self.tableView.tableHeaderView=self.yaoqingCell;
//                }
                [peopleArray addObject:model];
            }
            
            
            // Sort data
            UILocalizedIndexedCollation *theCollation = [UILocalizedIndexedCollation currentCollation];
            for (UserModel *model in peopleArray) {
                NSInteger sect = [theCollation sectionForObject:model
                                        collationStringSelector:@selector(nickName)];
                model.sectionNumber = sect;
            }
            
            NSInteger highSection = [[theCollation sectionTitles] count];
            NSMutableArray *sectionArrays = [NSMutableArray arrayWithCapacity:highSection];
            
            for (int i=0; i<=highSection; i++) {
                NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
                [sectionArrays addObject:sectionArray];
            }
            
            for (UserModel *model in peopleArray) {
                [(NSMutableArray *)[sectionArrays objectAtIndex:model.sectionNumber] addObject:model];
            }
            
            // 排好序的联系人加入数组中
            for (NSMutableArray *sectionArray in sectionArrays) {
                int count = 0; // 统计name为nil的个数
                for (UserModel *model in sectionArray) {
                    if (model.name.length==0) {
                        count++;
                    }
                }
                NSArray *sortedSection = [NSArray array];
                //        NSArray *sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(name)];
                if (count >= 2) { // 有2个以上name为nil,
                    sortedSection = [NSArray arrayWithArray:sectionArray];
                } else {
                    sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(nickName)];
                }
                [self.peopleData addObject:sortedSection];
            }
            self.tableView.hidden=NO;
            [self.tableView reloadData];
            [self.tableView headerEndRefreshing];
            
            if (self.peopleData.count==0) {
                //                self.tableView.backgroundView = [UIImageView imageViewWithImage:@"Create a group"];
                self.tableView.backgroundView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"address book"]];
            } else {
                self.tableView.backgroundView = nil;
            }
            
        }
//        else if([responseObject[@"code"] isEqualToNumber:@3]){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//        }
        else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.tableView headerEndRefreshing];
        
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return isOther?0:self.peopleData.count;
    return self.peopleData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.peopleData[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellId";
    
    AddressBookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AddressBookTableViewCell" owner:self options:nil] lastObject];
    }
//    if (isOther) {
//        return cell;
//    }
    UserModel *model = self.peopleData[indexPath.section][indexPath.row];
   
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.location==NSNotFound) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [cell.IconImageView setImageWithURL:iconUrl placeholderImage:nil];
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@:%@",NSLocalized(@"contact_list_name"),model.name];
    cell.nickNameLabel.text = model.nickName;
    
//    cell.phoneOrEmailLabel.text = [model.phone isEqual:[NSNull null]]?@"":model.phone;
    NSString *originTel = [model.phone isEqual:[NSNull null]]?@"":model.phone;
    if (originTel.length>7) { //隐藏手机号中间四位
        NSString *tel = [originTel stringByReplacingCharactersInRange:NSMakeRange(3, 7) withString:@"****"];

        cell.phoneOrEmailLabel.text =tel;
        
    }else{
        cell.phoneOrEmailLabel.text =[model.phone isEqual:[NSNull null]]?@"":model.phone;
    }
    
    if (indexPath.row==[self.peopleData[indexPath.section] count]-1) {
        cell.xianTiaoLabel.hidden = YES;
    } else {
        cell.xianTiaoLabel.hidden = NO;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UserModel *model = self.peopleData[indexPath.section][indexPath.row];
    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    
    userPerVC.userId = model.id;
    userPerVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:userPerVC animated:YES];
    

//    if (![model.name isEqualToString:@""]) {
//        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
//        [manager GET:[[NSString stringWithFormat:GetUserInfo, model.name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            if ([responseObject[@"code"] isEqualToNumber:@0]) {
//                LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
//                UserModel *userModel=[[UserModel alloc] init];
//                userModel.avatar=responseObject[@"data"][@"avatar"];
//                userModel.email=responseObject[@"data"][@"email"];
//                userModel.id=responseObject[@"data"][@"id"];
//                userModel.name=responseObject[@"data"][@"name"];
//                userModel.nickName=responseObject[@"data"][@"nickName"];
//                userModel.phone=responseObject[@"data"][@"phone"];
//                userModel.status=responseObject[@"data"][@"status"];
//                userModel.sex=responseObject[@"data"][@"sex"];
//                userPerVC.userPerfileModel = userModel;
//                
//                userPerVC.hidesBottomBarWhenPushed = YES;
//                [self.navigationController pushViewController:userPerVC animated:YES];
//            } else {
//                NSLog(@"%@",responseObject[@"message"]);
//            }
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            NSLog(@"error=%@",error);
//        }];
        
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    NSString *title = [self.peopleData[section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section]:nil;
    return title;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    
    tableView.sectionIndexColor = CustomColor(72, 193, 168);
    
    return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];

}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

@end
