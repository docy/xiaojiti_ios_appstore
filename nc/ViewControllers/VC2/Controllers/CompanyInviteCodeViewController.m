//
//  CompanyInviteCodeViewController.m
//  xjt
//
//  Created by guanxf on 15/8/17.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "CompanyInviteCodeViewController.h"
#import "CICTableViewCell.h"
//#import "UITabBarController+CustomTabBarController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

#import "AddBookViewController.h"
#import "UMSocial.h"
#import <MessageUI/MessageUI.h>
#import<MessageUI/MFMailComposeViewController.h>
#import "UIAlertView+AlertView.h"
#import <MessageUI/MessageUI.h>
#import<MessageUI/MFMailComposeViewController.h>

#import <AFNetworking/AFNetworking.h>

@interface CompanyInviteCodeViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>{
    UIAlertView *mfAlertview;//定义一个弹出框
    UITextView* txYaoqingma;
    NSString* userName;     //当前登录用户
}
@property (weak, nonatomic) IBOutlet UILabel *code1;
@property (weak, nonatomic) IBOutlet UILabel *code2;
@property (weak, nonatomic) IBOutlet UILabel *code3;
@property (weak, nonatomic) IBOutlet UILabel *code4;
@property (weak, nonatomic) IBOutlet UILabel *code5;
@property (weak, nonatomic) IBOutlet UILabel *code6;

@end

@implementation CompanyInviteCodeViewController
@synthesize code,Fname,FriendPhoneNum;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=NSLocalized(@"company_code_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(AddressBookTableViewBackBtnClick)];
    
    userName=[[NSString alloc] init];
    
    self.CICCodeLabel.text=code;
    
    UIView * view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
    
    UILabel * label=[[UILabel alloc] init];
    label.text=code;
    label.center=view.center;
    label.bounds=CGRectMake(0, 0, view.bounds.size.width*0.6, 30);
    label.textAlignment=NSTextAlignmentCenter;
    
    NSMutableString * codestr=[NSMutableString stringWithString:code];
    
    for (int i=0; i<5; i++) {
        [codestr insertString:@"," atIndex:i*2+1];
    }
    NSArray *array = [codestr componentsSeparatedByString:@","];
    
    _code1.text=array[0];
    _code2.text=array[1];
    _code3.text=array[2];
    _code4.text=array[3];
    _code5.text=array[4];
    _code6.text=array[5];
    
    [view addSubview:label];
    
    [self getUserInfo];
    
//    self.tableView.tableHeaderView=view;
}

//-(void)viewWillAppear:(BOOL)animated{
//    _tableView.tableHeaderView=self.TabHeardView;
//    [_tableView reloadData];
//}

// 导航栏返回按钮
- (void)AddressBookTableViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --UITableViewDelegate
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *TableSampleIdentifier = @"TableSampleIdentifier";
    
    CICTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             TableSampleIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CICTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case 0:
            cell.nameLabel.text=NSLocalized(@"company_code_mess");
            cell.heardImg.image=[UIImage imageNamed:@"SMS"];
            cell.pushCellButton.tag=1000;
            [cell.pushCellButton addTarget:self action:@selector(pushFenxiang:) forControlEvents:UIControlEventTouchUpInside];
            break;
        case 1:
            cell.nameLabel.text=NSLocalized(@"company_code_weChat");
            cell.heardImg.image=[UIImage imageNamed:@"chat friends"];
            cell.pushCellButton.tag=1001;
            [cell.pushCellButton addTarget:self action:@selector(pushFenxiang:) forControlEvents:UIControlEventTouchUpInside];
            break;
        case 2:
            cell.nameLabel.text=NSLocalized(@"company_code_copy");
            cell.heardImg.image=[UIImage imageNamed:@"link"];
            cell.pushCellButton.tag=1002;
            [cell.pushCellButton addTarget:self action:@selector(pushFenxiang:) forControlEvents:UIControlEventTouchUpInside];
            break;
            
        default:
            break;
    }

    return cell;
    
}

-(void)pushFenxiang:(UIButton*)button{
    if (button.tag==1000) {
        [self showSMSPicker];

    }else if (button.tag==1001){
        [self shareWeiXin];
    }else{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string=code;//复制邀请码
        [UIAlertView alertViewWithTitle:NSLocalized(@"company_code_alertTitle")];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 28;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 2;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return NSLocalized(@"company_code_text");
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    return _TabHeardView;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//获取用户信息
-(void)getUserInfo{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString * UserId=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager GET:[NSString stringWithFormat:GetUserInfo, UserId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (![responseObject[@"code"] isEqualToNumber:@200]) {
            userName=responseObject[@"code"][@"name"];
            NSLog(@"username--is :%@",userName);
        } else {
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark --分享相关

//-(void)showUomeng{
//    [UMSocialSnsService presentSnsIconSheetView:self appKey:@"55cd7986e0f55ae569002591"
//                                      shareText:[NSString stringWithFormat:@"您的好友‘%@’邀请你加入小集体，邀请码为：%@，请您尽快注册！www.docy.co",userName,code]
//                                     shareImage:[UIImage imageNamed:@"icon.png"]
//                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToWechatSession,UMShareToEmail,UMShareToSms,nil]
//                                       delegate:self];
////    注意：分享到微信好友、微信朋友圈、微信收藏、QQ空间、QQ好友、来往好友、来往朋友圈、易信好友、易信朋友圈、Facebook、Twitter、Instagram等平台需要参考各自的集成方法
//}

//自定义微信分享
-(void)shareWeiXin{
    [UMSocialData defaultData].extConfig.wechatSessionData.url = @"http://www.docy.com";
    NSUserDefaults * info=[NSUserDefaults standardUserDefaults];
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:[NSString stringWithFormat:@"‘%@’%@%@，%@",[info objectForKey:@"nickName"],NSLocalized(@"company_code_share_WX0"),code,NSLocalized(@"company_code_share_WX1")] image:nil location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
//            NSLog(@"分享成功！");
        }
    }];
    
}

//弹出列表方法presentSnsIconSheetView需要设置delegate为self
-(BOOL)isDirectShareInIconActionSheet
{
    return YES;
}

-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
        
    }
    
}


-(void)showMessageViewController
{
    if( [MFMessageComposeViewController canSendText] )//判断是否能发短息
    {
        
        MFMessageComposeViewController * controller = [[MFMessageComposeViewController alloc]init];
        controller.recipients = [NSArray arrayWithObject:@"10010"];//接收人,可以有很多,放入数组
        controller.body = txYaoqingma.text;//短信内容,自定义即可
        controller.messageComposeDelegate = self;//注意不是delegate
        
        [self presentViewController:controller animated:YES completion:nil];
        
        [[[[controller viewControllers] lastObject] navigationItem] setTitle:NSLocalized(@"company_code_mess_send")];//修改短信界面标题
    }
    else
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"company_code_mess_unUse") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
        [alert show];
    }
}

//短信发送成功后的回调
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    switch (result)
    {
        case MessageComposeResultCancelled:
        {
            //用户取消发送
        }
            break;
        case MessageComposeResultFailed://发送短信失败
        {
            mfAlertview=[[UIAlertView alloc]initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"company_code_mess_failure") delegate:nil cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
            
            [mfAlertview show];
            
        }
            break;
        case MessageComposeResultSent:
        {
            mfAlertview=[[UIAlertView alloc]initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"company_code_mess_success") delegate:nil cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
            
            [mfAlertview show];
            
        }
            break;
        default:
            break;
    }
}


//本地短信分享
//短信

-(void)showSMSPicker{
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    if (messageClass != nil) {
        // Check whether the current device is configured for sending SMS messages
        if ([messageClass canSendText]) {
            [self displaySMSComposerSheet];
        }
        else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@""message:NSLocalized(@"company_code_mess_unUse") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil];
            [alert show];
        }
        
    }else {
        
    }
    
}

-(void)displaySMSComposerSheet
{
    NSUserDefaults * info=[NSUserDefaults standardUserDefaults];
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate =self;
    NSArray * recipientsArray=[[NSArray alloc] initWithObjects:FriendPhoneNum, nil];
    picker.recipients=recipientsArray;
    NSString *smsBody =[NSString stringWithFormat:@"‘%@’%@%@，%@",[info objectForKey:@"nickName"],NSLocalized(@"company_code_share_WX0"),code,NSLocalized(@"company_code_share_WX1")];
    picker.body=smsBody;
    [self presentModalViewController:picker animated:YES];
}

@end
