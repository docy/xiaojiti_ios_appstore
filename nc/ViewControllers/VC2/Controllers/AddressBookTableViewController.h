//
//  AddressBookTableViewController.h
//  nc
//
//  Created by docy admin on 6/23/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 通讯录视图控制器（手机通讯录）

@interface AddressBookTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, assign) BOOL isTellPhone;
@property (nonatomic, copy) NSString *navigationItemTitle;

@end
