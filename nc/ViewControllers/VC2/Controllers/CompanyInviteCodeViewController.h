//
//  CompanyInviteCodeViewController.h
//  xjt
//
//  Created by guanxf on 15/8/17.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyInviteCodeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSString * code;
    NSString * Fname;
    NSString * FriendPhoneNum;
}
@property(nonatomic, strong) NSString*Fname;
@property(nonatomic, strong) NSString * code;
@property(nonatomic, strong) NSString * FriendPhoneNum;
@property (weak, nonatomic) IBOutlet UIView *TabHeardView;
@property (weak, nonatomic) IBOutlet UILabel *CICCodeLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
