//
//  PersonalMessageModel.h
//  nc
//
//  Created by docy admin on 6/28/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"
@class UIImage;
// 个人资料model
@interface PersonalMessageModel : CommonModel

@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, retain) NSNumber *sex;
@property (nonatomic, retain) NSDictionary *info;
@property (nonatomic, copy) NSString *origin;


//@property (nonatomic, retain) NSNumber *id; // id


@end
