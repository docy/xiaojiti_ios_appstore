//
//  AboutMyViewController.m
//  nc
//
//  Created by docy admin on 15/12/8.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import "AboutMyViewController.h"

@interface AboutMyViewController ()

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;


@property (weak, nonatomic) IBOutlet UILabel *Localized_text0;
@property (weak, nonatomic) IBOutlet UILabel *Localized_text0_1;

@property (weak, nonatomic) IBOutlet UILabel *Localized_text1;
@property (weak, nonatomic) IBOutlet UILabel *Localized_text2;

@end

@implementation AboutMyViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationItem.title = NSLocalized(@"info_about_title");
    self.versionLabel.text = CURRENT_VERSION;
    self.Localized_text0.text = NSLocalized(@"info_about_text0");
    self.Localized_text0_1.text = NSLocalized(@"info_about_text0_1");
    self.Localized_text1.text = NSLocalized(@"info_about_text1");
    self.Localized_text2.text = NSLocalized(@"info_about_text2");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
