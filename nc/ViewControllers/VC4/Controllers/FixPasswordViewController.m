//
//  FixPasswordViewController.m
//  nc
//
//  Created by docy admin on 15/10/20.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "FixPasswordViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "UIAlertView+AlertView.h"
#import "ResetPasswordViewController.h"

@interface FixPasswordViewController ()

@end

@implementation FixPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalized(@"info_reworkPW_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(setUpFixPasswordViewBack)];
    self.oldPasswordTF.placeholder = NSLocalized(@"info_reworkPW_old");
    self.newpass.placeholder = NSLocalized(@"info_reworkPW_new");
    [self.Localized_submitBtn setTitle:NSLocalized(@"info_reworkPW_submitBtn") forState:UIControlStateNormal];
    [self.Localized_forgetBtn setTitle:NSLocalized(@"info_reworkPW_forget") forState:UIControlStateNormal];
}

- (void)setUpFixPasswordViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onFixPasswordViewTiJiaoBtnClick:(id)sender {
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSString *password = self.newpass.text;
    
    if (self.oldPasswordTF.text.length==0) {
        [UIAlertView alertViewWithTitle:NSLocalized(@"info_reworkPW_needOld")];
    } else if (password.length == 0) {
        [UIAlertView alertViewWithTitle:NSLocalized(@"info_reworkPW_needNew")];
    } else if ([self.oldPasswordTF.text isEqualToString:password]) {
        [UIAlertView alertViewWithTitle:NSLocalized(@"info_reworkPW_notSame")];
    } else {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        parameter[@"password"] = password;
        parameter[@"oldpassword"] = self.oldPasswordTF.text;
        [manager POST:UpdateUserInfo parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@",responseObject);
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                
                [ToolOfClass showMessage:NSLocalized(@"HOME_show_remoke_success")];
                [info setObject:password forKey:@"password"];
                [self.navigationController popViewControllerAnimated:YES];
                
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:NSLocalized(@"info_reworkPW_failure")];
        }];
    }
}

- (IBAction)onResetPasswordBtnClick:(id)sender {
    ResetPasswordViewController *resetPass = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:resetPass animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.oldPasswordTF resignFirstResponder];
    [self.newpass resignFirstResponder];
}

@end
