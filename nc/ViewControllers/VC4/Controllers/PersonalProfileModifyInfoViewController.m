//
//  PersonalProfileModifyInfoViewController.m
//  nc
//
//  Created by docy admin on 15/8/27.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "PersonalProfileModifyInfoViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "NSString+XJTString.h"
#import "UIAlertView+AlertView.h"

@interface PersonalProfileModifyInfoViewController ()

@property (weak, nonatomic) IBOutlet UIButton *manBtn;
@property (weak, nonatomic) IBOutlet UIButton *womanBtn;

@property (weak, nonatomic) IBOutlet UILabel *daojishiLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *identifyTF;
@property (weak, nonatomic) IBOutlet UIButton *identifyBtn;
@property (weak, nonatomic) IBOutlet UIView *chagePhoneView;

@property (nonatomic, assign) NSInteger sex; // 用于记录性别（1男，0女）

@end

@implementation PersonalProfileModifyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_isSex) {
        _sexView.hidden=NO;
        if (_isSexMan==1) {
            _manBtn.selected = YES;
            self.womanBtn.selected = NO;
        }else{
            self.womanBtn.selected = YES;
            _manBtn.selected = NO;
        }
    }
    
    if (_isPhone) {
        _chagePhoneView.hidden=NO;
    }else{
        _chagePhoneView.hidden=YES;
    }
    
    [self SetUpPersonalProfileModifyInfoViewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)SetUpPersonalProfileModifyInfoViewData
{
    self.navigationItem.title = self.itemTitle;
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:NSLocalized(@"info_reworkInfo_save") target:self action:@selector(onPersonalProfileModifyInfoViewSaveBtnClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTitle:NSLocalized(@"info_reworkInfo_cancel") target:self action:@selector(onPersonalProfileModifyInfoViewBackBtnClick)];
    self.modifyTextField.text = self.modifyText;
}
#pragma mark 按钮点击事件
- (void)onPersonalProfileModifyInfoViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onPersonalProfileModifyInfoViewSaveBtnClick
{
     NSString *text = [NSString stringThrowOffBlankWithString:self.modifyTextField.text];
    if (text.length==0&&!_isSex) {
        [ToolOfClass showMessage:[NSString stringWithFormat:@"%@%@",self.itemTitle,NSLocalized(@"info_reworkInfo_notBlank")]];
    } else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        if ([self.itemTitle isEqualToString:NSLocalized(@"info_set_nick")]) {
//            if (text.length>15) {
//                [UIAlertView alertViewWithTitle:NSLocalized(@"info_reworkInfo_nickTooLong")];
//                return ;
//            }else{
            parameter[@"nickName"] = text;
//            }
        } else if ([self.itemTitle isEqualToString:NSLocalized(@"info_set_phone")]) {
            parameter[@"phone"] = text;
            parameter[@"code"] = _identifyTF.text;
        } else if ([self.itemTitle isEqualToString:@"邮箱"]) {
            parameter[@"email"] = text;
        } else if ([self.itemTitle isEqualToString:NSLocalized(@"group_set_name")]) {
            parameter[@"name"] = text;
        } else if ([self.itemTitle isEqualToString:NSLocalized(@"group_set_des")]) {
            parameter[@"desc"] = text;
        } else if (_isSex) {
            parameter[@"sex"] = [NSNumber numberWithInteger:_sex];
        } else if ([self.itemTitle isEqualToString:NSLocalized(@"info_set_name")]) {
            parameter[@"name"] = text;
        }
        
        NSString *strPath = nil;
        if (self.groupId != nil) {
            
            strPath = [NSString stringWithFormat:UpdateGroupInfo,self.groupId];
            
        } else {
            strPath = [UpdateUserInfo copy];
        }
        
        [manager POST:strPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                [ToolOfClass showMessage:NSLocalized(@"info_reworkInfo_success")];
                if ([self.itemTitle isEqualToString:NSLocalized(@"info_set_nick")]) {
                    [info setObject:text forKey:@"nickName"];
                }
                if (self.modifyBlock) {
                    self.modifyBlock(text);
                }
                [self.navigationController popViewControllerAnimated:YES];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:NSLocalized(@"info_reworkInfo_failure")];
        }];
    }
}

- (IBAction)onPPGetUpidentifyBtnClick:(id)sender {
    
    if (self.phoneTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"info_forgetPW_needPhone")];
    } else {
        _identifyBtn.enabled=NO;
        _identifyBtn.hidden=YES;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"phone"] = self.phoneTF.text;
        
        [manager POST:ResetPhoneSms parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] intValue] == 200) {
                NSLog(@"***************邀请码为:%@",responseObject[@"data"][@"code"]);
                //                [self startTimeSecondsCountDown];
                [ToolOfClass toolStartTimeSecondsCountDown_uilabel:self.daojishiLabel];
                double delayInSeconds = 30.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    _identifyBtn.enabled=YES;
                    _identifyBtn.hidden=NO;
                });
                //                [ToolOfClass toolStartTimeSecondsCountDown:self.identifyCodeBtn];
            } else {
                //                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                _identifyBtn.enabled=YES;
                _identifyBtn.hidden=NO;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            _identifyBtn.enabled=YES;
            _identifyBtn.hidden=NO;
        }];
    }
}

// 选择性别
- (IBAction)onSelectPersonalSexBtnClick:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (button.tag==99) {
        if (!button.selected) {
            button.selected = YES;
            self.womanBtn.selected = NO;
        }
        self.sex = 1;
    } else {
        if (!button.selected) {
            button.selected = YES;
            self.manBtn.selected = NO;
        }
        self.sex = 0;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    self.hidesBottomBarWhenPushed = YES;
}

@end
