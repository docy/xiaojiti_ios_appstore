//
//  UserJionedGroupViewController.m
//  xjt
//
//  Created by docy admin on 7/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "UserJionedGroupViewController.h"
#import "GroupDetailViewController.h"

#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UserJionedGroupTableViewCell.h"
#import "GroupModel.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"

@interface UserJionedGroupViewController () <UIAlertViewDelegate>

@property (nonatomic,retain) NSMutableArray *groupCountData; // 存储用户加入的所有群组信息

@end

@implementation UserJionedGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置基本属性
    [self setUpUserJionedGroupViewData];
    // 获取群组列表
    [self getUpUserJionedGroupViewUserGroupList];
}

- (void)setUpUserJionedGroupViewData
{
    self.navigationItem.title = NSLocalized(@"group_joined_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(UserJionedGroupViewNavcBackBtnClick)];
    self.groupCountData = [NSMutableArray array];
    
}
// 导航栏的返回按钮
- (void)UserJionedGroupViewNavcBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 获取用户群组
- (void)getUpUserJionedGroupViewUserGroupList
{
    [self.groupCountData removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [manager GET:userGroupsPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {

            for (NSDictionary *dict in responseObject[@"data"]) {
                
                if ([dict[@"category"] isEqualToNumber:@1]||[dict[@"category"] isEqualToNumber:@4]) {
                    GroupModel *model = [[GroupModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    [self.groupCountData addObject:model];
                }
            }
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupCountData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *celId = @"cellId";
    UserJionedGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:celId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"UserJionedGroupTableViewCell" owner:self options:nil] lastObject];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row==self.groupCountData.count-1) {
        cell.statusLabel.hidden = YES;
    } else {
        cell.statusLabel.hidden = NO;
    }
    
    CGRect rect = cell.statusLabel.frame;
    rect.size.height = 0.5;
    cell.statusLabel.frame = rect;
    
    GroupModel *model = self.groupCountData[indexPath.row];
    
    if (model.category.intValue == 4) {
        cell.lockIgView.hidden = NO;
    } else {
        cell.lockIgView.hidden = YES;
    }
    
    cell.groupName.text = model.name;
    [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
//    cell.groupUserCount.text = [NSString stringWithFormat:@"%@人",model.userCount];
    cell.groupUserCount.hidden = YES;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    GroupDetailViewController *detailVC = [[GroupDetailViewController alloc] initWithNibName:@"GroupDetailViewController" bundle:[NSBundle mainBundle]];
//    detailVC.groupId = model.id;
//    [self.navigationController pushViewController:detailVC animated:YES];
    
    GroupModel *model = self.groupCountData[indexPath.row];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"group_joined_shareTo") message:
                          [model.name stringByAppendingString:NSLocalized(@"group_joined_group")] delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"HOME_alert_sure"), nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==1) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        GroupModel *model = self.groupCountData[indexPath.row];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        parameter[@"groupId"] = model.id;
        parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:model.id];
        
        NSString *path = [NSString stringWithFormat:topicForward,self.topicId];
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                [ToolOfClass showMessage:NSLocalized(@"group_joined_convey_success")];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:NSLocalized(@"group_joined_convey_fail")];
        }];
    }
}

@end
