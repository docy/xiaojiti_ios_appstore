//
//  AboutViewController.m
//  nc
//
//  Created by docy admin on 15/11/27.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import "AboutViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end

@implementation AboutViewController

//- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
//    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
//        self.navigationItem.title = @"关于";
//        self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(aboutViewLeftBarItemBack)];
//    }
//    return self;
//}

- (void)aboutViewLeftBarItemBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.versionLabel.text = CURRENT_VERSION;
    
    self.descLabel.contentMode = UIViewContentModeTopLeft;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
