//
//  PersonalProfileViewController.m
//  nc
//
//  Created by docy admin on 6/27/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "PersonalProfileViewController.h"
#import "AppDelegate.h"

#import "SwitchCompanyViewController.h"
#import "UserJionedGroupViewController.h"
#import "LoginViewController.h"
#import "Login_ViewController.h"
#import "XJTNavigationController.h"
#import "PersonalProfileModifyInfoViewController.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "RegisterCreatCompanyViewController.h"

#import "PersonalMessageModel.h"
#import "CompanyModel.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "FixPasswordViewController.h"

#import "AboutMyViewController.h"
#import "usernamePW_ViewController.h"


@interface PersonalProfileViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    NSString *_authToken; // 用户的authToken
    NSNumber *_userId; // 用户id
    PersonalMessageModel *model; //
}

@property (nonatomic, copy) NSString *avarPath; // 获取上传头像的路径


@property (weak, nonatomic) IBOutlet UILabel *Localized_avator;
@property (weak, nonatomic) IBOutlet UILabel *Localized_name;
@property (weak, nonatomic) IBOutlet UILabel *Localized_nickName;
@property (weak, nonatomic) IBOutlet UILabel *Localized_sex;
@property (weak, nonatomic) IBOutlet UILabel *Localized_phone;
@property (weak, nonatomic) IBOutlet UILabel *Localized_password;
@property (weak, nonatomic) IBOutlet UILabel *Localized_about;
@property (weak, nonatomic) IBOutlet UIButton *chageUserName;
@property (weak, nonatomic) IBOutlet UIButton *chagPhoneButton;

@end

@implementation PersonalProfileViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    _userId = [info objectForKey:@"id"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // 设置基本属性
    [self setUpPersonalProfileViewdata];
    
    // 网络获取个人信息
    [self getPersonalProfileViewPersonalInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark setUpData

// 设置基本属性
- (void)setUpPersonalProfileViewdata{

    
    self.Localized_avator.text = NSLocalized(@"info_set_avator");
    self.Localized_name.text = NSLocalized(@"info_set_name");
    self.Localized_nickName.text = NSLocalized(@"info_set_nick");
    self.Localized_sex.text = NSLocalized(@"info_set_sex");
    self.Localized_phone.text = NSLocalized(@"info_set_phone");
    self.Localized_password.text = NSLocalized(@"info_set_password");
    self.Localized_about.text = NSLocalized(@"info_set_about");
    
    self.avarPath = [NSString string];
    self.navigationItem.title = NSLocalized(@"info_set_title");
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    _authToken = [info objectForKey:@"authToken"];
    _userId = [info objectForKey:@"id"];
    
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:nil];
//    [self.navigationItem setHidesBackButton:YES];
    self.scrollerView.contentSize = CGSizeMake(0, [[UIScreen mainScreen] bounds].size.height);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PersonalProfileViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StartingFromTheNotification:) name:@"StartingFromTheNotification" object:nil];
    //切换账号登录之后
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newUserLogin) name:@"newUserLogin" object:nil];
}

- (void)leftItemBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)StartingFromTheNotification:(NSNotification*)notifi{
    [self.tabBarController setSelectedIndex:0];
}

- (void)PersonalProfileViewInGoodNetWork{
    if (self.personalNameLabel.text.length==0) {
        [self getPersonalProfileViewPersonalInfo];
    }
}

-(void)newUserLogin{
    [self getPersonalProfileViewPersonalInfo];
}

#pragma mark getUpData

// 网络获取个人信息
- (void)getPersonalProfileViewPersonalInfo{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    _authToken = [info objectForKey:@"authToken"];
    _userId = [info objectForKey:@"id"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = _authToken;
    
    NSString *str = [personalInfoPath stringByAppendingString:[NSString stringWithFormat:@"%@",_userId]];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"userinfo is :%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            model=nil;
            model = [[PersonalMessageModel alloc] init];
            [model setValuesForKeysWithDictionary:responseObject[@"data"]];
           // 显示数据
            [self showPersonalProfileViewUserDataWithModel:model];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
#pragma mark showData
// 显示个人资料
- (void)showPersonalProfileViewUserDataWithModel:(PersonalMessageModel *)modell{
    self.personalNickNameLabel.text = model.nickName;
    self.personalNameLabel.text = model.name;
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.length==0) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [self.personalIconImageView sd_setImageWithURL:iconUrl];
    if ([model.sex intValue]==0) {
        [self.sexButton setBackgroundImage:[UIImage imageNamed:@"selected_women_icon"] forState:UIControlStateNormal];
    } else {
        [self.sexButton setBackgroundImage:[UIImage imageNamed:@"selected_men_icon"] forState:UIControlStateNormal];
    }
    self.companyPhoneLabel.text = model.phone;
//    self.companyEmailLabel.text = model.email;
    self.companyNameLabel.text = model.companyName;
    
    //如果没有changed字段或者 为true 都是修改过用户名的 就隐藏修改button
    if (model.info[@"changed"]==nil||![model.info[@"changed"] boolValue]) {
        _chageUserName.hidden=NO;
    }else{
        _chageUserName.hidden=YES;
    }
    
}

#pragma mark 按钮点击事件
// 更改昵称
- (IBAction)onModifityNickNameBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.personalNickNameLabel.text;
    infoVC.itemTitle = NSLocalized(@"info_set_nick");
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.personalNickNameLabel.text = modifyText;
    };
    
    infoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoVC animated:YES];
}
// 退出按钮
- (IBAction)onLogoutBtnClick:(id)sender {
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"退出当前账号?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"退出", nil];
//    [alert show];
    
}

- (IBAction)onModifityCompanyPhoneBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.companyPhoneLabel.text;
    infoVC.itemTitle = NSLocalized(@"info_set_phone");
    infoVC.isPhone=YES;
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.companyPhoneLabel.text = modifyText;
    };
    infoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)onModifityCompanyEmailBtnClick:(id)sender {
    
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.companyEmailLabel.text;
    infoVC.itemTitle = @"邮箱";
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.companyEmailLabel.text = modifyText;
    };
    infoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoVC animated:YES];
    
}

- (IBAction)onModifityCompanySexBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
//    infoVC.modifyText = self.companyPhoneLabel.text;
    infoVC.itemTitle = NSLocalized(@"info_set_sex");
    infoVC.modifyBlock = ^(NSString *modifyText) {
        [self getPersonalProfileViewPersonalInfo];
    };
    infoVC.isSex=YES;//[model.sex intValue]==1?1:0;
    infoVC.isSexMan=[model.sex intValue]==1?1:0; //1是男，0是女
    infoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)onModifityCompanyUserNameBtnClick:(id)sender {
    BOOL chaged=model.info[@"changed"];
    if ([model.origin isEqualToString:@"weixin"]&&!chaged) { //微信用户
        usernamePW_ViewController * unpwVC=[[usernamePW_ViewController alloc] initWithNibName:@"usernamePW_ViewController" bundle:nil];
        unpwVC.isPush=YES;
        unpwVC.itemTitle = @"注册";
        unpwVC.updataUserName=^(NSString * username){
            [self getPersonalProfileViewPersonalInfo];
        };
        [self.navigationController pushViewController:unpwVC animated:YES];
    }else{
        PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
        //    infoVC.modifyText = self.companyPhoneLabel.text;
        infoVC.itemTitle = NSLocalized(@"info_set_name");
        infoVC.modifyText = self.personalNameLabel.text;
        infoVC.modifyBlock = ^(NSString *modifyText) {
            [self getPersonalProfileViewPersonalInfo];
        };
        infoVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:infoVC animated:YES];
    }
}

- (IBAction)onFixPasswordBtnClick:(id)sender {
    
    FixPasswordViewController *fix = [[FixPasswordViewController alloc] initWithNibName:@"FixPasswordViewController" bundle:[NSBundle mainBundle]];
    fix.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fix animated:YES];
}

- (IBAction)aboutButtonClick {
    AboutMyViewController *aboutVC = [AboutMyViewController new];
    [self.navigationController pushViewController:aboutVC animated:YES];

}

// 加入的小组
- (IBAction)PersonalProfileViewGetUpUserJionedGroup:(id)sender {
    UserJionedGroupViewController *joinGroupVC = [[UserJionedGroupViewController alloc] init];
    joinGroupVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:joinGroupVC animated:YES];
}

// 选择个人头像
- (IBAction)onSelectePersonalIconBtnClick:(id)sender {
//    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
//    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    pickerVC.allowsEditing = YES;
//    pickerVC.delegate = self;
//    [self presentViewController:pickerVC animated:YES completion:nil];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
    [actionSheet showInView:self.view];
    
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        
        [manager POST:logoutPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@200]) { // 退出程序
                
                [info setObject:@"YES" forKey:@"isLogout"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postLoginVC" object:nil];
            } else {
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

#pragma mark UIimagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
//    NSLog(@"info=%@",info);
    if (picker.sourceType==UIImagePickerControllerSourceTypePhotoLibrary) {
        self.personalIconImageView.image = info[UIImagePickerControllerEditedImage];
        NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
        NSString *fileName = [url lastPathComponent];
        // 上传图片文件
        [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.personalIconImageView.image setAvatarOrLogoPath:setAvatarPath];
//
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {

        self.personalIconImageView.image = info[UIImagePickerControllerOriginalImage];
        NSString *fileName = @"origin.jpg";
        [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.personalIconImageView.image setAvatarOrLogoPath:setAvatarPath];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex == 2) {
        return;
    }
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
    } else if (buttonIndex==1){
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
