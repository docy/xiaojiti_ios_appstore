//
//  FixPasswordViewController.h
//  nc
//
//  Created by docy admin on 15/10/20.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FixPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTF;
@property (weak, nonatomic) IBOutlet UITextField *newpass;


@property (weak, nonatomic) IBOutlet UIButton *Localized_submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *Localized_forgetBtn;

- (IBAction)onFixPasswordViewTiJiaoBtnClick:(id)sender;
- (IBAction)onResetPasswordBtnClick:(id)sender;

@end
