//
//  PersonalProfileModifyInfoViewController.h
//  nc
//
//  Created by docy admin on 15/8/27.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^modifyBlock)(NSString *);

@interface PersonalProfileModifyInfoViewController : UIViewController

@property (nonatomic, copy) NSString *modifyText;
@property (nonatomic, copy) NSString *itemTitle;
@property (weak, nonatomic) IBOutlet UITextField *modifyTextField;

@property (nonatomic, strong) modifyBlock modifyBlock;
@property (nonatomic, retain) NSNumber *groupId;
@property (nonatomic, assign) BOOL isSex;  //是否为性别修改
@property (nonatomic, assign) BOOL isSexMan;  //是否原来性别为男
@property (nonatomic, assign) BOOL isPhone;  //是否为手机修改
@property (weak, nonatomic) IBOutlet UIView *sexView;

@end
