//
//  SwitchCompanyViewController.m
//  nc
//
//  Created by docy admin on 7/3/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SwitchCompanyViewController.h"
#import "CompanyModel.h"
#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "CompanyTableViewCell.h"
#import "ToolOfClass.h"
#import "SVProgressHUD.h"


@interface SwitchCompanyViewController ()
{
    NSNumber *_companyId; // 公司ID，设置当前公司用
}

@property (nonatomic, retain) NSMutableArray *companyList; // 公司列表

@end



@implementation SwitchCompanyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置基本属性
    [self setUpSwitchCompanyViewAttribute];
    
    // 获取用户的公司列表
    [self setUsersCompanyList];
}
// 设置基本属性
- (void)setUpSwitchCompanyViewAttribute
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.title = self.companyName;
    self.companyList = [NSMutableArray array];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onNavBackClick)];
}
- (void)onNavBackClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
// 获取用户的公司列表
- (void)setUsersCompanyList
{
    [self.companyList removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    [manager GET:userCompanyListPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                CompanyModel *model = [[CompanyModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                if ([self.companyName isEqualToString:model.name]) {
                    model.isCurrentCpy = YES;
                } else {
                    model.isCurrentCpy = NO;
                }
                [self.companyList addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.companyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellId";
    CompanyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CompanyTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    CompanyModel *model = self.companyList[indexPath.row];
    cell.companyNameLabel.text = model.name;
    
    [cell.setCurrentCpyBtn addTarget:self action:@selector(onSetCurrentCompanyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    if (model.isCurrentCpy) {
        cell.setCurrentCpyBtn.selected = YES;
    } else {
        cell.setCurrentCpyBtn.selected = NO;
    }
    cell.setCurrentCpyBtn.tag = 800+indexPath.row;
    
    return cell;
}
// cell的选择按钮点击事件
- (void)onSetCurrentCompanyBtnClick:(UIButton *)sender
{
    if (sender.selected) {
        return;
    }
    
    sender.selected = !sender.selected;
    for (NSInteger i = 0; i < self.companyList.count; i++) {
        CompanyModel *model = self.companyList[i];
        if (sender.tag-800==i) {
            model.isCurrentCpy = YES;
        } else {
            model.isCurrentCpy = NO;
        }
        
    }
    [self.tableView reloadData];

    CompanyModel *model = self.companyList[sender.tag-800];
    _companyId = model.id;
    //  发送设置为当前公司
    [self setUserCurrentCompany];

}
// 设置当前公司
- (void)setUserCurrentCompany
{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:@"正在切换公司..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    
    NSString *str = [NSString stringWithFormat:setCurrentCompany,_companyId];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [SVProgressHUD dismiss];
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            [info setObject:_companyId forKey:@"currentCompany"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
            sleep(1.0);
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"添加公司失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }];
}
@end
