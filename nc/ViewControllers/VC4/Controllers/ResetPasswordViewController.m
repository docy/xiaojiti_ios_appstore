//
//  ResetPasswordViewController.m
//  nc
//
//  Created by docy admin on 15/10/21.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import <AFNetworking/AFNetworking.h>
#import "SVProgressHUD.h"

@interface ResetPasswordViewController ()
@property (weak, nonatomic) IBOutlet UILabel *daojishiLabel;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalized(@"info_forgetPW_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(setUpResetPasswordViewBack)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpResetPasswordViewBack)];

    self.phoneTF.placeholder = NSLocalized(@"info_forgetPW_phone");
    self.identifyTF.placeholder = NSLocalized(@"info_forgetPW_code");
    self.passwordTF.placeholder = NSLocalized(@"info_forgetPW_input");
    
//    [self.identifyBtn setTitle:NSLocalized(@"info_forgetPW_getCode") forState:UIControlStateNormal];
    self.daojishiLabel.text = NSLocalized(@"info_forgetPW_getCode");
    [self.submieBtn setTitle:NSLocalized(@"info_forgetPW_submitBtn") forState:UIControlStateNormal];
}

- (void)setUpResetPasswordViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)onGetUpidentifyBtnClick:(id)sender {
    
    if (self.phoneTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"info_forgetPW_needPhone")];
    } else {
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:NSLocalized(@"info_forgetPW_sendRequest")];
        _identifyBtn.enabled=NO;
        _identifyBtn.hidden=YES;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"phone"] = self.phoneTF.text;
        
        [manager POST:ResetPasswordSms parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"code"] intValue] == 200) {
                NSLog(@"***************邀请码为:%@",responseObject[@"data"][@"code"]);
                //                [self startTimeSecondsCountDown];
                [ToolOfClass toolStartTimeSecondsCountDown_uilabel:self.daojishiLabel];
                double delayInSeconds = 30.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    _identifyBtn.enabled=YES;
                    _identifyBtn.hidden=NO;
                });
                //                [ToolOfClass toolStartTimeSecondsCountDown:self.identifyCodeBtn];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                _identifyBtn.enabled=YES;
                _identifyBtn.hidden=NO;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            _identifyBtn.enabled=YES;
            _identifyBtn.hidden=NO;
        }];
    }
}

- (IBAction)onResetPassTiJiaoBtnClick:(id)sender {
    
    if (self.identifyTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"info_forgetPW_needCode")];
    } else if (self.passwordTF.text.length==0) {
        [ToolOfClass showMessage:NSLocalized(@"info_forgetPW_needPassword")];
    } else {
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:NSLocalized(@"info_forgetPW_sendRequest")];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        
        parameter[@"phone"] = self.phoneTF.text;
        parameter[@"code"] = self.identifyTF.text;
        parameter[@"password"] = self.passwordTF.text;
        
        [manager POST:ResetPassword parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"code"] intValue] == 200) {
                [ToolOfClass showMessage:NSLocalized(@"info_forgetPW_success")];
                [info setObject:self.passwordTF.text forKey:@"password"];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (IBAction)onSetUpSecureTextEntryBtnClicl:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    self.passwordTF.secureTextEntry = !self.passwordTF.secureTextEntry;
}


//-(void)resetPassStartTimeSecondsCountDown:(UIButton *)sender{
//    __block int timeout = 30; //倒计时时间
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
//    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
//    dispatch_source_set_event_handler(_timer, ^{
//        if(timeout<=0){ //倒计时结束，关闭
//            dispatch_source_cancel(_timer);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                [sender setTitle:@"发送验证码" forState:UIControlStateNormal];
//                sender.userInteractionEnabled = YES;
//            });
//        }else{
//            int seconds = timeout % 60;
//            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                //NSLog(@"____%@",strTime);
//                [UIView beginAnimations:nil context:nil];
//                [UIView setAnimationDuration:1];
//                [sender setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
//                [UIView commitAnimations];
//                sender.userInteractionEnabled = NO;
//            });
//            timeout--;
//        }
//    });
//    dispatch_resume(_timer);
//}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.passwordTF resignFirstResponder];
    [self.identifyTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
}

@end
