//
//  UserJionedGroupViewController.h
//  xjt
//
//  Created by docy admin on 7/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 用户加入的小组
@interface UserJionedGroupViewController : UITableViewController

@property (nonatomic, retain) NSNumber *topicId; // 要转发的topicId

@end
