//
//  ResetPasswordViewController.h
//  nc
//
//  Created by docy admin on 15/10/21.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *identifyTF;
@property (weak, nonatomic) IBOutlet UIButton *identifyBtn;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *submieBtn;

- (IBAction)onGetUpidentifyBtnClick:(id)sender;
- (IBAction)onResetPassTiJiaoBtnClick:(id)sender;

- (IBAction)onSetUpSecureTextEntryBtnClicl:(id)sender;


@end
