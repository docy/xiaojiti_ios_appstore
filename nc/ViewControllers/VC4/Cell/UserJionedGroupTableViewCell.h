//
//  UserJionedGroupTableViewCell.h
//  nc
//
//  Created by docy admin on 8/13/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 用户加入的群组cell
@interface UserJionedGroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *groupIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *groupUserCount;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lockIgView;

@end
