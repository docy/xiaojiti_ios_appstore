//
//  FooterView.h
//  nc
//
//  Created by docy admin on 6/1/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *footerLabel;

@end
