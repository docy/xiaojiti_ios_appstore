//
//  GroupDetailUserView.h
//  xjt
//
//  Created by docy admin on 7/29/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupDetailUserView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *nameButton;
@property (weak, nonatomic) IBOutlet UIImageView *crownIg;

@end
