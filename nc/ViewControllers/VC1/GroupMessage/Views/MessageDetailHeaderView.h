//
//  MessageDetailHeaderView.h
//  xjt
//
//  Created by docy admin on 7/24/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLEmojiLabel.h"
@interface MessageDetailHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *creatAtLabel;
@property (weak, nonatomic) IBOutlet MLEmojiLabel *descLabel;
@property (weak, nonatomic) IBOutlet MLEmojiLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *subImageView;


@property (weak, nonatomic) IBOutlet UIView *BtnBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *collectButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end
