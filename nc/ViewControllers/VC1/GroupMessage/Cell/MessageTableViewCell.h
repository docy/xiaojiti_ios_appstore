//
//  MessageTableViewCell.h
//  nc
//
//  Created by jianghuan on 4/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kAvatarSize 30.0
#define kMinimumHeight 50.0

@interface MessageTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel; 
@property (nonatomic, strong) UILabel *bodyLabel;
@property (nonatomic, strong) UIImageView *thumbnailView;
@property (nonatomic, strong) UIImageView *attachmentView; // 头像
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, readonly) BOOL needsPlaceholder;
@property (nonatomic) BOOL usedForMessage;

@end