//
//  MessVoteTableViewCell.h
//  nc
//
//  Created by guanxf on 15/9/9.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessVoteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *rowNum;
@property (weak, nonatomic) IBOutlet UILabel *DetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *VoteCount;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UIImageView *selectImg;

@property (weak, nonatomic) IBOutlet UIView *jinduBG;
@property (weak, nonatomic) IBOutlet UIView *jinduv;


@end
