//
//  XJTMessageTableViewCell.h
//  nc
//
//  Created by docy admin on 6/3/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLEmojiLabel.h"
@interface XJTMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *senderLabel; // 用户名
@property (weak, nonatomic) IBOutlet UILabel *themeLabel; // 主题
@property (weak, nonatomic) IBOutlet UILabel *responseLabel; // 回复

@property (weak, nonatomic) IBOutlet MLEmojiLabel *bodyLabel; // 内容
@property (weak, nonatomic) IBOutlet UILabel *urlBodyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatarImageView; // 用户头像

@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 显示时间

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView; // 左视图

@property (weak, nonatomic) IBOutlet UIImageView *rightImageView; // 右视图


@property (weak, nonatomic) IBOutlet UILabel *createdAtLabel; // 日期

@property (weak, nonatomic) IBOutlet UIImageView *subImageView; // 子话题图片

@property (weak, nonatomic) IBOutlet UIButton *detailButton;

@property (weak, nonatomic) IBOutlet UIView *voteView;
@property (weak, nonatomic) IBOutlet UIImageView *voteIconImage;
@property (weak, nonatomic) IBOutlet UILabel *voteLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailVoteLabel;  //


@property (weak, nonatomic) IBOutlet UIView *RecAudioView;
@property (weak, nonatomic) IBOutlet UIImageView *audioPowerImg;
@property (weak, nonatomic) IBOutlet UILabel *audioTimeL;
@property (weak, nonatomic) IBOutlet UIImageView *audioViewBG;
@property (weak, nonatomic) IBOutlet UIImageView *audioViewBg2;
@property (weak, nonatomic) IBOutlet UIButton *audio;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *witeActivityInd;
@property (weak, nonatomic) IBOutlet UIImageView *dotImage;


@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, readonly) BOOL needsPlaceholder;
@property (nonatomic) BOOL usedForMessage;

@property (weak, nonatomic) IBOutlet UIImageView *ganTanHao;

@end
