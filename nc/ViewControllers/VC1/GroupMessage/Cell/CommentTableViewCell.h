//
//  CommentTableViewCell.h
//  nc
//
//  Created by docy admin on 15/9/8.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLEmojiLabel.h"
@interface CommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet MLEmojiLabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *creatAtLabel;
@property (weak, nonatomic) IBOutlet UIImageView *comImageView;
@property (weak, nonatomic) IBOutlet UILabel *urlBodyLabe;

@property (weak, nonatomic) IBOutlet UIView *RecAudioView;
@property (weak, nonatomic) IBOutlet UIImageView *audioPowerImg;
@property (weak, nonatomic) IBOutlet UILabel *audioTimeL;
@property (weak, nonatomic) IBOutlet UIImageView *audioViewBG;
@property (weak, nonatomic) IBOutlet UIImageView *audioViewBg2;
@property (weak, nonatomic) IBOutlet UIButton *audio;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *witeActivityInd;
@property (weak, nonatomic) IBOutlet UIImageView *dotImage;

@end
