//
//  AddUserToGroupViewController.m
//  nc
//
//  Created by docy admin on 15/10/22.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "AddUserToGroupViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "UserModel.h"
#import "CompanyTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIButton+XJTButton.h"

@interface AddUserToGroupViewController ()

@property (nonatomic, retain) NSMutableArray *userData;
@property (nonatomic, retain) UIButton *countBtn;

@property (nonatomic, assign) int counts;

@end

@implementation AddUserToGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpAddUserToGroupViewData];
    
    if ([self.rightItem isEqualToString:NSLocalized(@"group_set_invite")]) { // 邀请成员
       [self getUpCompanyAllUser];
        
    } else { // 踢人
        [self.userData removeAllObjects];
        
        NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
        for (UserModel *model in self.currentUsers) {
            if ([num isEqualToNumber:model.id]) {
                [self.currentUsers removeObject:model];
                break;
            }
        }
        [self sortUserWith:self.currentUsers];
        [self.tableView reloadData];
    }
}

- (void)setUpAddUserToGroupViewData{

    self.navigationItem.title = NSLocalized(@"group_userManager_title");
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpAddUserToGroupViewback)];
    
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:@"发送" target:self action:@selector(onRightItemSendClick)];
    self.counts = 0;
    UIButton *button = [[UIButton alloc] init];
    button.frame = CGRectMake(0, 0, 70, 30);
    button.titleLabel.textAlignment = NSTextAlignmentRight;
    
    [button setTitle:self.rightItem forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onRightItemSendClick) forControlEvents:UIControlEventTouchUpInside];
    UIButton *countBtn = [UIButton buttonWithFrame:CGRectMake(70-14, 0, 14, 14) backgroundColor:[UIColor whiteColor] cornerRadius:7];
    [countBtn setTitleColor:[UIColor colorWithHue:72/255.0 saturation:193/255.0 brightness:168/255.0 alpha:1.0] forState:UIControlStateNormal];
    countBtn.hidden = YES;
    countBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    [button addSubview:countBtn];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.countBtn = countBtn;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.userData = [NSMutableArray array];
}

- (void)setUpAddUserToGroupViewback{
//    self.currentUsers = nil;
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 邀请，删除成员操作
- (void)onRightItemSendClick{

    NSMutableArray *toAdd = [NSMutableArray array];
    for (NSArray *arr in self.userData) {
        for (UserModel *model in arr) {
            if (model.cellIsSelected) {
                [toAdd addObject:model.id];
            }
        }
    }
    if (toAdd.count==0) {
        [ToolOfClass showMessage:NSLocalized(@"group_userManager_small")];
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        NSString *path = nil;

        if ([self.rightItem isEqualToString:NSLocalized(@"group_set_invite")]) {
            parameter[@"toAdd"] = toAdd;
            path = [NSString stringWithFormat:InviteJoinGroup, self.groupId];
        } else {
            parameter[@"userIds"] = toAdd;
            path = [NSString stringWithFormat:Group_KickUser, self.groupId];
        }
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            NSLog(@"%@",responseObject);
            if ([responseObject[@"code"] isEqualToNumber:@200]) {

                self.navigationItem.rightBarButtonItem.enabled = YES;
                if (self.viewblock) {
                    self.viewblock();
                }
                if ([self.rightItem isEqualToString:NSLocalized(@"group_set_invite")]) {
                    [ToolOfClass showMessage:NSLocalized(@"group_userManager_inviteSuc")];
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    [ToolOfClass showMessage:NSLocalized(@"group_userManager_delegateSuc")];
                    [self.navigationController popViewControllerAnimated:self.navigationController.viewControllers[2]];
                }
                
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.navigationItem.rightBarButtonItem.enabled = YES;
            [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
        }];
    }
}

- (void)getUpCompanyAllUser{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString * cid=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentCompany"];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager GET:[NSString stringWithFormat:GetUserList, cid] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            // 清空数据
            [self.userData removeAllObjects];
            NSMutableArray *peopleArray = [NSMutableArray array]; // 用来存储联系人
            NSArray * array=responseObject[@"data"];
            
            if (array.count == self.currentUsers.count) {
                ALERT_HOME(NSLocalized(@"HOME_alert_cueTitle"), NSLocalized(@"group_userManager_invite_noUser"));
                return ;
            }
            
            for (int i=0; i < array.count; i++) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:array[i]];
                model.cellIsSelected = NO;
                [peopleArray addObject:model];
            }
            
            // 过滤已加入小组的成员
            for (UserModel *user in self.currentUsers) {
                for (int i=0; i < peopleArray.count; i++) {
                    UserModel *model = peopleArray[i];
                    if ([user.id isEqualToNumber:model.id]) {
                        [peopleArray removeObject:model];
                        break;
                    }
                }
            }
            
            [self sortUserWith:peopleArray];
            
            // Sort data
//            UILocalizedIndexedCollation *theCollation = [UILocalizedIndexedCollation currentCollation];
//            for (UserModel *model in peopleArray) {
//                NSInteger sect = [theCollation sectionForObject:model
//                                        collationStringSelector:@selector(nickName)];
//                model.sectionNumber = sect;
//            }
//            
//            NSInteger highSection = [[theCollation sectionTitles] count];
//            NSMutableArray *sectionArrays = [NSMutableArray arrayWithCapacity:highSection];
//            
//            for (int i=0; i<=highSection; i++) {
//                NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
//                [sectionArrays addObject:sectionArray];
//            }
//            
//            for (UserModel *model in peopleArray) {
//                [(NSMutableArray *)[sectionArrays objectAtIndex:model.sectionNumber] addObject:model];
//            }
//            
//            // 排好序的联系人加入数组中
//            for (NSMutableArray *sectionArray in sectionArrays) {
//                int count = 0; // 统计name为nil的个数
//                for (UserModel *model in sectionArray) {
//                    if (model.name.length==0) {
//                        count++;
//                    }
//                }
//                NSArray *sortedSection = [NSArray array];
//                //        NSArray *sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(name)];
//                if (count >= 2) { // 有2个以上name为nil,
//                    sortedSection = [NSArray arrayWithArray:sectionArray];
//                } else {
//                    sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(nickName)];
//                }
//                [self.userData addObject:sortedSection];
//            }
            
            [self.tableView reloadData];
            
           
        }
//        else if([responseObject[@"code"] isEqualToNumber:@3]){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//        }
        else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
    }];

}
// 成员数组排序
- (NSArray *)sortUserWith:(NSArray *)peopleArray{
    
    UILocalizedIndexedCollation *theCollation = [UILocalizedIndexedCollation currentCollation];
    for (UserModel *model in peopleArray) {
        NSInteger sect = [theCollation sectionForObject:model
                                collationStringSelector:@selector(nickName)];
        model.sectionNumber = sect;
    }
    
    NSInteger highSection = [[theCollation sectionTitles] count];
    NSMutableArray *sectionArrays = [NSMutableArray arrayWithCapacity:highSection];
    
    for (int i=0; i<=highSection; i++) {
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        [sectionArrays addObject:sectionArray];
    }
    
    for (UserModel *model in peopleArray) {
        [(NSMutableArray *)[sectionArrays objectAtIndex:model.sectionNumber] addObject:model];
    }
    
    // 排好序的联系人加入数组中
    for (NSMutableArray *sectionArray in sectionArrays) {
        int count = 0; // 统计name为nil的个数
        for (UserModel *model in sectionArray) {
            if (model.name.length==0) {
                count++;
            }
        }
        NSArray *sortedSection = [NSArray array];
        //        NSArray *sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(name)];
        if (count >= 2) { // 有2个以上name为nil,
            sortedSection = [NSArray arrayWithArray:sectionArray];
        } else {
            sortedSection = [theCollation sortedArrayFromArray:sectionArray collationStringSelector:@selector(nickName)];
        }
        [self.userData addObject:sortedSection];
    }
    return self.userData;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.userData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.userData[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellId";
    CompanyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CompanyTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserModel *model = self.userData[indexPath.section][indexPath.row];
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.location==NSNotFound) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [cell.iconImView sd_setImageWithURL:iconUrl placeholderImage:nil];
    cell.companyNameLabel.text = model.nickName;
    
    if ([self.rightItem isEqualToString:NSLocalized(@"group_set_invite")]) { // 邀请不隐藏电话
        cell.phoneLabel.hidden = NO;
        NSString *originTel = [model.phone isEqual:[NSNull null]]?@"":model.phone;
        if (originTel.length>7) { //隐藏手机号中间四位
            NSString *tel = [originTel stringByReplacingCharactersInRange:NSMakeRange(3, 7) withString:@"****"];
            cell.phoneLabel.text =tel;
            
        } else {
            cell.phoneLabel.text =[model.phone isEqual:[NSNull null]]?@"":model.phone;
        }
    } else {
        cell.phoneLabel.hidden = YES; // 踢人隐藏电话
    }
    
    CGRect rect = cell.line.frame;
    rect.size.height = 0.5;
    cell.line.frame = rect;
    
    if (indexPath.row==[self.userData[indexPath.section] count]-1) {
        cell.line.hidden = YES;
    } else {
        cell.line.hidden = NO;
    }
    
    
    if (model.cellIsSelected) {
        cell.setCurrentCpyBtn.selected = YES;
    } else {
        cell.setCurrentCpyBtn.selected = NO;
    }
    
    cell.setCurrentCpyBtn.tag = 100*indexPath.section+indexPath.row;
    
    [cell.setCurrentCpyBtn addTarget:self action:@selector(onSelectedUserBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString *title = [self.userData[section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section]:nil;
    return title;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)  indexPath{
    CompanyTableViewCell *cell = (CompanyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.setCurrentCpyBtn.selected = !cell.setCurrentCpyBtn.selected;
    UserModel *model = self.userData[indexPath.section][indexPath.row];
    if (cell.setCurrentCpyBtn.selected) {
        model.cellIsSelected = YES;
        self.counts++;
        
    } else {
        model.cellIsSelected = NO;
        self.counts--;
    }
    [self.countBtn setTitle:[NSString stringWithFormat:@"%d",self.counts] forState:UIControlStateNormal];
    if (self.counts==0) {
        self.countBtn.hidden = YES;
    } else {
        self.countBtn.hidden = NO;
    }
    
}

- (void)onSelectedUserBtnClick:(UIButton *)sender{
    
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag%100 inSection:sender.tag/100]];
}

@end
