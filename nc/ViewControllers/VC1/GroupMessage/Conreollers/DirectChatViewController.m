//
//  DirectChatViewController.m
//  nc
//
//  Created by docy admin on 15/9/19.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "DirectChatViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "MessageModel.h"
#import "XJTMessageTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "MJRefresh.h"
#import "NSString+XJTString.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "Map_ViewController.h"
#import "SVProgressHUD.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "ShowPictureViewController.h"
#import "AFNHttpRequest.h"
#import "MLEmojiLabel.h"
#import "AudioSession.h"
#import "UIColor+Hex.h"
#import "MessageFouncationView.h"
#import "UIButton+XJTButton.h"

#define defaultY 16

@interface DirectChatViewController () <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MLEmojiLabelDelegate,TTTAttributedLabelDelegate,AudioSessionDelegate>{
    UIToolbar * audioInputbar;   //录音功能toolbar
    UIView * recorderView;       //录音view
    UIImageView * audioPowerImgV;    //显示录音强度
    AudioSession * audioSession;    //语音类
    UILabel * alertL;           //提示录音操作
    UIImageView * RecImagev;     //录音image
    UIImageView * cancelRecImage;//取消录音image
    NSInteger AudioPlayerIsNum;  //当前播放的第几个cell的音频
    NSTimeInterval StartRecTime;         //录音开始时间
    UIButton * button;          //录音button
    BOOL RecTimeOut;            //录音超时
}

@property (nonatomic, retain) NSMutableArray *chatMessage;
@property (nonatomic, assign) CGFloat cellHeight; // cell的高度(自动计算)
@property (nonatomic, assign) int chatPage;
@property (nonatomic, copy) NSString *lastTimeString;

@property (nonatomic, strong) UIButton * buttonn;  //全局button 主要存储button tag  支持语音连续播放功能
@property (nonatomic, assign) BOOL isStopAutoAudioPlay;     //停止自动播放音频

@property (nonatomic,retain) UIImageView *bgImageView;
@property (nonatomic, retain) MessageFouncationView *founcationView; // topicView
@property (nonatomic, strong) UIButton * rebutt; //弹出菜单button；
@property (nonatomic, retain) UIButton *fcButton; // topic功能键

@end

@implementation DirectChatViewController

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [audioSession.audioPlayer stop];
    audioSession.timer2.fireDate=[NSDate distantFuture];
    audioSession.audioPlayer=nil;
    audioSession=nil;
//    [self audioPlayerDidFinishPlaying];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [self setUpDirectChatViewData];
    [self setUpDirectChatViewMJRefresh];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    self.tableView.hidden=YES;
    if (self.groupId!=nil) {
        self.chatPage = 1;
        [self getUpDirectChatViewChatListWithGroupId:self.groupId];
    } else {
        [self getUpDirectChatViewChatMessage];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpDirectChatViewNewMessage:) name:@"newMessage" object:nil];
    
    // Keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowOrHideKeyboard:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowOrHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideKeyboard:) name:UIKeyboardDidHideNotification object:nil];
    
    //添加录音相关
    audioSession = [AudioSession ShareAAudioSession];
    audioSession.delegate=self;
    [audioSession setAudioSession];
    RecTimeOut=NO;
    _isStopAutoAudioPlay=YES;
    [self initAudioInputBar];
    [self addRecorderView];
    
}

-(void)didShowKeyboard:(NSNotification*)notifi{
    
}

-(void)didHideKeyboard:(NSNotification*)notifi{
    
}

//关闭tableview reloaddata 时全屏刷动画的问题
- (void)textDidUpdate:(BOOL)animated
{
    // Notifies the view controller that the text did update.
    [super textDidUpdate:NO];//animated];
}

#pragma mark - 添加音频相关内容
-(void)initAudioInputBar{
    audioInputbar=[[UIToolbar alloc] init];
    float yyy=ScreenHeight-64-44;
    if (iPhone6Plus) {
        yyy=ScreenHeight-64-42.5;
    }
    audioInputbar.frame=CGRectMake(0, yyy, ScreenWidth, 40);
    NSMutableArray *myToolBarItems = [NSMutableArray array];
    
    UIBarButtonItem * audioBarbutton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"The keyboard"] style:UIBarButtonItemStyleDone target:self action:@selector(audioBarbuttonClick)];
    [audioBarbutton setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    
    UIButton * leftButton=[[UIButton alloc] initWithFrame:CGRectMake(3, 5, 34, 34)];
    [leftButton setImage:[UIImage imageNamed:@"The keyboard"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(audioBarbuttonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * leftBarItem=[[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [myToolBarItems addObject:leftBarItem];
    
    button=[[UIButton alloc] initWithFrame:CGRectMake(40, 5, ScreenWidth-40-10, 33)];
    [button setBackgroundColor:[UIColor whiteColor]];
    //    [button setImage:[UIImage imageNamed:@"鼠标抬起的颜色图片"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"butn_bg"] forState:UIControlStateHighlighted];
    [button setTitle:NSLocalized(@"group_chat_voice_downSpeak") forState:UIControlStateNormal];
    [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithHex:0x999999] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithHex:0xFFFFFF] forState:UIControlStateHighlighted];//colorWithHex:0xFFFFFF
    [button addTarget:self action:@selector(audioRecorderStart) forControlEvents:UIControlEventTouchDown];//开始录音
    [button addTarget:self action:@selector(audioRecorderSave) forControlEvents:UIControlEventTouchUpInside];//保存录音
    [button addTarget:self action:@selector(audioRecorderAlertCancel) forControlEvents:UIControlEventTouchDragExit];//提示取消录音
    [button addTarget:self action:@selector(audioRecorderCancelRec) forControlEvents:UIControlEventTouchUpOutside];//取消录音
    [button addTarget:self action:@selector(audioRecorderRecGoOn) forControlEvents:UIControlEventTouchDragEnter];//上滑又回到button恢复录音
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 3.0;
    button.layer.borderWidth = 0.5;
    button.layer.borderColor =[[UIColor colorWithHex:0xAAAAAA] CGColor];
    [button setExclusiveTouch:YES];
    UIBarButtonItem * recBarItem=[[UIBarButtonItem alloc] initWithCustomView:button];
    
    [myToolBarItems addObject:recBarItem];
    
    [audioInputbar addSubview:leftButton];
    [audioInputbar addSubview:button];
    //    [audioInputbar setItems:myToolBarItems];
    audioInputbar.hidden=YES;
    [self.view addSubview:audioInputbar];
}

//toolbar返回切换
-(void)audioBarbuttonClick{
    audioInputbar.hidden=YES;
}

-(void)audioRecorderStart{
//    NSLog(@"开始录音");
    if (![audioSession.audioRecorder isRecording]) {
        [audioSession.audioRecorder record];//首次使用应用时如果调用record方法会询问用户是否允许使用麦克风
        StartRecTime = [[NSDate date] timeIntervalSince1970];
//        NSLog(@"StartRecTime is %f",StartRecTime);
        audioSession.timer.fireDate=[NSDate distantPast];
        RecTimeOut=NO;
    }
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
    recorderView.hidden=NO;
}
-(void)audioRecorderSave{
    [audioSession.audioRecorder stop];
    double stopRecTime = [[NSDate date] timeIntervalSince1970];
    double dur=stopRecTime-StartRecTime;
    if (dur<0.30) {
        [self audioRecorderCancelRec];
        [ToolOfClass showMessage:NSLocalized(@"group_chat_voice_time_short")];
        return;
    }
    int duranton=dur<1?1:dur;
//    NSLog(@"StopRecTime is %d",duranton);
    audioSession.audioRecorder=nil;
    audioSession.timer.fireDate=[NSDate distantFuture];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    [audioPowerImgV setImage:[UIImage imageNamed:@"recording1"]];
    recorderView.hidden=YES;
    if (RecTimeOut) {
        //        [button setTitle:@"录音已结束,请抬起按钮" forState:UIControlStateHighlighted];
        [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
        return; //超时退出；
    }
    [self SendRecAudioWithDuration:duranton];
    NSLog(@"保存并发送录音");
}
-(void)audioRecorderAlertCancel{
    NSLog(@"提示取消录音");
    RecImagev.hidden=YES;
    audioPowerImgV.hidden=YES;
    cancelRecImage.hidden=NO;
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    alertL.backgroundColor=[UIColor colorWithHex:0xFF001F alpha:0.6];
}
-(void)audioRecorderCancelRec{
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [audioSession.audioRecorder stop];
    audioSession.audioRecorder=nil;
    audioSession.timer.fireDate=[NSDate distantFuture];
    alertL.backgroundColor=[UIColor clearColor];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    recorderView.hidden=YES;
    NSLog(@"取消录音");
}
-(void)audioRecorderRecGoOn{
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    alertL.backgroundColor=[UIColor clearColor];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    NSLog(@"上滑又回到button恢复录音");
}

-(void)addRecorderView{
    recorderView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 160)];
    recorderView.center=CGPointMake(ScreenWidth/2, ScreenHeight/2-80);
    recorderView.backgroundColor=[UIColor colorWithHex:0x000000 alpha:0.7];
    recorderView.layer.masksToBounds = YES;
    recorderView.layer.cornerRadius = 6.0;
    recorderView.layer.borderWidth = 1.0;
    recorderView.layer.borderColor = [[UIColor grayColor] CGColor];
    
    RecImagev=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paly"]];
    RecImagev.frame=CGRectMake(30, 35, 50, 80);
    [recorderView addSubview:RecImagev];
    
    audioPowerImgV=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recording1"]];
    audioPowerImgV.frame=CGRectMake(95, 35, 40, 80);
    [recorderView addSubview:audioPowerImgV];
    
    cancelRecImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cancel copy"]];
    cancelRecImage.frame=CGRectMake(50, 33, 50, 70);
    [recorderView addSubview:cancelRecImage];
    cancelRecImage.hidden=YES;
    
    alertL=[[UILabel alloc] initWithFrame:CGRectMake(10, 125, 140, 24)];
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [alertL setFont:[UIFont fontWithName:@"Heiti SC" size:15.0]];
    alertL.textColor=[UIColor whiteColor];
    alertL.textAlignment=NSTextAlignmentCenter;
    alertL.layer.masksToBounds=YES;
    alertL.layer.cornerRadius = 4.0;
    [recorderView addSubview:alertL];
    [self.view addSubview:recorderView];
    recorderView.hidden=YES;
    
    //    StartRecTim is 1451556208765.600830
    //StopRecTime is 1451556213570.474121
}

#pragma mark -AudioSessionDelegate
-(void)AudioRecorderProgress:(float)progress andRecDuration:(int)duration{
    int number=1;
    if (progress>0.91) {
        number=6;
    }else if (progress>0.87) {
        number=5;
    }else if (progress>0.83) {
        number=4;
    }else if (progress>0.79) {
        number=3;
    }else if (progress>0.75) {
        number=2;
    }else {
        number=1;
    }
//    NSLog(@"RecDurationRecDurationRecDuration::::::%d",duration);
    NSString * imageName=[NSString stringWithFormat:@"recording%d",number];
    [audioPowerImgV setImage:[UIImage imageNamed:imageName]];
    
    if (duration>=durationTime-100) {
        alertL.text=[NSString stringWithFormat:NSLocalized(@"group_chat_voice_canVoice"),(durationTime-duration)/10];
        alertL.backgroundColor=[UIColor clearColor];
        if (duration>durationTime) {  //录音超时了
            [self audioRecorderSave];
            RecTimeOut=YES;
            [button setTitle:NSLocalized(@"group_chat_voice_over") forState:UIControlStateHighlighted];
        }
    }
    
}

-(void)AudioPlayerProgress:(float)progress andDuration:(float)duration{
    
    if (duration==0.0) {
        [self audioPlayerDidFinishPlaying];
        return;
    }
    if (_chatMessage.count==0) {
        return;
    }
    MessageModel *messageModel = _chatMessage[AudioPlayerIsNum];
    messageModel.volumeAnimatedNum=progress+1;
    messageModel.duration=duration;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    //    NSLog(@"AudioPlayerProgress--- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
    if (array==nil) {
        return;
    }
    [self.tableView reloadData];
    //    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
}

-(void)SendRecAudioWithDuration:(int)duration{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sending")];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    parameter[@"duration"]=[NSNumber numberWithInt:duration];
    NSString * string=[[audioSession getSavePath] absoluteString];
    string = [string stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    
    NSData *audioData = [NSData dataWithContentsOfFile:string];
    [manager POST:directChatSendAudio parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:audioData name:@"audio" fileName:@"audioName" mimeType:@"audio/aac"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sendFail")];
            [SVProgressHUD dismissWithDelay:1.0];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)audioPlayerDidFinishPlaying{
    [audioSession.audioPlayer stop];
    //    audioSession.audioPlayer=nil;
    audioSession.timer2.fireDate=[NSDate distantFuture];
    if (_chatMessage.count==0) {
        return;
    }
    MessageModel *messageModel = _chatMessage[AudioPlayerIsNum];
    messageModel.volumeAnimatedNum=3;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
    //    NSLog(@"audioPlayerDidFinishPlaying--------- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    [self.tableView reloadData];
    //    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
    
    //添加未读语音自动播放功能
    if (AudioPlayerIsNum==0) {
        return;
    }
    MessageModel *messageModel2 =_chatMessage[AudioPlayerIsNum-1];
    if ([messageModel2.type isEqualToNumber:@15]) {
        if (messageModel2.readed!=YES) {//||messageModel2.info[@"readed"]==NULL
            if (_isStopAutoAudioPlay==NO) {
                _isStopAutoAudioPlay=YES;
            }else{
                if (!_buttonn) {
                    _buttonn=[[UIButton alloc] init];
                }
                AudioPlayerIsNum--;
                _buttonn.tag=AudioPlayerIsNum;
                [self AudioPlay:_buttonn];
            }
        }
    }
}

-(void)AudioPlay:(UIButton*)button{
    BOOL xiangtongCell=NO;
    
    if (AudioPlayerIsNum!=button.tag) {
        xiangtongCell=NO;
    }else{
        xiangtongCell=YES;
    }
    
    AudioPlayerIsNum=button.tag;
    MessageModel *model = _chatMessage[button.tag];
    
    if ([audioSession.audioPlayer isPlaying]) {
        if (xiangtongCell) {
            _isStopAutoAudioPlay=NO;
            [self audioPlayerDidFinishPlaying];
            return;
        }else{
            [self audioPlayerDidFinishPlaying];
        }
        
    }
    
    
    MessageModel *messageModel = _chatMessage[AudioPlayerIsNum];
    messageModel.witeActivityIsShow=YES;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
    //    NSLog(@"audioPlayerDidFinishPlaying--------- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    [self.tableView reloadData];
    //    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
    
    
    
    NSLog(@"播放语音");
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString * Inbox=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents"]];
    if(![fileManager fileExistsAtPath:Inbox]){
        BOOL bo=[fileManager createDirectoryAtPath:Inbox withIntermediateDirectories:YES attributes:nil error:nil];
        NSLog(@"bobobobo is %d",bo);
    }
    
    NSString *savedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",@"audioName.m4a"]];
//    NSLog(@"filepath_dow---%@",savedPath);
    NSMutableString * filep=[NSMutableString stringWithFormat:@"%@%@",iconPath,model.info[@"audioPath"]];
    //    NSRange range=[filep rangeOfString:@".aac"];
    //    if (range.length==4) {
    //        [filep replaceCharactersInRange:range withString:@""];
    //    }
    //    NSLog(@"filep-filep--%@",filep);
    [fileManager removeItemAtPath:savedPath error:nil];  //删除文件
    //    if(![fileManager fileExistsAtPath:savedPath]) //如果不存在 下载
    //    {
    [self downloadFileWithOption:nil withInferface:filep savedPath:savedPath downloadSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    } downloadFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    } progress:^(float progress) {
        
    }];
    
    model.readed=YES;
    _chatMessage[AudioPlayerIsNum]=messageModel;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString * path=[NSString stringWithFormat:MarkAudioReaded,messageModel.info[@"audioId"]];
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 发送成功
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    //    }
}

- (void)downloadFileWithOption:(NSDictionary *)paramDic
                 withInferface:(NSString*)requestURL
                     savedPath:(NSString*)savedPath
               downloadSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
               downloadFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
                      progress:(void (^)(float progress))progress
{
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    NSMutableURLRequest *request =[serializer requestWithMethod:@"GET" URLString:requestURL parameters:paramDic error:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    //    [serializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:savedPath append:NO]];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float p = (float)totalBytesRead / totalBytesExpectedToRead;
        progress(p);
        //        NSLog(@"totalBytesExpectedToRead：%f",  (float)totalBytesExpectedToRead);//(float)totalBytesRead /
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        NSLog(@"语音下载成功");
        MessageModel *messageModel = _chatMessage[AudioPlayerIsNum];
        messageModel.witeActivityIsShow=NO;
        NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
        NSArray * array=[NSArray arrayWithObjects:indexp, nil];
        [self.tableView reloadData];
        audioSession.audioPlayer=nil;
        if (![audioSession.audioPlayer isPlaying]) {
            [audioSession.audioPlayer play];
            audioSession.timer2.fireDate=[NSDate distantPast];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        success(operation,error);
        
        NSLog(@"语音下载失败");
        
    }];
    
    [operation start];
}

-(UIButton*)rebutt{
    if (!_rebutt) {
        _rebutt=[[UIButton alloc] init];
    }
    return _rebutt;
}

#pragma mark -  设置功能菜单
- (void)setUpDirectChatViewData{
    
    self.chatMessage = [NSMutableArray array];
    self.cellHeight = 0;
    self.chatPage = 1;

    self.navigationItem.title = self.groupName;
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(DirectChatViewBack)];
    
    [self.rightButton setTitle:NSLocalized(@"group_chat_private_sendBtn") forState:UIControlStateNormal];//NSLocalizedString(@"发送      ", nil)
    UIImage *image = [UIImage imageNamed:@"plus sign_icon"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIButton * rebutt=[[UIButton alloc] init];
    [self.rebutt setBackgroundImage:image forState:UIControlStateNormal];
    [self.rebutt addTarget:self action:@selector(rightButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.rebutt.frame=CGRectMake(ScreenWidth-32, 8, 27, 27);//self.textInputbar.rightButton.frame;
    _rebutt.translatesAutoresizingMaskIntoConstraints=NO;
    [self.textInputbar addSubview:self.rebutt];
    NSDictionary *views = @{@"_rebutt": self.rebutt,};
    NSDictionary *metrics = @{@"up": @8,@"down": @8,};
    [self.textInputbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_rebutt(27)]-down-|" options:0 metrics:metrics views:views]];
    [self.textInputbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_rebutt(27)]-7-|" options:0 metrics:metrics views:views]];
    
    self.textInputbar.textView.returnKeyType=UIReturnKeySend;
//    [self.textInputbar.rightButton setImage:image forState:UIControlStateNormal];
    
    // 添加灰色背景
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    bgImageView.backgroundColor = [UIColor blackColor];
    bgImageView.alpha = 0.5;
    bgImageView.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageViewTapGestureClick:)];
    bgImageView.userInteractionEnabled = YES;
    [bgImageView addGestureRecognizer:tap];
    self.bgImageView = bgImageView;
    [self.view addSubview:bgImageView];
    
    // 添加topic按钮
    UIButton *fcButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect rect = CGRectMake(self.view.frame.size.width-16-60, self.view.frame.size.height-50-60-64, 60, 60);
    [fcButton setNormalImage:@"add.png" andSelectedImage:@"back.png" andFrame:rect addTarget:self action:@selector(onFouncationBtnClick:)];
    //    [self.view addSubview:fcButton];
    self.fcButton = fcButton;
    
    MessageFouncationView *founcationView = [[[NSBundle mainBundle] loadNibNamed:@"MessageFouncationView" owner:self options:nil] lastObject];
    founcationView.frame =CGRectMake(0, ScreenHeight+founcationView.frame.size.height, ScreenWidth, 208);//founcationView.frame.size.height);
    founcationView.hidden = YES;
    //    founcationView.Localized_file.text = NSLocalized(@"topic_file");
    founcationView.Localized_map.text = NSLocalized(@"topic_map");
    //    founcationView.Localized_vote.text = NSLocalized(@"topic_vote");
    founcationView.Localized_topic.text = NSLocalized(@"topic_topic");
    founcationView.Localized_image.text = NSLocalized(@"topic_image");
    for (UIView *view in founcationView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn addTarget:self action:@selector(onAddFouncationButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    self.founcationView = founcationView;
    [self.view addSubview:founcationView];
    
    UIImage *image2 = [UIImage imageNamed:@"The recording"];
    image2 = [image2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.textInputbar.leftButton setImage:image2 forState:UIControlStateNormal];
    self.textInputbar.autoHideRightButton = NO;
    self.textInputbar.counterStyle = SLKCounterStyleNone;
//    self.textInputbar.counterPosition = SLKCounterPositionBottom;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.automaticallyAdjustsScrollViewInsets = YES;
 
}


#pragma mark 弹出菜单 文件 地图，图片，主题等功能键的点击事件
- (void)onAddFouncationButtonClick:(UIButton *)sender{
    [self onFouncationBtnClick:nil];
    if (sender.tag == 0) {
        //        VoteViewController *voteVC = [[VoteViewController alloc] init];
        //        voteVC.groupId=self.groupId;
        //        [self.navigationController pushViewController:voteVC animated:YES];
    } else if (sender.tag == 1) {
        
        // block 反向传值
        //        MapViewController *mapVC = [[MapViewController alloc] init];
        
//        UIAlertView * alertv=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"暂不支持该功能" delegate:self cancelButtonTitle:@"知道啦" otherButtonTitles:nil, nil];
//        [alertv show];
        
        Map_ViewController *mapVC = [[Map_ViewController alloc] init];
        mapVC.isFromPrivateVC = YES;
        mapVC.groupId = self.groupId;
        [self.navigationController pushViewController:mapVC animated:YES];
        
    } else if (sender.tag == 2) {
        //文件子话题
        //        FileListViewController * filevC=[[FileListViewController alloc] initWithNibName:@"FileListViewController" bundle:nil];
        //        filevC.groupId=self.groupId;
        //        [self.navigationController pushViewController:filevC animated:YES];
    }else if (sender.tag == 3) {
        
        //        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
        //        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];//self.view];
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //            picker.allowsEditing = YES;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
        
    } else if (sender.tag == 4) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
        
        //        MainThemeViewController *mainVC = [[MainThemeViewController alloc] init];
        //        mainVC.groupId = self.groupId;
        //        [self.navigationController pushViewController:mainVC animated:YES];
    }else if (sender.tag == 8) {
        [self onFouncationBtnClick:_rebutt];
    }else{
        
    }
}

// 功能键的点击触发方法
- (void)onFouncationBtnClick:(UIButton *)sender{
    [self.textView resignFirstResponder];
    [self.textInputbar.textView resignFirstResponder];
    
    sender.selected = !sender.selected;//YES;//
    
    [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        if (sender.selected) {
            self.founcationView.hidden = NO;
            self.bgImageView.hidden = NO;
            //            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGFloat founcationY = ScreenHeight - 64 -_founcationView.frame.size.height;
            CGRect point1=self.founcationView.frame;
            point1.origin.y=founcationY;//-=self.founcationView.center.x<160?0:280;
            point1.origin.x=0;
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.5;
        } else {
            //            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGFloat founcationY = ScreenHeight - _founcationView.bounds.size.height;//CGRectGetMaxY(_fcButton.frame)
            CGRect point1=self.founcationView.frame;
            point1.origin.y=founcationY +_founcationView.bounds.size.height;//+280;
            point1.origin.x=0;
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        if (sender.selected) {
            
        } else {
            self.founcationView.hidden = YES;
            self.bgImageView.hidden = YES;
            
        }
    }];
    
}

- (void)onMessageViewTapGestureClick:(UITapGestureRecognizer *)gesture{
    [self onFouncationBtnClick:_rebutt];
}

-(void)rightButtonClick:(UIButton*)sender{
//    [self didPressRightButton:sender];
    [self.textView resignFirstResponder];
    
    [self onFouncationBtnClick:_rebutt];
    
//    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"group_chat_private_picker"),NSLocalized(@"group_chat_private_map"), nil];
//    action.tag = 100;
//    [action showInView:self.view];
}

- (void)setUpDirectChatViewMJRefresh{
    
    [self.tableView addFooterWithCallback:^{
        [self getUpDirectChatViewChatListWithGroupId:self.groupId];
    }];
    
}
- (void)DirectChatViewBack{
    if (self.cellBlock) {
        self.cellBlock();
    }
    
    if (self.navigationController.viewControllers.count>2) {
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeUnReadConutPriGroup" object:self.groupId];
    }
    
    [AFNHttpRequest httpRequestResetUnreadCountWithGroupId:self.groupId];
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.groupId = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)getUpDirectChatViewChatMessage{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *parameter = [NSDictionary dictionaryWithObjects:@[[ToolOfClass authToken],self.directChatId] forKeys:@[@"accessToken",@"targetId"]];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"targetId"] = self.directChatId;
    
    [manager POST:directChat parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            self.groupId = responseObject[@"data"][@"id"];
            [self getUpDirectChatViewChatListWithGroupId:self.groupId];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.chatMessage.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"MessengerCellId";
    
    XJTMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell==nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"XJTMessageTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.responseLabel.hidden = YES;
    cell.themeLabel.hidden = YES;
    cell.detailButton.hidden = YES;
    cell.voteView.hidden = YES;
    MessageModel *model = self.chatMessage[indexPath.row];
    
    cell.senderLabel.text = model.nickName;
    
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.length==0) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [cell.userAvatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
    
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconTapGestureClick:)];
    cell.userAvatarImageView.tag = indexPath.row;
    cell.userAvatarImageView.userInteractionEnabled = YES;
    [cell.userAvatarImageView addGestureRecognizer:userIconTap];
    
    cell.createdAtLabel.text = [ToolOfClass toolGetLocalAmDateFormateWithUTCDate:model.updatedAt];
    
    CGRect avatarFrame = cell.userAvatarImageView.frame;
    CGRect senderFrame = cell.senderLabel.frame;
    CGRect creatAtFrame = cell.createdAtLabel.frame;
    
    CGRect leftViewFrame = cell.leftImageView.frame;
    if (model.flag==0) { // 显示时间框
        cell.timeView.hidden = NO;
        avatarFrame.origin.y = CGRectGetMaxY(cell.timeView.frame);
        senderFrame.origin.y = CGRectGetMaxY(cell.timeView.frame);
        creatAtFrame.origin.y = CGRectGetMaxY(cell.timeView.frame);
        
        cell.timeLabel.text = model.timeViewStr;
        if ([cell.timeLabel.text isEqualToString:NSLocalized(@"group_chat_private_today")]) {
            leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-30-32-20)/2.0;
        } else {
            leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-150-32-20)/2.0;
        }
        CGPoint timeCenter = cell.timeLabel.center;
        CGRect rightViewFrame = cell.rightImageView.frame;
        
        timeCenter.x = cell.timeView.center.x;
        cell.timeLabel.center = timeCenter;
        
        cell.leftImageView.frame = leftViewFrame;
        rightViewFrame.size.width = cell.leftImageView.frame.size.width;
        rightViewFrame.origin.x = self.view.frame.size.width - cell.leftImageView.frame.size.width-16;
        cell.rightImageView.frame = rightViewFrame;
        
        if ([model.type isEqualToNumber:@16]) {
            cell.timeView.hidden = YES;
        }
    } else {
        cell.timeView.hidden = YES;
        avatarFrame.origin.y = defaultY;
        senderFrame.origin.y = defaultY;
        creatAtFrame.origin.y = defaultY;
    }
    
    cell.userAvatarImageView.frame = avatarFrame;
    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = ceil(senderSize.width)+10;
    
    if (senderFrame.size.width>200) {
        senderFrame.size.width = 200;
    }
    
    cell.senderLabel.frame = senderFrame;
    creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
    cell.createdAtLabel.frame = creatAtFrame;
    
    
    
    NSDictionary *info = model.info;
    
    CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - defaultY;
    
    CGRect bodyFrame = cell.bodyLabel.frame;
    bodyFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
    
//    if ([model.type isEqualToNumber:@2] ||[model.type isEqualToNumber:@3]) {
//        cell.bodyLabel.hidden = NO;
//        cell.subImageView.hidden = YES;
//        cell.bodyLabel.text = [NSString stringWithFormat:@"欢迎%@加入小组",model.nickName];
//        bodyFrame.size.height = 21;
//        
//        cell.bodyLabel.frame = bodyFrame;
//        self.cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
//    } else
    cell.bodyLabel.emojiDelegate=self;
    if ([model.type isEqualToNumber:@1]||[model.type isEqualToNumber:@15]||[model.type isEqualToNumber:@16]) {
        cell.bodyLabel.hidden = NO;
        cell.subImageView.hidden = YES;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
//        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
//        cell.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:info[@"message"] attributes:attributes];
        [cell.bodyLabel setEmojiText:info[@"message"]];
        CGRect bodyRect = CGRectMake(0, 0, 0, 0);
        bodyRect = [cell.bodyLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        bodyFrame.size.height = ceil(bodyRect.size.height);
        bodyFrame.size.width = width;
        cell.bodyLabel.frame = bodyFrame;
        self.cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
        /**
         *  定制urlBodyLabel链接预加载内容
         */
        if ([model.type isEqualToNumber:@16]){
//            NSLog(@"---------Message:%@",model.info[@"content"]);
            cell.urlBodyLabel.hidden=NO;
            CGRect frame2 = cell.urlBodyLabel.frame;
            frame2.size.width = width;
            cell.urlBodyLabel.text = info[@"content"];
            frame2.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
            CGRect ubodyFrame = CGRectMake(0, 0, 0, 0);
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
            ubodyFrame = [cell.urlBodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
            frame2.size.height = ceil(ubodyFrame.size.height)+23;
            cell.urlBodyLabel.frame = frame2;
            self.cellHeight = CGRectGetMaxY(cell.urlBodyLabel.frame);
        }else{
            cell.urlBodyLabel.hidden=YES;
        }
        
        
    } else { // type=12,13
        cell.bodyLabel.hidden = YES;
        cell.bodyLabel.attributedText = nil;
        cell.subImageView.hidden = NO;
        cell.subImageView.userInteractionEnabled = YES;
        CGRect subIgFrame = cell.subImageView.frame;
        subIgFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        
        CGFloat subW = [model.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [model.info[@"thumbNail_h"] floatValue];
        if (subW>subH) {
            subH = 180*(subH/subW);
            subW = 180;
            
        } else {
            subW = 180*(subW/subH);
            subH = 180;
        }
        subIgFrame.size.width = subW;
        subIgFrame.size.height = subH;
        
        cell.subImageView.frame = subIgFrame;
        [cell.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSubImageViewTapClick:)];
        cell.subImageView.tag = indexPath.row;
        [cell.subImageView addGestureRecognizer:tap];
        
        // 存储高度，用于返回cell的高度
        self.cellHeight = CGRectGetMaxY(cell.subImageView.frame)+5;
        
    }
    
    /*
     定制语音消息
     */
    CGFloat cellHeightt;
    if ([model.type isEqualToNumber:@15]){ //语音信息
        cell.RecAudioView.hidden=NO;
        CGFloat MaxWidth=ScreenWidth-65-40;
        width=100;
        
        float widthP=(MaxWidth-100)*([model.info[@"duration"] intValue]/60.0f);
        CGRect audioVFrame = cell.RecAudioView.frame;
        audioVFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        //        NSLog(@"width+widthPwidth+widthP ----:%ld------:%f and:%f",indexPath.row,width,widthP);
        audioVFrame.size.width = width+widthP;
        // 存储高度，用于返回cell的高度
        cell.RecAudioView.frame=audioVFrame;
        cellHeightt = CGRectGetMaxY(cell.RecAudioView.frame)+5;
        
        cell.audioTimeL.text=[NSString stringWithFormat:@"%d\"",[model.info[@"duration"] intValue]];//
        int nnn=model.volumeAnimatedNum;
//        if (indexPath.row!=AudioPlayerIsNum) {
//            nnn=3;
//        }
        [cell.audioPowerImg setImage:[UIImage imageNamed:[NSString stringWithFormat:@"volume%d",nnn]]];
        cell.audio.tag=indexPath.row;
        [cell.audio addTarget:self action:@selector(AudioPlay:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString * UserId=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
        if ([model.userId isEqualToNumber:[NSNumber numberWithInt:[UserId intValue]]]) {//自己发的语音
            [cell.audioViewBG setImage:[UIImage imageNamed:@"Voice box_gray"]];
            [cell.audioViewBg2 setImage:[UIImage imageNamed:@"The triangle_gray"]];
        }else{
            [cell.audioViewBG setImage:[UIImage imageNamed:@"Voice box"]];
            [cell.audioViewBg2 setImage:[UIImage imageNamed:@"The triangle"]];
        }
        
        if (model.witeActivityIsShow) {
            cell.witeActivityInd.hidden=NO;
            [cell.witeActivityInd startAnimating];
            cell.audioPowerImg.hidden=YES;
        }else{
            cell.witeActivityInd.hidden=YES;
            cell.audioPowerImg.hidden=NO;
        }
        
        if (model.info[@"readed"]!=NULL||model.readed==YES) {
            cell.dotImage.hidden=YES;
        }else{
            cell.dotImage.hidden=NO;
        }
        
        self.cellHeight = cellHeightt;
    }else{
        cell.RecAudioView.hidden=YES;
    }
    
//    [self.cellHeightArray insertObject:[NSNumber numberWithFloat:self.cellHeight] atIndex:indexPath.row];
    // 详情按钮的位置设置
    CGPoint detailBtnCenter = cell.detailButton.center;
    if (model.flag==0) { // 显示时间头
        detailBtnCenter.y = (self.cellHeight-45)/2.0 +45+8;
        if (model.info[@"link"]!=nil){//[messageModel.type isEqualToNumber:@16]){
            detailBtnCenter.y = self.cellHeight/2.0 +16;
        }
    } else {
        detailBtnCenter.y = self.cellHeight/2.0 +16;
    }
    
    cell.transform = tableView.transform;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.cellHeight==0) {
        return 44;
    }
    return self.cellHeight;
}

- (void)didPressRightButton:(id)sender{
    [self.textView refreshFirstResponder];
    [self setUpSendChatMessage];
    [super didPressRightButton:sender];
}

- (void)didPressLeftButton:(id)sender{
    [self.textView resignFirstResponder];
    [self.textInputbar.textView refreshFirstResponder];
    audioInputbar.hidden=NO;
}

- (void)getUpDirectChatViewChatListWithGroupId:(NSNumber *)groupId{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    
    [manager GET:[NSString stringWithFormat:getMessageListPath,groupId,self.chatPage] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) {

            if (self.chatPage==1) {
                [self.chatMessage removeAllObjects];
            }
            for (NSDictionary *dict in responseObject[@"data"]) {
                
                if (![dict[@"type"] isEqualToNumber:@2] && ![dict[@"type"] isEqualToNumber:@3]) {
                    MessageModel *model = [[MessageModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    model.sendSuccessflag=1;
                    model.volumeAnimatedNum=3;
                    model.witeActivityIsShow=NO;
                    if (model.info[@"readed"]!=NULL) {
                        model.readed=YES;
                    }else{
                        model.readed=NO;
                    }
                    [self.chatMessage addObject:model];
                }
            }
            
            self.lastTimeString = @"";
            for (NSInteger i = self.chatMessage.count-1; i>=0; i--) {
                MessageModel *mes = self.chatMessage[i];
                NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:mes.updatedAt];
                if (![timeStr isEqualToString:self.lastTimeString]) {
                    mes.flag = 0; // 显示标志
                    self.lastTimeString = [timeStr copy];
                    mes.timeViewStr = [timeStr copy]; // 记录下来
                } else {
                    mes.flag = 1;
                    mes.timeViewStr = nil; // 记录下来
                }
            }
            [self.tableView reloadData];
            double delayInSeconds = 0.6;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.tableView footerEndRefreshing];
                [self.tableView reloadData];
                self.tableView.hidden=NO;
            });
            self.chatPage++;
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark 发送消息

- (void)setUpSendChatMessage{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    parameter[@"message"] = [NSString stringThrowOffLinesWithString:self.textView.text];
    
    [manager POST:sendMessagePath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
        }
//        else if ([responseObject[@"code"] isEqualToNumber:@3]){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//        }
        else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark 通知消息

- (void)getUpDirectChatViewNewMessage:(NSNotification *)not{
    NSArray *message = not.object;
    NSDictionary *dict = message[0];
    
    //接受消息去重
    NSString *clientId = dict[@"info"][@"clientId"];
    NSString *serverId = dict[@"id"];
    
    if (_chatMessage.count>=1) {
        MessageModel *model=_chatMessage[0];
        
        /**
         * 怎样界定一条msg？
         *  根据客户端打的唯一tag：clientId。但如果是服务器产生的msg则没有这个tag。另外客户端出错时可能没有这个tag
         *  根据服务器打的唯一tag：createAt。但如果是客户端先发送到本地list的msg里面没有这个tag。可以假定服务器不会错误滴遗失这个tag
         *
         * 判断步骤
         *  先判断clientId是否重复
         *  如果没有clientId，再判断createAt
         */
        
        if (dict[@"info"][@"link"]!=nil&&dict[@"info"][@"desc"]!=nil) {
            
        }else{
            if (clientId!=nil && model.info[@"clientId"]!=nil) {//some msg donot have clientId
                if ([clientId isEqualToString:model.info[@"clientId"]]) {
                    //手动去重操作，因为有时会出现收到2条相同的message
                    return;
                }
            } else if (serverId!=nil && model.id!=nil) {
                if ([serverId isEqualToString:model.id]) {
                    //手动去重操作，因为有时会出现收到2条相同的message
                    return;
                }
            }
        }
        
    }
    
    if ([dict[@"type"] isEqualToNumber:@16]) {   //当收到 url链接 预加载内容 时
        //            NSString *clientId = dict[@"info"][@"clientId"];
        NSString *clientId = dict[@"info"][@"clientId"];
        for (long i=0; i<_chatMessage.count; i++) {
            MessageModel *model=_chatMessage[i];
            if ([clientId isEqualToString:model.info[@"clientId"]]) {
                MessageModel *mdol=[[MessageModel alloc] init];
                [mdol setValuesForKeysWithDictionary:dict];
                mdol.sendSuccessflag=1;   //隐藏发送失败的感叹号
                [_chatMessage replaceObjectAtIndex:i withObject:mdol];//insertObject:mdol atIndex:i];
                NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第i行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationFade];
                return;
            }
        }
    }else{
        
    }
    
    if (self.groupId!=nil) {
        if ([dict[@"groupId"] isEqualToNumber:self.groupId]) {
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];

            NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
            if ([timeStr isEqualToString:self.lastTimeString]) {
                model.flag = 1; // 隐藏标志
                model.timeViewStr = nil; // 记录下来
            } else {
                model.flag = 0; // 显示标志
                model.timeViewStr = [timeStr copy]; // 记录下来
                self.lastTimeString = [timeStr copy];
            }
            model.sendSuccessflag=1;
            model.volumeAnimatedNum=3;
            NSUserDefaults * ud=[NSUserDefaults standardUserDefaults];
            NSNumber* numb=[ud objectForKey:@"id"];
            if ([model.userId isEqualToNumber:numb]) {//是自己发的消息设置为已读
                model.readed=YES;
            }
            [self.chatMessage insertObject:model atIndex:0];
            [self.tableView reloadData];
        }
    }
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag==100) {
        if (buttonIndex==0) {
            UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
            action.tag = 101;
            [action showInView:self.view];
        } else if (buttonIndex==1) {
//            MapViewController *mapVC = [[MapViewController alloc] init];
            Map_ViewController *mapVC = [[Map_ViewController alloc] init];            
            mapVC.isFromPrivateVC = YES;
            mapVC.groupId = self.groupId;
            [self.navigationController pushViewController:mapVC animated:YES];
        }
    } else {
        
        if (buttonIndex==2){
            return;
        }
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
        if (buttonIndex==0) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.allowsEditing = YES;
        } else  if (buttonIndex==1) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sending")];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    NSData *imageData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 1);
    [manager POST:directChatSendImage parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"imageName" mimeType:@"image/jpg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sendFail")];
            [SVProgressHUD dismissWithDelay:1.0];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark 图片的点击事件

- (void)onSubImageViewTapClick:(UITapGestureRecognizer *)tap{
    MessageModel *model = self.chatMessage[tap.view.tag];
    
    if ([model.type  isEqualToNumber: @13]) { // 地图
//        MapViewController *mapVC = [[MapViewController alloc] init];
        Map_ViewController *mapVC = [[Map_ViewController alloc] init];
        mapVC.latitude = [model.info[@"latitude"] floatValue];
        mapVC.longitude = [model.info[@"longitude"] floatValue];
        mapVC.isYulan=YES;
        [self.navigationController pushViewController:mapVC animated:YES];
    } else {
        ShowPictureViewController *picVC = [[ShowPictureViewController alloc] init];
        picVC.imageInfo = model.info;
        [self presentViewController:picVC animated:NO completion:nil];
    }
}

- (void)onMesDetailViewUserIconTapGestureClick:(UITapGestureRecognizer *)tap{
    UIImageView *userIconImageView = (UIImageView *)tap.view;
    
    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    MessageModel *model = self.chatMessage[userIconImageView.tag];
    
    userPerVC.userId = model.userId;
    [self.navigationController pushViewController:userPerVC animated:YES];
}

- (void)mlEmojiLabel:(MLEmojiLabel*)emojiLabel didSelectLink:(NSString*)link withType:(MLEmojiLabelLinkType)type
{
    switch(type){
        case MLEmojiLabelLinkTypeURL:
//            NSLog(@"点击了链接%@",link);
            ;
            NSRange range=[link rangeOfString:@"http://"];
            if (range.length==0) {
                link=[NSString stringWithFormat:@"http://%@",link];
            }
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:link]];
            //            [[[UIActionSheet alloc] initWithTitle:link delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
            break;
        case MLEmojiLabelLinkTypePhoneNumber:
//            NSLog(@"点击了电话%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"tel://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeEmail:
//            NSLog(@"点击了邮箱%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"mailto://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeAt:
//            NSLog(@"点击了用户%@",link);
            break;
        case MLEmojiLabelLinkTypePoundSign:
//            NSLog(@"点击了话题%@",link);
            break;
        default:
//            NSLog(@"点击了不知道啥%@",link);
            break;
    }
    
}


- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    [super textView:textView shouldChangeTextInRange:range replacementText:text];
    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
        [self didPressRightButton:nil];
        textView.text=@"";
        return NO;
    }
    return YES;
}


@end
