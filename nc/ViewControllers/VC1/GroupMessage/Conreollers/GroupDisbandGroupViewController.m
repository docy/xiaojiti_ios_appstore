//
//  GroupDisbandGroupViewController.m
//  nc
//
//  Created by docy admin on 15/10/16.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "GroupDisbandGroupViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import "GroupConveyGroupViewController.h"
#import "ToolOfClass.h"
#import <AFNetworking/AFNetworking.h>

@interface GroupDisbandGroupViewController () <UIAlertViewDelegate>

@property (nonatomic, assign) BOOL cannotDisband;

@end

@implementation GroupDisbandGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpGroupDisbandGroupViewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpGroupDisbandGroupViewData{
    self.navigationItem.title = @"转让或解散小组";
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpGroupDisbandGroupViewBack)];
    self.userCountLabel.text = [NSString stringWithFormat:@"%lu人",self.userCount];
    self.groupUserLabel.text = [NSString stringWithFormat:@"%@成员",self.groupName];
}
- (void)setUpGroupDisbandGroupViewBack{
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onGroupDisbandGroupViewConveyBtnClick:(id)sender {
    
    if (self.cannotDisband==YES) {
        [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"你已经不是群主，无权转让群主身份!" target:nil];
    } else {
        GroupConveyGroupViewController *convey = [[GroupConveyGroupViewController alloc] init];
        convey.groupId = self.groupId;
        convey.disbandBlock = ^(NSNumber *userId){
            self.cannotDisband = YES;
            if (self.conveyBlock) {
                self.conveyBlock(userId);
            }
        };
        [self.navigationController pushViewController:convey animated:YES];
    }
    
}

- (IBAction)onGroupDisbandGroupViewDisbandBtnClick:(id)sender {
    
    if (self.cannotDisband==YES) {
       [UIAlertView alertViewWithTitle:@"温馨提示" subTitle:@"你已经不是群主，无权解散小组!" target:nil];
    } else {
        [UIAlertView alertViewWithTitle:@"解散小组" subTitle:@"解散小组之后你将和好友失去联系，确定解散小组?" target:self];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        NSString *path = [NSString stringWithFormat:GroupOwnerDisbandGroup,self.groupId];
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] intValue] == 200) {
                
                [ToolOfClass showMessage:@"解散成功"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:disbandGroup object:self.groupId];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:@"解散失败，请检查网络"];
        }];
    }
}

@end
