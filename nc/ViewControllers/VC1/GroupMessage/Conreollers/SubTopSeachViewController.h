//
//  SubTopSeachViewController.h
//  nc
//
//  Created by guanxf on 15/12/1.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^upLoadTVC)(void);
@interface SubTopSeachViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

// 群组的子话题页面
@property (nonatomic,retain) NSMutableArray *subTopicListData;

@property (nonatomic, retain) NSNumber *groupId;
@property (nonatomic, strong) upLoadTVC uploadtvc;

@end
