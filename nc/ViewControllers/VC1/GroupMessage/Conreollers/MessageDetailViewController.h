//
//  MessageDetailViewController.h
//  nc
//
//  Created by docy admin on 6/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SLKTextViewController.h"
#import "MessageModel.h"
#import <QuickLook/QuickLook.h>
@interface MessageDetailViewController : SLKTextViewController<QLPreviewControllerDataSource,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate>

// 消息详情视图控制器
//@property (nonatomic, retain) MessageModel *message;
@property (nonatomic, retain) NSNumber *groupId; // 群组的ID，
@property (nonatomic, retain) NSNumber *topicId;
@property (nonatomic, retain) NSNumber *topicType;
@property (nonatomic, retain) NSNumber *createTopicUserID;
@property (nonatomic, strong) UIImage *fileImage;


@property (nonatomic, retain) NSArray *users; // 用户昵称数组(@传值)
@property (nonatomic, retain) NSArray *groupUsers; // 群组成员，存储model


@end
