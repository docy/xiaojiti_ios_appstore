//
//  FileListViewController.h
//  QuickLookDemo
//
//  Created by guanxf on 15/11/28.
//  Copyright © 2015年 yangjw . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
@interface FileListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate>

@property (nonatomic, retain) NSNumber *groupId; // 群组的ID，

@end
