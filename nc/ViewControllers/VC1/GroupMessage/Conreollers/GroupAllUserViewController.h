//
//  GroupAllUserViewController.h
//  nc
//
//  Created by docy admin on 15/11/18.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupAllUserViewController : UITableViewController

@property (nonatomic, retain) NSArray *allUserData;

@end
