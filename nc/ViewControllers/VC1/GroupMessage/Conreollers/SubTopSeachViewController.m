//
//  SubTopSeachViewController.m
//  nc
//
//  Created by guanxf on 15/12/1.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import "SubTopSeachViewController.h"
#import "SubTopicViewCell.h"
#import "SubTopicModel.h"
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "MessageDetailViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "MJRefresh.h"


@interface SubTopSeachViewController ()<UISearchBarDelegate,UISearchControllerDelegate,UISearchResultsUpdating>
@property (nonatomic, assign) int subTopicPage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) UISearchController* searchController;
@property (nonatomic, retain) UIImageView *bgSubTpView;
@end

@implementation SubTopSeachViewController

- (UIImageView *)bgSubTpView{
    if (_bgSubTpView == nil) {
        _bgSubTpView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"none_topic"]];
    }
    return _bgSubTpView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onSubTopicViewControllerNavBackClick)];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self; 
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    [self.searchController.searchBar sizeToFit];
    self.navigationItem.titleView = self.searchController.searchBar;
    
//    [self getUpTopicList];
    [_tableView reloadData];
    // Do any additional setup after loading the view from its nib.
}

-(void)onSubTopicViewControllerNavBackClick{
    if (self.searchController.active) {
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
        [self.searchController.view removeFromSuperview];
    }   
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark UISearchControllerDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = [self.searchController.searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if ([searchString isEqualToString:@""]) {
        return;
    }
    [self getUpTopicListByRule:searchString];
}

- (void)getUpTopicListWithMJFresh
{   
    [self.subTopicListData removeAllObjects];
    __block SubTopSeachViewController *weakSelf = self;
    [self.tableView addFooterWithCallback:^{
        weakSelf.subTopicPage++;
        [weakSelf getUpTopicList];
    }];
}

// 获取子话题列表
- (void)getUpTopicList
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    NSString *str = [NSString stringWithFormat:getSubTopicListPath,self.groupId,self.subTopicPage];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
            NSLog(@"%@",responseObject);
            for (NSDictionary *dict in responseObject[@"data"]) {
                SubTopicModel *model = [[SubTopicModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.subTopicListData addObject:model];
            }
            
            if (self.subTopicListData.count==0) {
                self.tableView.backgroundView = self.bgSubTpView;
            } else {
                self.tableView.backgroundView = nil;
            }
            
            
            [self.tableView footerEndRefreshing];
            [self.tableView reloadData];
            
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

// 获取子话题列表 带参数
- (void)getUpTopicListByRule:(NSString *)keyword
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    NSString *str = [NSString stringWithFormat:@"%@?groupId=%@&keyword=%@",getSubTopicList,self.groupId,keyword];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
            NSLog(@"%@",responseObject);
            [_subTopicListData removeAllObjects];
            for (NSDictionary *dict in responseObject[@"data"]) {
                SubTopicModel *model = [[SubTopicModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.subTopicListData addObject:model];
            }
            
            if (self.subTopicListData.count==0) {
                self.tableView.backgroundView = self.bgSubTpView;
            } else {
                self.tableView.backgroundView = nil;
            }
            
            
            [self.tableView footerEndRefreshing];
            [self.tableView reloadData];
            
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.subTopicListData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellId";
    SubTopicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SubTopicViewCell" owner:self options:nil] lastObject];
    }
    SubTopicModel *model = self.subTopicListData[indexPath.row];
    
    cell.titleLabel.text = model.title;
    
    if ([model.type intValue] == 2) { // 图片子话题
        [cell.iconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
    } else if ([model.type intValue] == 3){
        cell.iconImageView.image = [UIImage imageNamed:@"list_map"];
    } else if ([model.type intValue] == 4){
        cell.iconImageView.image = [UIImage imageNamed:@"list_vote"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"list_topic"];
    }
    
    cell.viewsLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"subTopic_list_click"),[model.views intValue]];
    cell.favoriteCountLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"subTopic_list_guanzhu"),[model.favoriteCount intValue]];
    cell.commentCountLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"subTopic_list_huifu"),[model.commentCount intValue]];
    
    if (model.comments.count!=0) {
        NSDictionary *comment = model.comments[0];
        
        NSString *text = nil;
        if ([comment[@"type"] isEqualToNumber:@1]) {
            text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        } else {
            text = [NSString stringWithFormat:@"%@：发表图片",comment[@"nickName"],NSLocalized(@"subTopic_list_sendImage")];
        }
        //        text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        NSMutableAttributedString *attributsStr = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange range = [text rangeOfString:[NSString stringWithFormat:@"%@：",comment[@"nickName"]]];
        [attributsStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:range];
        
        //        [attributsStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1] range:range];
        cell.descLabel.attributedText = attributsStr;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:comment[@"createdAt"]];
    } else {
        
        cell.descLabel.text = NSLocalized(@"subTopic_list_huifu_no");//nil;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.updatedAt];
        
    }
    
    if ([model.favorited isEqualToNumber:@1]) {
        [cell.FocusImage setImage:[UIImage imageNamed:@"Focus on"]];
    }else{
        [cell.FocusImage setImage:[UIImage imageNamed:@"Focus on_normal"]];
    }
    cell.FocusButton.tag=indexPath.row;
    [cell.FocusButton addTarget:self action:@selector(FocusButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)FocusButtonClick:(UIButton*)button{
    SubTopicModel *model = self.subTopicListData[button.tag];
    BOOL isF=[model.favorited isEqualToNumber:@1]?NO:YES;
    [self addMessageDetailViewFavorite:isF andIndexRow:button.tag];
    NSInteger num=[model.favoriteCount integerValue];
    isF?num++:num--;
    model.favoriteCount=[NSNumber numberWithInteger:num];
    model.favorited=[NSNumber numberWithBool:isF];
    [self.subTopicListData replaceObjectAtIndex:button.tag withObject:model];
}

- (void)addMessageDetailViewFavorite:(BOOL)isFavorite andIndexRow:(NSInteger)row{
    SubTopicModel *model = self.subTopicListData[row];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = nil;
    if (isFavorite) {
        parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    } else {
        parameter = [NSDictionary dictionaryWithObjects:@[[ToolOfClass authToken],@"true"] forKeys:@[@"accessToken",@"unfavorite"]];
    }
    [manager POST:[NSString stringWithFormat:addFavorite,model.id] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            if (isFavorite) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newFavorite" object:nil];
                NSIndexPath * indexPath=[NSIndexPath indexPathForRow:row inSection:0];
                NSArray * array=[NSArray arrayWithObjects:indexPath, nil];
                [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
                
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newUnFavorite" object:model.id];
                
                NSIndexPath * indexPath=[NSIndexPath indexPathForRow:row inSection:0];
                NSArray * array=[NSArray arrayWithObjects:indexPath, nil];
                [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
            }
            if (self.uploadtvc) {
                self.uploadtvc();
            }
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageDetailViewController *detailVC = [[MessageDetailViewController alloc] init];
    SubTopicModel *model = self.subTopicListData[indexPath.row];
    detailVC.topicId = model.id;
    detailVC.topicType = model.type;
    
    if (self.searchController.active) {
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
        [self.searchController.view removeFromSuperview];
    }
    
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
