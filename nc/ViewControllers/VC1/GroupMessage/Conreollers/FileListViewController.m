//
//  FileListViewController.m
//  QuickLookDemo
//
//  Created by guanxf on 15/11/28.
//  Copyright © 2015年 yangjw . All rights reserved.
//

#import "FileListViewController.h"
#import "MainThemeViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"


@interface FileListViewController ()
@property(nonatomic,strong)NSMutableArray *dirArray;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;
@property (strong, nonatomic) IBOutlet UIView *fileListNullView;
@property (strong, nonatomic) IBOutlet UIView *fileListNullView_en;

@end

@implementation FileListViewController
{
    __weak IBOutlet UITableView *readTable;
}

- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}
- (void)AllGroupViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalized(@"file_title");
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(AllGroupViewBack)];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    UIButton *rightItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightItem setImage:[UIImage imageNamed:@"navigation__more"] forState:UIControlStateNormal];
    CGSize size = [[rightItem currentImage] size];
    rightItem.frame = CGRectMake(0, 0, size.width, size.height);
    [rightItem addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:NSLocalized(@"file_edit") target:self action:@selector(editAction:)]; //[[UIBarButtonItem alloc] initWithCustomView:rightItem];
    
//    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
//    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
//    UINavigationBar *navBar = [UINavigationBar appearance];
//    [navBar setBackgroundImage:[UIImage imageNamed:@"bg_navigation bar"] forBarMetrics:UIBarMetricsDefault];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //在这里获取应用程序Documents文件夹里的文件及文件夹列表
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSError *error = nil;
    NSArray *fileList = [[NSArray alloc] init];
    //fileList便是包含有该文件夹下所有文件的文件名及文件夹名的数组
    fileList = [fileManager contentsOfDirectoryAtPath:documentDir error:&error];

    for (NSString *file in fileList)
    {
        NSRange range=[file rangeOfString:@"."];
        if (range.location==0||range.length==0) {
            //屏蔽.隐藏文件 和文件夹
        }else{
            if ([file isEqualToString:@"audioName.aac"]) {
                //屏蔽.隐藏文件录音文件
            }else if ([file isEqualToString:@"audioName.m4a"]) {
                //屏蔽.隐藏文件待播放音频文件
            }else {
                [self.dirArray addObject:file];
            }
        }
    }
    NSLog(@"Every Thing in the dir:%@",fileList);
    
    if (_dirArray.count==0) {
        readTable.backgroundView=isEnglish?_fileListNullView_en:_fileListNullView;
        readTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    }else{
        readTable.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        [readTable reloadData];
    }
    
}

-(void)editAction:(UIButton*)button{
    [readTable setEditing:readTable.editing?NO:YES animated:YES];
}

-(NSMutableArray*)dirArray{
    if (!_dirArray) {
        _dirArray=[[NSMutableArray alloc] init];
    }
    return _dirArray;
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)longPressGesture
{
    if (longPressGesture.state == UIGestureRecognizerStateBegan)
    {
        NSIndexPath *cellIndexPath = [readTable indexPathForRowAtPoint:[longPressGesture locationInView:readTable]];
        
        NSURL *fileURL;
        
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDir = [documentPaths objectAtIndex:0];
        
        
        if (cellIndexPath.section == 0)
        {
            // for section 0, we preview the docs built into our app
//            fileURL = [self.dirArray objectAtIndex:cellIndexPath.row];
            NSString *path = [documentDir stringByAppendingPathComponent:[self.dirArray objectAtIndex:cellIndexPath.row]];
            
            fileURL = [NSURL fileURLWithPath:path];
        }
        else
        {
            // for secton 1, we preview the docs found in the Documents folder
            fileURL = [self.dirArray objectAtIndex:cellIndexPath.row];
        }
        self.docInteractionController.URL = fileURL;
        
        [self.docInteractionController presentOptionsMenuFromRect:longPressGesture.view.frame
                                                           inView:longPressGesture.view
                                                         animated:YES];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellName = @"CellName";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellName];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellName];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    NSURL *fileURL= nil;
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path = [documentDir stringByAppendingPathComponent:[self.dirArray objectAtIndex:indexPath.row]];
    fileURL = [NSURL fileURLWithPath:path];
    
    [self setupDocumentControllerWithURL:fileURL];
    cell.textLabel.text = [self.dirArray objectAtIndex:indexPath.row];
    NSInteger iconCount = [self.docInteractionController.icons count];
    if (iconCount > 0)
    {
        cell.imageView.image = [self.docInteractionController.icons objectAtIndex:iconCount - 1];
    }
    
    NSString *fileURLString = [self.docInteractionController.URL path];
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURLString error:nil];
    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] intValue];
    NSString *fileSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize
                                                           countStyle:NSByteCountFormatterCountStyleFile];
    NSString * filess=[NSString stringWithFormat:@"%@ ", fileSizeStr];//- %@ , self.docInteractionController.UTI
    cell.detailTextLabel.text = filess;
    UILongPressGestureRecognizer *longPressGesture =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [cell.imageView addGestureRecognizer:longPressGesture];
    cell.imageView.userInteractionEnabled = YES;    // this is by default NO, so we need to turn it on
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dirArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainThemeViewController * mainvc=[[MainThemeViewController alloc] initWithNibName:@"MainThemeViewController" bundle:nil];
    NSURL *fileURL= nil;
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path = [documentDir stringByAppendingPathComponent:[self.dirArray objectAtIndex:indexPath.row]];
    fileURL = [NSURL fileURLWithPath:path];
    [self setupDocumentControllerWithURL:fileURL];
    NSInteger iconCount = [self.docInteractionController.icons count];
    
    UIImage * _fileImage= [self.docInteractionController.icons objectAtIndex:iconCount - 1];

    mainvc.image=_fileImage;
    mainvc.FileCopyPath=path;//_userStr;
    mainvc.isLoacFileOpen=YES;
    mainvc.isFileCreate=YES;
//    GroupModel *model = self.groups[indexPath.row];
    mainvc.groupId = _groupId;
    mainvc.titleString=[path lastPathComponent];
    
    mainvc.backblock=^(){
        [self.navigationController popViewControllerAnimated:YES];
    };
    
    [[self navigationController] pushViewController:mainvc animated:YES];
//    QLPreviewController *previewController = [[QLPreviewController alloc] init];
//    previewController.dataSource = self;
//    previewController.delegate = self;
//    // start previewing the document at the current section index
////    previewController.currentPreviewItemIndex = indexPath.row;
//    [self.navigationController pushViewController:previewController animated:YES];
    //	[self presentViewController:previewController animated:YES completion:nil];
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return UITableViewCellEditingStyleDelete;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        //        获取选中删除行索引值
        NSInteger row = [indexPath row];
        //        通过获取的索引值删除数组中的值
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *savedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",self.dirArray[row]]];
        NSLog(@"filepath_dow---%@",savedPath);
        
        BOOL booo= [fileManager removeItemAtPath:savedPath error:nil];  //删除文件
        if (booo) {
            [self.dirArray removeObjectAtIndex:row];
            //        删除单元格的某一行时，在用动画效果实现删除过程
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            double delayInSeconds = 0.3;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                if (_dirArray.count==0) {
                    readTable.backgroundView=_fileListNullView;
                    readTable.separatorStyle=UITableViewCellSeparatorStyleNone;
                }else{
                    readTable.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
                    [readTable reloadData];
                }
            });
        }
    }
}




#pragma mark - UIDocumentInteractionControllerDelegate

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
    return self;
}


#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    //    NSInteger numToPreview = 0;
    //
    //	numToPreview = [self.dirArray count];
    //
    //    return numToPreview;
    return [self.dirArray count];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{

//    [previewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"click.png"] forBarMetrics:UIBarMetricsDefault];
    NSURL *fileURL = nil;
    NSIndexPath *selectedIndexPath = [readTable indexPathForSelectedRow];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path = [documentDir stringByAppendingPathComponent:[self.dirArray objectAtIndex:selectedIndexPath.row]];
    fileURL = [NSURL fileURLWithPath:path];
    return fileURL;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
