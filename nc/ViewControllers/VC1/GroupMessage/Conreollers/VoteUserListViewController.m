//
//  VoteUserListViewController.m
//  nc
//
//  Created by guanxf on 15/9/12.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "VoteUserListViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "UIColor+Hex.h"
#import "AddressBookTableViewCell.h"

@interface VoteUserListViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray * sectionStateA; //用来存储section开合状态的array。
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation VoteUserListViewController
@synthesize choicesDic;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalized(@"vote_allUser_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(BackButtonClick)];
    
    sectionStateA=[[NSMutableArray alloc] init];
    for (int i=0; i<(choicesDic.count-1)/3; i++) {
        NSArray * arr=choicesDic[[NSString stringWithFormat:@"%d_Choices",i]][@"votes"];
        if (arr.count==0) {
            [sectionStateA addObject:@0];
        }else{
            [sectionStateA addObject:@1];
        }
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellid=@"cellid";
    AddressBookTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {

        cell=[[[NSBundle mainBundle] loadNibNamed:@"AddressBookTableViewCell" owner:self options:nil] lastObject];
    }
    
    NSArray * array=choicesDic[[NSString stringWithFormat:@"%ld_Choices",(long)indexPath.section]][@"votes"];
    NSDictionary * dic=[array objectAtIndex:indexPath.row];
//    cell.nameLabel.text=dic[@"nickName"];
    cell.nameLabel.hidden = YES;
    cell.nickNameLabel.text = dic[@"nickName"];
    
    CGPoint center = cell.nickNameLabel.center;
    center.y = cell.center.y;
    cell.nickNameLabel.center = center;
    
    //判断微信登录的图像
    NSRange range=[dic[@"avatar"] rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.location==NSNotFound) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:dic[@"avatar"]]];
    }else{
        iconUrl=[NSURL URLWithString:dic[@"avatar"]];
    }
    [cell.IconImageView setImageWithURL:iconUrl placeholderImage:nil];
    cell.phoneOrEmailLabel.hidden=YES;
    cell.xianTiaoLabel.hidden=YES;

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray * arr=choicesDic[[NSString stringWithFormat:@"%ld_Choices",(long)section]][@"votes"];

    return [sectionStateA[section] isEqualToNumber:@0]?0:arr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return (choicesDic.count-1)/3;
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 30)];
    
    [sectionView setBackgroundColor:[UIColor colorWithHex:0xf6f6f6]];
    UILabel * label=[[UILabel alloc] initWithFrame:CGRectMake(50 , 5, ScreenWidth, 20)];
    label.text=choicesDic[[NSString stringWithFormat:@"%ld",(long)section]];
    [label setTextColor:[UIColor colorWithHex:0x666666]];
    
    UIImageView * imgV=[[UIImageView alloc] initWithFrame:CGRectMake(16, 5, 20, 20)];
    NSNumber * num=sectionStateA[section];
//    imgV.image=[UIImage imageNamed:@"vote_arrow"];
    
    UIButton * but=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 30)];
    but.tag=200+section;
    [but addTarget:self action:@selector(selectSectionClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [sectionView addSubview:imgV];
    [sectionView addSubview:label];
    [sectionView addSubview:but];

    if ([num isEqualToNumber:@1]) {
        imgV.image=[UIImage imageNamed:@"vote_arrow"];
    }else{
        imgV.image=[UIImage imageNamed:@"vote_arrow right"];
    }
    
    return sectionView;
}

-(void)selectSectionClick:(UIButton*)button{
    
    NSInteger section=button.tag-200;
    NSNumber * num=sectionStateA[section];
    if ([num isEqualToNumber:@0]) {
        sectionStateA[section]=@1;
    }else{
        sectionStateA[section]=@0;
    }
    [self.tableview reloadData];
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * footView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    [footView setBackgroundColor:[UIColor colorWithHex:0xe5e5e5]];
    return footView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53;
}

-(void)BackButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
