//
//  ShowPictureViewController.m
//  nc
//
//  Created by docy admin on 6/26/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "ShowPictureViewController.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+LK.h"
#import "UIImage+GIF.h"
//#import "LK_THProgressView.h"
#import "ToolOfClass.h"
#import "ASProgressPopUpView.h"

@interface ShowPictureViewController () <UIGestureRecognizerDelegate,UIAlertViewDelegate>

@property (nonatomic, assign) CGRect imageRect;
@property (nonatomic, assign) CGRect pinchImageRect;
@property (nonatomic,retain) ASProgressPopUpView *progressView;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;

@end

@implementation ShowPictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, ScreenHeight/2.0, ScreenWidth, 20)];
//    self.progressView.tintColor = [UIColor redColor];
//    self.progressView.progress = 0;
//    [self.view addSubview:self.progressView];
    
    self.progressView= [[ASProgressPopUpView alloc] initWithFrame:CGRectMake(0, ScreenHeight/2.0, ScreenWidth, 20)];
    self.progressView.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:16];
    self.progressView.popUpViewAnimatedColors = @[[UIColor redColor], [UIColor orangeColor], [UIColor greenColor]];
    [self.progressView showPopUpViewAnimated:YES];
    //    self.progressView1.dataSource = self;
    [self.view addSubview:self.progressView];

    // 设置属性数据
    [self setUpShowPictureViewData];
}

- (void)setUpShowPictureViewData
{
    self.imageView.userInteractionEnabled = YES;
    // 点击手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onShowPictureViewTapGestureClick:)];
    tap.delegate = self;
    [self.imageView addGestureRecognizer:tap];
    
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onShowPictureViewLongPressGestureClick:)];
    longPress.delegate = self;

    [self.imageView addGestureRecognizer:longPress];
    
    
//     缩放手势
//    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(onShowPictureViewPinchGestureClick:)];
//    pinch.delegate = self;
//    [self.imageView addGestureRecognizer:pinch];
    // 拖动手势
//    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onShowPictureViewPanGestureClick:)];
//    pan.delegate = self;
//    [self.imageView addGestureRecognizer:pan];
    // 显示图片
    if (self.imageInfo) {
        
        CGRect imageViewFrame = self.imageView.frame;
        CGFloat width = [self.imageInfo[@"fullSize_w"] floatValue];
        CGFloat height = [self.imageInfo[@"fullSize_h"] floatValue];
        
        if (width>ScreenWidth) {
            imageViewFrame.size.width = ScreenWidth;
            imageViewFrame.size.height = ScreenWidth*(height/width);
        } else {
            imageViewFrame.size.height = height;
            imageViewFrame.size.width = width;
        }
        self.imageView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);//imageViewFrame;
        CGPoint center = self.imageView.center;
        
        center.x = ScreenWidth/2.0;
        center.y = ScreenHeight/2.0;
//        self.imageView.center = center;
        
        __block UIProgressView *pv = self.progressView;
        __weak UIImageView *weakImageView = self.imageView;
        NSString * str=[iconPath stringByAppendingString:self.imageInfo[@"fullSize"]];
        NSURL * imgURL=[NSURL URLWithString:str];
        [self.imageView sd_setImageWithURL:imgURL placeholderImage:nil
                             options:SDWebImageCacheMemoryOnly
                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {

                                [weakImageView addSubview:pv];
                                float showProgress = (float)receivedSize/(float)expectedSize;
                                [pv setProgress:showProgress animated:YES];

                            }
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) { 
                               [self.progressView removeFromSuperview];
                               pv = nil;
//                               NSString * urlstr=imageURL.lastPathComponent;
//                               NSRange range=[urlstr rangeOfString:@".GIF"];
//                               [self saveImage:self.imageView.image withName:@"xiaolian.gif"];
//                               if (range.length>0) {
//                                   YFGIFImageView *gifView = [[YFGIFImageView alloc] initWithFrame:self.imageView.frame];
//                                   //UIImageView *gifView = [[UIImageView alloc] initWithFrame:CGRectMake(8+i%3*(96+8), 28+i/3*(54+8), 96, 54)];
//                                   gifView.backgroundColor = [UIColor darkGrayColor];
//                                   gifView.gifData =  UIImageJPEGRepresentation(self.imageView.image, 1.0);
//                                   [self.view addSubview:gifView];
//                                   // notice: before start, content is nil. You can set image for yourself
//                                   [gifView startGIF];
//                               }
                           }];
        
        
//        [self.imageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:self.imageInfo[@"fullSize"]]] placeholderImage:nil];
        self.imageRect = self.imageView.frame;
        _imageScrollView.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    }
}

#pragma mark - 保存图片至沙盒
- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName
{
    
    NSData *imageData = UIImageJPEGRepresentation(currentImage, ImageYaSuo);
    // 获取沙盒目录
    
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
    // 将图片写入文件
    
    [imageData writeToFile:fullPath atomically:NO];
}

// 点击手势
- (void)onShowPictureViewTapGestureClick:(UITapGestureRecognizer *)tapGesture{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)onShowPictureViewLongPressGestureClick:(UILongPressGestureRecognizer *)longGesture{
    if (longGesture.state==UIGestureRecognizerStateBegan) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"picker_save_album") message:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"picker_save"), nil];
        [alert show];
        alert.delegate = self;
    }
}

// 缩放手势
- (void)onShowPictureViewPinchGestureClick:(UIPinchGestureRecognizer *)pinchGesture{
    UIImageView *imageView = (UIImageView *)pinchGesture.view;
    CGRect rect = imageView.frame;
    if (rect.size.width<=self.imageRect.size.width) {
        
        [UIView animateWithDuration:0.5 animations:^{
            imageView.frame = self.imageRect;
        } completion:^(BOOL finished) {
            
        }];
    } else {
        self.pinchImageRect = rect;
    }
    
    if (pinchGesture.state == UIGestureRecognizerStateBegan||pinchGesture.state == UIGestureRecognizerStateChanged )
    {
        NSLog(@"%f %f", pinchGesture.scale, pinchGesture.scale);
        
        // 每次都是以最初位置的中心点为起始参照
//        imageView.transform = CGAffineTransformMakeScale(pinchGesture.scale, pinchGesture.scale);
        
        // 每次都是以传入的transform为起始参照
        imageView.transform = CGAffineTransformScale(imageView.transform, pinchGesture.scale, pinchGesture.scale);
        pinchGesture.scale = 1.0;
    }
    rect.origin=CGPointMake(0, 0);
    _imageScrollView.contentSize=imageView.frame.size;
//    _imageScrollView.contentOffset=CGPointMake(imageView.frame.size.width-rect.size.width,imageView.frame.size.height-rect.size.height);

    
}
// 拖动手势
- (void)onShowPictureViewPanGestureClick:(UIPanGestureRecognizer *)panGesture{

    // 获取到视图
    UIImageView *imageView = (UIImageView *)panGesture.view;
    // 获取点
    CGPoint point = [panGesture translationInView:self.view];
    if (panGesture.state == UIGestureRecognizerStateBegan||panGesture.state == UIGestureRecognizerStateChanged)
    {
        // 获取相对于父视图里的点
//        imageView.center = [panGesture locationInView:self.view] ;
        
        //改变中心点的坐标,加上偏移量
        imageView.center = CGPointMake(imageView.center.x+point.x, imageView.center.y+point.y);
        
        //每次调用完此方法之后，需要重置手势的偏移量,否则移动手势会自动累加偏移量
        //CGPointZero CGPointMake(0,0)
        [panGesture setTranslation:CGPointZero inView:self.view];
        
        CGRect rect = imageView.frame;
        
        if (rect.size.width>self.imageRect.size.width) {
            
            if (rect.origin.x > 0) {
                rect.origin.x = 0;
            }
            if (self.pinchImageRect.origin.y<=0) {
                rect.origin.y=0;
            } else if (rect.origin.y>self.pinchImageRect.origin.y) {
                rect.origin.y = self.pinchImageRect.origin.y;
            }
//            CGFloat x = self.pinchImageRect.origin.x;
//            if (x<0) {
//                x = (-1)*x;
//            }
            
//            if (self.pinchImageRect.size.width-x<ScreenWidth) {
//                
//                x = self.pinchImageRect.size.width-ScreenWidth;
//                rect.origin.x = x;
//            }
//            CGFloat y = self.pinchImageRect.origin.y;
//            if (y<0) {
//                y = (-1)*y;
//            }
            
//            if (self.pinchImageRect.size.height-y<ScreenHeight) {
//                
//                y = self.pinchImageRect.size.height-ScreenHeight;
//                rect.origin.y = y;
//            }
            
            
            [UIView animateWithDuration:0.5 animations:^{
                imageView.frame = rect;
            } completion:^(BOOL finished) {
                
            }];
            
        } else {
            [UIView animateWithDuration:0.5 animations:^{
                imageView.frame = self.imageRect;
            } completion:^(BOOL finished) {
                
            }];
        }
    }
}

//告诉scrollview要缩放的是哪个子控件
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        UIImageWriteToSavedPhotosAlbum(self.imageView.image, self, @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
    }
}
- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (!error) {
        [ToolOfClass showMessage:NSLocalized(@"picker_save_success")];
    } else {
        
        [ToolOfClass showMessage:NSLocalized(@"picker_save_failure")];
    }
}
@end
