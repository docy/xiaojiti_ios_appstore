//
//  GroupAllUserViewController.m
//  nc
//
//  Created by docy admin on 15/11/18.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import "GroupAllUserViewController.h"
#import "UserModel.h"
#import "UIImageView+WebCache.h"

#import "UserJionedGroupTableViewCell.h"
#import "LookUpGroupUserPerfileViewController.h"

@interface GroupAllUserViewController ()

@end

@implementation GroupAllUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalized(@"group_set_allUser");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allUserData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellId";
    
    UserJionedGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"UserJionedGroupTableViewCell" owner:self options:nil] lastObject];
    }
    cell.lockIgView.hidden = YES;
    cell.groupUserCount.hidden = YES;
    
    CGRect rect = cell.statusLabel.frame;
    rect.size.height = 0.5;
    cell.statusLabel.frame = rect;
    
    UserModel *model = self.allUserData[indexPath.row];
    cell.groupName.text = model.nickName;
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.location==NSNotFound) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [cell.groupIconImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    LookUpGroupUserPerfileViewController *perfileVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:nil];
    perfileVC.userPerfileModel = self.allUserData[indexPath.row];
    [self.navigationController pushViewController:perfileVC animated:YES];
}

@end
