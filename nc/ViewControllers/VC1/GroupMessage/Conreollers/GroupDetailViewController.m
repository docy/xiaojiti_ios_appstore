//
//  GroupDetailViewController.m
//  nc
//
//  Created by docy admin on 7/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "GroupDetailViewController.h"
#import <AFNetworking/AFNetworking.h>

#import "GroupModel.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "GroupDetailUserView.h"
#import "GroupMessageManagerViewController.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "PersonalProfileModifyInfoViewController.h"
#import "GroupDisbandGroupViewController.h"
#import "AFNHttpRequest.h"
#import "UserModel.h"
#import "AddUserToGroupViewController.h"
#import "GroupConveyGroupViewController.h"

#import "GroupAllUserViewController.h"

#define MAXUSERCOUNT 14

@interface GroupDetailViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, retain) NSMutableArray *groupInfoData;
@property (nonatomic, assign) NSInteger userMaxCount; // 标记成员显示的最大个数（加上加减号）



@property (weak, nonatomic) IBOutlet UILabel *Localized_name;
@property (weak, nonatomic) IBOutlet UILabel *Localized_des;
@property (weak, nonatomic) IBOutlet UILabel *Localized_avator;
@property (weak, nonatomic) IBOutlet UILabel *Localized_isPrivate;
@property (weak, nonatomic) IBOutlet UILabel *Localized_cannotjion;
@property (weak, nonatomic) IBOutlet UILabel *Localized_messManager;

@end

@implementation GroupDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hiddenView.hidden=NO;
    
    // 设置基本信息
    [self setUpGroupDetailViewData];
    // 获取群组详细信息
    [self getGroupDetailViewData];
    // 获取群组成员数据
    [self getGroupDetailViewUserData];
    
    if (![_SuperViewStr isEqualToString:@"chakan"]) {
        _hiddenView.hidden=YES;
        self.scrollerView.contentSize=CGSizeMake([UIScreen mainScreen].bounds.size.width, self.scrollerView.bounds.size.height);
    }else{
        self.scrollerView.scrollEnabled = NO;
    }
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpGroupDetailViewUserListAgain) name:conveyGroupOwer object:nil];
    // 删除成员
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(delegateUser) name:@"delegateUser" object:nil];
}


// 设置基本信息
- (void)setUpGroupDetailViewData
{
    
    self.Localized_name.text = NSLocalized(@"group_set_name");
    self.Localized_des.text = NSLocalized(@"group_set_des");
    self.Localized_avator.text = NSLocalized(@"group_set_imageCover");
    self.Localized_isPrivate.text = NSLocalized(@"group_set_isPrivate");
    self.Localized_cannotjion.text = NSLocalized(@"group_set_cannotJion");
    self.Localized_messManager.text = NSLocalized(@"group_set_messManager");
    
    [self.logOutGroupBtn setTitle:NSLocalized(@"group_set_logoutBtn") forState:UIControlStateNormal];
    [self.conveyGroupBtn setTitle:NSLocalized(@"group_set_convey") forState:UIControlStateNormal];
    [self.disbandGroupBtn setTitle:NSLocalized(@"group_set_disband") forState:UIControlStateNormal];
    
    self.groupInfoData = [NSMutableArray array];
    self.navigationItem.title = NSLocalized(@"group_set_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:nil action:nil];
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onGroupDetailViewBackButtonClick)];
    self.userMaxCount = 0;
}

//- (void)onGroupDetailViewBackButtonClick
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}

// 获取群组详细信息
- (void)getGroupDetailViewData
{
    [self getUpGroupInfoOrLogoutGroup:@"GET"];
}
// 获取群组成员数据
- (void)getGroupDetailViewUserData
{
    if (!self.groupUsers) {
        self.groupUsers = [NSMutableArray array];
        [self getUpGroupDetailViewGroupUserListData]; // 请求成员数据
    }
}

#pragma mark 转让群主后的通知
- (void)getUpGroupDetailViewUserListAgain{
    
    [self.groupUsers removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:getSpecifyGroupUsersListPath,self.groupId];
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.groupUsers addObject:model];
            }
            
            for (GroupDetailUserView *view in self.userBgView.subviews) {
                [view removeFromSuperview];
            }
            
//            int i = 0;
//            for (GroupDetailUserView *view in self.userBgView.subviews) {
//                if (i==0) {
//                    
//                } else {
//                    UserModel *usermodel = self.groupUsers[i];
//                    [view.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]] placeholderImage:nil];
//                    [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
//                }
//                i++;
//            }
            GroupModel *gmodel = [self.groupInfoData lastObject];
            
            UserModel *usermodel = self.groupUsers[0];
            gmodel.creator = usermodel.id;
            
            //  创建成员列表
            [self creatUserListWithGroupModel:gmodel];
            
            self.selectedLogoBtn.enabled = NO;
            self.modifyGroupDescBtn.enabled = NO;
            self.modifyGroupNameBtn.enabled = NO;
            self.privateGroup.enabled = NO;
            self.conveyGroupBtn.hidden = YES;
            self.disbandGroupBtn.hidden = YES;
            self.logOutGroupBtn.hidden = NO;
            self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.logOutGroupBtn.frame)+50);
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

// 请求成员数据
- (void)getUpGroupDetailViewGroupUserListData
{
    [self.groupUsers removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:getSpecifyGroupUsersListPath,self.groupId];
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.groupUsers addObject:model];
            }
            if (self.groupInfoData.count!=0) {
                [self showGroupDetailViewGroupInfo];
            }
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 群组信息展示
- (void)showGroupDetailViewGroupInfo
{
    CGRect hiddenViewFrame = self.hiddenView.frame;
    hiddenViewFrame=CGRectMake(hiddenViewFrame.origin.x, hiddenViewFrame.origin.y*[UIScreen mainScreen].bounds.size.height/568-5, ScreenWidth, ScreenHeight);
    self.hiddenView.frame = hiddenViewFrame;
    
    GroupModel *model = [self.groupInfoData lastObject];
    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
    
    if (model.creator==nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"group_set_noMaster") message:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
        [alert show];
        alert.tag = 300;
        return;
    }
    
    if ([num isEqualToNumber:model.creator]) {
        self.selectedLogoBtn.enabled = YES;
        self.modifyGroupDescBtn.enabled = YES;
        self.modifyGroupNameBtn.enabled = YES;
        self.privateGroup.enabled = YES;
        self.logOutGroupBtn.hidden = YES;

    } else {
        self.selectedLogoBtn.enabled = NO;
        self.modifyGroupDescBtn.enabled = NO;
        self.modifyGroupNameBtn.enabled = NO;
        self.privateGroup.enabled = NO;
        
        self.conveyGroupBtn.hidden = YES;
        self.disbandGroupBtn.hidden = YES;
    }
    
    self.nameLabel.text = model.name;
    
    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
    self.descLabel.text = model.desc;
    if (model.category.intValue==4) {
        self.privateGroup.on = 1;
    } else {
        self.privateGroup.on = 0;
    }
    self.userCountLabel.text = [NSString stringWithFormat:@"%@%lu",NSLocalized(@"group_set_userCount"),self.groupUsers.count];
    
//    CGFloat width = CGRectGetWidth(self.view.frame)/5.0;
//    NSInteger y = 0;
//
//    NSInteger count = self.groupUsers.count;
//    if ([USER_ID isEqualToNumber:model.creator]) { // 是管理员
//        count += 2; // ＋2添加加减号
//    } else if (!(model.category.intValue==4)) { // 不是管理员，不是私密小组
//        count += 1; // +1添加减号
//    }
//    
//    if (count>MAXUSERCOUNT) { // >14
//        count = MAXUSERCOUNT+1;
//    } else {
//        
//    }
//    self.userMaxCount = count;
//    for (NSInteger i = 0; i < count; i++) {
//        GroupDetailUserView *view = [[[NSBundle mainBundle] loadNibNamed:@"GroupDetailUserView" owner:nil options:nil] lastObject];
//        
//        if ([USER_ID isEqualToNumber:model.creator]) { // 是管理员
//            if (i==0 || i==1 || ((count==MAXUSERCOUNT+1)&&(i==count-1))) {
//                if (i == 0) {
//                    view.avatarImageView.image = [UIImage imageNamed:@"invitation"];
//                } else if (i == 1) {
//                    view.avatarImageView.image = [UIImage imageNamed:@"uninvitation"];
//                } else if ((count==MAXUSERCOUNT+1)&&(i==count-1)){
//                    
//                    view.avatarImageView.image = [UIImage imageNamed:@"more"];
//                    
//                }
//                [view.nameButton setTitle:@"" forState:UIControlStateNormal];
//                view.crownIg.hidden = YES;
//            } else {
//                UserModel *usermodel = self.groupUsers[i-2];
//                [view.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]] placeholderImage:nil];
//                [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
//                
//                if ([model.creator isEqualToNumber:usermodel.id]) {
//                    view.crownIg.hidden = NO;
//                } else {
//                    view.crownIg.hidden = YES;
//                }
//            }
//        } else if (!(model.category.intValue==4)) { // 不是管理员，不是私密小组
//            if (i==0|| ((count==MAXUSERCOUNT+1)&&(i==count-1))) {
//                if (i == 0) {
//                    view.avatarImageView.image = [UIImage imageNamed:@"invitation"];
//                } else if ((count==MAXUSERCOUNT+1)&&(i==count-1)){
//                    view.avatarImageView.image = [UIImage imageNamed:@"more"];
//                }
//                [view.nameButton setTitle:@"" forState:UIControlStateNormal];
//                view.crownIg.hidden = YES;
//            }else {
//                UserModel *usermodel = self.groupUsers[i-1];
//                [view.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]] placeholderImage:nil];
//                [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
//                
//                if ([model.creator isEqualToNumber:usermodel.id]) {
//                    view.crownIg.hidden = NO;
//                } else {
//                    view.crownIg.hidden = YES;
//                }
//            }
//        } else {
//            if ((count==MAXUSERCOUNT+1)&&(i==count-1)){
//                view.avatarImageView.image = [UIImage imageNamed:@"more"];
//                [view.nameButton setTitle:@"" forState:UIControlStateNormal];
//                view.crownIg.hidden = YES;
//            } else {
//                UserModel *usermodel = self.groupUsers[i];
//                [view.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]] placeholderImage:nil];
//                [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
//                
//                if ([model.creator isEqualToNumber:usermodel.id]) {
//                    view.crownIg.hidden = NO;
//                } else {
//                    view.crownIg.hidden = YES;
//                }
//            }
//        }
//        y = i/5;
//        
//        CGRect viewFrame = view.frame;
//        viewFrame.size.width = width;
//        viewFrame.size.height = width+5;
//        viewFrame.origin.x = width*(i%5);
//        viewFrame.origin.y = viewFrame.size.height*y;
//        
//        view.frame = viewFrame;
//        [self.userBgView addSubview:view];
//        // 给头像添加点击手势
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupUserPerfileLookUpTapGesture:)];
//        [view.avatarImageView addGestureRecognizer:tap];
//        view.avatarImageView.tag = i;
//        view.avatarImageView.userInteractionEnabled = YES;
//    }
    
//    CGRect userBgViewFrame = self.userBgView.frame;
//    userBgViewFrame.size.height = (y+1)*(width+5);
//    self.userBgView.frame = userBgViewFrame;
    
    
    
    
//    CGRect logOutBtnFrame = self.logOutGroupBtn.frame;
//    logOutBtnFrame.origin.y = CGRectGetMaxY(self.userBgView.frame)+30;
//    self.logOutGroupBtn.frame = logOutBtnFrame;
//    
//    CGRect conveyBtnFrame = self.conveyGroupBtn.frame;
//    conveyBtnFrame.origin.y = CGRectGetMaxY(self.userBgView.frame)+30;
//    self.conveyGroupBtn.frame = conveyBtnFrame;
//    
//    CGRect disbandBtnFrame = self.disbandGroupBtn.frame;
//    disbandBtnFrame.origin.y = CGRectGetMaxY(self.conveyGroupBtn.frame)+30;
//    self.disbandGroupBtn.frame = disbandBtnFrame;
//    
//    if (self.conveyGroupBtn.hidden) {
//        self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.logOutGroupBtn.frame)+50);
//    } else {
//        self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.disbandGroupBtn.frame)+50);
//    }
//
//    if ([UIScreen mainScreen].bounds.size.height==568) {
//        return;
//    }
    //  创建成员列表
    [self creatUserListWithGroupModel:model];
}
//  创建成员列表
- (void)creatUserListWithGroupModel:(GroupModel *)model{
    CGFloat width = CGRectGetWidth(self.view.frame)/5.0;
    NSInteger y = 0;
    
    NSInteger count = self.groupUsers.count;
    if ([USER_ID isEqualToNumber:model.creator]) { // 是管理员
        count += 2; // ＋2添加加减号
    } else if (!(model.category.intValue==4)) { // 不是管理员，不是私密小组
        count += 1; // +1添加减号
    }
    
    if (count>MAXUSERCOUNT) { // >14
        count = MAXUSERCOUNT+1;
    } else {
        
    }
    self.userMaxCount = count;
    for (NSInteger i = 0; i < count; i++) {
        GroupDetailUserView *view = [[[NSBundle mainBundle] loadNibNamed:@"GroupDetailUserView" owner:nil options:nil] lastObject];
        
        if ([USER_ID isEqualToNumber:model.creator]) { // 是管理员
            if (i==0 || i==1 || ((count==MAXUSERCOUNT+1)&&(i==count-1))) {
                if (i == 0) {
                    view.avatarImageView.image = [UIImage imageNamed:@"invitation"];
                } else if (i == 1) {
                    view.avatarImageView.image = [UIImage imageNamed:@"uninvitation"];
                } else if ((count==MAXUSERCOUNT+1)&&(i==count-1)){
                    
                    view.avatarImageView.image = [UIImage imageNamed:@"more"];
                    
                }
                [view.nameButton setTitle:@"" forState:UIControlStateNormal];
                view.crownIg.hidden = YES;
            } else {
                UserModel *usermodel = self.groupUsers[i-2];
                //判断微信登录的图像
                NSRange range=[usermodel.avatar rangeOfString:@"http://wx.qlogo.cn"];
                NSURL * iconUrl;
                if (range.length==0) {
                    iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]];
                }else{
                    iconUrl=[NSURL URLWithString:usermodel.avatar];
                }
                [view.avatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
                [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
                
                if ([model.creator isEqualToNumber:usermodel.id]) {
                    view.crownIg.hidden = NO;
                } else {
                    view.crownIg.hidden = YES;
                }
            }
        } else if (!(model.category.intValue==4)) { // 不是管理员，不是私密小组
            if (i==0|| ((count==MAXUSERCOUNT+1)&&(i==count-1))) {
                if (i == 0) {
                    view.avatarImageView.image = [UIImage imageNamed:@"invitation"];
                } else if ((count==MAXUSERCOUNT+1)&&(i==count-1)){
                    view.avatarImageView.image = [UIImage imageNamed:@"more"];
                }
                [view.nameButton setTitle:@"" forState:UIControlStateNormal];
                view.crownIg.hidden = YES;
            }else {
                UserModel *usermodel = self.groupUsers[i-1];
                //判断微信登录的图像
                NSRange range=[usermodel.avatar rangeOfString:@"http://wx.qlogo.cn"];
                NSURL * iconUrl;
                if (range.length==0) {
                    iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]];
                }else{
                    iconUrl=[NSURL URLWithString:usermodel.avatar];
                }
                [view.avatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
                [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
                
                if ([model.creator isEqualToNumber:usermodel.id]) {
                    view.crownIg.hidden = NO;
                } else {
                    view.crownIg.hidden = YES;
                }
            }
        } else {
            if ((count==MAXUSERCOUNT+1)&&(i==count-1)){
                view.avatarImageView.image = [UIImage imageNamed:@"more"];
                [view.nameButton setTitle:@"" forState:UIControlStateNormal];
                view.crownIg.hidden = YES;
            } else {
                UserModel *usermodel = self.groupUsers[i];
                //判断微信登录的图像
                NSRange range=[usermodel.avatar rangeOfString:@"http://wx.qlogo.cn"];
                NSURL * iconUrl;
                if (range.length==0) {
                    iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:usermodel.avatar]];
                }else{
                    iconUrl=[NSURL URLWithString:usermodel.avatar];
                }
                [view.avatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
                [view.nameButton setTitle:usermodel.nickName forState:UIControlStateNormal];
                
                if ([model.creator isEqualToNumber:usermodel.id]) {
                    view.crownIg.hidden = NO;
                } else {
                    view.crownIg.hidden = YES;
                }
            }
        }
        y = i/5;
        
        CGRect viewFrame = view.frame;
        viewFrame.size.width = width;
        viewFrame.size.height = width+5;
        viewFrame.origin.x = width*(i%5);
        viewFrame.origin.y = viewFrame.size.height*y;
        
        view.frame = viewFrame;
        [self.userBgView addSubview:view];
        // 给头像添加点击手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupUserPerfileLookUpTapGesture:)];
        [view.avatarImageView addGestureRecognizer:tap];
        view.avatarImageView.tag = i;
        view.avatarImageView.userInteractionEnabled = YES;
    }
    CGRect userBgViewFrame = self.userBgView.frame;
    userBgViewFrame.size.height = (y+1)*(width+5);
    self.userBgView.frame = userBgViewFrame;
    
    CGRect logOutBtnFrame = self.logOutGroupBtn.frame;
    logOutBtnFrame.origin.y = CGRectGetMaxY(self.userBgView.frame)+30;
    self.logOutGroupBtn.frame = logOutBtnFrame;
    
    CGRect conveyBtnFrame = self.conveyGroupBtn.frame;
    conveyBtnFrame.origin.y = CGRectGetMaxY(self.userBgView.frame)+30;
    self.conveyGroupBtn.frame = conveyBtnFrame;
    
    CGRect disbandBtnFrame = self.disbandGroupBtn.frame;
    disbandBtnFrame.origin.y = CGRectGetMaxY(self.conveyGroupBtn.frame)+30;
    self.disbandGroupBtn.frame = disbandBtnFrame;
    
    if (self.conveyGroupBtn.hidden) {
        self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.logOutGroupBtn.frame)+50);
    } else {
        self.scrollerView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.disbandGroupBtn.frame)+50);
    }
    
    if ([UIScreen mainScreen].bounds.size.height==568) {
        return;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.logoImageView.image = info[UIImagePickerControllerEditedImage];
    
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    // 上传图片文件
    [ToolOfClass toolUploadFilePathWithHttpString:uploadFilePath fileName:fileName file:self.logoImageView.image setAvatarOrLogoPath:[NSString stringWithFormat:setLogoPath,self.groupId]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 更改群组信息

- (IBAction)onGroupDetailViewModifyGroupNameBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.nameLabel.text;
    infoVC.itemTitle = NSLocalized(@"group_set_name");
    infoVC.groupId = self.groupId;
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.nameLabel.text = modifyText;
        if ([self.namedelegate respondsToSelector:@selector(groupDetailViewSetNewGroupName:)]) {
            [self.namedelegate groupDetailViewSetNewGroupName:modifyText];
        }
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)onGroupDetailViewModifyGroupDescBtnClick:(id)sender {
    PersonalProfileModifyInfoViewController *infoVC = [[PersonalProfileModifyInfoViewController alloc] init];
    infoVC.modifyText = self.descLabel.text;
    infoVC.itemTitle = NSLocalized(@"group_set_des");
    infoVC.groupId = self.groupId;
    infoVC.modifyBlock = ^(NSString *modifyText) {
        self.descLabel.text = modifyText;
        
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

#pragma mark 更换群组头像,退出，转让，解散

- (IBAction)selectedLogoBtnClick:(id)sender {
    
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
}

- (IBAction)onLogoutGroupBtnClick:(id)sender {
    
//    GroupModel *model = [self.groupInfoData lastObject];
//    NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
//    if ([num isEqualToNumber:model.creator]) {
//        
//        GroupDisbandGroupViewController *disband = [[GroupDisbandGroupViewController alloc] initWithNibName:@"GroupDisbandGroupViewController" bundle:[NSBundle mainBundle]];
//        disband.groupId = self.groupId;
//        disband.userCount = self.groupUsers.count;
//        disband.groupName = self.nameLabel.text;
//        [self.navigationController pushViewController:disband animated:YES];
//    } else {
    
        [UIAlertView alertViewWithTitle:NSLocalized(@"group_set_sureToLogout") target:self];
//    }
    
}
// 转让
- (IBAction)onConveyGroupBtnClick:(id)sender {
    
    GroupConveyGroupViewController *convey = [[GroupConveyGroupViewController alloc] init];
    convey.groupId = self.groupId;
    convey.disbandBlock = ^(NSNumber *userId){
//        self.cannotDisband = YES;
//        if (self.conveyBlock) {
//            self.conveyBlock(userId);
//        }
        [self getUpGroupDetailViewUserListAgain];
        
    };
    [self.navigationController pushViewController:convey animated:YES];
}

- (IBAction)onDisbandGroupBtnClick:(id)sender {
    UIAlertView *alert =  [UIAlertView alertViewWithTitle:NSLocalized(@"group_set_disband_alertTitle") subTitle:NSLocalized(@"group_set_disband_alertMess") target:self];
    alert.tag = 100;
}
- (IBAction)addGroupButtonClick:(id)sender {
    [self addGroupViewJoinGroup];
}

// 添加群组
- (void)addGroupViewJoinGroup
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *path = [NSString stringWithFormat:joinGroupPath,_groupId];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [ToolOfClass showMessage:NSLocalized(@"group_search_jion_success")];
            if (self.block) {
                self.block();
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

#pragma mark 跳转群组消息管理界面
- (IBAction)GroupDetaiViewGroupMessageManager:(id)sender {
    GroupMessageManagerViewController *managerView = [[GroupMessageManagerViewController alloc] init];
    managerView.groupId = self.groupId;
    [self.navigationController pushViewController:managerView animated:YES];
}

- (IBAction)onprivateGroupSwitchClick:(UISwitch *)sender {
    
    NSString *path = [NSString stringWithFormat:SetGroupPrivate,self.groupId];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"setPrivate"] = @(sender.on);
    [AFNHttpRequest httpRequestSetDataWithUrl:path parameters:parameter];
}

// 获取群组详细信息或退出群组
- (void)getUpGroupInfoOrLogoutGroup:(NSString *)str
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path;
    if ([str isEqualToString:@"GET"]) {// 请求群组信息
        [self.groupInfoData removeAllObjects];
        path = [NSString stringWithFormat:groupDetailInfoPath,self.groupId];
        [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                GroupModel *model = [[GroupModel alloc] init];
                [model setValuesForKeysWithDictionary:responseObject[@"data"]];
                [self.groupInfoData addObject:model];
                
                if (self.groupUsers.count!=0) {
                    // 显示信息
                    [self showGroupDetailViewGroupInfo];
                }
                
            } else {
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    } else { // 退出群组
        path = [NSString stringWithFormat:logoutGroupPath,self.groupId];
        [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                // 发通知，刷新群组列表
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutGroup" object:self.groupId];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
//                [UIAlertView alertViewWithTitle:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

// 群组资料中的查看成员点击手势
- (void)groupUserPerfileLookUpTapGesture:(UITapGestureRecognizer *)gesture
{
    NSInteger tag = [(UIImageView *)gesture.view tag];
    GroupModel *model = [self.groupInfoData lastObject];
    
    if ([USER_ID isEqualToNumber:model.creator]) { // 是管理员
        if (tag==0 || tag == 1) {
            
//            AddUserToGroupViewController *addUser = [[AddUserToGroupViewController alloc] init];
//            addUser.groupId = self.groupId;
//            
//            for (UserModel *model in self.groupUsers) {
//                model.cellIsSelected = NO;
//            }
//            addUser.currentUsers = [NSMutableArray arrayWithArray:self.groupUsers];
            if (tag == 0) { // 组长拉人
//                addUser.rightItem = @"邀请";
                
                [self checkInOneUserWithRightItem:NSLocalized(@"group_set_invite")];
                
            } else if (tag == 1) { // 组长踢人
//                addUser.rightItem = @"删除";
                
                [self checkInOneUserWithRightItem:NSLocalized(@"group_set_delegate")];
            }
//
//            addUser.viewBlock = ^(void){
//                
//                for (GroupDetailUserView *view in self.userBgView.subviews) {
//                    [view removeFromSuperview];
//                }
//                [self getUpGroupDetailViewGroupUserListData];
//            };
//            
//            [self.navigationController pushViewController:addUser animated:YES];
            
        } else if (tag == self.userMaxCount-1 &&tag==MAXUSERCOUNT) { //
            [self checkInAllUser];
            
        } else {
            [self checkInUserPerfileWithUserPerfileModel:self.groupUsers[tag-2]];
        }

    } else if (!(model.category.intValue==4)) { // 不是管理员，不是私密小组
        if (tag==0) {
//            AddUserToGroupViewController *addUser = [[AddUserToGroupViewController alloc] init];
//            addUser.groupId = self.groupId;
//            addUser.currentUsers = self.groupUsers;
//            addUser.rightItem = @"邀请";
//            addUser.viewBlock = ^(void){
//                
//                for (GroupDetailUserView *view in self.userBgView.subviews) {
//                    [view removeFromSuperview];
//                }
//                [self getUpGroupDetailViewGroupUserListData];
//            };
//            
//            [self.navigationController pushViewController:addUser animated:YES];
            [self checkInOneUserWithRightItem:NSLocalized(@"group_set_invite")];
        } else if (tag == self.userMaxCount-1 &&tag==MAXUSERCOUNT) {
            [self checkInAllUser];
            
        } else {
            [self checkInUserPerfileWithUserPerfileModel:self.groupUsers[tag-1]];
        }
    } else {
        
        if (tag == self.userMaxCount-1 &&tag==MAXUSERCOUNT) {
            [self checkInAllUser];
        } else {
            [self checkInUserPerfileWithUserPerfileModel:self.groupUsers[tag]];
        }
    }
}

// 邀请，删除成员
- (void)checkInOneUserWithRightItem:(NSString *)rightItem{

    AddUserToGroupViewController *addUser = [[AddUserToGroupViewController alloc] initWithNibName:@"AddUserToGroupViewController" bundle:nil];
    addUser.groupId = self.groupId;

    for (UserModel *model in self.groupUsers) {
        model.cellIsSelected = NO;
    }
    addUser.currentUsers = [NSMutableArray arrayWithArray:self.groupUsers];
    
//    addUser.currentUsers = self.groupUsers;
    addUser.rightItem = rightItem;
    addUser.viewblock = ^(void){
        for (GroupDetailUserView *view in self.userBgView.subviews) {
            [view removeFromSuperview];
        }
        [self getUpGroupDetailViewGroupUserListData];
    };
    [self.navigationController pushViewController:addUser animated:YES];
}


// 查看所有的成员
- (void)checkInAllUser{
    GroupAllUserViewController *allUser = [[GroupAllUserViewController alloc] init];
    allUser.allUserData = self.groupUsers;
    [self.navigationController pushViewController:allUser animated:YES];
    
}
// 查看个人资料
- (void)checkInUserPerfileWithUserPerfileModel:(UserModel *)userPerfileModel{
    LookUpGroupUserPerfileViewController *perfileVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    perfileVC.userPerfileModel = userPerfileModel;
    [self.navigationController pushViewController:perfileVC animated:YES];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 300) { // 没有群主
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if (buttonIndex==1) {
        
        if (alertView.tag==100) {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
            parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
            NSString *path = [NSString stringWithFormat:GroupOwnerDisbandGroup,self.groupId];
            [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] intValue] == 200) {
                    [ToolOfClass showMessage:NSLocalized(@"group_set_disband_success")];
                    [[NSNotificationCenter defaultCenter] postNotificationName:disbandGroup object:self.groupId];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                } else {
//                    [ToolOfClass showMessage:responseObject[@"message"]];
                    [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [ToolOfClass showMessage:NSLocalized(@"group_set_disband_failure")];
            }];

        } else {
            [self getUpGroupInfoOrLogoutGroup:@"POST"];
        }
    }
}

@end
