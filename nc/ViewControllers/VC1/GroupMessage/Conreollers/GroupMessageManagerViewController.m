//
//  GroupMessageManagerViewController.m
//  xjt
//
//  Created by docy admin on 7/29/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "GroupMessageManagerViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "AFNHttpRequest.h"

@interface GroupMessageManagerViewController ()

@property (nonatomic, assign) int level;

@property (weak, nonatomic) IBOutlet UILabel *Localized_newNot;
@property (weak, nonatomic) IBOutlet UILabel *Localized_aboutMe;
@property (weak, nonatomic) IBOutlet UILabel *Localized_unFaze;

@end

@implementation GroupMessageManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpGroupMessageManagerViewData];
    // 获取通知级别
    [self getUpGetGroupNotificationLevel];
}

- (void)setUpGroupMessageManagerViewData
{
    
    self.Localized_newNot.text = NSLocalized(@"group_messManager_newNot");
    self.Localized_aboutMe.text = NSLocalized(@"group_messManager_aboutMe");
    self.Localized_unFaze.text = NSLocalized(@"group_messManager_unFaze");
    self.navigationItem.title = NSLocalized(@"group_messManager_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(groupMessageManagerViewNavBackBtnClick)];
}

// 导航栏返回按钮
- (void)groupMessageManagerViewNavBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpGetGroupNotificationLevel
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    
    NSString *path = [NSString stringWithFormat:GetGroupNotificationLevel,self.groupId];

        [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] intValue]==200) {
                NSDictionary *data = responseObject[@"data"];
                int level = [data[@"level"]intValue];
                [self showGroupMessageManagerViewLevel:level];
            } else {
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
}

- (void)setUpGetGroupNotificationLevel:(int)level
{
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"level"] = @(level);
    
    NSString *path = [NSString stringWithFormat:GetGroupNotificationLevel,self.groupId];
    [AFNHttpRequest httpRequestSetDataWithUrl:path parameters:parameter];
//    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if ([responseObject[@"code"] intValue]==0) {
//            [ToolOfClass showMessage:@"设置成功!"];
//        } else {
//            [ToolOfClass showMessage:@"设置失败!"];
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [ToolOfClass showMessage:@"设置失败!"];
//    }];
}

- (void)showGroupMessageManagerViewLevel:(int)level
{
    self.level = level;
    if (level==0) {
        self.NewMessageMode.hidden = NO;
        self.aboutMeMode.hidden = YES;
        self.freeMode.hidden = YES;
    } else if (level==1) {
        self.NewMessageMode.hidden = YES;
        self.aboutMeMode.hidden = NO;
        self.freeMode.hidden = YES;
    } else {
        self.NewMessageMode.hidden = YES;
        self.aboutMeMode.hidden = YES;
        self.freeMode.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 选择模式
- (IBAction)GroupMessageManagerViewSelectedModeClick:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    int level = (int)button.tag-100;
    if (level == self.level) {
        return;
    } else {
        [self showGroupMessageManagerViewLevel:level];
        [self setUpGetGroupNotificationLevel:level];
    }
}
@end
