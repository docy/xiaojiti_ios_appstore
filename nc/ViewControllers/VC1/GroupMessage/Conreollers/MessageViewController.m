//
//  MessageViewController.m
//  nc
//
//  Created by jianghuan on 4/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//


#import "MessageViewController.h" 
#import "AppDelegate.h"
#import "MessageTableViewCell.h"
#import "XJTMessageTableViewCell.h"
#import "MessageTextView.h"

#import <LoremIpsum/LoremIpsum.h>

#import "UIButton+XJTButton.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "XJTTitleButton.h"
#import "FouncationView.h"

#import "PictureViewController.h"
#import "MainThemeViewController.h"
#import "ThemeViewController.h"

#import "Map_ViewController.h"
#import "VoteViewController.h"

#import "MessageDetailViewController.h"
#import "ShowPictureViewController.h"
#import "GroupDetailViewController.h"
#import "SubTopicViewController.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "UISegmentedControl+XJTSegmentedControl.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "MessageModel.h"
#include "UserModel.h"
#import "FooterView.h"
#import "NSString+XJTString.h"
#import "MJRefresh.h" // 刷新数据
//#import "SRRefreshView.h"
//#import "SVProgressHUD.h"
#import "UIButton+XJTButton.h"
#import "MLEmojiLabel.h"
#import "UIColor+Hex.h"
#import "AudioSession.h"
#import "SVProgressHUD.h"

#import "OpenNetWorkViewController.h"
#import "FileListViewController.h"

#define kFooterViewHeight self.view.bounds.size.height*0.05
#define defaultSpaceWith 16 // cell的子控件距离cell边缘的默认间距
#define subImageViewWidth 180 // 图片子话题图片的固定高度
#define userIconImageViewTag 1000 // 成员头像的tag值

static NSString *AutoCompletionCellIdentifier = @"AutoCompletionCell";

@interface MessageViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,MLEmojiLabelDelegate,TTTAttributedLabelDelegate,GroupDetailViewControllerDelegate,AudioSessionDelegate>
{
    NetWorkBadView *badView;
    UIToolbar * audioInputbar;   //录音功能toolbar
    UIView * recorderView;       //录音view
    UIImageView * audioPowerImgV;    //显示录音强度
    AudioSession * audioSession;    //语音类
    UILabel * alertL;           //提示录音操作
    UIImageView * RecImagev;     //录音image
    UIImageView * cancelRecImage;//取消录音image
    NSInteger AudioPlayerIsNum;  //当前播放的第几个cell的音频
    NSTimeInterval StartRecTime;         //录音开始时间
    UIButton * button;          //录音button
    BOOL RecTimeOut;            //录音超时
    UISegmentedControl* segContro;      //切换tap
}

@property (nonatomic, assign) CGFloat cellHeight; // cell的高度
@property (nonatomic, strong) NSMutableArray * cellHeightArray; //存储cell高度，0925_gxf优化显示。
@property (nonatomic, retain) NSString * lastClientId;  //存储最后一条message的cid 用于区分本地生成的message 1015_gxf_Add

@property (nonatomic,retain) UIImageView *bgImageView;;
@property (nonatomic, retain) FouncationView *founcationView; // topicView
@property (nonatomic, retain) UIButton *fcButton; // topic功能键
@property (nonatomic, assign) int messagePage; // 消息页数
//@property (nonatomic, assign) BOOL isActivityFirstRefresh; // 消息页数

@property (nonatomic, retain) NSNumber * voteSingle; //投票是否关闭
@property (nonatomic, retain) NSNumber * voteAnonymous; //投票是否多选
@property (nonatomic, retain) NSNumber * voteClose; //投票是否多选

@property (nonatomic, retain) NSNumber * On_an_ZHTid;   //上一个子话题id
@property (nonatomic, retain) UIButton *unreadButton;

@property (nonatomic, assign) BOOL isScroToUnreadMess;

@property (nonatomic, retain) NSNumber * groupCreatorID;  //当前小组创建者id

@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController; //系统文档控制器
@property (nonatomic, strong) UIImage *fileImage; //系统文档控制器
@property (nonatomic, retain) NSMutableArray *notifyArray; // @人数组

@property (nonatomic, strong) UIButton * buttonn;  //全局button 主要存储button tag  支持语音连续播放功能
@property (nonatomic, assign) BOOL isStopAutoAudioPlay;     //停止自动播放音频
@property (nonatomic, strong) UIButton * rebutt; //弹出菜单button；

@end

@implementation MessageViewController

#pragma mark GroupDetailViewControllerDelegate

- (NSMutableArray *)notifyArray{
    if (_notifyArray == nil) {
        _notifyArray = [NSMutableArray array];
    }
    return _notifyArray;
}

- (void)groupDetailViewSetNewGroupName:(NSString *)newName{
    self.navigationItem.title = newName;
    if ([self.Namedelegate respondsToSelector:@selector(messageViewSetNewGroupName:groupId:)]) {
        [self.Namedelegate messageViewSetNewGroupName:newName groupId:_groupId];
    }
}

- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}


- (id)init
{
    self = [super initWithTableViewStyle:UITableViewStylePlain];
    if (self) {
        // Register a subclass of SLKTextView, if you need any special appearance and/or behavior customisation.
        [self registerClassForTextView:[MessageTextView class]];
//        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Register a subclass of SLKTextView, if you need any special appearance and/or behavior customisation.
        [self registerClassForTextView:[MessageTextView class]];
    }
    return self ;
}

+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return UITableViewStylePlain;
}

#pragma mark - View lifecycle

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [audioSession.audioPlayer stop];
    audioSession.timer2.fireDate=[NSDate distantFuture];
    audioSession.audioPlayer=nil;
    audioSession=nil;
//    [self audioPlayerDidFinishPlaying];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([DEFAULTS boolForKey:@"firstLaunch"]) {
        [UIImageView imageViewWithBgImage:isEnglish?@"Newbie guide_3English version":@"Newbie guide_3"];
        
        [DEFAULTS setBool:NO forKey:@"firstLaunch"];
    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    //断网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    //联网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    //注册键盘消失的通知
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
    badView.contentLabel.text = NSLocalized(@"NetWorkBadView_des");
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    badView.frame=CGRectMake(0, 0, ScreenWidth, 44);
    if ([AFNetworkReachabilityManager sharedManager].isReachable) {
        badView.hidden=YES;
    }else{
        badView.hidden=NO;
    }
    [self.view addSubview:badView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteTopicSuccessful:) name:@"deleteTopicSuccess" object:nil];
    // 设置基本属性
    [self settitleview];
    [self setUpAttribute];
    
    //添加录音相关
    audioSession = [AudioSession ShareAAudioSession];
    audioSession.delegate=self;
    [audioSession setAudioSession];
    RecTimeOut=NO;
    _isStopAutoAudioPlay=YES;
    [self initAudioInputBar];
    [self addRecorderView];
    
    // 设置加好功能键
    [self setUpFouncationButton];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
    // 注册通知观察者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MessageViewRefishNewMessage:) name:@"newMessage" object:nil];
    
    UIImage *image = [UIImage imageNamed:@"The recording"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.textInputbar.leftButton setImage:image forState:UIControlStateNormal];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    self.tableView.hidden=YES;
    // 获取小组用户列表
    [self getSpecifyGroupUsersListData];
    [self setUpMessageViewMJRefresh];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    //键盘高度
//    CGRect keyBoardFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:NO];
}

-(void)settitleview{
    segContro = [UISegmentedControl segmentedControlWithItems:@[NSLocalized(@"MessageView_title1"),NSLocalized(@"MessageView_title2")] target:self action:@selector(onSegClick:)];
    
    UIView * viewSeg=[[UIView alloc] initWithFrame:segContro.frame];
    [viewSeg addSubview:segContro];
//    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.navigationItem.titleView = viewSeg;
}

- (void)onSegClick:(UISegmentedControl *)seg{

    if (seg.selectedSegmentIndex==0) {
        
    } else {
        SubTopicViewController *subVC = [[SubTopicViewController alloc] init];
        subVC.groupName = self.groupName;
        subVC.groupId = self.groupId;
        subVC.back=^(){
//            [self.navigationController popToRootViewControllerAnimated:YES];
        };
        [self.navigationController pushViewController:subVC animated:NO];
    }
}

#pragma mark - 添加音频相关内容
-(void)initAudioInputBar{
    audioInputbar=[[UIToolbar alloc] init];
    float yyy=ScreenHeight-64-44;
    if (iPhone6Plus) {
        yyy=ScreenHeight-64-42.5;
    }
    audioInputbar.frame=CGRectMake(0, yyy, ScreenWidth, 40);
//    audioInputbar.backgroundColor=[UIColor redColor];
    NSMutableArray *myToolBarItems = [NSMutableArray array];
    
    UIBarButtonItem * audioBarbutton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"The keyboard"] style:UIBarButtonItemStyleDone target:self action:@selector(audioBarbuttonClick)];
    [audioBarbutton setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    
    UIButton * leftButton=[[UIButton alloc] initWithFrame:CGRectMake(3, 5, 34, 34)];
    [leftButton setImage:[UIImage imageNamed:@"The keyboard"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(audioBarbuttonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * leftBarItem=[[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [myToolBarItems addObject:leftBarItem];
    
    button=[[UIButton alloc] initWithFrame:CGRectMake(40, 5, ScreenWidth-40-10, 33)];
    [button setBackgroundColor:[UIColor whiteColor]];
//    [button setImage:[UIImage imageNamed:@"鼠标抬起的颜色图片"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"butn_bg"] forState:UIControlStateHighlighted];
    [button setTitle:NSLocalized(@"group_chat_voice_downSpeak") forState:UIControlStateNormal];
    [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithHex:0x999999] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithHex:0xFFFFFF] forState:UIControlStateHighlighted];//colorWithHex:0xFFFFFF
    [button addTarget:self action:@selector(audioRecorderStart) forControlEvents:UIControlEventTouchDown];//开始录音
    [button addTarget:self action:@selector(audioRecorderSave) forControlEvents:UIControlEventTouchUpInside];//保存录音
    [button addTarget:self action:@selector(audioRecorderAlertCancel) forControlEvents:UIControlEventTouchDragExit];//提示取消录音
    [button addTarget:self action:@selector(audioRecorderCancelRec) forControlEvents:UIControlEventTouchUpOutside];//取消录音
    [button addTarget:self action:@selector(audioRecorderRecGoOn) forControlEvents:UIControlEventTouchDragEnter];//上滑又回到button恢复录音
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 3.0;
    button.layer.borderWidth = 0.5;
    button.layer.borderColor =[[UIColor colorWithHex:0xAAAAAA] CGColor];
    [button setExclusiveTouch:YES];
    
    UIBarButtonItem * recBarItem=[[UIBarButtonItem alloc] initWithCustomView:button];
    
    [myToolBarItems addObject:recBarItem];
    
    [audioInputbar addSubview:leftButton];
    [audioInputbar addSubview:button];
    //    [audioInputbar setItems:myToolBarItems];
    audioInputbar.hidden=YES;
    [self.view addSubview:audioInputbar];
}

//toolbar返回切换
-(void)audioBarbuttonClick{
    audioInputbar.hidden=YES;
}

-(void)audioRecorderStart{
//    NSLog(@"开始录音");
    if (![audioSession.audioRecorder isRecording]) {
        [audioSession.audioRecorder record];//首次使用应用时如果调用record方法会询问用户是否允许使用麦克风
        StartRecTime = [[NSDate date] timeIntervalSince1970];
        NSLog(@"StartRecTime is %f",StartRecTime);
        audioSession.timer.fireDate=[NSDate distantPast];
        RecTimeOut=NO;
    }
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
    recorderView.hidden=NO;
}

-(void)audioRecorderSave{
    [audioSession.audioRecorder stop];
    double stopRecTime = [[NSDate date] timeIntervalSince1970];
    double dur=stopRecTime-StartRecTime;
    if (dur<0.30) {
        [self audioRecorderCancelRec];
        [ToolOfClass showMessage:NSLocalized(@"group_chat_voice_time_short")];
        return;
    }
    int duranton=dur<1?1:dur;
//    NSLog(@"StopRecTime is %d",duranton);
    audioSession.audioRecorder=nil;
    audioSession.timer.fireDate=[NSDate distantFuture];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    [audioPowerImgV setImage:[UIImage imageNamed:@"recording1"]];
    recorderView.hidden=YES;
    if (RecTimeOut) {
//        [button setTitle:@"录音已结束,请抬起按钮" forState:UIControlStateHighlighted];
        [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
        return; //超时退出；
    }
    duranton==0?duranton=1:duranton;;
    [self SendRecAudioWithDuration:duranton];
//    NSLog(@"保存并发送录音");
}
-(void)audioRecorderAlertCancel{
//    NSLog(@"提示取消录音");
    RecImagev.hidden=YES;
    audioPowerImgV.hidden=YES;
    cancelRecImage.hidden=NO;
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger1");
    alertL.backgroundColor=[UIColor colorWithHex:0xFF001F alpha:0.6];
}
-(void)audioRecorderCancelRec{
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [audioSession.audioRecorder stop];
    audioSession.audioRecorder=nil;
    audioSession.timer.fireDate=[NSDate distantFuture];
    alertL.backgroundColor=[UIColor clearColor];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    recorderView.hidden=YES;
//    NSLog(@"取消录音");
}
-(void)audioRecorderRecGoOn{
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    alertL.backgroundColor=[UIColor clearColor];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
//    NSLog(@"上滑又回到button恢复录音");
}

-(void)addRecorderView{
    recorderView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 160)];
    recorderView.center=CGPointMake(ScreenWidth/2, ScreenHeight/2-80);
    recorderView.backgroundColor=[UIColor colorWithHex:0x000000 alpha:0.7];
    recorderView.layer.masksToBounds = YES;
    recorderView.layer.cornerRadius = 6.0;
    recorderView.layer.borderWidth = 1.0;
    recorderView.layer.borderColor = [[UIColor grayColor] CGColor];
    
    RecImagev=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paly"]];
    RecImagev.frame=CGRectMake(30, 35, 50, 80);
    [recorderView addSubview:RecImagev];
    
    audioPowerImgV=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recording1"]];
    audioPowerImgV.frame=CGRectMake(95, 35, 40, 80);
    [recorderView addSubview:audioPowerImgV];
    
    cancelRecImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cancel copy"]];
    cancelRecImage.frame=CGRectMake(50, 33, 50, 70);
    [recorderView addSubview:cancelRecImage];
    cancelRecImage.hidden=YES;
    
    alertL=[[UILabel alloc] initWithFrame:CGRectMake(10, 125, 140, 24)];
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [alertL setFont:[UIFont fontWithName:@"Heiti SC" size:15.0]];
    alertL.textColor=[UIColor whiteColor];
    alertL.textAlignment=NSTextAlignmentCenter;
    alertL.layer.masksToBounds=YES;
    alertL.layer.cornerRadius = 4.0;
    [recorderView addSubview:alertL];
    [self.view addSubview:recorderView];
    recorderView.hidden=YES;
    
//    StartRecTim is 1451556208765.600830
    //StopRecTime is 1451556213570.474121
}

#pragma mark -AudioSessionDelegate
-(void)AudioRecorderProgress:(float)progress andRecDuration:(int)duration{
    int number=1;
    if (progress>0.91) {
        number=6;
    }else if (progress>0.87) {
        number=5;
    }else if (progress>0.83) {
        number=4;
    }else if (progress>0.79) {
        number=3;
    }else if (progress>0.75) {
        number=2;
    }else {
        number=1;
    }
//    NSLog(@"RecDurationRecDurationRecDuration::::::%d",duration);
    NSString * imageName=[NSString stringWithFormat:@"recording%d",number];
    [audioPowerImgV setImage:[UIImage imageNamed:imageName]];
    if (duration>=durationTime-100) {
        alertL.text=[NSString stringWithFormat:
                     NSLocalized(@"group_chat_voice_canVoice"),(durationTime-duration)/10];
        alertL.backgroundColor=[UIColor clearColor];
        if (duration>durationTime) {  //录音超时了
            [self audioRecorderSave];
            RecTimeOut=YES;
            [button setTitle:
             NSLocalized(@"group_chat_voice_over") forState:UIControlStateHighlighted];
        }
    }
    
}

-(void)AudioPlayerProgress:(float)progress andDuration:(float)duration{

    if (duration==0.0) {
        [self audioPlayerDidFinishPlaying];
        return;
    }
    if (_messagesData.count==0) {
        return;
    }
    MessageModel *messageModel = self.messagesData[AudioPlayerIsNum];
    messageModel.volumeAnimatedNum=progress+1;
    messageModel.duration=duration;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
//    NSLog(@"AudioPlayerProgress--- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
    if (array==nil) {
        return;
    }
    [self.tableView reloadData];
//    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
}

-(void)SendRecAudioWithDuration:(int)duration{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sending")];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    parameter[@"duration"]=[NSNumber numberWithInt:duration];
    NSString * string=[[audioSession getSavePath] absoluteString];
    string = [string stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    
    NSData *audioData = [NSData dataWithContentsOfFile:string];//[[audioSession getSavePath] absoluteString]];
    [manager POST:directChatSendAudio parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:audioData name:@"audio" fileName:@"audioName" mimeType:@"audio/aac"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sendFail")];
            [SVProgressHUD dismissWithDelay:1.0];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)audioPlayerDidFinishPlaying{
    [audioSession.audioPlayer stop];
//    audioSession.audioPlayer=nil;
    audioSession.timer2.fireDate=[NSDate distantFuture];
    MessageModel *messageModel = self.messagesData[AudioPlayerIsNum];
    messageModel.volumeAnimatedNum=3;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
//    NSLog(@"audioPlayerDidFinishPlaying--------- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    [self.tableView reloadData];
    
    //添加未读语音自动播放功能
    if (AudioPlayerIsNum==0) {
        return;
    }
    MessageModel *messageModel2 = self.messagesData[AudioPlayerIsNum-1];
    if ([messageModel2.type isEqualToNumber:@15]||([messageModel.type isEqualToNumber:@8]&&[messageModel.subType isEqualToNumber:@5])){ //
        if (messageModel2.readed!=YES) {//||messageModel2.info[@"readed"]==NULL
            if (_isStopAutoAudioPlay==NO) {
                _isStopAutoAudioPlay=YES;
            }else{
                if (!_buttonn) {
                    _buttonn=[[UIButton alloc] init];
                }
                AudioPlayerIsNum--;
                _buttonn.tag=AudioPlayerIsNum;
                [self AudioPlay:_buttonn];
            }
        }
    }
    
//    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
}

- (void)LeftViewRefreshAgainInBadNetWork{
    //    NSLog(@"%s",object_getClassName(self));
    badView.hidden=NO;
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)LeftViewInGoodNetWork{
    badView.hidden=YES;
    // 获取小组用户列表
    [self getSpecifyGroupUsersListData];
    [self setUpMessageViewMJRefresh];
}

- (void)setUpMessageViewMJRefresh{
    [self.messagesData removeAllObjects];
    
    [self.tableView addFooterWithCallback:^{
        [self getUpMessageListData];
    }];
    [self.tableView footerBeginRefreshing];
}
- (void)deleteTopicSuccessful:(NSNotification *)not{
    self.messagePage = 1;
//    [self.messagesData removeAllObjects];
    [self getUpMessageListData];
}

#pragma mark 新消息刷新（通知观察者方法调用）
- (void)MessageViewRefishNewMessage:(NSNotification *)notification{
    NSArray *arr = notification.object;
    
    for (NSDictionary *dict in arr) {
        if ([dict[@"type"] isEqualToNumber:@5]) { // 踢人message
//            if ([dict[@"userId"] isEqualToNumber:USER_ID]) {
//                [self onMessageViewNavBackClick]; // 自己被踢出小组，需要退出当前小组
//                return;
//            }
            return;
        }
        //接受消息去重
        NSString *clientId = dict[@"info"][@"clientId"];
        NSString *serverId = dict[@"id"];
        if ([dict[@"type"] isEqualToNumber:@16]) {   //当收到 url链接 预加载内容 时
//            NSString *clientId = dict[@"info"][@"clientId"];
            for (long i=0; i<_messagesData.count; i++) {
                MessageModel *model=_messagesData[i];
                if ([clientId isEqualToString:model.info[@"clientId"]]) {
                    MessageModel *mdol=[[MessageModel alloc] init];
                    [mdol setValuesForKeysWithDictionary:dict];
                    NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
                    if ([timeStr isEqualToString:self.lastTimeString]) {
                        mdol.flag = 1; // 隐藏标志
                        mdol.timeViewStr = nil; // 记录下来
                    } else {
                        mdol.flag = 0; // 显示标志
                        mdol.timeViewStr = [timeStr copy]; // 记录下来
                        self.lastTimeString = [timeStr copy];
                    }
                    mdol.sendSuccessflag=1;
                    [self.messagesData replaceObjectAtIndex:i withObject:mdol];//insertObject:mdol atIndex:i];
                    NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第0个section的第i行
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationFade];
                    return;
                }
            }
        }else{
            if (_messagesData.count>=1) {
                MessageModel *model=_messagesData[0];
                
                /**
                 * 怎样界定一条msg？
                 *  根据客户端打的唯一tag：clientId。但如果是服务器产生的msg则没有这个tag。另外客户端出错时可能没有这个tag
                 *  根据服务器打的唯一tag：createAt。但如果是客户端先发送到本地list的msg里面没有这个tag。可以假定服务器不会错误滴遗失这个tag
                 *
                 * 判断步骤
                 *  先判断clientId是否重复
                 *  如果没有clientId，再判断createAt
                 */
                
                if (dict[@"info"][@"link"]!=nil&&dict[@"info"][@"desc"]!=nil) {
                    
                }else{
                    if (clientId!=nil && model.info[@"clientId"]!=nil) {//some msg donot have clientId
                        if ([clientId isEqualToString:model.info[@"clientId"]]) {
                            //手动去重操作，因为有时会出现收到2条相同的message
                            return;
                        }
                    } else if (serverId!=nil && model.id!=nil) {
                        if ([serverId isEqualToString:model.id]) {
                            //手动去重操作，因为有时会出现收到2条相同的message
                            return;
                        }
                    }
                }
                
            }
        }
        
        //本地即时生成消息 替换处理
//        NSString *clientId = dict[@"info"][@"clientId"];
        if ([_lastClientId isEqualToString:clientId]) { //若clientId 相同  1019_gxf_add 替换消息防止修改名字头像不更新
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
            if ([timeStr isEqualToString:self.lastTimeString]) {
                model.flag = 1; // 隐藏标志
                model.timeViewStr = nil; // 记录下来
            } else {
                model.flag = 0; // 显示标志
                model.timeViewStr = [timeStr copy]; // 记录下来
                self.lastTimeString = [timeStr copy];
            }
            
            if (![model.info[@"link"] isEqual:[NSNull null]]&&![model.info[@"link"] isEqualToString:model.info[@"title"]]) {
                NSMutableDictionary* newInfo=[[NSMutableDictionary alloc] initWithDictionary:model.info];
                NSString * strrr=[NSString stringWithFormat:@"%@\n%@",newInfo[@"link"],newInfo[@"desc"]];
                [newInfo setValue:strrr forKey:@"desc"];
                model.info=newInfo;
            }
            model.sendSuccessflag=1;
            model.volumeAnimatedNum=3;
            [self.messagesData replaceObjectAtIndex:0 withObject:model];
            [self.tableView reloadData];
            return;
        }
        NSNumber *groupId = dict[@"groupId"];
        if (groupId.intValue ==self.groupId.intValue) {
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            
            NSUserDefaults * ud=[NSUserDefaults standardUserDefaults];
            NSNumber* numb=[ud objectForKey:@"id"];
            if ([model.userId isEqualToNumber:numb]) {//是自己发的消息设置为已读
                model.readed=YES;
            }
            
            NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:model.updatedAt];
            if ([timeStr isEqualToString:self.lastTimeString]) {
                model.flag = 1; // 隐藏标志
                model.timeViewStr = nil; // 记录下来
            } else {
                model.flag = 0; // 显示标志
                model.timeViewStr = [timeStr copy]; // 记录下来
                self.lastTimeString = [timeStr copy];
            }
            model.sendSuccessflag=1;
            model.volumeAnimatedNum=3;
            [self.messagesData insertObject:model atIndex:0];
            [self.tableView reloadData];
        }
    }
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - // 设置加号功能键
- (void)setUpFouncationButton{
    // 添加灰色背景
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    bgImageView.backgroundColor = [UIColor blackColor];
    bgImageView.alpha = 0.5;
    bgImageView.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageViewTapGestureClick:)];
    bgImageView.userInteractionEnabled = YES;
    [bgImageView addGestureRecognizer:tap];
    self.bgImageView = bgImageView;
    [self.view addSubview:bgImageView];
    
    // 添加topic按钮
    UIButton *fcButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect rect = CGRectMake(self.view.frame.size.width-16-60, self.view.frame.size.height-50-60-64, 60, 60);
    [fcButton setNormalImage:@"add.png" andSelectedImage:@"back.png" andFrame:rect addTarget:self action:@selector(onFouncationBtnClick:)];
//    [self.view addSubview:fcButton];
    self.fcButton = fcButton;
    
    // 添加topic功能图
    FouncationView *founcationView = [[[NSBundle mainBundle] loadNibNamed:@"FouncationView" owner:self options:nil] lastObject];
//    CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - founcationView.bounds.size.width;
//    CGFloat founcationY = CGRectGetMinY(_fcButton.frame) - founcationView.bounds.size.height - 10;
//    CGRect founcationRect = founcationView.frame;
//    founcationRect.origin = CGPointMake(founcationX, founcationY);
    founcationView.frame =CGRectMake(0, ScreenHeight+founcationView.frame.size.height, ScreenWidth, founcationView.frame.size.height);
    //CGRectMake(founcationRect.origin.x, founcationRect.origin.y, founcationRect.size.width, founcationRect.size.height+70);
    founcationView.hidden = YES;
    founcationView.Localized_file.text = NSLocalized(@"topic_file");
    founcationView.Localized_map.text = NSLocalized(@"topic_map");
    founcationView.Localized_vote.text = NSLocalized(@"topic_vote");
    founcationView.Localized_topic.text = NSLocalized(@"topic_topic");
    founcationView.Localized_image.text = NSLocalized(@"topic_image");
    for (UIView *view in founcationView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn addTarget:self action:@selector(onAddFouncationButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    self.founcationView = founcationView;
    [self.view addSubview:founcationView];
    
    CGPoint point1=self.founcationView.center;
    point1.x+=280;
    CGSize size=[[UIScreen mainScreen] bounds].size;
    if (size.height==480) {
    point1.y+=71;
    }
//    self.founcationView.center=point1;
    
    self.view.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, ([UIScreen mainScreen].bounds.size.height/2)+64);

}
- (void)onMessageViewTapGestureClick:(UITapGestureRecognizer *)gesture{
    [self onFouncationBtnClick:_rebutt];
//    self.founcationView.hidden = YES;
//    self.bgImageView.hidden = YES;
//    self.fcButton.selected = NO;
}

#pragma mark 弹出菜单 文件 地图，图片，主题等功能键的点击事件

- (void)onAddFouncationButtonClick:(UIButton *)sender{
    [self onFouncationBtnClick:self.fcButton];
    if (sender.tag == 0) {
        VoteViewController *voteVC = [[VoteViewController alloc] init];
        voteVC.groupId=self.groupId;
        [self.navigationController pushViewController:voteVC animated:YES];
    } else if (sender.tag == 1) {
        
        // block 反向传值
//        MapViewController *mapVC = [[MapViewController alloc] init];
        Map_ViewController * mapVC =[[Map_ViewController alloc] init];
        mapVC.groupId=self.groupId;
        mapVC.isYulan=NO;
        [self.navigationController pushViewController:mapVC animated:YES];
        
    } else if (sender.tag == 2) {
        //文件子话题
        FileListViewController * filevC=[[FileListViewController alloc] initWithNibName:@"FileListViewController" bundle:nil];
        filevC.groupId=self.groupId;
        [self.navigationController pushViewController:filevC animated:YES];
    }else if (sender.tag == 3) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];//self.view];
        
    } else if (sender.tag == 4) {
//        MainThemeViewController *mainVC = [[MainThemeViewController alloc] init];
        
        ThemeViewController *mainVC = [[ThemeViewController alloc] init];
        mainVC.groupId = self.groupId;
        [self.navigationController pushViewController:mainVC animated:YES];
    }else if (sender.tag == 8) {
        [self onFouncationBtnClick:_rebutt];
    }else{
    
    }
}

#pragma mark  功能键的点击触发方法
- (void)onFouncationBtnClick:(UIButton *)sender{
    [self.textView resignFirstResponder];
    [self.textInputbar.textView resignFirstResponder];

    sender.selected = !sender.selected;//YES;//

    [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        if (sender.selected) {
            self.founcationView.hidden = NO;
            self.bgImageView.hidden = NO;
//            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGFloat founcationY = ScreenHeight - 64 -_founcationView.frame.size.height;//+40
            CGRect point1=self.founcationView.frame;
            point1.origin.y=founcationY;//-=self.founcationView.center.x<160?0:280;
            point1.origin.x=0;
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.5;
        } else {
//            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGFloat founcationY = ScreenHeight - _founcationView.bounds.size.height;//CGRectGetMaxY(_fcButton.frame)
            CGRect point1=self.founcationView.frame;
            point1.origin.y=founcationY +_founcationView.bounds.size.height;//+280;
            point1.origin.x=0;
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        if (sender.selected) {
            
        } else {
            self.founcationView.hidden = YES;
            self.bgImageView.hidden = YES;
            
        }
    }];
    
}

- (void)onMessageViewNavBackClick{
    if (badView.hidden==YES) {
        if (self.cellBlock) {
            self.cellBlock();
        }
        [self sendResetUnread];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
//通知服务器退出聊天详情页面
-(void)sendResetUnread{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    [manager POST:[NSString stringWithFormat:ResetUnread,_groupId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 发送失败
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark 设置基本属性
- (void)setUpAttribute{
    self.navigationItem.title = self.groupName;
    // 导航栏属性
//    CGSize titleSize = [self.groupName sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:17.0]}];
//    XJTTitleButton *titleButton = [[XJTTitleButton alloc] initWithFrame:CGRectMake(0, 0, ceil(titleSize.width)+20, 30)];
//    [titleButton setTitle:self.groupName forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleButton;
//    [titleButton addTarget:self action:@selector(onGetGroupAttributeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightItem1 = [UIBarButtonItem itemWithIcon:@"navigation_groupdata " target:self action:@selector(onGetGroupAttributeBtnClick)];
//    UIBarButtonItem *rightItem2 = [UIBarButtonItem itemWithIcon:@"topic list" target:self action:@selector(onGetUpGroupSubTopicBtnClick)];
    self.navigationItem.rightBarButtonItems = @[rightItem1];//,rightItem2
    
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onMessageViewNavBackClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onMessageViewNavBackClick)];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    // 数据初始化
    self.cellHeight = 0;
    self.messagesData = [NSMutableArray array];
    self.users = [NSMutableArray array];
    self.cellHeightArray=[NSMutableArray array];
//    self.usersAvatar = [NSMutableArray array];
    self.groupUsers = [NSMutableArray array];
    self.messagePage = 1;
    self.lastTimeString = [NSString string];

//    self.isActivityFirstRefresh = YES;
    //    self.users = @[@"Allen", @"Anna", @"Alicia", @"Arnold", @"Armando", @"Antonio", @"Brad", @"Catalaya", @"Christoph", @"Emerson", @"Eric", @"Everyone", @"Steve"];
    
    self.channels = @[@"General", @"Random", @"iOS", @"Bugs", @"Sports", @"Android", @"UI", @"SSB"];
    self.emojis = @[@"m", @"man", @"machine", @"block-a", @"block-b", @"bowtie", @"boar", @"boat", @"book", @"bookmark", @"neckbeard", @"metal", @"fu", @"feelsgood"];
    
    //  设置tableView属性
    self.bounces = YES;
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = NO;
    self.inverted = YES;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // send 按钮
    [self.rightButton setTitle:NSLocalized(@"group_chat_private_sendBtn") forState:UIControlStateNormal];
    self.textView.placeholder = NSLocalized(@"group_chat_textPH");
    
    // 设置textInputbar属性
    [self.textInputbar.editorTitle setTextColor:[UIColor darkGrayColor]];
    [self.textInputbar.editorLeftButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
    [self.textInputbar.editorRightButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];

    self.textInputbar.autoHideRightButton = NO;
    self.rightButton.hidden=YES;
    
    [self cancelAutoCompletion];
    
    UIImage *image = [UIImage imageNamed:@"plus sign_icon"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIButton * rebutt=[[UIButton alloc] init];
    [self.rebutt setBackgroundImage:image forState:UIControlStateNormal];
    [self.rebutt addTarget:self action:@selector(rightButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.rebutt.frame=CGRectMake(ScreenWidth-33, 8, 27, 27);//self.textInputbar.rightButton.frame;
    
    _rebutt.translatesAutoresizingMaskIntoConstraints=NO;
    [self.textInputbar addSubview:self.rebutt];
    NSDictionary *views = @{@"_rebutt": self.rebutt,};
    NSDictionary *metrics = @{@"up": @8,@"down": @8,};
    [self.textInputbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_rebutt(27)]-down-|" options:0 metrics:metrics views:views]];
    [self.textInputbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_rebutt(27)]-7-|" options:0 metrics:metrics views:views]];
    
//    self.textInputbar.maxCharCount = 256;
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
//        self.textInputbar.counterPosition = SLKCounterPositionBottom;
    self.textInputbar.textView.returnKeyType=UIReturnKeySend;
    
    self.typingIndicatorView.canResignByTouch = YES;
    
    [self.autoCompletionView registerClass:[MessageTableViewCell class] forCellReuseIdentifier:AutoCompletionCellIdentifier];
    [self registerPrefixesForAutoCompletion:@[@"@", @"#", @":"]];
    
    /**
     *  未读条数button
     */
    if (self.unreadCount.intValue!=0) {
        
        UIImage *image = [UIImage imageNamed:@"news tips"];
        
        UIButton *unreadButton = [UIButton buttonWithType:UIButtonTypeCustom];
        unreadButton.frame = CGRectMake(0, 0, ScreenWidth, image.size.height);
        unreadButton.backgroundColor = [UIColor colorWithRed:254/255.0 green:236/255.0 blue:196/255.0 alpha:1.0];
        self.unreadButton = unreadButton;
        [unreadButton setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        unreadButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [unreadButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if (self.unreadCount.intValue>99) {
          [unreadButton setTitle:NSLocalized(@"group_chat_unread0") forState:UIControlStateNormal];
        } else {
//            [unreadButton setTitle:self.unreadCount.stringValue forState:UIControlStateNormal];
            [unreadButton setTitle:[NSString stringWithFormat:NSLocalized(@"group_chat_unread1"),self.unreadCount] forState:UIControlStateNormal];
        }
        
        [self.view addSubview:unreadButton];
        
        [UIView animateWithDuration:3 animations:^{
            unreadButton.alpha = 0;
        } completion:^(BOOL finished) {
            [unreadButton removeFromSuperview];
        }];
        
    }
}

-(UIButton*)rebutt{
    if (!_rebutt) {
        _rebutt=[[UIButton alloc] init];
    }
    return _rebutt;
}

-(void)rightButtonClick:(UIButton*)sender{
    NSLog(@"%s",__func__);
    [self onFouncationBtnClick:sender];
}

#pragma mark 请求数据
// 请求数据,获取消息列表
- (void)getUpMessageListData{
    [self getUpMessageListOrSpecifyGroupUsersList:getMessageListPath];
}
// 获取小组用户列表
- (void)getSpecifyGroupUsersListData{
    [self.users removeAllObjects];
//    [self.usersAvatar removeAllObjects];
    [self.groupUsers removeAllObjects];
    [self getUpMessageListOrSpecifyGroupUsersList:getSpecifyGroupUsersListPath];
}
// 获取消息列表或者用户列表
- (void)getUpMessageListOrSpecifyGroupUsersList:(NSString *)urlPath{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *str = nil;
    if ([urlPath isEqualToString:getMessageListPath]) {
        str = [NSString stringWithFormat:urlPath,self.groupId,self.messagePage];
    } else {
        str = [NSString stringWithFormat:urlPath,self.groupId];
    }
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
            if ([urlPath isEqualToString:getMessageListPath]) { // 获取小组消息列表
//                NSLog(@"%@",responseObject);
                
                if (self.messagePage==1) {
                    [self.messagesData removeAllObjects];
                }
                for (NSDictionary *dict in responseObject[@"data"]) {
//                    NSLog(@"data is %@",dict);
                    if (![dict[@"type"] isEqualToNumber:@5]) { // 被踢出
                        MessageModel *model = [[MessageModel alloc] init];
                        [model setValuesForKeysWithDictionary:dict];
                        model.sendSuccessflag=1;
                        if (model.info[@"link"]!=nil) {
                            if (![model.info[@"link"] isEqualToString:model.info[@"title"]]) {
                                NSMutableDictionary* newInfo=[[NSMutableDictionary alloc] initWithDictionary:model.info];
                                NSString * strrr=[NSString stringWithFormat:@"%@\n%@",newInfo[@"link"],newInfo[@"desc"]];
                                [newInfo setValue:strrr forKey:@"desc"];
                                model.info=newInfo;
                            }
                        }
                        model.volumeAnimatedNum=3;
                        model.witeActivityIsShow=NO;
                        
                        if (model.info[@"readed"]!=NULL) {
                            model.readed=YES;
                        }else{
                            model.readed=NO;
                        }
                        
                        if ([model.info[@"message"] isEqualToString:@""]) {
                            
                        }else{
                            [self.messagesData addObject:model];
                        }
                        
//                        if ([model.type isEqualToNumber:@7]&&[model.subType isEqualToNumber:@2]) {  //图片子话题  存到图片数组
//                            [self.messImageData addObject:model];
//                        }
                    }
                }
                self.lastTimeString = @"";
                for (NSInteger i = self.messagesData.count-1; i>=0; i--) {
                    
                    MessageModel *mes = self.messagesData[i];
                    NSString *timeStr = [ToolOfClass toolGetLocalWeekDateFormateWithUTCDate:mes.updatedAt];
                    if (![timeStr isEqualToString:self.lastTimeString]) {
                        mes.flag = 0; // 显示标志
                        self.lastTimeString = [timeStr copy];
                        mes.timeViewStr = [timeStr copy]; // 记录下来
                    } else {
                        mes.flag = 1;
                        mes.timeViewStr = nil; // 记录下来
                    }
                }
                
                badView.hidden=YES;
                [self.tableView reloadData];
                double delayInSeconds = 0.6;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self.tableView footerEndRefreshing];
                    [self.tableView reloadData];
                    self.tableView.hidden=NO;
                });
                
                self.messagePage++;
                
            } else { // 获取小组用户列表
                for (NSDictionary *dict in responseObject[@"data"]) {
                    [self.users addObject:dict[@"nickName"]];
//                    [self.usersAvatar addObject:dict[@"avatar"]];
                    UserModel *model = [[UserModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    if ([model.userType isEqualToNumber:@0]) {
                        _groupCreatorID=model.id;
                    }
                    [self.groupUsers addObject:model];
                }
            }
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.tableView footerEndRefreshing];
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    segContro.selectedSegmentIndex = 0;
    
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_editing"] style:UIBarButtonItemStylePlain target:self action:@selector(editRandomMessage:)];
    editItem.title = @"edit";
    
    UIBarButtonItem *appendItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_append"] style:UIBarButtonItemStylePlain target:self action:@selector(fillWithText:)];
    appendItem.title = @"append";
    
    UIBarButtonItem *typeItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_typing"] style:UIBarButtonItemStylePlain target:self action:@selector(simulateUserTyping:)];
    typeItem.title = @"typing";
    
//    self.navigationItem.rightBarButtonItems = @[editItem, appendItem, typeItem];
    
    self.founcationView.hidden = YES;
    self.bgImageView.hidden = YES;
    self.fcButton.selected = NO;
    
    audioSession = [AudioSession ShareAAudioSession];
    audioSession.delegate=self;
    [audioSession setAudioSession];
    RecTimeOut=NO;
    _isStopAutoAudioPlay=YES;
    [self initAudioInputBar];
    [self addRecorderView];
    
}

#pragma mark - Action Methods

#pragma mark 获取小组详情
- (void)onGetGroupAttributeBtnClick{
    [self.view endEditing:YES];
    GroupDetailViewController *groupDetailVC = [[GroupDetailViewController alloc] init];
    groupDetailVC.groupId = self.groupId;
//    groupDetailVC.groupUsers = self.groupUsers;
    groupDetailVC.namedelegate = self;
    [self.navigationController pushViewController:groupDetailVC animated:YES];
}

#pragma mark 获取小组子话题
- (void)onGetUpGroupSubTopicBtnClick{
    [self.view endEditing:YES];
    SubTopicViewController *subVC = [[SubTopicViewController alloc] init];
    subVC.groupName = self.groupName;
    subVC.groupId = self.groupId;
    [self.navigationController pushViewController:subVC animated:YES];
}
#pragma mark 导航栏按钮点击事件
//
- (void)fillWithText:(id)sender{
    if (self.textView.text.length == 0)
    {
        int sentences = (arc4random() % 4);
        if (sentences <= 1) sentences = 1;
        self.textView.text = [LoremIpsum sentencesWithNumber:sentences];
    }
    else {
        [self.textView slk_insertTextAtCaretRange:[NSString stringWithFormat:@" %@", [LoremIpsum word]]];
    }
}

- (void)simulateUserTyping:(id)sender{
    if (!self.isEditing && !self.isAutoCompleting) {
        [self.typingIndicatorView insertUsername:[LoremIpsum name]];
    }
}

- (void)editRandomMessage:(id)sender{
    int sentences = (arc4random() % 10);
    if (sentences <= 1) sentences = 1;
    
    [self editText:[LoremIpsum sentencesWithNumber:sentences]];
}

#pragma mark 长按cell的触发事件
- (void)editCellMessage:(UIGestureRecognizer *)gesture
{

    MessageTableViewCell *cell = (MessageTableViewCell *)gesture.view;
    MessageModel *message = self.messagesData[cell.indexPath.row];
//    [self editText:message.body];
    
    [self.tableView scrollToRowAtIndexPath:cell.indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)editLastMessage:(id)sender
{
    if (self.textView.text.length > 0) {
        return;
    }
    
    NSInteger lastSectionIndex = [self.tableView numberOfSections]-1;
    NSInteger lastRowIndex = [self.tableView numberOfRowsInSection:lastSectionIndex]-1;
    
    MessageModel *lastMessage = [self.messagesData objectAtIndex:lastRowIndex];
    
//    [self editText:lastMessage.body];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - Overriden Methods

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status
{
    // Notifies the view controller that the keyboard changed status.
}

- (void)textWillUpdate
{
    // Notifies the view controller that the text will update.
    
    [super textWillUpdate];
}

//关闭tableview reloaddata 时全屏刷动画的问题
- (void)textDidUpdate:(BOOL)animated
{
    // Notifies the view controller that the text did update.
    [super textDidUpdate:NO];//animated];
}

#pragma mark 输入框左侧按钮的事件处理
- (void)didPressLeftButton:(id)sender
{
    [self.textView resignFirstResponder];
    [self.textInputbar.textView resignFirstResponder];
    audioInputbar.hidden=NO;
//    [super didPressLeftButton:sender];
}

#pragma mark send 发送消息按钮的事件处理
- (void)didPressRightButton:(id)sender
{
    
    if (self.broadcast==1&&![USER_ID isEqualToNumber:self.groupCreator]) {
        ALERT_HOME(NSLocalized(@"HOME_alert_cueTitle"),NSLocalized(@"group_chat_cannotSpeak"));
        return;
    }
    // 发送消息至服务器
    [self sendMessageViewMessage];
//    [super didPressRightButton:sender];
}
#pragma mark 发送消息至服务器
- (void)sendMessageViewMessage{
    if ([self.textView.text isEqualToString:@""]) {
        return;
    }
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    [app socketStatusIsConnect];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];

    NSString *text = self.textView.text;
    
    for (NSInteger i = 0; ; i++) {
        if ([text hasSuffix:@"\n"]) {
            text = [text stringByReplacingCharactersInRange:NSMakeRange(text.length-1, 1) withString:@""];
        } else {
            break;
        }
    }
    
    parameter[@"message"] = text;
    parameter[@"groupId"] = self.groupId;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    _lastClientId=parameter[@"clientId"];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    if (self.notifyArray.count!=0) {
        parameter[@"targets"] = self.notifyArray;
    }
    
    NSUserDefaults * ud=[NSUserDefaults standardUserDefaults];
    NSNumber* numb=[ud objectForKey:@"id"];
    //本地即时显示消息
    if (![self.groupName isEqualToString:NSLocalized(@"group_chat_bugaolan")]) {
        //不是布告栏就发消息
        MessageModel *model = [[MessageModel alloc] init];
        model.avatar = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"];
        model.sender=[[NSUserDefaults standardUserDefaults] objectForKey:@"nickName"];
        model.sendSuccessflag=1;
        NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        NSDate *dateFormatted = [NSDate date];
        NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600*2];
        NSString *currentTime = [formatter stringFromDate:currentDate];
        model.updatedAt=currentTime;
        NSDictionary * dic=[[NSDictionary alloc] initWithObjectsAndKeys:parameter[@"clientId"],@"clientId",parameter[@"message"],@"message", nil];
        model.info =dic;
        model.flag = 1; // 隐藏标志
        model.timeViewStr = nil; // 记录下来
        [self.messagesData insertObject:model atIndex:0];
        [self.tableView reloadData];
    } else {
        if ([_groupCreatorID isEqualToNumber:numb]) {  //是布告栏但是是创建者就可以发消息
            MessageModel *model = [[MessageModel alloc] init];
            model.avatar = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"];
            model.sender=[[NSUserDefaults standardUserDefaults] objectForKey:@"nickName"];
            model.sendSuccessflag=1;
            NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            NSDate *dateFormatted = [NSDate date];
            NSDate *currentDate = [dateFormatted dateByAddingTimeInterval:8*3600*2];
            NSString *currentTime = [formatter stringFromDate:currentDate];
            model.updatedAt=currentTime;
            NSDictionary * dic=[[NSDictionary alloc] initWithObjectsAndKeys:parameter[@"clientId"],@"clientId",parameter[@"message"],@"message", nil];
            model.info =dic;
            model.flag = 1; // 隐藏标志
            model.timeViewStr = nil; // 记录下来
            [self.messagesData insertObject:model atIndex:0];
            [self.tableView reloadData];
        }
    }
    
    [manager POST:sendMessagePath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (![responseObject[@"code"] isEqualToNumber:@200]) { // 发送失败
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:responseObject[@"message"] message:nil delegate:nil cancelButtonTitle:NSLocalized(@"HOME_alert_sure") otherButtonTitles:nil, nil];
            [alert show];
        } else if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [self.notifyArray removeAllObjects];
            self.notifyArray = nil;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        MessageModel * modell=_messagesData[0];
        modell.sendSuccessflag=0;
        [self.messagesData replaceObjectAtIndex:0 withObject:modell];//insertObject:model atIndex:0];
        NSIndexPath *te=[NSIndexPath indexPathForRow:0 inSection:0];//刷新第一个section的第二行
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te, nil] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark  设置时间显示格式
- (NSString *)setUpFormatterDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    return [formatter stringFromDate:[NSDate date]];
}

- (void)didPressArrowKey:(id)sender{
    [super didPressArrowKey:sender];

    UIKeyCommand *keyCommand = (UIKeyCommand *)sender;
    
    if ([keyCommand.input isEqualToString:UIKeyInputUpArrow]) {
        [self editLastMessage:nil];
    }
}

- (NSString *)keyForTextCaching{
    return [[NSBundle mainBundle] bundleIdentifier];
}

- (void)didPasteMediaContent:(NSDictionary *)userInfo{
    // Notifies the view controller when the user has pasted a media (image, video, etc) inside of the text view.
    
    SLKPastableMediaType mediaType = [userInfo[SLKTextViewPastedItemMediaType] integerValue];

    NSData *contentData = userInfo[SLKTextViewPastedItemData];
    
    if ((mediaType & SLKPastableMediaTypePNG) || (mediaType & SLKPastableMediaTypeJPEG)) {
        
        MessageModel *message = [MessageModel new];
        message.sender = [LoremIpsum name];
//        message.body = @"Attachment";
        message.createdAt = [self setUpFormatterDate];
//        message.userAvatar = [UIImage imageWithData:contentData scale:[UIScreen mainScreen].scale];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
        UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
        
        [self.tableView beginUpdates];
        [self.messagesData insertObject:message atIndex:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
        [self.tableView endUpdates];
        
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    }
}

- (void)willRequestUndo{
    // Notifies the view controller when a user did shake the device to undo the typed text
    
    [super willRequestUndo];
}

- (void)didCommitTextEditing:(id)sender{
    // Notifies the view controller when tapped on the right "Accept" button for commiting the edited text
    
    MessageModel *message = [MessageModel new];
    message.sender = [LoremIpsum name];
//    message.body = [self.textView.text copy];
    
    [self.messagesData removeObjectAtIndex:0];
    [self.messagesData insertObject:message atIndex:0];
    [self.tableView reloadData];
    
    [super didCommitTextEditing:sender];
}

// 取消编辑
- (void)didCancelTextEditing:(id)sender{
    // Notifies the view controller when tapped on the left "Cancel" button
    
    [super didCancelTextEditing:sender];
}


- (BOOL)canPressRightButton{
    return [super canPressRightButton];
}

// @,#,:条件筛选
//- (BOOL)canShowAutoCompletion{
//    NSArray *array = nil;
//    NSString *prefix = self.foundPrefix;
//    NSString *word = self.foundWord;
//    self.searchResult = nil;
//    
//    if ([prefix isEqualToString:@"@"]) {
//        if (word.length > 0) {
//            array = [self.users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
//        }
//        else {
//            array = self.users;
//        }
//    }
//    else if ([prefix isEqualToString:@"#"] && word.length > 0) {
//        array = [self.channels filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
//    }
//    else if ([prefix isEqualToString:@":"] && word.length > 1) {
//        array = [self.emojis filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
//    }
//    
//    if (array.count > 0) {
////        array = [array sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
//    }
//    
//    self.searchResult = [[NSMutableArray alloc] initWithArray:array];
//    
//    return self.searchResult.count > 0;
//}

- (void)didChangeAutoCompletionPrefix:(NSString *)prefix andWord:(NSString *)word{
    NSArray *array = nil;
    self.searchResult = nil;

    if ([prefix isEqualToString:@"@"]) {
        if (word.length > 0) {
            array = [self.users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
        }
        else {
            array = self.users;
        }
    }
    else if ([prefix isEqualToString:@"#"] && word.length > 0) {
        array = [self.channels filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
    }
    else if ([prefix isEqualToString:@":"] && word.length > 1) {
        array = [self.emojis filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
    }
    
    if (array.count > 0) {
//                array = [array sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    
    self.searchResult = [[NSMutableArray alloc] initWithArray:array];
    
    BOOL show = (self.searchResult.count > 0);
    [self showAutoCompletionView:show];
}



- (CGFloat)heightForAutoCompletionView{
    CGFloat cellHeight = [self.autoCompletionView.delegate tableView:self.autoCompletionView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    return cellHeight*self.searchResult.count;
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableView]) {
        return self.messagesData.count;
    }
    else {
        return self.searchResult.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.tableView]) {
        return [self messageCellForRowAtIndexPath:indexPath];
    }
    else {
        return [self autoCompletionCellForRowAtIndexPath:indexPath];
    }
}

- (XJTMessageTableViewCell *)messageCellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *MessengerCellIdentifier = @"MessengerCellId";
    XJTMessageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"XJTMessageTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(editCellMessage:)];
        [cell addGestureRecognizer:longPress];
    }
//    if (!cell.textLabel.text) {
//        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(editCellMessage:)];
//        [cell addGestureRecognizer:longPress];
//    }
    // 固定内容
    MessageModel *next_messageModel = nil;
    MessageModel *messageModel = self.messagesData[indexPath.row];
    if (self.messagesData.count-1>indexPath.row) {
        next_messageModel = self.messagesData[indexPath.row+1];
    }
    cell.subImageView.hidden = YES;
//    NSString *str=nil;
    NSURL * iconUrl=nil;
    if (messageModel.avatar!=nil) {
        //判断微信登录的图像
        NSRange range=[messageModel.avatar rangeOfString:@"http://wx.qlogo.cn"];
        if (range.length==0) {
            iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:messageModel.avatar]];
        }else{
            iconUrl=[NSURL URLWithString:messageModel.avatar];
        }
    }
    cell.createdAtLabel.text = [ToolOfClass toolGetLocalAmDateFormateWithUTCDate:messageModel.updatedAt];
    
    if ([messageModel.type isEqualToNumber:@3] || [messageModel.type isEqualToNumber:@2]||[messageModel.type isEqualToNumber:@4]) {
        cell.senderLabel.text = nil;
        cell.userAvatarImageView.image = [UIImage imageNamed:@"horn"];
        cell.userAvatarImageView.userInteractionEnabled = NO;
    } else {
        cell.senderLabel.text = messageModel.sender;
        
        [cell.userAvatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
        // 为用户头像添加手势，点击时进入用户个人详情
        UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageViewUserIconTapGestureClick:)];
        cell.userAvatarImageView.tag = userIconImageViewTag+indexPath.row;
        cell.userAvatarImageView.userInteractionEnabled = YES;
        [cell.userAvatarImageView addGestureRecognizer:userIconTap];
    }
    
    cell.indexPath = indexPath;
    cell.usedForMessage = YES;
    
    CGFloat timeMaxY = CGRectGetMaxY(cell.timeView.frame);
    CGRect senderFrame = cell.senderLabel.frame;
    CGRect avatarFrame = cell.userAvatarImageView.frame;
    CGRect creatAtFrame = cell.createdAtLabel.frame;
    CGRect GanTHFrame=cell.ganTanHao.frame;
    
    CGPoint timeCenter = cell.timeLabel.center;
    CGRect leftViewFrame = cell.leftImageView.frame;
    CGRect rightViewFrame = cell.rightImageView.frame;
    
    /*
        显示时间框
     */
    if (messageModel.flag==0) { // 显示时间框
//        if (indexPath.row!=_messagesData.count-1) {//如果是数组最后一条 不显示时间戳 0925修改_gxf
            cell.timeView.hidden = NO;
            cell.timeLabel.text = messageModel.timeViewStr;
            if ([cell.timeLabel.text isEqualToString:NSLocalized(@"group_chat_today")]) {
                leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-30-32-20)/2.0;
                
            } else {
                leftViewFrame.size.width = (CGRectGetWidth(self.view.frame)-150-32-20)/2.0;
            }
//        }
        
        if (messageModel.info[@"link"]!=nil){//[messageModel.type isEqualToNumber:@16]){
            cell.timeView.hidden = YES;
            timeMaxY = defaultSpaceWith;
        }
    } else { // 隐藏时间框
        cell.timeView.hidden = YES;
        timeMaxY = defaultSpaceWith;
    }
    timeCenter.x = cell.timeView.center.x;
    cell.timeLabel.center = timeCenter;
    cell.leftImageView.frame = leftViewFrame;
    rightViewFrame.size.width = cell.leftImageView.frame.size.width;
    rightViewFrame.origin.x = self.view.frame.size.width - cell.leftImageView.frame.size.width-16;
   
    cell.rightImageView.frame = rightViewFrame;
    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = ceil(senderSize.width);
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    
    if (senderFrame.size.width>200) {
         senderFrame.size.width = 200;
    }
    
    senderFrame.origin.y = timeMaxY;
    avatarFrame.origin.y = timeMaxY;
    creatAtFrame.origin.y = timeMaxY;
    cell.senderLabel.frame = senderFrame;
    cell.userAvatarImageView.frame = avatarFrame;
    if (cell.senderLabel.text==nil) {
        creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame);
    } else {
        creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
    }
    
    cell.createdAtLabel.frame = creatAtFrame;
    
    GanTHFrame.origin.x = CGRectGetMaxX(cell.createdAtLabel.frame);
    cell.ganTanHao.frame=GanTHFrame;
    if (messageModel.sendSuccessflag==1) {
        cell.ganTanHao.hidden=YES;
    }else{
        cell.ganTanHao.hidden=NO;
    }
    
    // Cells must inherit the table view's transform
    // This is very important, since the main table view may be inverted
    cell.transform = self.tableView.transform;
    
    // 定制bodyLabel,themeLabel高度
    // 设置文本行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    NSDictionary *attributeTheme = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    CGFloat width;
    
    NSDictionary *info = messageModel.info;
    
    /*
     定制回复label（对主题的回复）
     */
    CGRect responseFrame = cell.responseLabel.frame;
    if ([messageModel.type isEqualToNumber:@8]) { // 消息为评论message
        
        NSString * string;
        if ([messageModel.info[@"topicType"] isEqualToNumber:@4]) {
            string = [NSString stringWithFormat:@"%@@%@%@",NSLocalized(@"group_chat_response"),info[@"creatorName"],NSLocalized(@"group_chat_send_vote")];
        }else{
            string = [NSString stringWithFormat:@"%@@%@%@",NSLocalized(@"group_chat_response"),info[@"creatorName"],NSLocalized(@"group_chat_send_topic")];
        }
        cell.responseLabel.text = string;
        cell.responseLabel.hidden = NO;
        
//        NSLog(@"[messageModel.info[topicid]:%@",messageModel.info[@"topicId"]);
        
        /**
         *  对联系子话题的回复合并显示，不重复输出主题
         */
        if ([next_messageModel.info[@"topicId"] isEqual:messageModel.info[@"topicId"]]) {//&&!firstLoad
            cell.responseLabel.hidden = YES;
            cell.userAvatarImageView.hidden=YES;
        }else{
            cell.userAvatarImageView.hidden=NO;
        }
        responseFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
    } else {
        cell.responseLabel.hidden = YES;
        cell.userAvatarImageView.hidden = NO;
    }
    cell.responseLabel.frame = responseFrame;
    /* 
        定制主题存在（消息为topic,或者评论）
     */
    
    if ([messageModel.type isEqualToNumber:@7]||[messageModel.type isEqualToNumber:@8]) {
        NSMutableString * themstr=[NSMutableString stringWithFormat:@"%@",info[@"title"]];
        NSRange range=[themstr rangeOfString:NSLocalized(@"group_chat_send_vote_sending")];
        if (range.length!=0) {
            [themstr deleteCharactersInRange:range];
        }
        
        NSString * closedVote=@"";
        if ([messageModel.type isEqualToNumber:@7]){
            if ([messageModel.subType isEqualToNumber:@6]) {
                //关闭投票
                closedVote=NSLocalized(@"group_chat_send_vote_close");
            }
        }
        cell.themeLabel.text = [NSString stringWithFormat:@"#%@%@#",closedVote,themstr];
        
        width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 2*defaultSpaceWith;
        
        CGRect themeFrame = cell.themeLabel.frame;
        if (cell.responseLabel.hidden) {
            themeFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        } else {
            
            themeFrame.origin.y = CGRectGetMaxY(cell.responseLabel.frame);
        }
        themeFrame.size.width = width;
        
//        CGRect themeRect = [info[@"title"] boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        CGRect themeRect = CGRectMake(0, 0, 0, 0);
        if (cell.themeLabel.text.length==0) {
            themeRect = [cell.themeLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributeTheme context:nil];
        } else {
            cell.themeLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.themeLabel.text attributes:attributeTheme];
            themeRect = [cell.themeLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        }
        
        themeFrame.size.height = ceil(themeRect.size.height);
        
        /**
         *  对联系子话题的回复合并显示，不重复输出主题 so隐藏控件
         */
        if (cell.userAvatarImageView.hidden==YES) {
            cell.themeLabel.hidden = YES;
            themeFrame.size.height = 0;
        }else{
            cell.themeLabel.hidden = NO;
        }
        
        cell.themeLabel.frame = themeFrame;
        
        cell.detailButton.hidden = NO;
        cell.detailButton.tag = indexPath.row;
        [cell.detailButton addTarget:self action:@selector(onMessageDetailVCButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
    } else {
        cell.themeLabel.hidden = YES;
        cell.themeLabel.text = nil;
        cell.detailButton.hidden = YES;
        
        width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - defaultSpaceWith;
    }
    /*
        定制bodyLabel
     */
    CGRect frame = cell.bodyLabel.frame;
    frame.size.width = width;
    cell.bodyLabel.emojiDelegate = self;
    CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
    if ([messageModel.type isEqualToNumber:@7]||[messageModel.type isEqualToNumber:@8]) {
        if ([messageModel.type isEqualToNumber:@7]) {
//            cell.bodyLabel.text = info[@"desc"];
            [cell.bodyLabel setEmojiText:info[@"desc"]];
        } else {
//            cell.bodyLabel.text = info[@"message"];
            [cell.bodyLabel setEmojiText:info[@"message"]];
            if ([messageModel.subType isEqualToNumber:@5]) {
                cell.bodyLabel.text = NSLocalized(@"group_list_send_voice");
            }
        }
        
        frame.origin.y = CGRectGetMaxY(cell.themeLabel.frame);
//        cell.bodyLabel.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1];
    } else {
//        cell.bodyLabel.text = info[@"message"];
        [cell.bodyLabel setEmojiText:info[@"message"]];
        frame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
//        cell.bodyLabel.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1];
        // 判断加入，退出小组
        if ([messageModel.type isEqualToNumber:@4]) {
            [cell.bodyLabel setEmojiText:[NSString stringWithFormat:@"%@%@",messageModel.sender,NSLocalized(@"group_list_logout")]];
//            cell.bodyLabel.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
        }  else if ([messageModel.type isEqualToNumber:@3] || [messageModel.type isEqualToNumber:@2]) {
            [cell.bodyLabel setEmojiText:[NSString stringWithFormat:@"%@%@%@",NSLocalized(@"group_chat_wolcom"),messageModel.sender,NSLocalized(@"group_list_join")]];
//            cell.bodyLabel.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
        }
    }
//    if (cell.bodyLabel.text.length==0) {
        bodyFrame = [cell.bodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        cell.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.bodyLabel.text attributes:attributes];
//        bodyFrame = [cell.bodyLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//
//    }
    frame.size.height = ceil(bodyFrame.size.height);
    cell.bodyLabel.frame = frame;
    [cell.bodyLabel sizeToFit];
    
    /**
     *  定制urlBodyLabel链接预加载内容
     */
    if (messageModel.info[@"link"]!=nil){//[messageModel.type isEqualToNumber:@16]
//        NSLog(@"---------Message:%@",messageModel.info[@"content"]);
        cell.urlBodyLabel.hidden=NO;
        CGRect frame2 = cell.urlBodyLabel.frame;
        frame2.size.width = width;
        cell.urlBodyLabel.text = info[@"content"];
        frame2.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
        CGRect ubodyFrame = CGRectMake(0, 0, 0, 0);
        ubodyFrame = [cell.urlBodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        frame2.size.height = ceil(ubodyFrame.size.height)+23;
        cell.urlBodyLabel.frame = frame2;
    }else{
        cell.urlBodyLabel.hidden=YES;
    }
    
    
    // 存储高度，用于返回cell的高度
    CGFloat cellHeight = 0;
    if (frame.size.height==0) {
//        cellHeight = CGRectGetMaxY(cell.senderLabel.frame)+ 21;
        cellHeight = CGRectGetMaxY(cell.themeLabel.frame);
    } else {
        cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
        if (messageModel.info[@"link"]!=nil){//[messageModel.type isEqualToNumber:@16]){
            cellHeight = CGRectGetMaxY(cell.urlBodyLabel.frame);
        }
    }
    self.cellHeight = cellHeight;
    
    /*
        定制subImageView(子话题图片)
     */
    cell.voteView.hidden=YES;
    if ([messageModel.type isEqualToNumber:@7] || [messageModel.type isEqualToNumber:@8]) { // topic
        if ([messageModel.subType isEqualToNumber:@2] || ([messageModel.subType isEqualToNumber:@3]&&![messageModel.info[@"topicType"] isEqualToNumber:@4]&&![messageModel.type isEqualToNumber:@8])) { // 图片子话题
            
            cell.subImageView.userInteractionEnabled = YES;
            cell.subImageView.hidden = NO;
            CGRect subImageFrame = cell.subImageView.frame;
            subImageFrame.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
            CGFloat subW = [messageModel.info[@"thumbNail_w"] floatValue];
            CGFloat subH = [messageModel.info[@"thumbNail_h"] floatValue];
            CGFloat w = 0;
            CGFloat h = 0;
            if (subW>subH) {
                w = 180;
                h = 180*(subH/subW);
            } else {
                h = 180;
                w = 180*(subW/subH);
            }
            
            subImageFrame.size.width = w;
            subImageFrame.size.height = h;
            
            cell.subImageView.frame = subImageFrame;
            [cell.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:messageModel.info[@"thumbNail"]]] placeholderImage:nil];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSubImageViewTapGestureClick:)];
            cell.subImageView.tag = indexPath.row;
            [cell.subImageView addGestureRecognizer:tap];
            
            // 存储高度，用于返回cell的高度
            cellHeight = CGRectGetMaxY(cell.subImageView.frame)+5;
            self.cellHeight = cellHeight;
        }else if ([messageModel.subType isEqualToNumber:@4]||[messageModel.subType isEqualToNumber:@1]) {//[messageModel.info[@"topicType"] isEqualToNumber:@4]
            if ([messageModel.type isEqualToNumber:@8]) {
                //回复时不显示
            }else{
                cell.voteView.hidden=NO;
                cell.subImageView.hidden = YES;
                cell.subImageView.image = nil;
                if ([messageModel.subType isEqualToNumber:@1]) { //文件子话题
                    NSURL *fileURL= nil;
                    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *documentDir = [documentPaths objectAtIndex:0];
                    NSString *path = [NSString stringWithFormat:@"%@/%@",iconPath,messageModel.info[@"filePath"]] ;
                    fileURL = [NSURL fileURLWithPath:path];
                    
                    [self setupDocumentControllerWithURL:fileURL];
                    NSInteger iconCount = [self.docInteractionController.icons count];
                    UIImage * imageic=nil;
                    if (iconCount > 0)
                    {
                        imageic = [self.docInteractionController.icons objectAtIndex:iconCount - 1];
                    }
                    [cell.voteIconImage setImage:imageic];//[UIImage imageNamed:@"System address book"]];
                }else{
                    [cell.voteIconImage setImage:[UIImage imageNamed:@"vote box"]];
                }
                
                width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 2*defaultSpaceWith;
                CGRect voteVFrame = cell.voteView.frame;
                
                if (info[@"desc"]!=nil){
                    voteVFrame.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
                }else{
                    voteVFrame.origin.y = CGRectGetMaxY(cell.themeLabel.frame);
                }
                voteVFrame.size.width = width;
                // 存储高度，用于返回cell的高度
                cell.voteView.frame=voteVFrame;
                cellHeight = CGRectGetMaxY(cell.voteView.frame)+5;
                if ([messageModel.subType isEqualToNumber:@1]) {
                    cell.voteLabel.text=[NSString stringWithFormat:@"  %@",messageModel.info[@"fileName"]];
                    
                    long long sizee=[messageModel.info[@"fileSize"] longLongValue];
                    NSString * sizeeef= [self convertFileSize:sizee];
                    cell.detailVoteLabel.text=[NSString stringWithFormat:@"  %@",sizeeef];
                }else{
                    NSString * str1;
                    NSString * str2;
                    NSString * str3;
                    
                    if ([messageModel.info[@"closed"] isEqualToNumber:@1]) {    str1=NSLocalized(@"group_chat_vote_close");  }else{   str1=NSLocalized(@"group_chat_vote_voteing");   }
                    if ([messageModel.info[@"single"] isEqualToNumber:@1]) {    str2=NSLocalized(@"group_chat_vote_selOne");  }else{   str2=NSLocalized(@"group_chat_vote_selMore");   }
                    if ([messageModel.info[@"anonymous"] isEqualToNumber:@1]) {    str3=NSLocalized(@"group_chat_vote_anonymity");  }else{   str3=NSLocalized(@"group_chat_vote_sign");   }
                    
                    cell.voteLabel.text=[NSString stringWithFormat:@"%@",str1];
                    cell.detailVoteLabel.text=[NSString stringWithFormat:@"(%@/%@)",str2,str3];
                }
                
                self.cellHeight = cellHeight;
            }
            
        }else {
            cell.subImageView.hidden = YES;
            cell.subImageView.image = nil;
            cell.voteView.hidden=YES;
        }
        
    } else { // 非topic
        cell.subImageView.hidden = YES;
        cell.subImageView.image = nil;
        cell.voteView.hidden=YES;
        CGRect subImageFrame = cell.subImageView.frame;
        subImageFrame.size.height = 0;
        cell.subImageView.frame = subImageFrame;
        self.cellHeight = cellHeight;
    }
    
    /*
     定制语音消息
     */
    cell.bodyLabel.hidden=NO;
    if ([messageModel.type isEqualToNumber:@15]||([messageModel.type isEqualToNumber:@8]&&[messageModel.subType isEqualToNumber:@5])){ //语音信息  后添加了回复里的语音显示
        cell.RecAudioView.hidden=NO;
        CGFloat MaxWidth=ScreenWidth-65-40;
        width=100;
//        if ([messageModel.info[@"duration"] intValue]==0.0) {
//            
//        }else{
//            
//            NSLog(@"widthwidthwidthwidth===:%f",width);
////            if (width<100) {
////                width=100;
////            }
//        }
        float widthP=(MaxWidth-100)*([messageModel.info[@"duration"] intValue]/60.0f);
        CGRect audioVFrame = cell.RecAudioView.frame;
        audioVFrame.origin.y = CGRectGetMinY(cell.bodyLabel.frame);//cell.senderLabel.frame);
        
        cell.bodyLabel.hidden=YES;
        
//        NSLog(@"width+widthPwidth+widthP ----:%ld------:%f and:%f",indexPath.row,width,widthP);
        audioVFrame.size.width = width+widthP;
        // 存储高度，用于返回cell的高度
        cell.RecAudioView.frame=audioVFrame;
        cellHeight = CGRectGetMaxY(cell.RecAudioView.frame)+5;
        
        cell.audioTimeL.text=[NSString stringWithFormat:@"%d\"",[messageModel.info[@"duration"] intValue]];//
        int nnn=messageModel.volumeAnimatedNum;
        if (indexPath.row!=AudioPlayerIsNum) {
            nnn=3;
        }
        [cell.audioPowerImg setImage:[UIImage imageNamed:[NSString stringWithFormat:@"volume%d",nnn]]];
        cell.audio.tag=indexPath.row;
        [cell.audio addTarget:self action:@selector(AudioPlay:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString * UserId=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
        if ([messageModel.userId isEqualToNumber:[NSNumber numberWithInt:[UserId intValue]]]) {//自己发的语音
            [cell.audioViewBG setImage:[UIImage imageNamed:@"Voice box_gray"]];
            [cell.audioViewBg2 setImage:[UIImage imageNamed:@"The triangle_gray"]];
        }else{
            [cell.audioViewBG setImage:[UIImage imageNamed:@"Voice box"]];
            [cell.audioViewBg2 setImage:[UIImage imageNamed:@"The triangle"]];
        }
        if (messageModel.witeActivityIsShow) {
            cell.witeActivityInd.hidden=NO;
            [cell.witeActivityInd startAnimating];
            cell.audioPowerImg.hidden=YES;
        }else{
            cell.witeActivityInd.hidden=YES;
            cell.audioPowerImg.hidden=NO;
        }
        
        if (messageModel.info[@"readed"]!=NULL||messageModel.readed==YES) {
            cell.dotImage.hidden=YES;
        }else{
            cell.dotImage.hidden=NO;
        }
        
    }else{
        cell.RecAudioView.hidden=YES;
    }
    self.cellHeight = cellHeight;
    
    [self.cellHeightArray insertObject:[NSNumber numberWithFloat:self.cellHeight] atIndex:indexPath.row];
    // 详情按钮的位置设置
    CGPoint detailBtnCenter = cell.detailButton.center;
    if (messageModel.flag==0) { // 显示时间头
        detailBtnCenter.y = (self.cellHeight-45)/2.0 +45+8;
        if (messageModel.info[@"link"]!=nil){//[messageModel.type isEqualToNumber:@16]){
            detailBtnCenter.y = self.cellHeight/2.0 +16;
        }
    } else {
        detailBtnCenter.y = self.cellHeight/2.0 +16;
    }
//    cell.voteIconImg.center=CGPointMake(cell.voteIconImg.center.x, 0);
    
    cell.detailButton.center = detailBtnCenter;
    
    return cell;
}

-(NSString *) convertFileSize:(long long)size {
    long kb = 1024;
    long mb = kb * 1024;
    long gb = mb * 1024;
    
    if (size >= gb) {
        return [NSString stringWithFormat:@"%.1f GB", (float) size / gb];
    } else if (size >= mb) {
        float f = (float) size / mb;
        if (f > 100) {
            return [NSString stringWithFormat:@"%.0f MB", f];
        }else{
            return [NSString stringWithFormat:@"%.1f MB", f];
        }
    } else if (size >= kb) {
        float f = (float) size / kb;
        if (f > 100) {
            return [NSString stringWithFormat:@"%.0f KB", f];
        }else{
            return [NSString stringWithFormat:@"%.1f KB", f];
        }
    } else
        return [NSString stringWithFormat:@"%lld B", size];
}

- (MessageTableViewCell *)autoCompletionCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageTableViewCell *cell = (MessageTableViewCell *)[self.autoCompletionView dequeueReusableCellWithIdentifier:AutoCompletionCellIdentifier];
    cell.indexPath = indexPath;
    
    NSString *item = self.searchResult[indexPath.row];
    NSLog(@"foundPrefix==%@",self.foundPrefix);
//    if ([self.foundPrefix isEqualToString:@"#"]) {
//        item = [NSString stringWithFormat:@"# %@", item];
//    }
//    else if ([self.foundPrefix isEqualToString:@":"]) {
//        item = [NSString stringWithFormat:@":%@:", item];
//    }
    
    cell.titleLabel.text = item;
    cell.titleLabel.font = [UIFont systemFontOfSize:14.0];
    
    UserModel *userModel = self.groupUsers[indexPath.row];
//    NSString *str = [iconPath stringByAppendingString:self.usersAvatar[indexPath.row]];
    
    //判断微信登录的图像
    NSRange range=[userModel.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.length==0) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:userModel.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:userModel.avatar];
    }
    [cell.thumbnailView sd_setImageWithURL:iconUrl placeholderImage:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.tableView]) {

        if (self.cellHeight==0) {
            return 44;
        }
        
        float hight=0;
        if (indexPath.row>=_cellHeightArray.count) {
            hight=_cellHeight;
//            NSLog(@"heightForRowAtIndexPath height is ---new-----:%f indexPath.row %ld",hight,indexPath.row);
        }else{
            hight=[_cellHeightArray[indexPath.row] floatValue];
//            NSLog(@"heightForRowAtIndexPath height is ---lod-----:%f indexPath.row %ld",hight,indexPath.row);
        }
        
        return hight; //indexPath.row>=_cellHeightArray.count? _cellHeight: [_cellHeightArray[indexPath.row] floatValue];//self.cellHeight+5;
        
//        return self.cellHeight;
    }
    else {
//        NSLog(@"heightForRowAtIndexPath height is ----22----:%f",kMinimumHeight);
        return kMinimumHeight;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:self.autoCompletionView]) {
        UIView *topView = [UIView new];
        topView.backgroundColor = self.autoCompletionView.separatorColor;
        return topView;
    }

    return nil;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([tableView isEqual:self.autoCompletionView]) {
        return 0.5;
    }
    return 0.0;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.autoCompletionView]) {
        
        NSMutableString *item = [self.searchResult[indexPath.row] mutableCopy];
        
        if ([self.foundPrefix isEqualToString:@"@"] && self.foundPrefixRange.location == 0) {
            [item appendString:@":"];
        }
        else if ([self.foundPrefix isEqualToString:@":"]) {
            [item appendString:@":"];
        }
        
        [item appendString:@" "];
        
        [self acceptAutoCompletionWithString:item keepPrefix:YES];
        
        UserModel *model = self.groupUsers[indexPath.row];
        [self.notifyArray addObject:model.id];

        
        
    } else {
        
        XJTMessageTableViewCell *cell = (XJTMessageTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (cell.detailButton.hidden==NO) {
            MessageDetailViewController *messageDetail = [[MessageDetailViewController alloc] init];
            MessageModel *model = self.messagesData[indexPath.row];
            messageDetail.topicId = model.info[@"topicId"];
            messageDetail.groupId = self.groupId;
            
            messageDetail.users = self.users;
            messageDetail.groupUsers = self.groupUsers;
            
            if ([model.type isEqualToNumber:@8]) {
                messageDetail.topicType = model.info[@"topicType"];
            }else if ([model.type isEqualToNumber:@7]){
                if ([model.subType isEqualToNumber:@6]) {
                    //关闭投票
                    messageDetail.topicType = [NSNumber numberWithInt:4];
                }else{
                    messageDetail.topicType = model.subType;
                }
                if ([model.subType isEqualToNumber:@1]) {
                    self.fileImage=cell.voteIconImage.image;
                }
            }
            //    messageDetail.topicId = self.messagesData[sender.tag];
            messageDetail.fileImage=_fileImage;
//            messageDetail.hidesBottomBarWhenPushed = YES; // 01.30改
            [self.navigationController pushViewController:messageDetail animated:YES];
        }
    }
}

-(void)AudioPlay:(UIButton*)button{
    BOOL xiangtongCell=NO;

    if (AudioPlayerIsNum!=button.tag) {
        xiangtongCell=NO;
    }else{
        xiangtongCell=YES;
    }
    
    AudioPlayerIsNum=button.tag;
    MessageModel *model = self.messagesData[button.tag];
    
    if ([audioSession.audioPlayer isPlaying]) {
        if (xiangtongCell) {
            _isStopAutoAudioPlay=NO;
            [self audioPlayerDidFinishPlaying];
            return;
        }else{
            [self audioPlayerDidFinishPlaying];
        }
        
    }
    
    MessageModel *messageModel = self.messagesData[AudioPlayerIsNum];
    messageModel.witeActivityIsShow=YES;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
//    NSLog(@"audioPlayerDidFinishPlaying--------- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    [self.tableView reloadData];
//    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];

    
    
//    NSLog(@"播放语音");
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString * Inbox=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents"]];
    if(![fileManager fileExistsAtPath:Inbox]){
        BOOL bo=[fileManager createDirectoryAtPath:Inbox withIntermediateDirectories:YES attributes:nil error:nil];
        NSLog(@"bobobobo is %d",bo);
    }
    
    NSString *savedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",@"audioName.m4a"]];
//    NSLog(@"filepath_dow---%@",savedPath);
    NSMutableString * filep=[NSMutableString stringWithFormat:@"%@%@",iconPath,model.info[@"audioPath"]];
    [fileManager removeItemAtPath:savedPath error:nil];  //删除文件
//    if(![fileManager fileExistsAtPath:savedPath]) //如果不存在 下载
//    {
        [self downloadFileWithOption:nil withInferface:filep savedPath:savedPath downloadSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
        } downloadFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        } progress:^(float progress) {
            
        }];
    
    messageModel.readed=YES;
    self.messagesData[AudioPlayerIsNum]=messageModel;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString * path=[NSString stringWithFormat:MarkAudioReaded,messageModel.info[@"audioId"]];
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 发送成功
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
//    }
}

- (void)downloadFileWithOption:(NSDictionary *)paramDic
                 withInferface:(NSString*)requestURL
                     savedPath:(NSString*)savedPath
               downloadSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
               downloadFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
                      progress:(void (^)(float progress))progress
{
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    NSMutableURLRequest *request =[serializer requestWithMethod:@"GET" URLString:requestURL parameters:paramDic error:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    //    [serializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:savedPath append:NO]];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float p = (float)totalBytesRead / totalBytesExpectedToRead;
        progress(p);
//        NSLog(@"totalBytesExpectedToRead：%f",  (float)totalBytesExpectedToRead);//(float)totalBytesRead /
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        NSLog(@"语音下载成功");
        MessageModel *messageModel = self.messagesData[AudioPlayerIsNum];
        messageModel.witeActivityIsShow=NO;
        NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
        NSArray * array=[NSArray arrayWithObjects:indexp, nil];
        [self.tableView reloadData];
        audioSession.audioPlayer=nil;
        if (![audioSession.audioPlayer isPlaying]) {
            [audioSession.audioPlayer play];
            audioSession.timer2.fireDate=[NSDate distantPast];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        success(operation,error);
        
        NSLog(@"语音下载失败");
        
    }];
    
    [operation start];
}

// 点击cell上的按钮进入message详情页
#pragma mark 点击cell上的按钮进入message详情页
- (void)onMessageDetailVCButtonClick:(UIButton *)sender{
    [self.textInputbar.textView resignFirstResponder];
    MessageDetailViewController *messageDetail = [[MessageDetailViewController alloc] init];
    MessageModel *model = self.messagesData[sender.tag];
    messageDetail.topicId = model.info[@"topicId"];
    messageDetail.users = self.users;
    messageDetail.groupUsers = self.groupUsers;
    if ([model.type isEqualToNumber:@8]) {
        messageDetail.topicType = model.info[@"topicType"];
    }else if ([model.type isEqualToNumber:@7]){
        messageDetail.topicType = model.subType;
    }
//    messageDetail.topicId = self.messagesData[sender.tag];
    [self.navigationController pushViewController:messageDetail animated:YES];
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you ovveride this method, to call super.
    [super scrollViewDidScroll:scrollView];
    
//    if (self.unreadButton!=nil) {
//        self.unreadButton.hidden = YES;
//        [self.unreadButton removeFromSuperview];
//        self.unreadButton = nil;
//    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
}

#pragma mark - UITextViewDelegate Methods

- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    [super textView:textView shouldChangeTextInRange:range replacementText:text];
    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
        [self didPressRightButton:nil];
        textView.text=@"";
        return NO;
    }
    return YES;
//    return [super textView:textView shouldChangeTextInRange:range replacementText:text];
}

- (void)textViewDidChangeSelection:(SLKTextView *)textView{
    [super textViewDidChangeSelection:textView];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    //    [self.tableView beginUpdates];
    //    [self.tableView endUpdates];
    if (self.tableView.contentSize.height>0) { //解决异常闪退  1014_gxf添加
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    }
}
#pragma mark 图片子话题的图片的手势点击事件
- (void)onSubImageViewTapGestureClick:(UITapGestureRecognizer *)gesture{
    UIImageView *imageView = (UIImageView *)gesture.view;
    
    MessageModel *model = self.messagesData[imageView.tag];
    
    if ([model.subType  isEqual: @3]) { // 地图
//        MapViewController *mapVC = [[MapViewController alloc] init];
        Map_ViewController *mapVC = [[Map_ViewController alloc] init];
        mapVC.latitude = [model.info[@"latitude"] floatValue];
        mapVC.longitude = [model.info[@"longitude"] floatValue];
        mapVC.groupId  =self.groupId;
        mapVC.isYulan=YES;
        [self.navigationController pushViewController:mapVC animated:YES];
    } else {
        ShowPictureViewController *picVC = [[ShowPictureViewController alloc] init];
        picVC.imageInfo = model.info;
        [self presentViewController:picVC animated:NO completion:nil];
    }
    
}
// 成员头像的点击事件
- (void)onMessageViewUserIconTapGestureClick:(UITapGestureRecognizer *)gesture{
    
    UIImageView *userIconImageView = (UIImageView *)gesture.view;
    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    MessageModel *model = self.messagesData[userIconImageView.tag-userIconImageViewTag];
    for (UserModel *userModel in self.groupUsers) {
        if ([model.userId isEqualToNumber:userModel.id]) {
            userPerVC.userPerfileModel = userModel;
            [self.navigationController pushViewController:userPerVC animated:YES];
        }
    }
}

#pragma mark delegate for imagePickerConreoller
// 协议中的方法 ,当用户选中图片时被调用
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    MainThemeViewController *themeVC = [[MainThemeViewController alloc] init];
//    themeVC.titleString=@"图片主题";
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        
        NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
        NSString *imageName = [url lastPathComponent];
        themeVC.imageName = imageName;
    } else {
        themeVC.imageName = @"origin.jpg";
    }
    
    themeVC.titleString = [ToolOfClass toolGetTopic_title_Image];
    
    themeVC.image = info[UIImagePickerControllerOriginalImage];
    themeVC.groupId = self.groupId;
    [self.navigationController pushViewController:themeVC animated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==2){
        [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        picker.allowsEditing = YES;
    } else if (buttonIndex==1) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark 未读消息按钮点击事件
//- (void)checkInUnreadMessage{
//    
//    int count = self.unreadCount.intValue;
//    if (count != 0) {
//        
//        if (count <= self.messagesData.count) {
//            
//            [self.tableView reloadData];
//            
//            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
//            
//            [self performSelector:@selector(scrollToUnreadMessage) withObject:nil afterDelay:1.0];
//            
//        } else {
//            
//            self.isScroToUnreadMess = YES;
//            [self getUpMessageListData];
//        }
//        
//        if (self.unreadButton!=nil) {
//            self.unreadButton.hidden = YES;
//            [self.unreadButton removeFromSuperview];
//            self.unreadButton = nil;
//        }
//    }
//}


//- (void)scrollToUnreadMessage{
//
//    int count = self.unreadCount.intValue;
//    
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:count-1 inSection:0]];
//    
//    [UIView animateWithDuration:2.0 animations:^{
//        cell.alpha = 0;
//    }];
//    [UIView animateWithDuration:2.0 animations:^{
//        cell.alpha = 1;
//    }];
//}

//- (void)scrollerToMoreUnreadMess{
//    
//    int count = self.unreadCount.intValue;
//    
//    if (count <= self.messagesData.count) {
//        [self.tableView reloadData];
//        
//        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
//        
//        [self performSelector:@selector(scrollToUnreadMessage) withObject:nil afterDelay:1.0];
//    } else {
//        
//        self.isScroToUnreadMess = YES;
//        [self getUpMessageListData];
//    }
//}


- (void)mlEmojiLabel:(MLEmojiLabel*)emojiLabel didSelectLink:(NSString*)link withType:(MLEmojiLabelLinkType)type
{
    switch(type){
        case MLEmojiLabelLinkTypeURL:
//            NSLog(@"点击了链接%@",link);
            ;
            NSRange range=[link rangeOfString:@"http://"];
            if (range.length==0) {
                link=[NSString stringWithFormat:@"http://%@",link];
            }
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:link]];
            //            [[[UIActionSheet alloc] initWithTitle:link delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
            
            break;
        case MLEmojiLabelLinkTypePhoneNumber:
//            NSLog(@"点击了电话%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"tel://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeEmail:
//            NSLog(@"点击了邮箱%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"mailto://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeAt:
//            NSLog(@"点击了用户%@",link);
            break;
        case MLEmojiLabelLinkTypePoundSign:
//            NSLog(@"点击了话题%@",link);
            break;
        default:
//            NSLog(@"点击了不知道啥%@",link);
            break;
    }
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
    return self;
}


#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    //    NSInteger numToPreview = 0;
    //
    //	numToPreview = [self.dirArray count];
    //
    //    return numToPreview;
    return 1;//[self.dirArray count];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    [previewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"click.png"] forBarMetrics:UIBarMetricsDefaultPrompt];
    
    NSURL *fileURL = nil;
    //    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path = [[NSString stringWithFormat:@"%@/Inbox",documentDir] stringByAppendingPathComponent:@"filename"];
    fileURL = [NSURL fileURLWithPath:path];
    return fileURL;
}

-(id)fileImage{
    if (!_fileImage) {
        _fileImage=[[UIImage alloc] init];
    }
    return _fileImage;
}

@end