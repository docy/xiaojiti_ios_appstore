//
//  GroupConveyGroupViewController.m
//  nc
//
//  Created by docy admin on 15/10/16.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "GroupConveyGroupViewController.h"
#import "CompanyTableViewCell.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIAlertView+AlertView.h"
#import <AFNetworking/AFNetworking.h>
#import "UserModel.h"
#import "ToolOfClass.h"
#import "UIImageView+WebCache.h"

@interface GroupConveyGroupViewController () <UIAlertViewDelegate>

@property (nonatomic, retain) NSMutableArray *groupUsers;
@property (nonatomic, retain) NSNumber *userId;
@property (nonatomic ,retain) UIAlertView *alert;

@property (nonatomic ,assign) NSInteger row;


@end

@implementation GroupConveyGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpGroupConveyGroupViewData];
}

- (void)setUpGroupConveyGroupViewData{
    self.navigationItem.title = NSLocalized(@"group_convey_title");
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(setUpGroupConveyGroupViewBack)];
    self.groupUsers = [NSMutableArray array];
    self.row = -1;
    [self getUpGroupConveyGroupViewUserListData];
}

- (void)setUpGroupConveyGroupViewBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpGroupConveyGroupViewUserListData
{
    [self.groupUsers removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *path = [NSString stringWithFormat:getSpecifyGroupUsersListPath,self.groupId];
    [manager GET:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] intValue] == 200) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                UserModel *model = [[UserModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                if (![model.id isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
                    model.cellIsSelected = NO;
                    [self.groupUsers addObject:model];
                }
            }
            
            if (self.groupUsers.count == 0) {
                [ToolOfClass showMessage:NSLocalized(@"group_convey_noUserToConvey")];
                return ;
            }
            
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"CellId";
    CompanyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell==nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CompanyTableViewCell" owner:self options:nil] lastObject];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.phoneLabel.hidden = YES;
    if (indexPath.row==self.groupUsers.count-1) {
        cell.line.hidden = YES;
    } else {
        cell.line.hidden = NO;
    }
    
    CGRect rect = cell.line.frame;
    rect.size.height = 0.5;
    cell.line.frame = rect;
    
    UserModel *model = self.groupUsers[indexPath.row];
    cell.companyNameLabel.text = model.nickName;
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.location==NSNotFound) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [cell.iconImView sd_setImageWithURL:iconUrl placeholderImage:nil];
    cell.setCurrentCpyBtn.tag = indexPath.row;
    [cell.setCurrentCpyBtn addTarget:self action:@selector(conveyToUser:) forControlEvents:UIControlEventTouchUpInside];
    
    if (model.cellIsSelected==YES) {
        cell.setCurrentCpyBtn.selected = YES;
    } else {
        cell.setCurrentCpyBtn.selected = NO;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return NSLocalized(@"group_convey_hTitle");
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    [self.tableView reloadData];
//    CompanyTableViewCell *cell = (CompanyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.setCurrentCpyBtn.selected = YES;
//    
//    UserModel *model = self.groupUsers[indexPath.row];
//    self.userId = model.id;
//
//    if (self.alert==nil) {
//        self.alert = [UIAlertView alertViewWithTitle:@"转让小组" subTitle:[NSString stringWithFormat:@"%@将成为该小组的组长，确定之后你将失去组长的身份",[cell.companyNameLabel text]] target:self];
//        self.alert.tag = 100;
//    }
//    
//    [self.alert setMessage:[NSString stringWithFormat:@"%@将成为该小组的组长，确定之后你将失去组长的身份",[cell.companyNameLabel text]]];
//    
//    [self.alert show];

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    CompanyTableViewCell *cell = (CompanyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.setCurrentCpyBtn.selected = NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex==1) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        
        if (alertView.tag==100) { // 转让小组
            NSString *path = [NSString stringWithFormat:GroupOwnerTransfer,self.groupId,self.userId];
            [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] intValue] == 200) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:conveyGroupOwer object:self.userId];
                    if (self.disbandBlock) {
                        self.disbandBlock(self.userId);
                    }
                    
                    [self.navigationController popViewControllerAnimated:YES];
//                    [UIAlertView alertViewWithTitle:@"转让成功" subTitle:@"是否继续退出小组?" target:self];
                } else {
//                    [ToolOfClass showMessage:responseObject[@"message"]];
                    [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [ToolOfClass showMessage:NSLocalized(@"group_convey_failure")];
                
            }];
        } else {
            
            NSString *path = [NSString stringWithFormat:logoutGroupPath,self.groupId];
            [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([responseObject[@"code"] isEqualToNumber:@200]) {
                    // 发通知，刷新群组列表
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutGroup" object:self.groupId];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                } else {
//                    [UIAlertView alertViewWithTitle:responseObject[@"message"]];
                    [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [ToolOfClass showMessage:NSLocalized(@"group_convey_logout_failure")];
            }];
        }
        
    } else {
        
        if (alertView.tag!=100) {
            
//            if (self.navigationController.viewControllers.count>3) {
//               [self.navigationController popToViewController:self.navigationController.viewControllers[2] animated:YES];
//            } else {
//                [self.navigationController popToRootViewControllerAnimated:YES];
//            }
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
}

- (void)conveyToUser:(UIButton *)sender{
    
    if (self.row==-1) {
        self.row = sender.tag;
    }
    
    if (!sender.selected) {
        
        UserModel *model = self.groupUsers[sender.tag];
        model.cellIsSelected = YES;
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:sender.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
        self.userId = model.id;
        if (self.alert==nil) {
            self.alert = [UIAlertView alertViewWithTitle:NSLocalized(@"group_convey_title") subTitle:[NSString stringWithFormat:@"%@%@",model.nickName,NSLocalized(@"group_convey_alertMess")] target:self];
            self.alert.tag = 100;
        }
    
        [self.alert setMessage:[NSString stringWithFormat:@"%@%@",model.nickName,NSLocalized(@"group_convey_alertMess")]];
        
//        [self.alert show];
    }
    
    if (self.row!=sender.tag) {
        UserModel *lastUser = self.groupUsers[self.row];
        lastUser.cellIsSelected = NO;
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        self.row = sender.tag;
    }
    
    [self.alert show];
    
//    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
}

@end
