//
//  showFileInfoViewController.m
//  nc
//
//  Created by guanxf on 15/11/24.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import "showFileInfoViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ASProgressPopUpView.h"

@interface showFileInfoViewController ()<ASProgressPopUpViewDataSource>
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController; //系统文档控制器
//@property (nonatomic,retain) UIProgressView *progressView;
@property (nonatomic,retain) ASProgressPopUpView *progressView1;

@end

@implementation showFileInfoViewController

- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}

- (void)AllGroupViewBack{
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
        }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalized(@"showFileInfoViewController_title");
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(AllGroupViewBack)];
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;

    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
//    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, ScreenHeight/2.0-50, ScreenWidth, 20)];
//    self.progressView.tintColor = [UIColor redColor];
//    self.progressView.progress = 0;
//    [self.view addSubview:self.progressView];
    
    self.progressView1= [[ASProgressPopUpView alloc] initWithFrame:CGRectMake(0, ScreenHeight/2.0-50, ScreenWidth, 20)];
    self.progressView1.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:16];
    self.progressView1.popUpViewAnimatedColors = @[[UIColor redColor], [UIColor orangeColor], [UIColor greenColor]];
    [self.progressView1 showPopUpViewAnimated:YES];
//    self.progressView1.dataSource = self;
    [self.view addSubview:self.progressView1];
    
//    self.progressView2.popUpViewCornerRadius = 12.0;
//    self.progressView2.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:28];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString * Inbox=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents"]];
    if(![fileManager fileExistsAtPath:Inbox]){
        BOOL bo=[fileManager createDirectoryAtPath:Inbox withIntermediateDirectories:YES attributes:nil error:nil];
        NSLog(@"bobobobo is %d",bo);
    }
    
    NSString *savedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",_fileName]];
    NSLog(@"filepath_dow---%@",savedPath);
    NSString * filep=[NSString stringWithFormat:@"%@%@",iconPath,_filePath];
    
//    [fileManager removeItemAtPath:savedPath error:nil];  //删除文件 
    if(![fileManager fileExistsAtPath:savedPath]) //如果不存在 下载
    {
        [self downloadFileWithOption:nil withInferface:filep savedPath:savedPath downloadSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
        } downloadFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        } progress:^(float progress) {
//            self.progressView.progress=progress;
            [self.progressView1 setProgress:progress animated:YES];
//            NSLog(@"download：%f",progress);
        }];
        
    } else {
//        self.progressView.progress=1.0;
        [self.progressView1 setProgress:1.0 animated:NO];
        //展示内容
        QLPreviewController *previewController = [[QLPreviewController alloc] init];
        previewController.dataSource = self;
        previewController.delegate = self;
        [[self navigationController] pushViewController:previewController animated:YES];
    }
    // Do any additional setup after loading the view from its nib.
}


/**
 *  @author Jakey
 *
 *  @brief  下载文件
 *
 *  @param paramDic   附加post参数
 *  @param requestURL 请求地址
 *  @param savedPath  保存 在磁盘的位置
 *  @param success    下载成功回调
 *  @param failure    下载失败回调
 *  @param progress   实时下载进度回调
 */
- (void)downloadFileWithOption:(NSDictionary *)paramDic
                 withInferface:(NSString*)requestURL
                     savedPath:(NSString*)savedPath
               downloadSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
               downloadFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
                      progress:(void (^)(float progress))progress
{
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    NSMutableURLRequest *request =[serializer requestWithMethod:@"GET" URLString:requestURL parameters:paramDic error:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
//    [serializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:savedPath append:NO]];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float p = (float)totalBytesRead / totalBytesExpectedToRead;
        progress(p);
//        NSLog(@"download：%f", (float)totalBytesRead / totalBytesExpectedToRead);
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        NSLog(@"下载成功");
        //展示内容
        QLPreviewController *previewController = [[QLPreviewController alloc] init];
        previewController.dataSource = self;
        previewController.delegate = self;
        [[self navigationController] pushViewController:previewController animated:YES];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        success(operation,error);
        
        NSLog(@"下载失败");
        
    }];
    
    [operation start];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
    return self;
}


#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    //    NSInteger numToPreview = 0;
    //
    //	numToPreview = [self.dirArray count];
    //
    //    return numToPreview;
    return 1;//[self.dirArray count];
}

- (void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        
    }];
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    [previewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"click.png"] forBarMetrics:UIBarMetricsDefaultPrompt];
    
    NSURL *fileURL = nil;
    //    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path =[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",_fileName]]; //[[NSString stringWithFormat:@"%@",documentDir] stringByAppendingPathComponent:_fileName];
    fileURL = [NSURL fileURLWithPath:path];
    return fileURL;
}

#pragma mark - ASProgressPopUpView dataSource

// <ASProgressPopUpViewDataSource> is entirely optional
// it allows you to supply custom NSStrings to ASProgressPopUpView
- (NSString *)progressView:(ASProgressPopUpView *)progressView stringForProgress:(float)progress
{
    NSString *s;
    if (progress < 0.2) {
        s = @"Just starting";
    } else if (progress > 0.4 && progress < 0.6) {
        s = @"About halfway";
    } else if (progress > 0.75 && progress < 1.0) {
        s = @"Nearly there";
    } else if (progress >= 1.0) {
        s = @"Complete";
    }
    return s;
}

// by default ASProgressPopUpView precalculates the largest popUpView size needed
// it then uses this size for all values and maintains a consistent size
// if you want the popUpView size to adapt as values change then return 'NO'
- (BOOL)progressViewShouldPreCalculatePopUpViewSize:(ASProgressPopUpView *)progressView;
{
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
