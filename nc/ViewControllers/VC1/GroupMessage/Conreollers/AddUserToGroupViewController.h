//
//  AddUserToGroupViewController.h
//  nc
//
//  Created by docy admin on 15/10/22.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^viewBlock)(void);

@interface AddUserToGroupViewController : UITableViewController

@property (nonatomic, retain) NSNumber *groupId;
@property (nonatomic, strong) viewBlock viewblock;


@property (nonatomic, retain) NSMutableArray *currentUsers;
@property (nonatomic, copy) NSString *rightItem;

@end
