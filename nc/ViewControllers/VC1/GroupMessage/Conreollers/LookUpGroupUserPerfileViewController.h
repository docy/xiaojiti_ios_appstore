//
//  LookUpGroupUserPerfileViewController.h
//  xjt
//
//  Created by docy admin on 7/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

// 查看成员的资料（群组资料中的成员）
@interface LookUpGroupUserPerfileViewController : UIViewController

@property (nonatomic, retain) UserModel *userPerfileModel;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *avatarBtn;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;


@property (weak, nonatomic) IBOutlet UILabel *nickLabel;
@property (weak, nonatomic) IBOutlet UIButton *sexBtn;

@property (weak, nonatomic) IBOutlet UILabel *contactMode;

@property (weak, nonatomic) IBOutlet UIButton *sendMessageButton;
- (IBAction)avatarLook:(UIButton *)sender;


- (IBAction)onSendPrivateMessageBtnClick:(id)sender;

@property (nonatomic, retain) NSNumber *userId;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;


@end
