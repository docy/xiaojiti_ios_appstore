//
//  MessageViewController.h
//  nc
//
//  Created by jianghuan on 4/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SLKTextViewController.h"
#import "MessageViewController.h"
#import <QuickLook/QuickLook.h>

// 群组消息聊天视图控制器

@protocol MessageViewControllerDelegate <NSObject>

- (void)messageViewSetNewGroupName:(NSString *)newName groupId:(NSNumber *)groupId;

@end

typedef void(^cellBlock)(void);
//typedef void (^cellBackNav)(void);

@interface MessageViewController : SLKTextViewController <QLPreviewControllerDataSource,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate,UIActionSheetDelegate>

@property (nonatomic, strong) id<MessageViewControllerDelegate> Namedelegate; // 用来更改小组名称

@property (nonatomic, retain) NSNumber *groupId; // 群组的ID，
@property (nonatomic, copy) NSString *groupName; // 群组名称

@property (nonatomic, retain) NSMutableArray *messagesData; // 消息数组
@property (nonatomic, retain) NSMutableArray *messImageData; // 群聊图片数组

@property (nonatomic, retain) NSMutableArray *users; // 用户名

//@property (nonatomic, retain) NSMutableArray *usersAvatar; // 用户头像数组
@property (nonatomic, retain) NSArray *channels;
@property (nonatomic, retain) NSArray *emojis;
@property (nonatomic, retain) NSArray *searchResult; // 查询结果数组

@property (nonatomic, copy) NSString *lastTimeString;

@property (nonatomic, retain) NSMutableArray *groupUsers; // 群组成员，存储model

@property (nonatomic, strong) cellBlock cellBlock;

//@property (nonatomic, strong) cellBackNav cellBackNav;


@property (nonatomic, retain) NSNumber *unreadCount;
@property (nonatomic, assign) int broadcast; // 布告栏只有管理员可以发消息 0 不是布告栏，1 布告栏

@property (nonatomic, retain) NSNumber *groupCreator; // 群组创建者

@end