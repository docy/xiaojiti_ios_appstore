//
//  showFileInfoViewController.h
//  nc
//
//  Created by guanxf on 15/11/24.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>

@interface showFileInfoViewController : UIViewController<QLPreviewControllerDataSource,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate>

@property(nonatomic, strong) NSString * filePath;  //文件地址
@property(nonatomic, strong) NSString * fileName;  //文件名字
@end
