//
//  GroupDetailViewController.h
//  nc
//
//  Created by docy admin on 7/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^joinGroupBlock) (void);

@protocol GroupDetailViewControllerDelegate <NSObject>

- (void)groupDetailViewSetNewGroupName:(NSString *)newName;

@end

// 群组的详情页面
@interface GroupDetailViewController : UIViewController

@property (nonatomic, strong) id<GroupDetailViewControllerDelegate> namedelegate; // 用来更改小组名称

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollerView;
@property (weak, nonatomic) IBOutlet UILabel *userCountLabel;

@property (weak, nonatomic) IBOutlet UIView *userBgView;

@property (weak, nonatomic) IBOutlet UIButton *logOutGroupBtn; // 退出

@property (weak, nonatomic) IBOutlet UIButton *conveyGroupBtn; // 转让

@property (weak, nonatomic) IBOutlet UIButton *disbandGroupBtn; // 解散



@property (weak, nonatomic) IBOutlet UIButton *modifyGroupNameBtn;
@property (weak, nonatomic) IBOutlet UIButton *modifyGroupDescBtn;

@property (weak, nonatomic) IBOutlet UIButton *selectedLogoBtn;

@property (weak, nonatomic) IBOutlet UIView *hiddenView;

- (IBAction)onGroupDetailViewModifyGroupNameBtnClick:(id)sender;
- (IBAction)onGroupDetailViewModifyGroupDescBtnClick:(id)sender;

- (IBAction)selectedLogoBtnClick:(id)sender;

- (IBAction)onLogoutGroupBtnClick:(id)sender;

- (IBAction)onConveyGroupBtnClick:(id)sender;

- (IBAction)onDisbandGroupBtnClick:(id)sender;

- (IBAction)GroupDetaiViewGroupMessageManager:(id)sender;

- (IBAction)onprivateGroupSwitchClick:(UISwitch *)sender;

@property (weak, nonatomic) IBOutlet UISwitch *privateGroup;


@property (nonatomic,strong) joinGroupBlock block;

@property (nonatomic, retain) NSNumber *groupId; // 群组ID
//@property (nonatomic, retain) NSMutableArray *userName; // 用户名
//
//@property (nonatomic, retain) NSMutableArray *usersAvatar; // 用户头像数组
@property (nonatomic, retain) NSMutableArray *groupUsers;

@property (nonatomic, retain) NSString *SuperViewStr;  //  默认设置为@“chakan” 表示从待加入群组列表查看组信息

@end
