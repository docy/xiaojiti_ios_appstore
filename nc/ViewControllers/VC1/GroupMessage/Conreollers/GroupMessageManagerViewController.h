//
//  GroupMessageManagerViewController.h
//  xjt
//
//  Created by docy admin on 7/29/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 群组消息管理页面
@interface GroupMessageManagerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *NewMessageMode;
@property (weak, nonatomic) IBOutlet UIButton *aboutMeMode;
@property (weak, nonatomic) IBOutlet UIButton *freeMode;

- (IBAction)GroupMessageManagerViewSelectedModeClick:(id)sender;

@property (nonatomic, retain) NSNumber *groupId;

@end
