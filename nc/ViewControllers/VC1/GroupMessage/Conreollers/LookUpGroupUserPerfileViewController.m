//
//  LookUpGroupUserPerfileViewController.m
//  xjt
//
//  Created by docy admin on 7/30/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LookUpGroupUserPerfileViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "UIImageView+WebCache.h"
#import "DirectChatViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"

@interface LookUpGroupUserPerfileViewController ()
@property (nonatomic , weak) UIView *bgView; // 分类的灰色背景
@property (nonatomic , retain) UIImage *avatar; // 分类的灰色背景
@property (nonatomic , retain) UIImage *avatarOrigin; // 分类的灰色背景




@property (weak, nonatomic) IBOutlet UILabel *Localized_avator;
@property (weak, nonatomic) IBOutlet UILabel *Localized_name;
@property (weak, nonatomic) IBOutlet UILabel *Localized_nick;
@property (weak, nonatomic) IBOutlet UILabel *Localized_sex;
@property (weak, nonatomic) IBOutlet UILabel *Localized_phone;

@end

@implementation LookUpGroupUserPerfileViewController

- (UIImage *)avatarOrigin{
    if (_avatarOrigin==nil) {
        NSRange range=[self.userPerfileModel.avatar rangeOfString:@"http://wx.qlogo.cn"];
        NSURL * iconUrl;
        if (range.length==0) {
            iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:self.userPerfileModel.avatarOrigin]];
        }else{
            iconUrl=[NSURL URLWithString:self.userPerfileModel.avatar];
        }
        _avatarOrigin = [UIImage imageWithData:[NSData dataWithContentsOfURL:iconUrl]];
    }
    return _avatarOrigin;
}

- (UIImage *)avatar{
    if (_avatar==nil) {
        //判断微信登录的图像
        NSRange range=[self.userPerfileModel.avatar rangeOfString:@"http://wx.qlogo.cn"];
        NSURL * iconUrl;
        if (range.length==0) {
            iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:self.userPerfileModel.avatar]];
        }else{
            iconUrl=[NSURL URLWithString:self.userPerfileModel.avatar];
        }
        _avatar = [UIImage imageWithData:[NSData dataWithContentsOfURL:iconUrl]];
    }
    return _avatar;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置基本属性
    [self setUpLookUpGroupUserPerfileViewData];
    if (self.userPerfileModel!=nil) {
        // 显示数据
        [self setUpLookUpGroupUserPerfileViewUserPerfile];
    } else {
        // 获取用户信息
        [self getUpUserData];
    }
}

// 设置基本属性
- (void)setUpLookUpGroupUserPerfileViewData{
    
    
    self.Localized_avator.text = NSLocalized(@"info_else_tavator");
    self.Localized_name.text = NSLocalized(@"info_else_name");
    self.Localized_nick.text = NSLocalized(@"info_else_nick");
    self.Localized_sex.text = NSLocalized(@"info_else_sex");
    self.Localized_phone.text = NSLocalized(@"info_else_phone");
    [self.sendMessageButton setTitle:NSLocalized(@"info_else_sendMessBtn") forState:UIControlStateNormal];
    
    self.navigationItem.title = NSLocalized(@"info_else_title");
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(LookUpGroupUserPerfileViewNavcBackBtnClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(LookUpGroupUserPerfileViewNavcBackBtnClick)];
}

- (void)getUpUserData{
    
    self.sendMessageButton.enabled = NO;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    
    NSString *str = [personalInfoPath stringByAppendingString:[NSString stringWithFormat:@"%@",self.userId]];
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            self.sendMessageButton.enabled = YES;
            self.userPerfileModel = [[UserModel alloc] init];
            [self.userPerfileModel setValuesForKeysWithDictionary:responseObject[@"data"]];
            // 显示数据
            [self setUpLookUpGroupUserPerfileViewUserPerfile];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            self.sendMessageButton.enabled = NO;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
        self.sendMessageButton.enabled = NO;
    }];
}

#pragma mark -显示数据
- (void)setUpLookUpGroupUserPerfileViewUserPerfile{

//    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:self.userPerfileModel.avatar]] placeholderImage:nil];
//    NSURL *url = [NSURL URLWithString:[iconPath stringByAppendingString:self.userPerfileModel.avatar]];
//    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    [self.avatarBtn setBackgroundImage:self.avatar forState:UIControlStateNormal];
    self.avatarImageView.image = self.avatar;
    self.nameLabel.text = self.userPerfileModel.name;
    self.nickLabel.text = self.userPerfileModel.nickName;
    if ([self.userPerfileModel.sex intValue] ==0) {
        [self.sexBtn setBackgroundImage:[UIImage imageNamed:@"selected_women_icon"] forState:UIControlStateNormal];
    } else {
        [self.sexBtn setBackgroundImage:[UIImage imageNamed:@"selected_men_icon"] forState:UIControlStateNormal];
    }
    
    NSString *originTel = self.userPerfileModel.phone;
    if (originTel.length>7) {
        NSString *tel = [originTel stringByReplacingCharactersInRange:NSMakeRange(3, 7) withString:@"****"];
        self.contactMode.text = tel;
    }
    
    if ([self.userPerfileModel.id isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
        self.sendMessageButton.hidden = YES;
    } else {
        self.sendMessageButton.hidden = NO;
    }
    

}

- (void)LookUpGroupUserPerfileViewNavcBackBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIView *)bgView{
    if (_bgView == nil) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        view.alpha = 0.0;
        view.backgroundColor = [UIColor grayColor];
//        AppDelegate *delegate = APP_DELEGATE;
        [self.scroller addSubview:view];
        _bgView = view;
    }
    return _bgView;
}

#pragma mark -头像的点击事件
- (IBAction)avatarLook:(UIButton *)sender {
    
    if (sender.frame.size.width==ScreenWidth) {
        [UIView animateWithDuration:0.5 animations:^{
            sender.frame = self.avatarImageView.frame;
            [sender setBackgroundImage:self.avatar forState:UIControlStateNormal];
            self.bgView.alpha = 0.0;
        }completion:^(BOOL finished) {
            [self.bgView removeFromSuperview];
        }];
        return;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.avatarBtn setBackgroundImage:self.avatarOrigin forState:UIControlStateNormal];
        CGFloat y = (ScreenHeight-ScreenWidth-self.navigationController.navigationBar.bounds.size.height-20)/2.0;
        sender.frame = CGRectMake(0, y, ScreenWidth, ScreenWidth);
        self.bgView.alpha = 0.5;
        [self.scroller bringSubviewToFront:self.avatarBtn];
    } completion:^(BOOL finished) {

        
    }];
}

#pragma mark -发消息按钮
- (IBAction)onSendPrivateMessageBtnClick:(id)sender {
    
    DirectChatViewController *directVC = [[DirectChatViewController alloc] init];
    directVC.directChatId = self.userPerfileModel.id;
    directVC.groupName = self.userPerfileModel.nickName;
    [self.navigationController pushViewController:directVC animated:YES];
    
}
@end
