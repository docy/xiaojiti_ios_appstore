//
//  SubTopicViewController.m
//  nc
//
//  Created by docy admin on 7/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "SubTopicViewController.h"
#import "MessageDetailViewController.h"
#import "SubTopSeachViewController.h"
#import "XJTNavigationController.h"
#import "SubTopicViewCell.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import "SubTopicModel.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+AlertView.h"
#import "UIImageView+WebCache.h"
#import "MJRefresh.h"
#import "UISegmentedControl+XJTSegmentedControl.h"
//#import "SVProgressHUD.h"

#import "KxMenu.h"

/*
 order[favorite]=asc,desc  收藏排序
 order[views]  点击排序
 order[comment] 评论数量排序
 */
typedef NS_ENUM(NSInteger, TopicListByRule) {
    TopicListByRuleDefault=0,      //默认无
    TopicListByRuleTime,         // 按时间排序
    TopicListByRuleViews,            // 按点击率排序
    TopicListByRuleFavorite,           // 按关注度排序
    TopicListByRuleComment,          //按时间排序
};

#define SubTopicViewCellHeight 100
@interface SubTopicViewController (){
    UISegmentedControl* segContro;
}

@property (nonatomic, assign) int subTopicPage;
@property (nonatomic, retain) UIView *bgSubTpView;
@property (nonatomic, assign) BOOL KxMenu_isShow;

@property (nonatomic, retain)NSString * order;
@property (nonatomic, retain)NSString * sort;

@end

@implementation SubTopicViewController
@synthesize order=_order,sort=_sort;

- (UIView *)bgSubTpView{
    if (_bgSubTpView == nil) {
        _bgSubTpView = [[[NSBundle mainBundle] loadNibNamed:isEnglish?@"subListNullBgView_en":@"subListNullBgView" owner:self options:nil] lastObject];//[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"none_topic"]];
    }
    return _bgSubTpView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _order=@"updatedAt";
    _sort=@"asc";
    
    // 设置基本属性
    
    [self settitleview];
    
    [self setUpSubTopicViewControllerAttribute];
    [self getUpTopicListWithMJFresh];
    
    // 获取子话题列表
    [self getUpTopicList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSubTopicViewNewMessage:) name:@"newMessage" object:nil];
    // 取消关注
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteTopicSuccess:) name:@"deleteTopicSuccess" object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    segContro.selectedSegmentIndex = 1;
}

-(void)settitleview{
    segContro = [UISegmentedControl segmentedControlWithItems:@[NSLocalized(@"MessageView_title1"),NSLocalized(@"MessageView_title2")] target:self action:@selector(onSegClick:)];
    
    UIView * viewSeg=[[UIView alloc] initWithFrame:segContro.frame];
    [viewSeg addSubview:segContro];
    //    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.navigationItem.titleView = viewSeg;
}

- (void)onSegClick:(UISegmentedControl *)seg{
   
    if (seg.selectedSegmentIndex==0) {
        [self.navigationController popViewControllerAnimated:NO];
    } else {
//        SubTopicViewController *subVC = [[SubTopicViewController alloc] init];
//        subVC.groupName = self.groupName;
//        subVC.groupId = self.groupId;
//        [self.navigationController pushViewController:subVC animated:NO];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)deleteTopicSuccess:(NSNotification *)not{
    for (NSInteger i = 0; i < self.subTopicListData.count; i++) {
        SubTopicModel *model = self.subTopicListData[i];
        if ([not.object isEqualToNumber:model.id]) {
            [self.subTopicListData removeObject:model];
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
    }
    if (self.subTopicListData.count == 0) {
        self.tableView.backgroundView = self.bgSubTpView;
    }
}

- (void)getSubTopicViewNewMessage:(NSNotification *)notification
{
    NSArray *array = notification.object;
    NSDictionary * dict=array[0];
    
    if ([dict[@"type"] intValue] == 8) {
        for (int i = 0;i < self.subTopicListData.count;i++) {
            SubTopicModel *mode=self.subTopicListData[i];
            if (mode.id.intValue ==[(NSNumber*)dict[@"info"][@"topicId"] intValue]) { // 刷新message
                NSMutableDictionary *com = [NSMutableDictionary dictionary];
                com[@"createdAt"] = dict[@"updatedAt"];
                com[@"creatorAvatar"] = dict[@"info"][@"creatorAvatar"];
                com[@"creatorName"] = dict[@"info"][@"creatorName"];
                if (dict[@"info"][@"message"]==nil) {
                    com[@"message"] = @"";
                } else {
                    com[@"type"] = @1;
                    com[@"message"] = dict[@"info"][@"message"];
                }
//                com[@"message"] = dict[@"info"][@"message"];
                com[@"nickName"] = dict[@"nickName"];
                mode.comments = @[com];
                [self.subTopicListData replaceObjectAtIndex:i withObject:mode];
            
                NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第二行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
                if (i!=0) {
                    [self.tableView moveRowAtIndexPath:te toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    [self.subTopicListData insertObject:mode atIndex:0];
                    [self.subTopicListData removeObjectAtIndex:i+1];
                }
                break;
            }
        }
    }
}


// 设置基本属性
- (void)setUpSubTopicViewControllerAttribute
{
    self.navigationItem.title = [self.groupName stringByAppendingString:NSLocalized(@"subTopic_list_title")];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onSubTopicViewControllerNavBackClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onSubTopicViewControllerNavBackClick)];
    self.subTopicListData = [NSMutableArray array];
    self.subTopicPage = 1;
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 50, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
//    self.navigationItem.leftBarButtonItem=nil;
//    self.navigationItem.hidesBackButton=YES;
    
    UIBarButtonItem* rightBarItem = [UIBarButtonItem itemWithIcon:@"sorting" target:self action:@selector(onLeftViewSearchButtonClick:)];
    UIBarButtonItem *creatItem = [UIBarButtonItem itemWithIcon:@"navigation_search" target:self action:@selector(onCreatGroupButtonClick)];
    
    self.navigationItem.rightBarButtonItems = @[rightBarItem,creatItem];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (_KxMenu_isShow) {
        _KxMenu_isShow=NO;
        [KxMenu dismissMenu];
    }
}

//显示菜单
- (void)showMenu:(UIButton *)sender{
    CGRect rect = sender.frame;
    rect.origin.y = 0;
    
    if (_KxMenu_isShow) {
        _KxMenu_isShow=NO;
        [KxMenu dismissMenu];
    }else{
        NSArray *menuItems =
        @[
          [KxMenuItem menuItem:NSLocalized(@"subTopic_list_sort_time")
                         image:nil   //[UIImage imageNamed:@"topic list"]
                        target:self
                        action:@selector(SortByTime:)],
          [KxMenuItem menuItem:NSLocalized(@"subTopic_list_sort_click")
                         image:nil
                        target:self
                        action:@selector(SortByViews:)],
          [KxMenuItem menuItem:NSLocalized(@"subTopic_list_sort_active")
                         image:nil
                        target:self
                        action:@selector(pushMenuItemSet:)],
          
          [KxMenuItem menuItem:NSLocalized(@"subTopic_list_sort_guanzhu")
                         image:nil
                        target:self
                        action:@selector(SortByFavorite:)],
          ];
        
        [KxMenu setTitleFont:[UIFont fontWithName:@"Heiti SC" size:15.0]];
        [KxMenu showMenuInView:self.view
                      fromRect:rect
                     menuItems:menuItems];
        _KxMenu_isShow=YES;
    }
}

- (void)SortByTime:(id)sender{
    [self getUpTopicListByRule:TopicListByRuleTime];
}

- (void)SortByViews:(id)sender{
    [self getUpTopicListByRule:TopicListByRuleViews];
}

- (void)pushMenuItemSet:(id)sender{
    [self getUpTopicListByRule:TopicListByRuleComment];
}
- (void)SortByFavorite:(id)sender
{
    [self getUpTopicListByRule:TopicListByRuleFavorite];
}

#pragma mark 导航栏点击事件

- (void)onLeftViewSearchButtonClick:(UIButton *) button{
    [self showMenu:button];
}

- (void)onCreatGroupButtonClick{
    SubTopSeachViewController * seachVC=[[SubTopSeachViewController alloc] initWithNibName:@"SubTopSeachViewController" bundle:nil];
    seachVC.groupId=self.groupId;
    seachVC.subTopicListData=self.subTopicListData;
    seachVC.uploadtvc=^(){
        [self.tableView reloadData];
    };
    XJTNavigationController * nav=[[XJTNavigationController alloc] initWithRootViewController:seachVC];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)onSubTopicViewControllerNavBackClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUpTopicListWithMJFresh
{
    
    [self.subTopicListData removeAllObjects];
    __block SubTopicViewController *weakSelf = self;
    [self.tableView addFooterWithCallback:^{
        weakSelf.subTopicPage++;
        [weakSelf getUpTopicList];
    }];
}

// 获取子话题列表
- (void)getUpTopicList
{
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
//    NSString *str = [NSString stringWithFormat:getSubTopicListPath,self.groupId,self.subTopicPage];
//    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if ([responseObject[@"code"] isEqualToNumber:@0]) {
//            
//            NSLog(@"%@",responseObject);
//            for (NSDictionary *dict in responseObject[@"data"]) {
//                SubTopicModel *model = [[SubTopicModel alloc] init];
//                [model setValuesForKeysWithDictionary:dict];
//                [self.subTopicListData addObject:model];
//            }
//            
//            if (self.subTopicListData.count==0) {
//                self.tableView.backgroundView = self.bgSubTpView;
//            } else {
//                self.tableView.backgroundView = nil;
//            }
//
//            
//            [self.tableView footerEndRefreshing];
//            [self.tableView reloadData];
//            
//        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
//        }
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    NSString *str = [NSString stringWithFormat:@"%@?groupId=%@&order[%@]=%@&page=%d",getSubTopicList,self.groupId,_order,_sort,self.subTopicPage];  //先默认正序asc desc
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
//            NSLog(@"%@",responseObject);
            for (NSDictionary *dict in responseObject[@"data"]) {
                SubTopicModel *model = [[SubTopicModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.subTopicListData addObject:model];
            }
            
            if (self.subTopicListData.count==0) {
                self.tableView.backgroundView = self.bgSubTpView;
            } else {
                self.tableView.backgroundView = nil;
            }
            
            
            [self.tableView footerEndRefreshing];
            [self.tableView reloadData];
            
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

// 获取子话题列表 带参数
- (void)getUpTopicListByRule:(TopicListByRule)Rule
{
    
    switch (Rule) {
        case TopicListByRuleFavorite:
            _order=@"favorite";
            _sort=@"asc";
            break;
        case TopicListByRuleViews:
            _order=@"views";
            _sort=@"desc";
            break;
        case TopicListByRuleTime:
            _order=@"updatedAt";
            _sort=@"asc";
            break;
        case TopicListByRuleComment:
            _order=@"comment";
            _sort=@"asc";
            break;
            
        default:
            break;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    NSString *str = [NSString stringWithFormat:@"%@?groupId=%@&order[%@]=%@",getSubTopicList,self.groupId,_order,_sort];  //先默认正序asc desc
    
    if (Rule==TopicListByRuleTime) { // 按时间排序
        str = [NSString stringWithFormat:@"%@?groupId=%@",getSubTopicList,self.groupId];
    }
    
    [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
//            NSLog(@"%@",responseObject);
            [_subTopicListData removeAllObjects];
            for (NSDictionary *dict in responseObject[@"data"]) {
                SubTopicModel *model = [[SubTopicModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.subTopicListData addObject:model];
            }
            
            if (self.subTopicListData.count==0) {
                self.tableView.backgroundView = self.bgSubTpView;
            } else {
                self.tableView.backgroundView = nil;
            }
            
            
            [self.tableView footerEndRefreshing];
            [self.tableView reloadData];
            
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.subTopicListData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellId";
    SubTopicViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SubTopicViewCell" owner:self options:nil] lastObject];
    }
    SubTopicModel *model = self.subTopicListData[indexPath.row];
    
    cell.titleLabel.text = model.title;
    
    if ([model.type intValue] == 2) { // 图片子话题
        [cell.iconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
    } else if ([model.type intValue] == 3){
        cell.iconImageView.image = [UIImage imageNamed:@"list_map"];
    } else if ([model.type intValue] == 4){
        cell.iconImageView.image = [UIImage imageNamed:@"list_vote"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"list_topic"];
    }
    
    cell.viewsLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"subTopic_list_click"),[model.views intValue]];
    cell.favoriteCountLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"subTopic_list_guanzhu"),[model.favoriteCount intValue]];
    cell.commentCountLabel.text=[NSString stringWithFormat:@"%@%d",NSLocalized(@"subTopic_list_huifu"),[model.commentCount intValue]];
    
    if (model.comments.count!=0) {
        NSDictionary *comment = model.comments[0];
        
        NSString *text = nil;
        if ([comment[@"type"] isEqualToNumber:@1]) {
            text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        } else {
            text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],NSLocalized(@"subTopic_list_sendImage")];
        }
//        text = [NSString stringWithFormat:@"%@：%@",comment[@"nickName"],comment[@"message"]];
        NSMutableAttributedString *attributsStr = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange range = [text rangeOfString:[NSString stringWithFormat:@"%@：",comment[@"nickName"]]];
        [attributsStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:range];
        
//        [attributsStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1] range:range];
        cell.descLabel.attributedText = attributsStr;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:comment[@"createdAt"]];
    } else {
        
        cell.descLabel.text = NSLocalized(@"subTopic_list_huifu_no");//nil;
        cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.updatedAt];
        
    }
    
    if ([model.favorited isEqualToNumber:@1]) {
        [cell.FocusImage setImage:[UIImage imageNamed:@"Focus on"]];
    }else{
        [cell.FocusImage setImage:[UIImage imageNamed:@"Focus on_normal"]];
    }
    cell.FocusButton.tag=indexPath.row;
    [cell.FocusButton addTarget:self action:@selector(FocusButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)FocusButtonClick:(UIButton*)button{
    SubTopicModel *model = self.subTopicListData[button.tag];
    BOOL isF=[model.favorited isEqualToNumber:@1]?NO:YES;
    [self addMessageDetailViewFavorite:isF andIndexRow:button.tag];
    NSInteger num=[model.favoriteCount integerValue];
    isF?num++:num--;
    model.favoriteCount=[NSNumber numberWithInteger:num];
    model.favorited=[NSNumber numberWithBool:isF];
    [self.subTopicListData replaceObjectAtIndex:button.tag withObject:model];
}

- (void)addMessageDetailViewFavorite:(BOOL)isFavorite andIndexRow:(NSInteger)row{
    SubTopicModel *model = self.subTopicListData[row];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = nil;
    if (isFavorite) {
        parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    } else {
        parameter = [NSDictionary dictionaryWithObjects:@[[ToolOfClass authToken],@"true"] forKeys:@[@"accessToken",@"unfavorite"]];
    }
    [manager POST:[NSString stringWithFormat:addFavorite,model.id] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            if (isFavorite) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newFavorite" object:nil];
                NSIndexPath * indexPath=[NSIndexPath indexPathForRow:row inSection:0];
                NSArray * array=[NSArray arrayWithObjects:indexPath, nil];
                [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
                
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newUnFavorite" object:model.id];
                
                NSIndexPath * indexPath=[NSIndexPath indexPathForRow:row inSection:0];
                NSArray * array=[NSArray arrayWithObjects:indexPath, nil];
                [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
            }
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return SubTopicViewCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageDetailViewController *detailVC = [[MessageDetailViewController alloc] init];
    SubTopicModel *model = self.subTopicListData[indexPath.row];
    detailVC.topicId = model.id;
    detailVC.topicType = model.type;
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
