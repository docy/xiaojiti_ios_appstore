//
//  MessageDetailViewController.m
//  nc
//
//  Created by docy admin on 6/9/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "MessageDetailViewController.h"
#import "XJTMessageTableViewCell.h"
#import "CommentTableViewCell.h"
#import "MessVoteTableViewCell.h"
#import "MessageDetailHeaderView.h"
#import "MessageVoteDetailHeaderView.h"
#import "ShowPictureViewController.h"
#import "UIColor+Hex.h"
#import "UIColor+XJTColor.h"
#import "UIImageView+WebCache.h"
#import "ToolOfClass.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+AlertView.h"
#import "VoteUserListViewController.h"
#import "Map_ViewController.h"
#import "UMSocial.h"
#import "NSString+XJTString.h"
#import "LookUpGroupUserPerfileViewController.h"
#import "SVProgressHUD.h"
#import "MLEmojiLabel.h"
#import "AppDelegate.h"
#import "showFileInfoViewController.h"
#import "XJTNavigationController.h"
#import "OpenNetWorkViewController.h"
#import "UserJionedGroupViewController.h"
#import "AudioSession.h"
#import "MessageFouncationView.h"
#import "FileListViewController.h"
#import "UIButton+XJTButton.h"

//#import "SVProgressHUD.h"

#define defaultSpaceWith 16 // cell的子控件距离cell边缘的默认间距

#import "MessageTableViewCell.h"
static NSString *AutoCompletionCellIdentifier = @"AutoCompletionCell";


// cell的重用标识符
static NSString *MessengerCellIdentifier = @"MessengerCellId";

@interface MessageDetailViewController () <UIAlertViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MLEmojiLabelDelegate,TTTAttributedLabelDelegate,AudioSessionDelegate>
{
    NetWorkBadView *badView;
    UIToolbar * audioInputbar;   //录音功能toolbar
    UIView * recorderView;       //录音view
    UIImageView * audioPowerImgV;    //显示录音强度
    AudioSession * audioSession;    //语音类
    UILabel * alertL;           //提示录音操作
    UIImageView * RecImagev;     //录音image
    UIImageView * cancelRecImage;//取消录音image
    NSInteger AudioPlayerIsNum;  //当前播放的第几个cell的音频
    NSTimeInterval StartRecTime;         //录音开始时间
    UIButton * button;          //录音button
    BOOL RecTimeOut;            //录音超时
    NSTimeInterval KeyB_StartShowTime;       //上次弹出键盘时间    用于解决三方输入法通知调用3次的问题
}

@property (nonatomic, retain) NSMutableArray *messageData; // 评论数据
@property (nonatomic, retain) NSMutableArray *VoteDataArray; // 投票选项列表
@property (nonatomic, retain) NSMutableDictionary *VoteDataDic; // 投票选项列表
@property (nonatomic, assign) CGFloat cellHeight; // cell的高度(自动计算)
@property (nonatomic, retain) MessageDetailHeaderView *headerView; //tableView的头视图
@property (nonatomic, retain) MessageVoteDetailHeaderView *voteHeaderView; //tableView的vote头视图
@property (nonatomic, retain) NSMutableArray *headerViewData; //tableView的头视图数据
@property (nonatomic, assign) NSNumber* resouceId;  //子话题接口ID
@property (nonatomic, assign) NSNumber* closed;  //投票是否关闭
@property (nonatomic, assign) NSNumber* voted;  //投票当前用户是否参与过
@property (nonatomic, assign) NSNumber* anonymous;  //投票是否匿名
@property (nonatomic, assign) NSNumber* single;     //投票是否单选
@property (nonatomic, retain) NSArray * ColorStrArray; //投票子话题cell进度条颜色数组
@property (nonatomic, strong) NSMutableArray * cellHeightArray;

@property (nonatomic, strong) UIButton * buttonn;  //全局button 主要存储button tag  支持语音连续播放功能
@property (nonatomic, assign) BOOL isStopAutoAudioPlay;     //停止自动播放音频

@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController; //系统文档控制器

@property (nonatomic,retain) UIImageView *bgImageView;
@property (nonatomic, retain) MessageFouncationView *founcationView; // topicView
@property (nonatomic, strong) UIButton * rebutt; //弹出菜单button；
@property (nonatomic, retain) UIButton *fcButton; // topic功能键


@property (nonatomic, retain) NSArray *searchResult; // @查询结果数组
@property (nonatomic, retain) NSMutableArray *notifyArray; // @人数组

@end



@implementation MessageDetailViewController
{
    
    CGFloat _kCellBodyLabelWidth;

}
#pragma mark @人数组
- (NSMutableArray *)notifyArray{
    if (_notifyArray == nil) {
        _notifyArray = [NSMutableArray array];
    }
    return _notifyArray;
}

-(UIButton*)rebutt{
    if (!_rebutt) {
        _rebutt=[[UIButton alloc] init];
    }
    return _rebutt;
}

- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [audioSession.audioPlayer stop];
    audioSession.timer2.fireDate=[NSDate distantFuture];
    audioSession.audioPlayer=nil;
    audioSession=nil;

//    [self audioPlayerDidFinishPlaying];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"1==%@",self.users);
    _ColorStrArray=[[NSArray alloc] initWithObjects:@"#87D1D0",@"#C1DCB9",@"F5869C",@"FCE0E9",@"#02B9CE",@"#F6B3BC",@"#FEE9BC",@"F2C486",@"B8E6FE",@"8CDBCD", nil];
    [self registerPrefixesForAutoCompletion:@[@"@"]];
    [self.autoCompletionView registerClass:[MessageTableViewCell class] forCellReuseIdentifier:AutoCompletionCellIdentifier];
    //增加监听，当键盘出现或改变时收出消息
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillChangeFrameNotification
//                                               object:nil];
    // 设置基本属性
    [self setUpMessageDetailViewAttribute];
    // 获取评论列表
    [self getTopicComments];
    
    //添加录音相关
    audioSession = [AudioSession ShareAAudioSession];
    audioSession.delegate=self;
    [audioSession setAudioSession];
    RecTimeOut=NO;
    _isStopAutoAudioPlay=YES;
    [self initAudioInputBar];
    [self addRecorderView];
}

#pragma mark - 添加音频相关内容
-(void)initAudioInputBar{
    audioInputbar=[[UIToolbar alloc] init];
    float yyy=ScreenHeight-64-44;
    if (iPhone6Plus) {
        yyy=ScreenHeight-64-42.5;
    }
    audioInputbar.frame=CGRectMake(0, yyy, ScreenWidth, 40);
    NSMutableArray *myToolBarItems = [NSMutableArray array];
    
    UIBarButtonItem * audioBarbutton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"The keyboard"] style:UIBarButtonItemStyleDone target:self action:@selector(audioBarbuttonClick)];
    [audioBarbutton setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    
    UIButton * leftButton=[[UIButton alloc] initWithFrame:CGRectMake(3, 5, 34, 34)];
    [leftButton setImage:[UIImage imageNamed:@"The keyboard"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(audioBarbuttonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * leftBarItem=[[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [myToolBarItems addObject:leftBarItem];
    
    button=[[UIButton alloc] initWithFrame:CGRectMake(40, 5, ScreenWidth-40-10, 33)];
    [button setBackgroundColor:[UIColor whiteColor]];
    //    [button setImage:[UIImage imageNamed:@"鼠标抬起的颜色图片"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"butn_bg"] forState:UIControlStateHighlighted];
    [button setTitle:NSLocalized(@"group_chat_voice_downSpeak") forState:UIControlStateNormal];
    [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithHex:0x999999] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithHex:0xFFFFFF] forState:UIControlStateHighlighted];//colorWithHex:0xFFFFFF
    [button addTarget:self action:@selector(audioRecorderStart) forControlEvents:UIControlEventTouchDown];//开始录音
    [button addTarget:self action:@selector(audioRecorderSave) forControlEvents:UIControlEventTouchUpInside];//保存录音
    [button addTarget:self action:@selector(audioRecorderAlertCancel) forControlEvents:UIControlEventTouchDragExit];//提示取消录音
    [button addTarget:self action:@selector(audioRecorderCancelRec) forControlEvents:UIControlEventTouchUpOutside];//取消录音
    [button addTarget:self action:@selector(audioRecorderRecGoOn) forControlEvents:UIControlEventTouchDragEnter];//上滑又回到button恢复录音
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 3.0;
    button.layer.borderWidth = 0.5;
    button.layer.borderColor =[[UIColor colorWithHex:0xAAAAAA] CGColor];
    [button setExclusiveTouch:YES];
    UIBarButtonItem * recBarItem=[[UIBarButtonItem alloc] initWithCustomView:button];
    
    [myToolBarItems addObject:recBarItem];
    
    [audioInputbar addSubview:leftButton];
    [audioInputbar addSubview:button];
    //    [audioInputbar setItems:myToolBarItems];
    audioInputbar.hidden=YES;
    [self.view addSubview:audioInputbar];
}

//toolbar返回切换
-(void)audioBarbuttonClick{
    audioInputbar.hidden=YES;
}

-(void)audioRecorderStart{
    NSLog(@"开始录音");
    if (![audioSession.audioRecorder isRecording]) {
        [audioSession.audioRecorder record];//首次使用应用时如果调用record方法会询问用户是否允许使用麦克风
        StartRecTime = [[NSDate date] timeIntervalSince1970];
        //        NSLog(@"StartRecTime is %f",StartRecTime);
        audioSession.timer.fireDate=[NSDate distantPast];
        RecTimeOut=NO;
    }
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
    recorderView.hidden=NO;
}
-(void)audioRecorderSave{
    [audioSession.audioRecorder stop];
    double stopRecTime = [[NSDate date] timeIntervalSince1970];
    double dur=stopRecTime-StartRecTime;
    if (dur<0.30) {
        [self audioRecorderCancelRec];
        [ToolOfClass showMessage:NSLocalized(@"group_chat_voice_time_short")];
        return;
    }
    int duranton=dur<1?1:dur;
    //    NSLog(@"StopRecTime is %d",duranton);
    audioSession.audioRecorder=nil;
    audioSession.timer.fireDate=[NSDate distantFuture];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    [audioPowerImgV setImage:[UIImage imageNamed:@"recording1"]];
    recorderView.hidden=YES;
    if (RecTimeOut) {
        //        [button setTitle:@"录音已结束,请抬起按钮" forState:UIControlStateHighlighted];
        [button setTitle:NSLocalized(@"group_chat_voice_cancelSend") forState:UIControlStateHighlighted];
        return; //超时退出；
    }
    [self SendRecAudioWithDuration:duranton];
    NSLog(@"保存并发送录音");
}
-(void)audioRecorderAlertCancel{
    NSLog(@"提示取消录音");
    RecImagev.hidden=YES;
    audioPowerImgV.hidden=YES;
    cancelRecImage.hidden=NO;
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger1");
    alertL.backgroundColor=[UIColor colorWithHex:0xFF001F alpha:0.6];
}
-(void)audioRecorderCancelRec{
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [audioSession.audioRecorder stop];
    audioSession.audioRecorder=nil;
    audioSession.timer.fireDate=[NSDate distantFuture];
    alertL.backgroundColor=[UIColor clearColor];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    recorderView.hidden=YES;
    NSLog(@"取消录音");
}
-(void)audioRecorderRecGoOn{
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    alertL.backgroundColor=[UIColor clearColor];
    RecImagev.hidden=NO;
    audioPowerImgV.hidden=NO;
    cancelRecImage.hidden=YES;
    NSLog(@"上滑又回到button恢复录音");
}

-(void)addRecorderView{
    recorderView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 160)];
    recorderView.center=CGPointMake(ScreenWidth/2, ScreenHeight/2-80);
    recorderView.backgroundColor=[UIColor colorWithHex:0x000000 alpha:0.7];
    recorderView.layer.masksToBounds = YES;
    recorderView.layer.cornerRadius = 6.0;
    recorderView.layer.borderWidth = 1.0;
    recorderView.layer.borderColor = [[UIColor grayColor] CGColor];
    
    RecImagev=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paly"]];
    RecImagev.frame=CGRectMake(30, 35, 50, 80);
    [recorderView addSubview:RecImagev];
    
    audioPowerImgV=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recording1"]];
    audioPowerImgV.frame=CGRectMake(95, 35, 40, 80);
    [recorderView addSubview:audioPowerImgV];
    
    cancelRecImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cancel copy"]];
    cancelRecImage.frame=CGRectMake(50, 33, 50, 70);
    [recorderView addSubview:cancelRecImage];
    cancelRecImage.hidden=YES;
    
    alertL=[[UILabel alloc] initWithFrame:CGRectMake(10, 125, 140, 24)];
    alertL.text=NSLocalized(@"group_chat_voice_cancelSend_finger");
    [alertL setFont:[UIFont fontWithName:@"Heiti SC" size:15.0]];
    alertL.textColor=[UIColor whiteColor];
    alertL.textAlignment=NSTextAlignmentCenter;
    alertL.layer.masksToBounds=YES;
    alertL.layer.cornerRadius = 4.0;
    [recorderView addSubview:alertL];
    [self.view addSubview:recorderView];
    recorderView.hidden=YES;
    
    //    StartRecTim is 1451556208765.600830
    //StopRecTime is 1451556213570.474121
}

#pragma mark -AudioSessionDelegate
-(void)AudioRecorderProgress:(float)progress andRecDuration:(int)duration{
    int number=1;
    if (progress>0.91) {
        number=6;
    }else if (progress>0.87) {
        number=5;
    }else if (progress>0.83) {
        number=4;
    }else if (progress>0.79) {
        number=3;
    }else if (progress>0.75) {
        number=2;
    }else {
        number=1;
    }
    //    NSLog(@"RecDurationRecDurationRecDuration::::::%d",duration);
    NSString * imageName=[NSString stringWithFormat:@"recording%d",number];
    [audioPowerImgV setImage:[UIImage imageNamed:imageName]];
    
    if (duration>=durationTime-100) {
        alertL.text=[NSString stringWithFormat:NSLocalized(@"group_chat_voice_canVoice"),(durationTime-duration)/10];
        alertL.backgroundColor=[UIColor clearColor];
        if (duration>durationTime) {  //录音超时了
            [self audioRecorderSave];
            RecTimeOut=YES;
            [button setTitle:NSLocalized(@"group_chat_voice_over") forState:UIControlStateHighlighted];
        }
    }
    
}

-(void)AudioPlayerProgress:(float)progress andDuration:(float)duration{
    
    if (duration==0.0) {
        [self audioPlayerDidFinishPlaying];
        return;
    }
    if (_messageData.count==0) {
        return;
    }
    MessageModel *messageModel = _messageData[AudioPlayerIsNum];
    messageModel.volumeAnimatedNum=progress+1;
    messageModel.duration=duration;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    //    NSLog(@"AudioPlayerProgress--- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
    if (array==nil) {
        return;
    }
    [self.tableView reloadData];
    //    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
}

-(void)SendRecAudioWithDuration:(int)duration{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sending")];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"duration"]=[NSNumber numberWithInt:duration];
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    parameter[@"message"] = [NSString stringThrowOffLinesWithString:self.textView.text];
    
    NSString * string=[[audioSession getSavePath] absoluteString];
    string = [string stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    
    NSString * URLstr = [NSString stringWithFormat:addCommentToTopicPath,self.topicId];
    
    NSData *audioData = [NSData dataWithContentsOfFile:string];
    [manager POST:URLstr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:audioData name:@"audio" fileName:@"audioName" mimeType:@"audio/aac"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sendFail")];
            [SVProgressHUD dismissWithDelay:1.0];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)audioPlayerDidFinishPlaying{
    [audioSession.audioPlayer stop];
    //    audioSession.audioPlayer=nil;
    audioSession.timer2.fireDate=[NSDate distantFuture];
    if (_messageData.count==0) {
        return;
    }
    MessageModel *messageModel = _messageData[AudioPlayerIsNum];
    messageModel.volumeAnimatedNum=3;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
    //    NSLog(@"audioPlayerDidFinishPlaying--------- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    [self.tableView reloadData];
    //    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
    
    //添加未读语音自动播放功能
    if (AudioPlayerIsNum>=_messageData.count-1) {
        return;
    }
    MessageModel *messageModel2 = self.messageData[AudioPlayerIsNum+1];
    if ([messageModel2.type isEqualToNumber:@8]&&[messageModel2.subType isEqualToNumber:@5]) {
        if (messageModel2.readed!=YES) {//||messageModel2.info[@"readed"]==NULL
            if (_isStopAutoAudioPlay==NO) {
                _isStopAutoAudioPlay=YES;
            }else{
                if (!_buttonn) {
                    _buttonn=[[UIButton alloc] init];
                }
                AudioPlayerIsNum++;
                _buttonn.tag=AudioPlayerIsNum;
                [self AudioPlay:_buttonn];
            }
        }
    }
    
}

-(void)AudioPlay:(UIButton*)button{
    BOOL xiangtongCell=NO;
    
    if (AudioPlayerIsNum!=button.tag) {
        xiangtongCell=NO;
    }else{
        xiangtongCell=YES;
    }
    
    AudioPlayerIsNum=button.tag;
    MessageModel *model = _messageData[button.tag];
    
    if ([audioSession.audioPlayer isPlaying]) {
        if (xiangtongCell) {
            _isStopAutoAudioPlay=NO;
            [self audioPlayerDidFinishPlaying];
            return;
        }else{
            [self audioPlayerDidFinishPlaying];
        }
        
    }
    
    MessageModel *messageModel = _messageData[AudioPlayerIsNum];
    messageModel.witeActivityIsShow=YES;
    NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
    NSArray * array=[NSArray arrayWithObjects:indexp, nil];
    //    NSLog(@"audioPlayerDidFinishPlaying--------- AudioPlayerIsNum is %ld  cellHeightis %d",AudioPlayerIsNum,[_cellHeightArray[AudioPlayerIsNum] intValue]);
    [self.tableView reloadData];
    //    [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
    
    
    
    NSLog(@"播放语音");
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString * Inbox=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents"]];
    if(![fileManager fileExistsAtPath:Inbox]){
        BOOL bo=[fileManager createDirectoryAtPath:Inbox withIntermediateDirectories:YES attributes:nil error:nil];
        NSLog(@"bobobobo is %d",bo);
    }
    
    NSString *savedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",@"audioName.m4a"]];
        NSLog(@"filepath_dow---%@",savedPath);
    NSMutableString * filep=[NSMutableString stringWithFormat:@"%@%@",iconPath,model.info[@"audioPath"]];
    //    NSRange range=[filep rangeOfString:@".aac"];
    //    if (range.length==4) {
    //        [filep replaceCharactersInRange:range withString:@""];
    //    }
        NSLog(@"filep-filep--%@",filep);
    [fileManager removeItemAtPath:savedPath error:nil];  //删除文件
    //    if(![fileManager fileExistsAtPath:savedPath]) //如果不存在 下载
    //    {
    [self downloadFileWithOption:nil withInferface:filep savedPath:savedPath downloadSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    } downloadFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    } progress:^(float progress) {
        
    }];
    
    messageModel.readed=YES;
    self.messageData[AudioPlayerIsNum]=messageModel;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString * path=[NSString stringWithFormat:MarkAudioReaded,messageModel.info[@"audioId"]];
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 发送成功
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    //    }
}

- (void)downloadFileWithOption:(NSDictionary *)paramDic
                 withInferface:(NSString*)requestURL
                     savedPath:(NSString*)savedPath
               downloadSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
               downloadFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
                      progress:(void (^)(float progress))progress
{
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    NSMutableURLRequest *request =[serializer requestWithMethod:@"GET" URLString:requestURL parameters:paramDic error:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:request];
    //    [serializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:savedPath append:NO]];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float p = (float)totalBytesRead / totalBytesExpectedToRead;
        progress(p);
        //        NSLog(@"totalBytesExpectedToRead：%f",  (float)totalBytesExpectedToRead);//(float)totalBytesRead /
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        NSLog(@"语音下载成功");
        MessageModel *messageModel = _messageData[AudioPlayerIsNum];
        messageModel.witeActivityIsShow=NO;
        NSIndexPath * indexp=[NSIndexPath indexPathForRow:AudioPlayerIsNum inSection:0];
        NSArray * array=[NSArray arrayWithObjects:indexp, nil];
        [self.tableView reloadData];
        audioSession.audioPlayer=nil;
        if (![audioSession.audioPlayer isPlaying]) {
            [audioSession.audioPlayer play];
            audioSession.timer2.fireDate=[NSDate distantPast];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        success(operation,error);
        
        NSLog(@"语音下载失败");
        
    }];
    
    [operation start];
}

#pragma mark -

//当键盘出现或改变时调用
//- (void)keyboardWillShow:(NSNotification *)aNotification
//{
//    CGFloat duration = [aNotification.userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
//    CGRect frame = [aNotification.userInfo[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
//    CGFloat offsetY = frame.origin.y - self.view.frame.size.height;
//    [UIView animateWithDuration:duration animations:^{
//        self.view.transform = CGAffineTransformMakeTranslation(0, offsetY);
//    }];
//}

// 设置基本属性
- (void)setUpMessageDetailViewAttribute{
    
    self.navigationItem.title = NSLocalized(@"subTopic_detail_title");
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.bounces = YES; 
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = YES;
    self.typingIndicatorView.canResignByTouch = YES;
    
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
    
    self.cellHeightArray=[NSMutableArray array];
    self.messageData = [NSMutableArray array];
    self.VoteDataArray=[NSMutableArray array];
    self.VoteDataDic=[[NSMutableDictionary alloc] init];
    self.headerViewData = [NSMutableArray array];
    self.cellHeight = 0;
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(MessageDetailViewBackButtonClick)];
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(MessageDetailViewBackButtonClick)];
//    [self.rightButton setTitle:NSLocalizedString(@"发送", nil) forState:UIControlStateNormal];
//    [self setInverted:NO]; // 设置tableView的翻转模式（从上往下显示）
    self.inverted = NO;
    
    [self.rightButton setTitle:NSLocalized(@"group_chat_private_sendBtn") forState:UIControlStateNormal];//NSLocalizedString(@"发送      ", nil)
    UIImage *image = [UIImage imageNamed:@"plus sign_icon"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIButton * rebutt=[[UIButton alloc] init];
    [self.rebutt setBackgroundImage:image forState:UIControlStateNormal];
    [self.rebutt addTarget:self action:@selector(rightButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.rebutt.frame=CGRectMake(ScreenWidth-33, 8, 27, 27);//self.textInputbar.rightButton.frame;
    _rebutt.translatesAutoresizingMaskIntoConstraints=NO;
    [self.textInputbar addSubview:self.rebutt];
    NSDictionary *views = @{@"_rebutt": self.rebutt,};
    NSDictionary *metrics = @{@"up": @8,@"down": @8,};
    [self.textInputbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_rebutt(27)]-down-|" options:0 metrics:metrics views:views]];
    [self.textInputbar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_rebutt(27)]-7-|" options:0 metrics:metrics views:views]];
        
    UIImage *image2 = [UIImage imageNamed:@"The recording"];
    image2 = [image2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.textInputbar.leftButton setImage:image2 forState:UIControlStateNormal];
    self.textInputbar.autoHideRightButton = NO;
    self.textInputbar.counterStyle = SLKCounterStyleNone;
    
    self.textInputbar.textView.returnKeyType=UIReturnKeySend;
    self.rightButton.hidden=YES;
    
    // 添加灰色背景
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    bgImageView.backgroundColor = [UIColor blackColor];
    bgImageView.alpha = 0.5;
    bgImageView.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageViewTapGestureClick:)];
    bgImageView.userInteractionEnabled = YES;
    [bgImageView addGestureRecognizer:tap];
    self.bgImageView = bgImageView;
    [self.view addSubview:bgImageView];
    
    // 添加topic按钮
    UIButton *fcButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect rect = CGRectMake(self.view.frame.size.width-16-60, self.view.frame.size.height-50-60-64, 60, 60);
    [fcButton setNormalImage:@"add.png" andSelectedImage:@"back.png" andFrame:rect addTarget:self action:@selector(onFouncationBtnClick:)];
    //    [self.view addSubview:fcButton];
    self.fcButton = fcButton;
    
    MessageFouncationView *founcationView = [[[NSBundle mainBundle] loadNibNamed:@"MessageFouncationView" owner:self options:nil] lastObject];
    //    CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - founcationView.bounds.size.width;
    //    CGFloat founcationY = CGRectGetMinY(_fcButton.frame) - founcationView.bounds.size.height - 10;
    //    CGRect founcationRect = founcationView.frame;
    //    founcationRect.origin = CGPointMake(founcationX, founcationY);
    
    founcationView.frame =CGRectMake(0, ScreenHeight+founcationView.frame.size.height, ScreenWidth, 208);//founcationView.frame.size.height);
    //CGRectMake(founcationRect.origin.x, founcationRect.origin.y, founcationRect.size.width, founcationRect.size.height+70);
    founcationView.hidden = YES;
//    founcationView.Localized_file.text = NSLocalized(@"topic_file");
    founcationView.Localized_map.text = NSLocalized(@"topic_map");
//    founcationView.Localized_vote.text = NSLocalized(@"topic_vote");
    founcationView.Localized_topic.text = NSLocalized(@"topic_topic");
    founcationView.Localized_image.text = NSLocalized(@"topic_image");
    
    founcationView.mapButton.hidden=YES;
    founcationView.Localized_map.hidden=YES;
    for (UIView *view in founcationView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn addTarget:self action:@selector(onAddFouncationButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    self.founcationView = founcationView;
    [self.view addSubview:founcationView];
    
    
//    UIImage *image = [UIImage imageNamed:@"plus sign_icon"];
//    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    [self.textInputbar.leftButton setImage:image forState:UIControlStateNormal];
    
    // 设置tableView的headerView
    if ([_topicType isEqualToNumber:@4]) {//设置投票的headview
        MessageVoteDetailHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"MessageVoteDetailHeaderView" owner:self options:nil] lastObject];
        self.voteHeaderView = headerView;
        [self.voteHeaderView.closeVoteButton setTitle:NSLocalized(@"subTopic_detail_vote_Close") forState:UIControlStateNormal];
        [self.voteHeaderView.sendVoteButton setTitle:NSLocalized(@"subTopic_detail_vote") forState:UIControlStateNormal];
        [self.voteHeaderView.shareButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.voteHeaderView.collectButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.voteHeaderView.deleteButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        MessageDetailHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"MessageDetailHeaderView" owner:self options:nil] lastObject];
        self.headerView = headerView;
    }
    
    [self.headerView.shareButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.collectButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.deleteButton addTarget:self action:@selector(onShareOrCollectOrDelecteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // 注册通知观察者(发表的评论)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMessageDetailViewNewMessage:) name:@"newMessage" object:nil];
    
    //断网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    //联网通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    //注册键盘消失的通知
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
    badView.contentLabel.text = NSLocalized(@"NetWorkBadView_des");
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    badView.frame=CGRectMake(0, 0, ScreenWidth, 44);
    if ([AFNetworkReachabilityManager sharedManager].isReachable) {
        badView.hidden=YES;
    }else{
        badView.hidden=NO;
    }
    [self.view addSubview:badView];
}


#pragma mark 这里有问题，需要重新写
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    //键盘高度
    //    CGRect keyBoardFrame = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    [self.tableView setContentOffset:CGPointMake(0, 0) animated:NO];
    
    double kyeB_nowShowTime = [[NSDate date] timeIntervalSince1970];
    if (kyeB_nowShowTime-KeyB_StartShowTime>1.0) {
        float hight=self.tableView.contentSize.height;
        float hh=183;
        if (iPhone6Plus) {
            hh=340;
        }
        NSLog(@"--- %f-%f=%f -%f--%f",ScreenHeight,hh,ScreenHeight-hh,self.tableView.tableHeaderView.frame.size.height,self.tableView.contentSize.height);
        if (ScreenHeight-hh-39<self.tableView.contentSize.height) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.28 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                CGFloat offset = self.tableView.contentSize.height - self.tableView.bounds.size.height;
                if (offset > 0)
                {
                    [self.tableView setContentOffset:CGPointMake(0, offset) animated:YES];
                }
                
//                [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentSize.height) animated:YES];// hight-hh
            });
        }
        KeyB_StartShowTime= [[NSDate date] timeIntervalSince1970];
    }
    
//    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_messageData.count inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void)onMessageViewTapGestureClick:(UITapGestureRecognizer *)gesture{
    [self onFouncationBtnClick:_rebutt];
}

#pragma mark 弹出菜单 文件 地图，图片，主题等功能键的点击事件
- (void)onAddFouncationButtonClick:(UIButton *)sender{
    [self onFouncationBtnClick:nil];
    if (sender.tag == 0) {
//        VoteViewController *voteVC = [[VoteViewController alloc] init];
//        voteVC.groupId=self.groupId;
//        [self.navigationController pushViewController:voteVC animated:YES];
    } else if (sender.tag == 1) {
        
        // block 反向传值
        //        MapViewController *mapVC = [[MapViewController alloc] init];
        
        UIAlertView * alertv=[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"暂不支持该功能" delegate:self cancelButtonTitle:@"知道啦" otherButtonTitles:nil, nil];
        [alertv show];
        
//        Map_ViewController * mapVC =[[Map_ViewController alloc] init];
//        mapVC.groupId=self.groupId;
//        mapVC.isYulan=NO;
//        [self.navigationController pushViewController:mapVC animated:YES];
//        
    } else if (sender.tag == 2) {
        //文件子话题
//        FileListViewController * filevC=[[FileListViewController alloc] initWithNibName:@"FileListViewController" bundle:nil];
//        filevC.groupId=self.groupId;
//        [self.navigationController pushViewController:filevC animated:YES];
    }else if (sender.tag == 3) {
        
//        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
//        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];//self.view];

        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//            picker.allowsEditing = YES;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
        
    } else if (sender.tag == 4) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
        
//        MainThemeViewController *mainVC = [[MainThemeViewController alloc] init];
//        mainVC.groupId = self.groupId;
//        [self.navigationController pushViewController:mainVC animated:YES];
    }else if (sender.tag == 8) {
        [self onFouncationBtnClick:_rebutt];
    }else{
        
    }
}

// 功能键的点击触发方法
- (void)onFouncationBtnClick:(UIButton *)sender{
    [self.textView resignFirstResponder];
    [self.textInputbar.textView resignFirstResponder];
    
    sender.selected = !sender.selected;//YES;//
    
    [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        if (sender.selected) {
            self.founcationView.hidden = NO;
            self.bgImageView.hidden = NO;
//            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGFloat founcationY = ScreenHeight - 64 -_founcationView.frame.size.height;
            CGRect point1=self.founcationView.frame;
            point1.origin.y=founcationY;//-=self.founcationView.center.x<160?0:280;
            point1.origin.x=0;
            NSLog(@"founcationY22222 is:%f",point1.origin.y);
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.5;
        } else {
//            CGFloat founcationX = CGRectGetMaxX(_fcButton.frame) - _founcationView.bounds.size.width;
            CGFloat founcationY = ScreenHeight - _founcationView.bounds.size.height;//CGRectGetMaxY(_fcButton.frame)
            CGRect point1=self.founcationView.frame;
            point1.origin.y=founcationY +_founcationView.bounds.size.height;//+280;
            point1.origin.x=0;
            NSLog(@"founcationY33333 is:%f",point1.origin.y);
            self.founcationView.frame=point1;
            self.bgImageView.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        if (sender.selected) {
            
        } else {
            self.founcationView.hidden = YES;
            self.bgImageView.hidden = YES;
            
        }
    }];
    
}

-(void)rightButtonClick:(UIButton*)sender{
    //    [self didPressRightButton:sender];
//    [self.textView resignFirstResponder];
//    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"选择照片",@"地图", nil];
//    action.tag = 100;
//    [action showInView:self.view];
    
    [self.textView resignFirstResponder];
//    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
//    [action showInView:self.view];
    
    [self onFouncationBtnClick:sender];
}

- (void)LeftViewRefreshAgainInBadNetWork{
    //    NSLog(@"%s",object_getClassName(self));
    badView.hidden=NO;
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)LeftViewInGoodNetWork{
    badView.hidden=YES;
}

// 返回按钮的点击事件
- (void)MessageDetailViewBackButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

//关闭tableview reloaddata 时全屏刷动画的问题
- (void)textDidUpdate:(BOOL)animated
{
    // Notifies the view controller that the text did update.
    [super textDidUpdate:NO];//animated];
}


#pragma mark - 收到新消息 刷新评论
- (void)getMessageDetailViewNewMessage:(NSNotification *)notification{
    NSArray *array = notification.object;
    for (NSDictionary *dict in array) {
        NSString *clientId = dict[@"info"][@"clientId"];
        NSString *serverId = dict[@"id"];
        if ([dict[@"type"] isEqualToNumber:@16]) {   //当收到 url链接 为 预加载内容 时
            for (long i=0; i<_messageData.count; i++) {
                MessageModel *model=_messageData[i];
                if ([clientId isEqualToString:model.info[@"clientId"]]) {
                    MessageModel *mdol=[[MessageModel alloc] init];
                    [mdol setValuesForKeysWithDictionary:dict];

                    [_messageData replaceObjectAtIndex:i withObject:mdol];//insertObject:mdol atIndex:i];
                    NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第一个section的第i行
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationFade];
                    return;
                }
            }
        }else{
            if (_messageData.count>=1) {
                MessageModel *model=_messageData[_messageData.count-1];
                /**
                 * 怎样界定一条msg？
                 *  根据客户端打的唯一tag：clientId。但如果是服务器产生的msg则没有这个tag。另外客户端出错时可能没有这个tag
                 *  根据服务器打的唯一tag：createAt。但如果是客户端先发送到本地list的msg里面没有这个tag。可以假定服务器不会错误滴遗失这个tag
                 *
                 * 判断步骤
                 *  先判断clientId是否重复
                 *  如果没有clientId，再判断createAt
                 */
                
                if (clientId!=nil && model.info[@"clientId"]!=nil) {//some msg donot have clientId
                    if ([clientId isEqualToString:model.info[@"clientId"]]) {
                        //手动去重操作，因为有时会出现收到2条相同的message
                        return;
                    }
                } else if (serverId!=nil && model.id!=nil) {
                    if ([serverId isEqualToString:model.id]) {
                        //手动去重操作，因为有时会出现收到2条相同的message
                        return;
                    }
                }
            }
        }
        
        if ([dict[@"type"] isEqualToNumber:@8]&&[dict[@"info"][@"topicId"] isEqualToNumber:self.topicId]) { // 说明是评论message
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            model.volumeAnimatedNum=3;
            
            NSUserDefaults * ud=[NSUserDefaults standardUserDefaults];
            NSNumber* numb=[ud objectForKey:@"id"];
            if ([model.userId isEqualToNumber:numb]) {//是自己发的消息设置为已读
                model.readed=YES;
            }
            
            [self.messageData addObject:model];
            [self.tableView reloadData];
            
            if (_messageData.count==1) {
                [self.textInputbar.textView resignFirstResponder];
            }
            // tableView滚动到最下面
            [self setUpMessageDetailViewTableViewScroller];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    
//    if (tableView.tag==100) {
//        return _VoteDataArray.count;
//    }
//    
//    return self.messageData.count;
    
    
    if (tableView.tag==100) {
        return _VoteDataArray.count;
    } else if ([tableView isEqual:self.autoCompletionView]) {
        return self.searchResult.count;
    }
    
    return self.messageData.count;
    
}

//投票cell选中 button 事件
-(void)cellSelectButtonClick:(UIButton*)button{
    long tag=button.tag;
    NSLog(@"button.tag=%ld",tag);
    
    if (![_single isEqualToNumber:@0]) {
        for (int i=0; i<_VoteDataArray.count; i++) {
            if (tag==i) {
                [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d_State",i]];
            }else{
                [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%d_State",i]];
            }
        }
    }else{
        if ([_VoteDataDic[[NSString stringWithFormat:@"%ld_State",tag]] isEqualToString:@"1"]) {
            [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
        }else{
            [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
        }
    }
    
    [self.voteHeaderView.headVoteTableView reloadData];
//    MessVoteTableViewCell * cell=[button superview];
//    cell.selectImg=[UIImage imageNamed:@"choose_selected"];
//    self.tableView.tableHeaderView.headVoteTableView
    
}

#pragma mark @人功能
- (void)didChangeAutoCompletionPrefix:(NSString *)prefix andWord:(NSString *)word{
    NSArray *array = nil;
    self.searchResult = nil;
    
    if ([prefix isEqualToString:@"@"]) {
        if (word.length > 0) {
            array = [self.users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self BEGINSWITH[c] %@", word]];
        }
        else {
            array = self.users;
        }
    }
    
    if (array.count > 0) {
        //                array = [array sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    self.searchResult = [[NSMutableArray alloc] initWithArray:array];
    
    BOOL show = (self.searchResult.count > 0);
    [self showAutoCompletionView:show];

}


- (CGFloat)heightForAutoCompletionView{
    CGFloat cellHeight = [self.autoCompletionView.delegate tableView:self.autoCompletionView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    return cellHeight*self.searchResult.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([tableView isEqual:self.autoCompletionView]) { // @功能
        
        NSMutableString *item = [self.searchResult[indexPath.row] mutableCopy];
        
        if ([self.foundPrefix isEqualToString:@"@"] && self.foundPrefixRange.location == 0) {
            [item appendString:@":"];
        }
        
        [item appendString:@" "];
        
        [self acceptAutoCompletionWithString:item keepPrefix:YES];
        
        UserModel *model = self.groupUsers[indexPath.row];
        [self.notifyArray addObject:model.id];
        
    }
    
    if (tableView.tag==100) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        if ([_closed isEqualToNumber:@1]||[_voted isEqualToNumber:@1]) {
            return;
        }
        NSInteger tag=indexPath.row;
        if (![_single isEqualToNumber:@0]) {
            for (int i=0; i<_VoteDataArray.count; i++) {
                if (tag==i) {
                    [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d_State",i]];
                }else{
                    [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%d_State",i]];
                }
            }
//            [self.voteHeaderView.headVoteTableView reloadData];
        }else{
            if ([_VoteDataDic[[NSString stringWithFormat:@"%ld_State",tag]] isEqualToString:@"1"]) {
                [_VoteDataDic setObject:@"0" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
            }else{
                [_VoteDataDic setObject:@"1" forKey:[NSString stringWithFormat:@"%ld_State",tag]];
            }
//            [self.voteHeaderView.headVoteTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        [self.voteHeaderView.headVoteTableView reloadData];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (MessageTableViewCell *)autoCompletionCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageTableViewCell *cell = (MessageTableViewCell *)[self.autoCompletionView dequeueReusableCellWithIdentifier:AutoCompletionCellIdentifier];
    cell.indexPath = indexPath;
    
    NSString *item = self.searchResult[indexPath.row];
    cell.titleLabel.text = item;
    cell.titleLabel.font = [UIFont systemFontOfSize:14.0];
    
    UserModel *userModel = self.groupUsers[indexPath.row];
    //判断微信登录的图像
    NSRange range=[userModel.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.length==0) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:userModel.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:userModel.avatar];
    }
    [cell.thumbnailView sd_setImageWithURL:iconUrl placeholderImage:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if ([tableView isEqual:self.autoCompletionView]) {
        
        return [self autoCompletionCellForRowAtIndexPath:indexPath];
    }
//
    /*
     
//    XJTMessageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
//    if (!cell) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:@"XJTMessageTableViewCell" owner:self options:nil] lastObject];
//        
//        cell.userInteractionEnabled = NO;
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    
//    cell.detailButton.hidden = YES; // 隐藏详情按钮
//    
//    MessageModel *messageModel = self.messageData[indexPath.row];
//    
//    cell.senderLabel.text = messageModel.sender;
//    NSString *str = [iconPath stringByAppendingString:messageModel.avatar];
//    [cell.userAvatarImageView setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
//    // 设置时间显示格式
//    
//    cell.createdAtLabel.text = [ToolOfClass toolGetLocalAmDateFormateWithUTCDate:messageModel.updatedAt];
//    
//    CGRect senderFrame = cell.senderLabel.frame;
//    CGRect avatarFrame = cell.userAvatarImageView.frame;
//    CGRect creatAtFrame = cell.createdAtLabel.frame;
//    cell.timeView.hidden = YES;
//    cell.responseLabel.hidden = YES;
//    
//    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
//    senderFrame.size.width = ceil(senderSize.width);
//    
//    senderFrame.origin.y = defaultSpaceWith;
//    avatarFrame.origin.y = defaultSpaceWith;
//    creatAtFrame.origin.y = defaultSpaceWith;
//    cell.senderLabel.frame = senderFrame;
//    
//    creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
//    cell.userAvatarImageView.frame = avatarFrame;
//    cell.createdAtLabel.frame = creatAtFrame;
//    
//    
//    NSDictionary *info = messageModel.info;
//    // 设置属性来设置行高
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.lineSpacing = 3;
//    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
//    // label的宽度
//     CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 16;
//    
//    // 主题存在（消息为topic）
//    if ([messageModel.type isEqualToNumber:@7]) { // 此model为topic
//        
//        // 设置themeLabel
//        cell.themeLabel.text = [NSString stringWithFormat:@"#%@#",info[@"title"]];
//        cell.themeLabel.hidden = NO;
//        CGRect themeFrame = cell.themeLabel.frame;
//        themeFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
//        themeFrame.size.width = width;
//        // 设置行高
//        CGRect themeRect = CGRectMake(0, 0, 0, 0);
//        if (cell.themeLabel.text.length==0) {
//            themeRect = [cell.themeLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//        } else {
//            cell.themeLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.themeLabel.text attributes:attributes];
//            themeRect = [cell.themeLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//        }
//        themeFrame.size.height = ceil(themeRect.size.height);
//        cell.themeLabel.frame = themeFrame;
//        
//    } else { // 此model为comment
//        cell.themeLabel.hidden = YES;
//        cell.themeLabel.text = nil;
//    }
//    
//    // 定制bodyLabel
//    CGRect frame = cell.bodyLabel.frame;
//    frame.size.width = width;
//    
//    CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
//    if ([messageModel.type isEqualToNumber:@7]) {
//        cell.bodyLabel.text = info[@"desc"];
//        frame.origin.y = CGRectGetMaxY(cell.themeLabel.frame);
//    } else {
//        cell.bodyLabel.text = info[@"message"];
//        frame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
//    }
//    if (cell.bodyLabel.text.length==0) {
//        bodyFrame = [cell.bodyLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        cell.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.bodyLabel.text attributes:attributes];
//        bodyFrame = [cell.bodyLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//        
//    }
//
//    frame.size.height = ceil(bodyFrame.size.height);
//    cell.bodyLabel.frame = frame;
//    
//    // 存储高度，用于返回cell的高度
//    CGFloat cellHeight = 0;
//    if (frame.size.height==0) {
//        cellHeight = CGRectGetMaxY(cell.senderLabel.frame)+ 21;
//    } else {
//        cellHeight = CGRectGetMaxY(cell.bodyLabel.frame);
//    }
//    self.cellHeight = cellHeight;
//    
//    // 定制subImageView(子话题图片)
//    if (![messageModel.type isEqualToNumber:@7]) {
//        cell.subImageView.hidden = YES;
//        cell.subImageView.image = nil;
//    } else {
//        cell.subImageView.userInteractionEnabled = YES;
//        cell.subImageView.hidden = NO;
//        CGRect subFrame = cell.subImageView.frame;
//        subFrame.origin.y = CGRectGetMaxY(cell.bodyLabel.frame);
//        cell.subImageView.frame = subFrame;
//        //        cell.subImageView.image = message.subImage;
//        cell.userInteractionEnabled = YES;
//        
//        // 存储高度，用于返回cell的高度
//        //        float cellHeight = CGRectGetMaxY(cell.subImageView.frame);
//        //        self.cellHeight = cellHeight;
//    }

    */
    
    if (tableView.tag==100) {//投票子话题里tableview
        MessVoteTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MessVoteTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MessVoteTableViewCell" owner:self options:nil] lastObject];
        }
        cell.rowNum.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        cell.DetailLabel.text=_VoteDataArray[indexPath.row];

        [cell.selectButton addTarget:self action:@selector(cellSelectButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectButton.tag=indexPath.row;
        UIImage * selectImg=nil;
        if ([_VoteDataDic[[NSString stringWithFormat:@"%ld_State",(long)indexPath.row]] isEqualToString:@"0"]) {
            selectImg=[UIImage imageNamed:@"choose_normal"];
        }else{
            selectImg=[UIImage imageNamed:@"choose_selected"];   
        }

        if ([_voted isEqualToNumber:@1]&&[_VoteDataDic[@"MyVoteID"] integerValue]==indexPath.row) {  //若已经投票
            cell.DetailLabel.textColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
        }
        
        cell.selectImg.image=selectImg;
        cell.VoteCount.text=[NSString stringWithFormat:@"%@",_VoteDataDic[[NSString stringWithFormat:@"%ld_Choices",(long)indexPath.row]][@"votesCount"]];
//        [NSString stringWithFormat:@"%d",[_VoteDataDic[@"voterNum"] intValue]];
        //计算柱状图宽度
        
        NSInteger allVotesCount=0;
        for (int i=0; i<(_VoteDataDic.count-1)/2; i++) {
            NSInteger count=[_VoteDataDic[[NSString stringWithFormat:@"%d_Choices",i]][@"votesCount"] integerValue];
            allVotesCount+=count;
        }
        
        float danWeiChang=cell.jinduBG.frame.size.width/(allVotesCount==0.0?1.0:allVotesCount);//[cell.VoteCount.text intValue]
        CGRect frame=cell.jinduv.frame;
        float beilu=(ScreenWidth-67)/(320.0-67);
        frame.size.width=danWeiChang*[cell.VoteCount.text intValue]*beilu;
        cell.jinduv.frame=frame;
        /*
//        if ([_voted isEqualToNumber:@1]) {
//            cell.selectImg.hidden=YES;
//            cell.jinduBG.hidden=NO;
//            cell.VoteCount.hidden=NO;
//            UIView* view=[[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 0, cell.jinduv.frame.size.height)];
//            view.backgroundColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
//            [cell addSubview:view];
//            [UIView animateWithDuration:0.8 animations:^{
//                view.frame=frame;
//            }];
//            
//        }else{
//            if ([_closed isEqualToNumber:@1]) {
//                cell.selectImg.hidden=YES;
//            }else{
//                cell.selectImg.hidden=NO;
//            }
//            cell.jinduBG.hidden=YES;
//            cell.VoteCount.hidden=YES;
//        }
        */
        
        if ([_closed isEqualToNumber:@1]) {
            cell.selectImg.hidden=YES;
            cell.jinduBG.hidden=NO;
            cell.VoteCount.hidden=NO;
            UIView* view=[[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 0, cell.jinduv.frame.size.height)];
            view.backgroundColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
            [cell addSubview:view];
            [UIView animateWithDuration:0.8 animations:^{
                view.frame=frame;
            }];
        }else{
            if ([_voted isEqualToNumber:@1]) {
                cell.selectImg.hidden=YES;
                cell.jinduBG.hidden=NO;
                cell.VoteCount.hidden=NO;
                UIView* view=[[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 0, cell.jinduv.frame.size.height)];
                view.backgroundColor=[UIColor colorWithHexString:_ColorStrArray[indexPath.row]];//[UIColor colorWithHex:0x48C1A8];
                [cell addSubview:view];
                [UIView animateWithDuration:0.8 animations:^{
                    view.frame=frame;
                }];
                
            }else{
                cell.selectImg.hidden=NO;
                cell.jinduBG.hidden=YES;
                cell.VoteCount.hidden=YES;
            }
        }
        return cell;
    }
    
    //*******************************
    CommentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    MessageModel *messageModel = self.messageData[indexPath.row];
    
    cell.senderLabel.text = messageModel.sender;

//    NSString *str = [iconPath stringByAppendingString:messageModel.avatar];
    //判断微信登录的图像
    NSRange range=[messageModel.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.length==0) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:messageModel.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:messageModel.avatar];
    }
    [cell.avatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
    // 设置时间显示格式
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconTapGestureClick:)];
    cell.avatarImageView.tag = indexPath.row;
    cell.avatarImageView.userInteractionEnabled = YES;
    [cell.avatarImageView addGestureRecognizer:userIconTap];
//    cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllAmDateFormateWithUTCDate:messageModel.updatedAt];
    cell.creatAtLabel.text = [ToolOfClass toolGetLocalAllDetailDateFormateWithUTCDate:messageModel.updatedAt];
    
    CGRect senderFrame = cell.senderLabel.frame;
    CGRect creatAtFrame = cell.creatAtLabel.frame;
    CGSize senderSize = [cell.senderLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = ceil(senderSize.width);
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    
    if (senderFrame.size.width>200) {
        senderFrame.size.width = 200;
    }
    
    cell.senderLabel.frame = senderFrame;
    creatAtFrame.origin.x = CGRectGetMaxX(cell.senderLabel.frame)+10;
    cell.creatAtLabel.frame = creatAtFrame;
    
    NSDictionary *info = messageModel.info;
    // 设置属性来设置行高
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(cell.senderLabel.frame) - 16;
    CGRect frame = cell.descLabel.frame;
    frame.size.width = width;
    
    cell.descLabel.emojiDelegate = self;
    [cell.descLabel sizeToFit];
    
//    cell.descLabel.text = info[@"message"];
    [cell.descLabel setEmojiText:info[@"message"]];
    
    CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
//    if (cell.descLabel.text!=NULL) {
        bodyFrame = [cell.descLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        cell.descLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.descLabel.text attributes:attributes];
//        bodyFrame = [cell.descLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//        
//    }
    frame.size.height = ceil(bodyFrame.size.height);
    cell.descLabel.frame = frame;

    CGFloat cellHeight = 0;
    if (messageModel.subType.intValue==1||messageModel.subType.intValue==4||messageModel.subType.intValue==3) {
        cell.descLabel.hidden = NO;
        cell.comImageView.hidden = YES;
        
        cell.comImageView.image = nil;
        CGRect bodyFrame = CGRectMake(0, 0, 0, 0);
        if (cell.descLabel.text!=NULL) {
            bodyFrame = [cell.descLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        } else {
            cell.descLabel.attributedText = [[NSAttributedString alloc] initWithString:cell.descLabel.text attributes:attributes];
            bodyFrame = [cell.descLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            
        }
        frame.size.height = ceil(bodyFrame.size.height)+10;
        cell.descLabel.frame = frame;
        
        
        /**
         *  定制urlBodyLabel链接预加载内容
         */
        if (messageModel.info[@"link"]!=nil){//[messageModel.type isEqualToNumber:@16]
//            NSLog(@"---------Message:%@",messageModel.info[@"content"]);
            cell.urlBodyLabe.hidden=NO;
            CGRect frame2 = cell.urlBodyLabe.frame;
            frame2.size.width = width;
            cell.urlBodyLabe.text = info[@"content"];
            frame2.origin.y = CGRectGetMaxY(cell.descLabel.frame);
            CGRect ubodyFrame = CGRectMake(0, 0, 0, 0);
            ubodyFrame = [cell.urlBodyLabe.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
            frame2.size.height = ceil(ubodyFrame.size.height)+23;
            cell.urlBodyLabe.frame = frame2;
        }else{
            cell.urlBodyLabe.hidden=YES;
        }
        
        // 存储高度，用于返回cell的高度
        if (frame.size.height==0) {
            cellHeight = CGRectGetMaxY(cell.senderLabel.frame);
        } else {
            cellHeight = CGRectGetMaxY(cell.descLabel.frame);
            if (messageModel.info[@"link"]!=nil){//[messageModel.type isEqualToNumber:@16]
                cellHeight = CGRectGetMaxY(cell.urlBodyLabe.frame)+23;
            }
        }
        self.cellHeight = cellHeight; 
    } else if([messageModel.subType intValue]==5){
        //语音消息
    }else{
        
        cell.descLabel.hidden = YES;
        cell.comImageView.hidden = NO;


        CGRect subIgFrame = cell.comImageView.frame;
        subIgFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        
        CGFloat subW = [messageModel.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [messageModel.info[@"thumbNail_h"] floatValue];
        if (subW>subH) {
            subH = 180*(subH/subW);
            subW = 180;
            
        } else {
            subW = 180*(subW/subH);
            subH = 180;
        }
        subIgFrame.size.width = isnan(subW)?180:subW;
        subIgFrame.size.height = subH;
        
        cell.comImageView.frame = subIgFrame;

        [cell.comImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:messageModel.info[@"thumbNail"]]] placeholderImage:nil];

        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onCommentImageViewTapClick:)];
        cell.comImageView.tag = indexPath.row;
        cell.comImageView.userInteractionEnabled = YES;
        [cell.comImageView addGestureRecognizer:tap];
        
        // 存储高度，用于返回cell的高度
        self.cellHeight = CGRectGetMaxY(cell.comImageView.frame)+5;
        
    }
    
    /*
     定制语音消息
     */
    CGFloat cellHeightt;
    if ([messageModel.subType isEqualToNumber:@5]){ //语音信息
        cell.RecAudioView.hidden=NO;
        CGFloat MaxWidth=ScreenWidth-65-40;
        width=100;
        
        float widthP=(MaxWidth-100)*([messageModel.info[@"duration"] intValue]/60.0f);
        CGRect audioVFrame = cell.RecAudioView.frame;
        audioVFrame.origin.y = CGRectGetMaxY(cell.senderLabel.frame);
        //        NSLog(@"width+widthPwidth+widthP ----:%ld------:%f and:%f",indexPath.row,width,widthP);
        audioVFrame.size.width = width+widthP;
        // 存储高度，用于返回cell的高度
        cell.RecAudioView.frame=audioVFrame;
        cellHeightt = CGRectGetMaxY(cell.RecAudioView.frame)+5;
        
        cell.audioTimeL.text=[NSString stringWithFormat:@"%d\"",[messageModel.info[@"duration"] intValue]];//
        int nnn=messageModel.volumeAnimatedNum;
        //        if (indexPath.row!=AudioPlayerIsNum) {
        //            nnn=3;
        //        }
        [cell.audioPowerImg setImage:[UIImage imageNamed:[NSString stringWithFormat:@"volume%d",nnn]]];
        cell.audio.tag=indexPath.row;
        [cell.audio addTarget:self action:@selector(AudioPlay:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString * UserId=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
        if ([messageModel.userId isEqualToNumber:[NSNumber numberWithInt:[UserId intValue]]]) {//自己发的语音
            [cell.audioViewBG setImage:[UIImage imageNamed:@"Voice box_gray"]];
            [cell.audioViewBg2 setImage:[UIImage imageNamed:@"The triangle_gray"]];
        }else{
            [cell.audioViewBG setImage:[UIImage imageNamed:@"Voice box"]];
            [cell.audioViewBg2 setImage:[UIImage imageNamed:@"The triangle"]];
        }
        
        if (messageModel.witeActivityIsShow) {
            cell.witeActivityInd.hidden=NO;
            [cell.witeActivityInd startAnimating];
            cell.audioPowerImg.hidden=YES;
        }else{
            cell.witeActivityInd.hidden=YES;
            cell.audioPowerImg.hidden=NO;
        }
        
        if (messageModel.info[@"readed"]!=NULL||messageModel.readed==YES) {
            cell.dotImage.hidden=YES;
        }else{
            cell.dotImage.hidden=NO;
        }
        
        self.cellHeight = cellHeightt;
    }else{
        cell.RecAudioView.hidden=YES;
    }
    
    [self.cellHeightArray insertObject:[NSNumber numberWithFloat:self.cellHeight] atIndex:indexPath.row];
    
    //****************************************
    
    cell.transform = self.tableView.transform;
    
    return cell;
}

#pragma mark 发送按钮，左侧按钮点击事件

- (void)didPressRightButton:(id)sender{
    [self.textView refreshFirstResponder];
    // 发表评论
    [self addCommentToTopic];
    /*
    // tableView滚动到最下面
//    [self setUpMessageDetailViewTableViewScroller];
    
//    UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.messageData.count-1 inSection:0];
//    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    
//    [self.tableView beginUpdates];
//    [_messageData insertObject:message atIndex:0];
//    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
//    [self.tableView endUpdates];
//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];

//    // Fixes the cell from blinking (because of the transform, when using translucent cells)
//    // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
//    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView reloadData];
     */
    [super didPressRightButton:sender];
}

//- (void)didPressLeftButton:(id)sender{
//    
//    [self.textView resignFirstResponder];
//    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册选取",@"拍照", nil];
//    [action showInView:self.view];
//}

- (void)didPressLeftButton:(id)sender{
    [self.textView resignFirstResponder];
    [self.textInputbar.textView resignFirstResponder];
    float delayInSeconds=0.2;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        audioInputbar.hidden=NO;
    });
}

// tableView滚动到最下面
- (void)setUpMessageDetailViewTableViewScroller{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.messageData.count-1 inSection:0];
    
//    UITableViewScrollPosition scrollPosition = (self.inverted) ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
//    [self.tableView beginUpdates];
//    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView.tag==100) {
        if ([_voted isEqualToNumber:@1]||[_closed isEqualToNumber:@1]) {
            return 70;
        }
        return 44;
    }
    
//    float hight=0;
//    if (indexPath.row>=_cellHeightArray.count) {
//        hight=_cellHeight;
//    }else{
//        hight=[_cellHeightArray[indexPath.row] floatValue];
//    }
//    
//    return hight;
    
    
    if ([tableView isEqual:self.tableView]) {
        float hight=0;
        if (indexPath.row>=_cellHeightArray.count) {
            hight=_cellHeight;
        }else{
            hight=[_cellHeightArray[indexPath.row] floatValue];
        }
        
        return hight;
    }
    else {
        return kMinimumHeight;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag==100) {
        return 0;
    }
    
    if ([tableView isEqual:self.autoCompletionView]) {
        return 0.5;
    }
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView.tag==100) {
        return nil;
    }
    
    if ([tableView isEqual:self.autoCompletionView]) {
        UIView *topView = [UIView new];
        topView.backgroundColor = self.autoCompletionView.separatorColor;
        return topView;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    view.backgroundColor = [UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, self.view.frame.size.width-16, 30)];
    label.text = NSLocalized(@"subTopic_detail_comment");
    label.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    label.font = [UIFont fontWithName:@"Heiti SC" size:15.0];
    [view addSubview:label];
    return view;
}

//  设置时间显示格式
- (NSString *)setUpFormatterDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    return [formatter stringFromDate:[NSDate date]];
}

// 发表评论
- (void)addCommentToTopic{
    [self addOrGetTopicComments:@"POST"];
}

// 获取评论列表
- (void)getTopicComments{
    [self addOrGetTopicComments:@"GET"];
}

#pragma mark // 发表评论或获取评论列表
- (void)addOrGetTopicComments:(NSString *)urlType{
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    [app socketStatusIsConnect];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    NSString *str;
    
    if ([urlType isEqualToString:@"POST"]) { // 发表评论
        
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        
        str = [NSString stringWithFormat:addCommentToTopicPath,self.topicId];
        parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
        parameter[@"message"] = [NSString stringThrowOffLinesWithString:self.textView.text];
        
        if (self.notifyArray.count!=0) {
            parameter[@"targets"] = self.notifyArray;
        }
        
        [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                
                [self.notifyArray removeAllObjects];
                self.notifyArray = nil;
                
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
    } else { // 获取评论列表
        str = [NSString stringWithFormat:GetTopicDetail,self.topicId];
        [manager GET:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
//                NSLog(@"%@",responseObject);
//                [SVProgressHUD dismissWithDelay:0.5];
//                NSLog(@"responseObject：%@",responseObject);
                // 清空之前的数据
                [self.messageData removeAllObjects];
                [self.headerViewData removeAllObjects];
                [_VoteDataArray removeAllObjects];
                [_VoteDataDic removeAllObjects];
                
                NSDictionary *data = responseObject[@"data"];
                MessageModel *model = [[MessageModel alloc] init];
                [model setValuesForKeysWithDictionary:data];
                
                //如果是url解析的 消息体 将link地址拼接上
                if (model.info[@"link"]!=nil) {
                    if (![model.info[@"link"] isEqualToString:model.info[@"title"]]) {
                        NSMutableDictionary* newInfo=[[NSMutableDictionary alloc] initWithDictionary:model.info];
                        NSString * strrr=[NSString stringWithFormat:@"%@\n%@",newInfo[@"link"],newInfo[@"desc"]];
                        [newInfo setValue:strrr forKey:@"desc"];
                        model.info=newInfo;
                    }
                }
                
                [self.headerViewData addObject:model];
                for (NSDictionary *dict in data[@"comments"]) {
//                    NSLog(@"dict is huifu data is :%@",dict);
                    MessageModel *model = [[MessageModel alloc] init];
                    [model setValuesForKeysWithDictionary:dict];
                    model.volumeAnimatedNum=3;
                    if (model.info[@"readed"]!=NULL) {
                        model.readed=YES;
                    }else{
                        model.readed=NO;
                    }
                    [self.messageData addObject:model];
                }
                
                _createTopicUserID=data[@"userId"];
                
                NSDictionary * choicesDic=data[@"info"][@"choices"];
                _resouceId=data[@"info"][@"resouceId"];
                _closed=data[@"info"][@"closed"];
                _voted =data[@"info"][@"voted"];
                _anonymous=data[@"info"][@"anonymous"];
                _single=data[@"info"][@"single"];
                

                self.VoteDataDic=data[@"info"][@"options"];
                for ( int i=0; i<_VoteDataDic.count; i++) {
                    [_VoteDataArray addObject:_VoteDataDic[[NSString stringWithFormat:@"%d",i]]];
                }
                long count=_VoteDataDic.count;
                NSMutableDictionary * dicc=[[NSMutableDictionary alloc] initWithDictionary:_VoteDataDic];
                for ( int i=0; i<count; i++) {
                    [dicc setObject:@"0" forKey:[NSString stringWithFormat:@"%d_State",i]];
                    [dicc setObject:choicesDic[[NSString stringWithFormat:@"%d",i]] forKey:[NSString stringWithFormat:@"%d_Choices",i]];
                }
                if (count!=0) {
                    [dicc setObject:data[@"info"][@"voterNum"] forKey:@"voterNum"];
                }
                
                for (int i=0; i<count; i++) {
                    NSArray * array=choicesDic[[NSString stringWithFormat:@"%d",i]][@"votes"];
                    for (int n=0; n<array.count; n++) {
                        NSDictionary * dic=[array objectAtIndex:n];
                        NSUserDefaults * usd=[NSUserDefaults standardUserDefaults];
                        if ([dic[@"id"] isEqualToNumber:[usd objectForKey:@"id"] ]) {
                            [dicc setObject:[NSNumber numberWithInt:i] forKey:@"MyVoteID"];  //设置我投票的选项id
                        }
                    }
                    
                }
                
                _VoteDataDic=dicc;
                // 显示 vote headerView数据
                
                if ([_topicType isEqualToNumber:@4]) {
                    [self setUpVoteMessageDetailViewHeaderViewData];
                }else{
                    [self setUpMessageDetailViewHeaderViewData];
                }
                
                [self.tableView reloadData];
            } else {
//                [UIAlertView alertViewWithTitle:responseObject[@"message"]];
//                [SVProgressHUD showErrorWithStatus:@"加载失败!"];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
        }];
    }
}

//发送投票信息
-(void)sendVoteButClick:(UIButton*)button{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSMutableArray* array=[NSMutableArray array];
    for (int i=0; i<_VoteDataArray.count; i++) {
        if ([_VoteDataDic[[NSString stringWithFormat:@"%d_State",i]] isEqualToString:@"1"]) {
            [array addObject:[NSNumber numberWithInt:i]];
        }
    }
    parameter[@"choice"] =array;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    [manager POST:[NSString stringWithFormat:sendVoteRes,self.resouceId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [ToolOfClass showMessage:NSLocalized(@"subTopic_detail_vote_success")];
            
            self.textView.text=NSLocalized(@"subTopic_detail_vote_join");
            [self addCommentToTopic];
            //重新刷新界面数据
            [self getTopicComments];
//            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"投票成功");
            self.textView.text=@"";
        }
//        else if ([responseObject[@"code"] intValue] == 1) {
//            UIAlertView * alv=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"投票失败"] message:[NSString stringWithFormat:@"%@",responseObject[@"message"]] delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
//            [alv show];
//        }
        else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

//发送关闭投票
-(void)closeVoteButClick:(UIButton*)button{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSMutableArray* array=[NSMutableArray array];
    for (int i=0; i<_VoteDataArray.count; i++) {
        if ([_VoteDataDic[[NSString stringWithFormat:@"%d_State",i]] isEqualToString:@"1"]) {
            [array addObject:[NSNumber numberWithInt:i]];
        }
    }
    parameter[@"choice"] =array;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    [manager POST:[NSString stringWithFormat:sendCloseVote,self.resouceId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            [ToolOfClass showMessage:NSLocalized(@"subTopic_detail_vote_Close")];
            //重新刷新界面数据
            [self getTopicComments];
            //            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"关闭投票成功");
        }
//        else if ([responseObject[@"code"] intValue] == 1) {
//            UIAlertView * alv=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"关闭投票失败"] message:[NSString stringWithFormat:@"%@",responseObject[@"message"]] delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
//            [alv show];
//        }
        else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark// 显示投票 Vote topic的数据
- (void)setUpVoteMessageDetailViewHeaderViewData{
    MessageModel *model = self.headerViewData[0];
    //判断微信登录的图像
    NSRange range11=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range11.length==0) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [self.voteHeaderView.avatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
    
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconClick)];
    self.voteHeaderView.avatarImageView.userInteractionEnabled = YES;
    [self.voteHeaderView.avatarImageView addGestureRecognizer:userIconTap];
    
    self.voteHeaderView.senderLabel.text = model.sender;
    self.voteHeaderView.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.createdAt];
    
    // senderLabel,时间label
    CGRect senderFrame = self.voteHeaderView.senderLabel.frame;
    CGSize senderSize = [model.sender sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = senderSize.width;
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    self.voteHeaderView.senderLabel.frame = senderFrame;
    CGRect creatAtFrame = self.voteHeaderView.creatAtLabel.frame;
    creatAtFrame.origin.x = CGRectGetMaxX(self.voteHeaderView.senderLabel.frame)+10;
    self.voteHeaderView.creatAtLabel.frame = creatAtFrame;
    
    /**
     title定制
     */
    // 设置属性来设置行高
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    // title加粗
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:17.0],NSParagraphStyleAttributeName:paragraphStyle};
    [self.voteHeaderView.titleLabel setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    self.voteHeaderView.titleLabel.emojiDelegate=self;
    
    CGRect titleFrame = self.voteHeaderView.titleLabel.frame;
    CGFloat width = CGRectGetWidth(self.view.frame)-16;//-CGRectGetMinX(self.voteHeaderView.senderLabel.frame)
    titleFrame.size.width = width;
    
    NSMutableString * titleStr=[NSMutableString stringWithFormat:@"%@",[model.info[@"title"] copy]];
    NSRange range=[titleStr rangeOfString:NSLocalized(@"subTopic_detail_vote_faQi")];
    if (range.length!=0) {
        [titleStr deleteCharactersInRange:range];
    }
    CGRect titleRect = CGRectMake(0, 0, 0, 0);
//    if (titleStr.length==0) { // 其实title不会为空（判断多余）
//        self.voteHeaderView.titleLabel.text = nil;
    [self.voteHeaderView.titleLabel setEmojiText:titleStr];
        titleRect = [self.voteHeaderView.titleLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        self.voteHeaderView.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:titleStr attributes:attributes];
//        titleRect = [self.voteHeaderView.titleLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//    }
    
    titleFrame.size.height = ceil(titleRect.size.height);
    self.voteHeaderView.titleLabel.frame = titleFrame;
    
    /**
     *  包含图片的投票子话题
     */
    CGRect subImageFrame = self.voteHeaderView.subImageView.frame;
    BOOL isAddimg=[[model.info allKeys] containsObject:@"imageId"];
    if (isAddimg){ // 是包含图片的投票子话题
        // 添加手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageDetailViewTapGestureClick:)];
        self.voteHeaderView.subImageView.userInteractionEnabled = YES;
        [self.voteHeaderView.subImageView addGestureRecognizer:tap];
        [self.voteHeaderView.subImageView setTranslatesAutoresizingMaskIntoConstraints:YES];
        subImageFrame.origin.y = CGRectGetMaxY(self.voteHeaderView.titleLabel.frame)+5;
        CGFloat subW = [model.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [model.info[@"thumbNail_h"] floatValue];
        CGFloat w = 0;
        CGFloat h = 0;
        if (subW>subH) {
            w = 180;
            h = 180*(subH/subW);
        } else {
            h = 180;
            w = 180*(subW/subH);
        }
        
        subImageFrame.size.width = w;
        subImageFrame.size.height = h;
        [self.voteHeaderView.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
    } else {
        self.voteHeaderView.subImageView.hidden = YES;
        subImageFrame.origin.y=CGRectGetMaxY(titleFrame);
        subImageFrame.size.height = 0;
    }
    self.voteHeaderView.subImageView.frame = subImageFrame;
    
    /**
     *  alertview.frame定制
     */
    [self.voteHeaderView.alertview setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect framm=CGRectMake(0, CGRectGetMaxY(self.voteHeaderView.subImageView.frame)+5, CGRectGetWidth(self.view.frame), 33);
    self.voteHeaderView.alertview.frame =framm;

    
    NSString * string1=NSLocalized(@"subTopic_detail_vote_jiMing");
    NSString * string2=NSLocalized(@"subTopic_detail_vote_oneOption");
    if ([_anonymous isEqualToNumber:@1]) {
        string1=NSLocalized(@"subTopic_detail_vote_anonymity");
    }
    if ([_single isEqualToNumber:@0]) {
        string2=NSLocalized(@"subTopic_detail_vote_moreOption");
    }
    [self.voteHeaderView.allVoteButton setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect allVoteButtonff=self.voteHeaderView.allVoteButton.frame;
    allVoteButtonff.origin.x=CGRectGetMaxX(self.view.frame)-112;
    self.voteHeaderView.allVoteButton.frame=allVoteButtonff;
    self.voteHeaderView.alertLabel.text=[NSString stringWithFormat:@"%@ %@",string1,string2];
    
    //设置发送投票信息Button
    [self.voteHeaderView.sendVoteButton addTarget:self action:@selector(sendVoteButClick:) forControlEvents:UIControlEventTouchUpInside];
    
    //如果投票关闭了 隐藏投票button
    if (![_closed isEqualToNumber:@0]||[_voted isEqualToNumber:@1]) {
        self.voteHeaderView.sendVoteButton.hidden=YES;
    }
    
    NSUserDefaults * info=[NSUserDefaults standardUserDefaults];
    if (![_createTopicUserID isEqualToNumber:[info objectForKey:@"id"]]) {
        self.voteHeaderView.closeVoteButton.hidden=YES;
    }
    if (![_closed isEqualToNumber:@0]) {
        self.voteHeaderView.closeVoteButton.hidden=YES;
    }
    //设置创建者的关闭投票的button
    [self.voteHeaderView.closeVoteButton addTarget:self action:@selector(closeVoteButClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.voteHeaderView.headVoteTableView setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect  framee=self.voteHeaderView.headVoteTableView.frame;
    framee.origin.y=CGRectGetMaxY(self.voteHeaderView.alertview.frame);
    framee.size.width=ScreenWidth;
    framee.size.height=_VoteDataArray.count*(([_voted isEqualToNumber:@1]||[_closed isEqualToNumber:@1])?70:44);
    self.voteHeaderView.headVoteTableView.frame=framee;
    
    
    //控制是否显示投票及关闭but
    int X=0;
    if (self.voteHeaderView.closeVoteButton.hidden==YES&&self.voteHeaderView.sendVoteButton.hidden==YES) {
        X=60;
    }
    
    // 按钮的背景view
    CGRect btnBackGroundViewFrame = self.voteHeaderView.BtnBackgroundView.frame;
    
    btnBackGroundViewFrame.origin.y = CGRectGetMaxY(self.voteHeaderView.headVoteTableView.frame)+70-X;
    
    self.voteHeaderView.BtnBackgroundView.frame = btnBackGroundViewFrame;
    
    // 整个headerView
    CGRect headerViewFrame = self.headerView.frame;
    headerViewFrame.size.height = CGRectGetMaxY(self.voteHeaderView.BtnBackgroundView.frame);
    self.voteHeaderView.frame = headerViewFrame;
    
    
    [self.voteHeaderView.allVoteButton setTitle:[NSString stringWithFormat:@"%d%@",[_VoteDataDic[@"voterNum"] intValue],NSLocalized(@"subTopic_detail_voteCount")] forState:UIControlStateNormal];
    [self.voteHeaderView.allVoteButton addTarget:self action:@selector(showAllVoterVC:) forControlEvents:UIControlEventTouchUpInside];
    self.voteHeaderView.headVoteTableView.delegate=self;
    self.voteHeaderView.headVoteTableView.dataSource=self;
    [self.voteHeaderView.headVoteTableView reloadData];
    self.tableView.tableHeaderView = self.voteHeaderView;
    
    if ([model.favorited intValue] == 1) {
        self.voteHeaderView.collectButton.selected = YES;
    }
    if (![model.userId isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
        self.voteHeaderView.deleteButton.enabled = NO;
    }
}

//显示投票名单详情
-(void)showAllVoterVC:(UIButton*)button{
    if ([_anonymous isEqualToNumber:@1]) {
        UIAlertView * alv=[[UIAlertView alloc] initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"subTopic_detail_vote_unLook") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_know") otherButtonTitles:nil, nil];
        [alv show];
        return;
    }else{
        if ([_voted isEqualToNumber:@0]&&[_closed isEqualToNumber:@0]) {
            UIAlertView * alv=[[UIAlertView alloc] initWithTitle:NSLocalized(@"HOME_alert_cueTitle") message:NSLocalized(@"subTopic_detail_vote_unVote") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_know") otherButtonTitles:nil, nil];
            [alv show];
            return;
        }
    }
    VoteUserListViewController* voteAllUserVC=[[VoteUserListViewController alloc] initWithNibName:@"VoteUserListViewController" bundle:nil];
    voteAllUserVC.choicesDic=_VoteDataDic;
    [self.navigationController pushViewController:voteAllUserVC animated:YES];
    //showAllVoterVC
}

#pragma mark // 显示topic的数据
- (void)setUpMessageDetailViewHeaderViewData{
    MessageModel *model = self.headerViewData[0];
    
    //判断微信登录的图像
    NSRange range=[model.avatar rangeOfString:@"http://wx.qlogo.cn"];
    NSURL * iconUrl;
    if (range.length==0) {
        iconUrl=[NSURL URLWithString:[iconPath stringByAppendingString:model.avatar]];
    }else{
        iconUrl=[NSURL URLWithString:model.avatar];
    }
    [self.headerView.avatarImageView sd_setImageWithURL:iconUrl placeholderImage:nil];
    
    UITapGestureRecognizer *userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMesDetailViewUserIconClick)];
    self.headerView.avatarImageView.userInteractionEnabled = YES;
    [self.headerView.avatarImageView addGestureRecognizer:userIconTap];
    
    self.headerView.senderLabel.text = model.sender;
    self.headerView.creatAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:model.createdAt];
    
    // senderLabel,时间label
    CGRect senderFrame = self.headerView.senderLabel.frame;
    CGSize senderSize = [model.sender sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Heiti SC" size:15.0]}];
    senderFrame.size.width = senderSize.width;
    if (senderFrame.size.width>0) {
        senderFrame.size.width=ceil(senderSize.width)+10;
    }
    self.headerView.senderLabel.frame = senderFrame;
    CGRect creatAtFrame = self.headerView.creatAtLabel.frame;
    creatAtFrame.origin.x = CGRectGetMaxX(self.headerView.senderLabel.frame)+10;
    self.headerView.creatAtLabel.frame = creatAtFrame;
    
    /**
        title,desc定制
     */
    // 设置属性来设置行高
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    // title加粗
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    // desc不加粗
    NSDictionary *attributes1 = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],NSParagraphStyleAttributeName:paragraphStyle};
    
    CGRect titleFrame = self.headerView.titleLabel.frame;
    CGRect descFrame = self.headerView.descLabel.frame;
    CGFloat width = CGRectGetWidth(self.view.frame)-CGRectGetMinX(self.headerView.senderLabel.frame)-16;
    titleFrame.size.width = width;
    descFrame.size.width = width;
    
    self.headerView.titleLabel.emojiDelegate=self;
    NSString *titleStr=nil;
    if ([model.type isEqualToNumber:@7]&& [model.subType isEqualToNumber:@1]) {
        titleStr = [NSString stringWithFormat:@"%@(%@)",model.info[@"title"],[ToolOfClass convertFileSize:[model.info[@"fileSize"] longLongValue]]];
    }else{
        titleStr = [model.info[@"title"] copy];
    }
    CGRect titleRect = CGRectMake(0, 0, 0, 0);
//    if (titleStr.length==0) { // 其实title不会为空（判断多余）
//        self.headerView.titleLabel.text = nil;
        [self.headerView.titleLabel setEmojiText:titleStr];
        titleRect = [self.headerView.titleLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
//    } else {
//        self.headerView.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:titleStr attributes:attributes];
//        titleRect = [self.headerView.titleLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//    }
    [self.headerView.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
    titleFrame.size.height = ceil(titleRect.size.height+10);
    self.headerView.titleLabel.frame = titleFrame;
    /**
        desc定制
     */
    NSString *descStr = [model.info[@"desc"] copy];
    CGRect descRect = CGRectMake(0, 0, 0, 0);
//    if (descStr.length==0) {
//        self.headerView.descLabel.text = nil;
////        descRect = [self.headerView.descLabel.text boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes1 context:nil];
//        descRect.size.height = 0;
//    } else {
//        self.headerView.descLabel.attributedText = [[NSAttributedString alloc] initWithString:descStr attributes:attributes1];
//        descRect = [self.headerView.descLabel.attributedText boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//    }
    self.headerView.descLabel.emojiDelegate=self;
    [self.headerView.descLabel setEmojiText:descStr];
    descRect = [descStr boundingRectWithSize:CGSizeMake(width, 500) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes1 context:nil];
    descFrame.size.height = ceil(descRect.size.height)+10;
    descFrame.origin.y = CGRectGetMaxY(self.headerView.titleLabel.frame);
    self.headerView.descLabel.frame = descFrame;
    [self.headerView.descLabel sizeToFit];
    
    /**
     *  判断是否为图片子话题
     */
    CGRect subImageFrame = self.headerView.subImageView.frame;
    if (model.subType.intValue==2 || model.subType.intValue==3||model.subType.intValue==1) { // 是图片子话题\地图、文件
        // 添加手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessageDetailViewTapGestureClick:)];
        self.headerView.subImageView.userInteractionEnabled = YES;
        [self.headerView.subImageView addGestureRecognizer:tap];

        subImageFrame.origin.y = CGRectGetMaxY(self.headerView.descLabel.frame);
        CGFloat subW = [model.info[@"thumbNail_w"] floatValue];
        CGFloat subH = [model.info[@"thumbNail_h"] floatValue];
        CGFloat w = 0;
        CGFloat h = 0;
        if (subW>subH) {
            w = 180;
            h = 180*(subH/subW);
        } else {
            h = 180;
            w = 180*(subW/subH);
        }
        
        if (model.subType.intValue==1) {
            subImageFrame.size.width = 80;
            subImageFrame.size.height = 110;
            NSString * strurl=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/Inbox/%@",model.info[@"fileName"]]];
            NSURL * url=[NSURL fileURLWithPath:strurl];
//            [self setupDocumentControllerWithURL:url];
            
            if (self.docInteractionController == nil)
            {
                self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
                self.docInteractionController.delegate = self;
            }
            else
            {
                self.docInteractionController.URL = url;
            }
            
            NSInteger iconCount = [self.docInteractionController.icons count];
            UIImage * imageic=nil;
            if (iconCount > 0)
            {
                imageic = [self.docInteractionController.icons objectAtIndex:iconCount - 1];
            }
            _fileImage =imageic;
            [self.headerView.subImageView setImage:_fileImage];
            
        }else{
            subImageFrame.size.width = w;
            subImageFrame.size.height = h;
            
            [self.headerView.subImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.info[@"thumbNail"]]] placeholderImage:nil];
        }
    } else {
        self.headerView.subImageView.hidden = YES;
        subImageFrame.size.height = 0;
    }
    self.headerView.subImageView.frame = subImageFrame;
    
    // 按钮的背景view
    CGRect btnBackGroundViewFrame = self.headerView.BtnBackgroundView.frame;
    if (model.subType.intValue==2 || model.subType.intValue==3||model.subType.intValue==1) {
        btnBackGroundViewFrame.origin.y = CGRectGetMaxY(self.headerView.subImageView.frame);
    } else {
        btnBackGroundViewFrame.origin.y = CGRectGetMaxY(self.headerView.descLabel.frame);
    }
    
    self.headerView.BtnBackgroundView.frame = btnBackGroundViewFrame;
    
    // 整个headerView
    CGRect headerViewFrame = self.headerView.frame;
    headerViewFrame.size.height = CGRectGetMaxY(self.headerView.BtnBackgroundView.frame);
    self.headerView.frame = headerViewFrame;
    
    self.tableView.tableHeaderView = self.headerView;

    if ([model.favorited intValue] == 1) {
        self.headerView.collectButton.selected = YES;
    }
    if (![model.userId isEqualToNumber:[[NSUserDefaults standardUserDefaults] objectForKey:@"id"]]) {
        self.headerView.deleteButton.enabled = NO;
    }
    
}

#pragma mark 分享，收藏，删除操作

- (void)onShareOrCollectOrDelecteBtnClick:(UIButton *)sender
{
    if (sender.tag==100) {
        [self.textView resignFirstResponder];
//        [UMSocialSnsService presentSnsIconSheetView:self
//                                             appKey:UM_key
//                                          shareText:@"小集体(www.docy.co)——专业的团队交流app，小组讨论必备神器小集体，把讨论装进手机里，随时随地带走你的工作和社交，拥有真正无隔阂的交流、协作平台。通过创建小集体，让话题更丰富，让沟通更有趣，让聊天更容易。"
//                                         shareImage:[UIImage imageNamed:@"logo"]//self.headerView.subImageView.image
//                                    shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToRenren,nil]
//                                           delegate:nil];
        
        MessageModel *model = self.headerViewData[0];
        if (model.subType.intValue==1) { // 文件子话题
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"subTopic_detail_file_topic"),NSLocalized(@"subTopic_detail_file_file"), nil];
            actionSheet.tag = 100; // 标志
            [actionSheet showInView:self.view];
        } else { // 非文件子话题
            UserJionedGroupViewController *joinGroupVC = [[UserJionedGroupViewController alloc] init];
            joinGroupVC.topicId = self.topicId;
            [self.navigationController pushViewController:joinGroupVC animated:YES];
        }
    } else if (sender.tag==101) {
        if (sender.selected==YES) {
            [self addMessageDetailViewFavorite:NO];
        } else {
            [self addMessageDetailViewFavorite:YES];
        }
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"subTopic_detail_topic_dele_sure") message:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"subTopic_detail_topic_dele"), nil];
        [alert show];
    }
}
- (void)deleteMessageDetailViewTopic{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    
    [manager POST:[NSString stringWithFormat:deleteTopic,self.topicId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            
            [ToolOfClass showMessage:NSLocalized(@"subTopic_detail_topic_dele_success")];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteTopicSuccess" object:self.topicId userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"subTopic_detail_topic_dele_fail")];
    }];
    
}

- (void)addMessageDetailViewFavorite:(BOOL)isFavorite{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = nil;
    if (isFavorite) {
        parameter = [NSDictionary dictionaryWithObject:[ToolOfClass authToken] forKey:@"accessToken"];
    } else {
        parameter = [NSDictionary dictionaryWithObjects:@[[ToolOfClass authToken],@"true"] forKeys:@[@"accessToken",@"unfavorite"]];
    }
    [manager POST:[NSString stringWithFormat:addFavorite,self.topicId] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            if (isFavorite) {
                self.headerView.collectButton.selected = YES;
                self.voteHeaderView.collectButton.selected = YES;
            } else {
               self.headerView.collectButton.selected = NO;
                self.voteHeaderView.collectButton.selected = NO;
            }
            if (isFavorite) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newFavorite" object:nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newUnFavorite" object:self.topicId];
            }
            
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
#pragma mark 图片子话题图片的点击手势

- (void)onMessageDetailViewTapGestureClick:(UITapGestureRecognizer *)tapGesture{
    
    MessageModel *model = self.headerViewData[0];
    if ([model.subType isEqualToNumber:@3]) { // 地图子话题
//        MapViewController *mapVC = [[MapViewController alloc] init];
        Map_ViewController *mapVC = [[Map_ViewController alloc] init];
        mapVC.latitude = [model.info[@"latitude"] floatValue];
        mapVC.longitude = [model.info[@"longitude"] floatValue];
        mapVC.isYulan=YES;  //预览
        [self.navigationController pushViewController:mapVC animated:YES];
    } else {
        if ([model.subType isEqualToNumber:@1]) { // 文件子话题
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString * Inbox=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents"]];
            if(![fileManager fileExistsAtPath:Inbox]){
                BOOL bo=[fileManager createDirectoryAtPath:Inbox withIntermediateDirectories:YES attributes:nil error:nil];
                NSLog(@"bobobobo is %d",bo);
            }
            NSString * filname=model.info[@"fileName"];
            NSString * filpath=model.info[@"filePath"];
            NSString *savedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",filname]];
            NSLog(@"filepath_dow---%@",savedPath);
            NSString * filep=[NSString stringWithFormat:@"%@%@",iconPath,filpath];
            
            //    [fileManager removeItemAtPath:savedPath error:nil];  //删除文件
            if(![fileManager fileExistsAtPath:savedPath]) //如果不存在 下载
            {
                showFileInfoViewController *showPictureVC = [[showFileInfoViewController alloc] initWithNibName:@"showFileInfoViewController" bundle:[NSBundle mainBundle]];
                showPictureVC.fileName = model.info[@"fileName"];
                showPictureVC.filePath = model.info[@"filePath"];
                
                XJTNavigationController * fileShowNav=[[XJTNavigationController alloc] initWithRootViewController:showPictureVC];
                [self presentViewController:fileShowNav animated:YES completion:nil];
            }else{
                //展示内容
                QLPreviewController *previewController = [[QLPreviewController alloc] init];
                previewController.dataSource = self;
                previewController.delegate = self;
                [[self navigationController] pushViewController:previewController animated:YES];
            }

        }else{ // 其他子话题
            ShowPictureViewController *showPictureVC = [[ShowPictureViewController alloc] initWithNibName:@"ShowPictureViewController" bundle:[NSBundle mainBundle]];
            showPictureVC.imageInfo = model.info;
            [self presentViewController:showPictureVC animated:YES completion:nil];
        }
    }
 }

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you ovveride this method, to call super.
    [super scrollViewDidScroll:scrollView];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{

    if (self.messageData.count>0) {
        [self setUpMessageDetailViewTableViewScroller];
    }
}

- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    [super textView:textView shouldChangeTextInRange:range replacementText:text];
    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
        [self didPressRightButton:nil];
        textView.text=@"";
        return NO;
    }
    return YES;
}

- (void)textViewDidChangeSelection:(SLKTextView *)textView{
    [super textViewDidChangeSelection:textView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [super alertView:alertView clickedButtonAtIndex:buttonIndex];
    if (buttonIndex==1) {
        [self deleteMessageDetailViewTopic];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.textView resignFirstResponder];
    if (actionSheet.tag==100) { // 转发文件子话题

        if (buttonIndex==0) {
            UserJionedGroupViewController *joinGroupVC = [[UserJionedGroupViewController alloc] init];
            joinGroupVC.topicId = self.topicId;
            [self.navigationController pushViewController:joinGroupVC animated:YES];
        } else if (buttonIndex==1) {
            
            MessageModel *model = self.headerViewData[0];
            NSString * filname=model.info[@"fileName"];
//            NSString * filpath=model.info[@"filePath"];
//            NSString * filep=[NSString stringWithFormat:@"%@%@",iconPath,filpath];
            NSString *savedPath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",filname]];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if(![fileManager fileExistsAtPath:savedPath]) // 不存在
            {
//                ALERT_HOME(@"请先下载文件", nil);
                showFileInfoViewController *showPictureVC = [[showFileInfoViewController alloc] initWithNibName:@"showFileInfoViewController" bundle:[NSBundle mainBundle]];
                showPictureVC.fileName = model.info[@"fileName"];
                showPictureVC.filePath = model.info[@"filePath"];
                
                XJTNavigationController * fileShowNav=[[XJTNavigationController alloc] initWithRootViewController:showPictureVC];
                [self presentViewController:fileShowNav animated:YES completion:nil];
                return;
            }
            NSURL *URL = [NSURL fileURLWithPath:savedPath];
            if (URL) {
                [self setupDocumentControllerWithURL:URL];
                [self.docInteractionController setDelegate:self];
                [self.docInteractionController presentOptionsMenuFromRect:[actionSheet frame] inView:self.view animated:YES];
            }
        }
        
        return;
    }
    
    //**************************************************
    
    if (buttonIndex==2){
//        [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
    } else  if (buttonIndex==1) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)onMesDetailViewUserIconTapGestureClick:(UITapGestureRecognizer *)tap{
    UIImageView *userIconImageView = (UIImageView *)tap.view;

    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    MessageModel *model = self.messageData[userIconImageView.tag];

    userPerVC.userId = model.userId;
    [self.navigationController pushViewController:userPerVC animated:YES];
}

- (void)onMesDetailViewUserIconClick{
    MessageModel *model = self.headerViewData[0];
    LookUpGroupUserPerfileViewController *userPerVC = [[LookUpGroupUserPerfileViewController alloc] initWithNibName:@"LookUpGroupUserPerfileViewController" bundle:[NSBundle mainBundle]];
    userPerVC.userId = model.userId;
    [self.navigationController pushViewController:userPerVC animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:NSLocalized(@"group_chat_voice_sending")];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [ToolOfClass authToken];
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    NSData *imageData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], ImageYaSuo);
    [manager POST:[NSString stringWithFormat:addCommentToTopicPath,self.topicId] parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"imageName" mimeType:@"image/jpg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

- (void)onCommentImageViewTapClick:(UITapGestureRecognizer *)tap{
    MessageModel *model = self.messageData[tap.view.tag];
    ShowPictureViewController *picVC = [[ShowPictureViewController alloc] init];
    picVC.imageInfo = model.info;
    [self presentViewController:picVC animated:NO completion:nil];
}

- (void)mlEmojiLabel:(MLEmojiLabel*)emojiLabel didSelectLink:(NSString*)link withType:(MLEmojiLabelLinkType)type
{
    switch(type){
        case MLEmojiLabelLinkTypeURL:
//            NSLog(@"点击了链接%@",link);
            ;
            NSRange range=[link rangeOfString:@"http://"];
            if (range.length==0) {
                link=[NSString stringWithFormat:@"http://%@",link];
            }
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:link]];
            //            [[[UIActionSheet alloc] initWithTitle:link delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
            break;
        case MLEmojiLabelLinkTypePhoneNumber:
//            NSLog(@"点击了电话%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"tel://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeEmail:
//            NSLog(@"点击了邮箱%@",link);
            [[UIApplication sharedApplication]openURL:[NSURL   URLWithString:[NSString stringWithFormat:@"mailto://%@",link]]];
            break;
        case MLEmojiLabelLinkTypeAt:
//            NSLog(@"点击了用户%@",link);
            break;
        case MLEmojiLabelLinkTypePoundSign:
//            NSLog(@"点击了话题%@",link);
            break;
        default:
//            NSLog(@"点击了不知道啥%@",link);
            break;
    }
    
}

#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    //    NSInteger numToPreview = 0;
    //
    //	numToPreview = [self.dirArray count];
    //
    //    return numToPreview;
    return 1;//[self.dirArray count];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    [previewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"click.png"] forBarMetrics:UIBarMetricsDefaultPrompt];
    
    NSURL *fileURL = nil;
    //    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    MessageModel *model = self.headerViewData[0];
    NSString *path =[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",model.info[@"fileName"]]]; //[[NSString stringWithFormat:@"%@",documentDir] stringByAppendingPathComponent:_fileName];
    fileURL = [NSURL fileURLWithPath:path];
    return fileURL;
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
    return self;
}

@end
