//
//  SubTopicModel.h
//  nc
//
//  Created by docy admin on 7/14/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"

@interface SubTopicModel : CommonModel

@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, retain) NSNumber *type;
@property (nonatomic, retain) NSNumber *favorited;       //关注与否
@property (nonatomic, retain) NSNumber *commentCount;   //回复数
@property (nonatomic, retain) NSNumber *views;          //点击数
@property (nonatomic, retain) NSNumber *favoriteCount;  //关注数
@property (nonatomic, retain) NSNumber *id;
@property (nonatomic, retain) NSNumber *groupId;
@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, copy) NSString *updatedAt;
@property (nonatomic, retain) NSDictionary *info;
@property (nonatomic, retain) NSArray *comments;
@property (nonatomic, retain) NSNumber *subType; // topic类型，用于区分不同topic

@end
