//
//  MessageModel.h
//  nc
//
//  Created by docy admin on 7/6/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"
@class UIImage;
@interface MessageModel : CommonModel

@property (nonatomic, copy) NSString *id;    // 服务器消息体唯一id
@property (nonatomic, copy) NSString *sender;// 发送者
@property (nonatomic, copy) NSString *avatar; // 发送者头像
@property (nonatomic, copy) NSString *createdAt; // 创建时间
@property (nonatomic, copy) NSString *updatedAt; // 创建时间
//@property (nonatomic, copy) NSString *link; //  纯链接消息
@property (nonatomic, retain) NSDictionary *info; // 消息详细信息
@property (nonatomic, retain) NSNumber *type; // 消息类型，用于区分普通消息和topic
@property (nonatomic, retain) NSNumber *subType; // topic类型，用于区分不同topic

@property (nonatomic, copy) NSString *nickName; // 昵称
@property (nonatomic, retain) NSNumber *userId; // 事件的触发者
@property (nonatomic, retain) NSNumber *favorited; // 是否收藏


@property (nonatomic, assign) int flag;
@property (nonatomic, assign) int sendSuccessflag;  //发送成功标记
@property (nonatomic,copy) NSString *timeViewStr;

@property (nonatomic, assign) int  volumeAnimatedNum; //音频播放动画帧
@property (nonatomic, assign) int  duration;          //音频总时长
@property (nonatomic, assign) BOOL witeActivityIsShow;   //音频缓冲显示状态
@property (nonatomic, assign) BOOL readed;

@end
