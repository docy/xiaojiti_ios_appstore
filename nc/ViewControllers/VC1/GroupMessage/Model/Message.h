//
//  Message.h
//  nc
//
//  Created by jianghuan on 4/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Message : NSObject

@property (nonatomic, copy) NSString *sender;// 发送者
@property (nonatomic, retain) UIImage *userAvatar; // 发送者头像
@property (nonatomic, copy) NSString *createdAt; // 创建时间
@property (nonatomic, retain) NSDictionary *info; // 消息详细信息


//@property (nonatomic, copy) NSString *body; // 内容
//@property (nonatomic, copy) NSString *theme; // 主题
//@property (nonatomic, retain) UIImage *subImage; // 子视图（图片）

@end