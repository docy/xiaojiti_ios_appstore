//
//  UserModel.h
//  nc
//
//  Created by docy admin on 7/10/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"


// 群组用户model
@interface UserModel : CommonModel

@property (nonatomic, copy) NSString *avatar; // 用户头像
@property (nonatomic, copy) NSString *avatarOrigin; // 用户原始头像

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSNumber *id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, retain) NSNumber *sex;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, retain) NSNumber *userType;

@property NSInteger sectionNumber;


@property (nonatomic, assign) BOOL cellIsSelected;

@end
