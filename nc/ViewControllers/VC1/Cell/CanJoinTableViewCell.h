//
//  CanJoinTableViewCell.h
//  nc
//
//  Created by docy admin on 15/12/15.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CanJoinTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupDescribeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *groupIconImageView;

@property (weak, nonatomic) IBOutlet UIButton *joinGroupBtn;
@property (weak, nonatomic) IBOutlet UILabel *userCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;

@property (weak, nonatomic) IBOutlet UIImageView *bgHyd;

@property (weak, nonatomic) IBOutlet UILabel *Localized_active;



@property (nonatomic, weak) UIImageView *starView;

@end
