//
//  CanJoinTableViewCell.m
//  nc
//
//  Created by docy admin on 15/12/15.
//  Copyright © 2015年 cn.dossi. All rights reserved.
//

#import "CanJoinTableViewCell.h"

@implementation CanJoinTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setStarView:(UIImageView *)starView{
    
    _starView = starView;
    CGRect frame = self.bgHyd.frame;
    UIImage *image = [UIImage imageNamed:@"active-selected"];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.contentMode = UIViewContentModeLeft;
    imageView.clipsToBounds = YES;
    imageView.image = image;
    [self.contentView addSubview:imageView];
}

@end
