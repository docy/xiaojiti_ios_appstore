//
//  Map_ViewController.h
//  nc
//
//  Created by guanxf on 16/1/12.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Map_ViewController : UIViewController

@property (nonatomic, retain) NSNumber *groupId; // 群组的ID，

@property (nonatomic, assign) double latitude; // 纬度
@property (nonatomic, assign) double longitude; // 经度

@property (nonatomic, assign) BOOL isFromPrivateVC; // 判断是否是从私聊界面进入
@property (nonatomic, assign) BOOL isYulan; // 判断是否是预览位置

@end
