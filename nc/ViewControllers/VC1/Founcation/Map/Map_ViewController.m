//
//  Map_ViewController.m
//  nc
//
//  Created by guanxf on 16/1/12.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "Map_ViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MainThemeViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"


#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self

static NSTimeInterval const kTimeDelay = 2.5;

@interface Map_ViewController () <CLLocationManagerDelegate, MKMapViewDelegate>

@property (nonatomic, assign) BOOL isSearchFromDragging;
@property (nonatomic, retain) UIButton *currentAddressBtn;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *detailStr;
@property (weak, nonatomic) IBOutlet MKMapView   *mapView;
@property (weak, nonatomic) IBOutlet UILabel     *resultLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressField;
@property (weak, nonatomic) IBOutlet UITextField *latitudeField;
@property (weak, nonatomic) IBOutlet UITextField *longitudeField;

//@property (strong, nonatomic) MBProgressHUD      *hud;
@property (strong, nonatomic) CLLocationManager  *locMgr;
@property (strong, nonatomic) CLGeocoder         *geocoder;
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;  //当前位置
@property (nonatomic,assign) CLLocationDistance altitude;
@property (strong, nonatomic) NSString * weizhiStr;

@end

@implementation Map_ViewController

- (CLLocationManager *)locMgr {
    if (!_locMgr) {
        _locMgr = [[CLLocationManager alloc] init];
        _locMgr.delegate = self;
        if (![CLLocationManager locationServicesEnabled]) {
            NSLog(@"定位服务当前可能尚未打开，请设置打开！");
        }
        
        //如果没有授权则请求用户授权
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
            [_locMgr requestWhenInUseAuthorization];
        }else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse){
            //设置代理
//            _locMgr.delegate=self;
            //设置定位精度
//            _locMgr.desiredAccuracy=kCLLocationAccuracyBest;
//            //定位频率,每隔多少米定位一次
//            CLLocationDistance distance=10.0;//十米定位一次
//            _locMgr.distanceFilter=distance;
            //启动跟踪定位
//            [_locMgr startUpdatingLocation];
        }
        // 定位精度
        _locMgr.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        // 距离过滤器，当移动距离小于这个值时不会收到回调
        //        _locMgr.distanceFilter = 50;
    }
    return _locMgr;
}

- (CLGeocoder *)geocoder {
    if (!_geocoder) {
        _geocoder = [[CLGeocoder alloc] init];
    }
    return _geocoder;
}

- (MKMapView *)mapView {
    if (!_mapView) {
        MKMapView *mapView = [[MKMapView alloc] init];
        _mapView = mapView;
        
        _mapView.delegate = self;
    }
    
    return _mapView;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 添加背景点击事件
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped)];
    recognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:recognizer];
    
    _weizhiStr=[[NSString alloc] init];
    
    self.locMgr.delegate=self;
    
    [self setUpMapViewAttribute];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (_isYulan) {//预览显示内容
        CLLocationCoordinate2D coord=CLLocationCoordinate2DMake(_latitude, _longitude);
        _coordinate=coord;
        [self addAnnotation:coord];
        [self reverseGeocode]; //获得文本街道地址
    }else{//准备创建地图子话题
        self.mapView.delegate=self;
        [self.mapView setShowsUserLocation:YES];
//        [self startLocating];
    }
}

//实时显示我的位置
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    CLLocationCoordinate2D coord = [userLocation coordinate];
    _coordinate.latitude=coord.latitude;
    _coordinate.longitude=coord.longitude;
    [self reverseGeocode];
    self.latitude=coord.latitude;
    self.longitude=coord.longitude;
    NSLog(@"-----经度:%f,纬度:%f",coord.latitude,coord.longitude);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 设置导航栏属性
- (void)setUpMapViewAttribute{
    self.view.backgroundColor = [UIColor grayColor];
    self.titleStr=[NSString string];
    self.detailStr=[NSString string];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:NSLocalized(@"map_title") target:self action:@selector(onRightButtonClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onMapViewBackBtnClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onMapViewBackBtnClick)];
    
    if (_isYulan) {
        self.navigationItem.rightBarButtonItem=nil;
    }
    
    self.currentAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.currentAddressBtn setBackgroundImage:[UIImage imageNamed:@"map"] forState:UIControlStateNormal];
    CGSize size = self.currentAddressBtn.currentBackgroundImage.size;
    self.currentAddressBtn.frame = CGRectMake(self.view.bounds.size.width-size.width, self.view.bounds.size.height*0.8, size.width, size.height);
    [self.currentAddressBtn addTarget:self action:@selector(onMapViewCurrentAddressBtnClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)onMapViewBackBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

// 截屏按钮点击事件
- (void)onRightButtonClick{
    self.navigationItem.rightBarButtonItem.enabled = NO;
    if (self.isFromPrivateVC==NO) { // 发表主题
        MainThemeViewController *themeVC = [[MainThemeViewController alloc] init];
        themeVC.image = [self getImageFromImage];
        themeVC.groupId=_groupId;
        themeVC.titleString=_weizhiStr;
        themeVC.detailString=_detailStr;
        themeVC.latitude = _coordinate.latitude;
        themeVC.longitude = _coordinate.longitude;
        //    themeVC.block = ^(Message *message) {
        //        if (self.mapBlock) {
        //            self.mapBlock(message);
        //        }
        //    };
        // 代理传值
        themeVC.block = ^() {
            [self.navigationController popViewControllerAnimated:NO];
        };
        
        themeVC.backblock = ^(){
            [self.navigationController popViewControllerAnimated:NO];
        };
        
        [self.navigationController pushViewController:themeVC animated:YES];
        
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else { // 私聊发表地图
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [ToolOfClass authToken];
        parameter[@"groupId"] = self.groupId;
        parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
        parameter[@"latitude"] = @(self.latitude);
        parameter[@"longitude"] = @(self.longitude);
        
        NSData *imageData = UIImageJPEGRepresentation([self getImageFromImage], 1);
        [manager POST:directChatSendImage parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"imageName" mimeType:@"image/jpg"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

#pragma mark - IBAction

- (void)startLocating {
    [self keyboardResign];
    [self locationAuthorizationJudge];
}

/**
 *  地理编码
 */
- (void)geocode {
    [self keyboardResign];
    
    if (self.addressField.text.length == 0) {
//        [self showCommonTip:@"请填写地址"];
        return;
    }
    
//    [self showProcessHud:@"正在获取位置信息"];
    
    WS(weakSelf);
    [self.geocoder geocodeAddressString:self.addressField.text completionHandler:^(NSArray *placemarks, NSError *error) {
//        [weakSelf.hud hide:YES];
        
        if (error) {
//            [weakSelf showCommonTip:@"地理编码出错，或许你选的地方在冥王星"];
            NSLog(@"%@", error);
            return;
        }
        
        CLPlacemark *placemark = [placemarks firstObject];
        NSString *formatString = [NSString stringWithFormat:@"经度：%lf，纬度：%lf\n%@ %@ %@\n%@\n%@ %@", placemark.location.coordinate.latitude, placemark.location.coordinate.longitude, placemark.addressDictionary[@"City"], placemark.addressDictionary[@"Country"], placemark.addressDictionary[@"CountryCode"], [placemark.addressDictionary[@"FormattedAddressLines"] firstObject], placemark.addressDictionary[@"Name"], placemark.addressDictionary[@"State"]];
        weakSelf.resultLabel.text = formatString;
        [weakSelf showInMapWithCoordinate:CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude)];
        for (CLPlacemark *pm in placemarks) {
            NSLog(@"经度：%lf，纬度：%lf\n%@ %@ %@\n%@\n%@ %@", pm.location.coordinate.latitude, pm.location.coordinate.longitude, pm.addressDictionary[@"City"], pm.addressDictionary[@"Country"], pm.addressDictionary[@"CountryCode"], [pm.addressDictionary[@"FormattedAddressLines"] firstObject], pm.addressDictionary[@"Name"], pm.addressDictionary[@"State"]);
        }
        _weizhiStr=[placemark.addressDictionary[@"FormattedAddressLines"] firstObject];
        [self addAnnotation:_coordinate];
    }];
}

/**
 *  反地理编码
 */
- (void)reverseGeocode {
    [self keyboardResign];

    CLLocation *location = [[CLLocation alloc] initWithLatitude:_coordinate.latitude longitude:_coordinate.longitude];
    
    WS(weakSelf);
    [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
//        [weakSelf.hud hide:YES];
        
        if (error) {
//            [weakSelf showCommonTip:@"地理编码出错，或许你选的地方在冥王星"];
            NSLog(@"%@", error);
            return;
        }
        
        CLPlacemark *placemark = [placemarks firstObject];
        NSString *formatString = [NSString stringWithFormat:@"经度：%lf，纬度：%lf\n%@ %@ %@\n%@\n%@ %@", placemark.location.coordinate.latitude, placemark.location.coordinate.longitude, placemark.addressDictionary[@"City"], placemark.addressDictionary[@"Country"], placemark.addressDictionary[@"CountryCode"], [placemark.addressDictionary[@"FormattedAddressLines"] firstObject], placemark.addressDictionary[@"Name"], placemark.addressDictionary[@"State"]];
        weakSelf.resultLabel.text = formatString;
        
        _weizhiStr=[placemark.addressDictionary[@"FormattedAddressLines"] firstObject];
        if (_isYulan) {
            [weakSelf showInMapWithCoordinate:CLLocationCoordinate2DMake(_coordinate.latitude, _coordinate.longitude)];
            [self addAnnotation:_coordinate];
        }else{
            MKCoordinateRegion region = MKCoordinateRegionMake(_coordinate, MKCoordinateSpanMake(0.025, 0.025));
            [self.mapView setRegion:region animated:YES];
            self.mapView.userLocation.title=_weizhiStr;
        }
        
        
        for (CLPlacemark *pm in placemarks) {
            NSLog(@"经度：%lf，纬度：%lf\n%@ %@ %@\n%@\n%@ %@", pm.location.coordinate.latitude, pm.location.coordinate.longitude, pm.addressDictionary[@"City"], pm.addressDictionary[@"Country"], pm.addressDictionary[@"CountryCode"], [pm.addressDictionary[@"FormattedAddressLines"] firstObject], pm.addressDictionary[@"Name"], pm.addressDictionary[@"State"]);
        }
    }];
}

#pragma mark - Private

//- (void)showCommonTip:(NSString *)tip {
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.hud.mode = MBProgressHUDModeText;
//    self.hud.labelText = tip;
//    self.hud.removeFromSuperViewOnHide = YES;
//    [self.hud hide:YES afterDelay:kTimeDelay];
//}
//
//- (void)showProcessHud:(NSString *)msg {
//    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
//    [self.view  addSubview:self.hud];
//    self.hud.removeFromSuperViewOnHide = YES;
//    self.hud.mode = MBProgressHUDModeIndeterminate;
//    self.hud.labelText = msg;
//    [self.hud show:NO];
//}

/**
 *  点击空白处收起键盘
 */
- (void)backgroundTapped {
    [self keyboardResign];
}

- (void)keyboardResign {
    [self.view endEditing:YES];
}

/**
 *  判断定位授权
 */
- (void)locationAuthorizationJudge {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    NSString *locationServicesEnabled = [CLLocationManager locationServicesEnabled] ? @"YES" : @"NO";
    NSLog(@"location services enabled = %@", locationServicesEnabled);
    
    if (status == kCLAuthorizationStatusNotDetermined) { // 如果授权状态还没有被决定就弹出提示框
        if ([self.locMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locMgr requestWhenInUseAuthorization];
            //            [self.locMgr requestAlwaysAuthorization];
        }
        
        // 也可以判断当前系统版本是否大于8.0
        //        if ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0) {
        //            [self.locMgr requestWhenInUseAuthorization];
        //        }
    } else if (status == kCLAuthorizationStatusDenied) { // 如果授权状态是拒绝就给用户提示
//        [self showCommonTip:@"请前往设置-隐私-定位中打开定位服务"];
    } else if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways) { // 如果授权状态可以使用就开始获取用户位置
        [self.locMgr startUpdatingLocation];
    }
}

/**
 *  计算两个坐标之间的直线距离;
 */
- (void)calculateStraightDistance {
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:30 longitude:123];
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:31 longitude:124];
    CLLocationDistance distances = [loc1 distanceFromLocation:loc2];
    NSLog(@"两点之间的直线距离是%lf", distances);
}

/**
 *  将位置信息显示到mapView上
 */
- (void)showInMapWithCoordinate:(CLLocationCoordinate2D)coordinate {
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.025, 0.025));
    [self.mapView setRegion:region animated:YES];
    [self addAnnotation:coordinate];
}

/**
 *  添加大头针
 */
- (void)addAnnotation:(CLLocationCoordinate2D)coordinate {
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.title = _weizhiStr;
    annotation.coordinate = coordinate;
    [self.mapView addAnnotation:annotation];
}

#pragma mark - CLLocationManagerDelegate

/**
 *  只要定位到位置，就会调用，调用频率频繁
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    _coordinate=location.coordinate;
    NSLog(@"我的位置是 - %@", location);
    [self showInMapWithCoordinate:location.coordinate];
    [self reverseGeocode];
    // 根据不同需要停止更新位置
    [self.locMgr stopUpdatingLocation];
}

//根据给定得图片，从其指定区域截取一张新得图片
-(UIImage *)getImageFromImage{
    CGSize size;
    size.width = self.mapView.frame.size.width;
    size.height = self.mapView.frame.size.height;

    UIGraphicsBeginImageContext(size);
    [self.mapView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end