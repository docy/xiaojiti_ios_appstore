//
//  PictureViewController.h
//  nc
//
//  Created by docy admin on 6/5/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *pictureData;

@end
