//
//  VoteViewController.m
//  nc
//
//  Created by docy admin on 6/10/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "VoteViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "VoteTableViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"

@interface VoteViewController () <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>{
    BOOL isAddImage;  //添加图片标致
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSMutableArray * listData;
@property (weak, nonatomic) IBOutlet UITextField *DetailLabel;
@property (strong, nonatomic) IBOutlet UIView *footview;
@property (strong, nonatomic) IBOutlet UIView *headerVeiw;
@property (weak, nonatomic) IBOutlet UISwitch *singleSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *anonymousSwitch;
- (IBAction)addPictureButClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) NSNumber * imageID;

@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property (weak, nonatomic) IBOutlet UILabel *whiteLable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivitView;



@property (weak, nonatomic) IBOutlet UILabel *Localized_moreVote;
@property (weak, nonatomic) IBOutlet UILabel *Localized_anonymity;
@property (weak, nonatomic) IBOutlet UIButton *Localized_addImage;


@end

@implementation VoteViewController
@synthesize groupId,image,imageName;

- (void)viewDidLoad {
    [super viewDidLoad];
    isAddImage=NO;
    self.listData=[[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
    
    // 设置导航栏
    [self setUpNavigation];
    
    [self.tableview setEditing:YES];
    [self.tableview setTranslatesAutoresizingMaskIntoConstraints:YES];
    self.tableview.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    
    //设置等待view位置大小
    _whiteView.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    _ActivitView.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2-40);
    _whiteLable.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    _whiteView.hidden=YES;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldChanged:) name:UITextFieldTextDidChangeNotification object:nil];
}


// 设置导航栏
- (void)setUpNavigation
{
    
    self.themeTextFiled.placeholder = NSLocalized(@"vote_captionPH");
    self.Localized_moreVote.text = NSLocalized(@"vote_moreVote");
    self.Localized_anonymity.text = NSLocalized(@"vote_anonymity");
    
    [self.Localized_addImage setTitle:NSLocalized(@"vote_addImage") forState:UIControlStateNormal];
    
    self.navigationItem.title = NSLocalized(@"vote_title");
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithTitle:NSLocalized(@"vote_title_right") target:self action:@selector(onAddButtonClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(backBtnClick)];
    
//    self.navigationController.navigationBar.translucent = YES;
}

- (void)backBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 导航栏右侧按钮的点击事件
- (void)onAddButtonClick
{
    
//    CGRect frame=_tableview.frame;
//    frame.size.height=frame.size.height+44;
//    _tableview.frame=frame;
    
    NSMutableDictionary * options=[NSMutableDictionary dictionary];
    for (int i=0; i<self.listData.count; i++) {
        if (![_listData[i] isEqualToString:@""]) {
            [options setObject:_listData[i] forKey:[NSString stringWithFormat:@"%ld",options.count]];
        }
    }
    if (options.count<2) {
        UIAlertView * alertv=[[UIAlertView alloc] initWithTitle:NSLocalized(@"vote_option_last") message:NSLocalized(@"vote_option_lastTwo") delegate:self cancelButtonTitle:NSLocalized(@"vote_option_know") otherButtonTitles:nil, nil];
        [alertv show];
        return;
    }
    
    if ([_themeTextFiled.text isEqualToString:@""]) {
        UIAlertView * alertv=[[UIAlertView alloc] initWithTitle:NSLocalized(@"vote_title_unBlank") message:nil delegate:self cancelButtonTitle:NSLocalized(@"vote_option_know") otherButtonTitles:nil, nil];
        [alertv show];
    }else{
        if (isAddImage) {
            self.whiteView.hidden=NO;
            [self sendTopicImageMessage];
        }else{
            [self sendCreateMess];
        }
    }
}
#pragma mark 发送创建信息
-(void)sendCreateMess{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary * options=[NSMutableDictionary dictionary];
    for (int i=0; i<self.listData.count; i++) {
        if (![_listData[i] isEqualToString:@""]) {
            [options setObject:_listData[i] forKey:[NSString stringWithFormat:@"%ld",options.count]];
        }
    }
    
    if (options.count<2) {
        UIAlertView * alertv=[[UIAlertView alloc] initWithTitle:NSLocalized(@"vote_option_last") message:NSLocalized(@"vote_option_lastTwo") delegate:self cancelButtonTitle:NSLocalized(@"vote_option_know") otherButtonTitles:nil, nil];
        [alertv show];
        return;
    }
    
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    parameter[@"groupId"]=self.groupId;
    parameter[@"title"]=[NSString stringWithFormat:@"%@%@",NSLocalized(@"group_chat_send_vote_sending"),self.themeTextFiled.text];
    parameter[@"single"]=[NSNumber numberWithBool:self.singleSwitch.on==YES?NO:YES];
    parameter[@"anonymous"]=[NSNumber numberWithBool:self.anonymousSwitch.on];
    parameter[@"options"]=options;
    if (isAddImage) {
        parameter[@"imageId"]=self.imageID;
    }
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    NSString * URLstr=addTopicsVote;
    [manager POST:URLstr parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"Dic is %@",responseObject);
        
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [ToolOfClass showMessage:NSLocalized(@"vote_send_success")];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

// 发送图片
- (void)sendTopicImageMessage
{
    [self.themeTextFiled resignFirstResponder];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    NSString *postStr = nil;
    NSData *imageData = UIImageJPEGRepresentation(self.image, 1);
    if (self.imageName==nil) {
        self.imageName=@"image";
    }
    postStr = [sendImage copy];
    [manager POST:postStr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image" fileName:self.imageName mimeType:@"image/jpg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {// 发送成功
            self.imageID=responseObject[@"data"][@"id"];
            //上传图片成功后 发送创建投票的接口数据
            [self sendCreateMess];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        self.navigationItem.rightBarButtonItem.enabled=YES;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"vote_send_failure")];
        self.navigationItem.rightBarButtonItem.enabled=YES;
    }];
    
    /*
     //        [manager POST:creatPostTopic parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
     //
     //            if ([responseObject[@"code"] isEqualToNumber:@0]) {// 发送成功
     //                [ToolOfClass showMessage:@"发送成功!"];
     //                [self.navigationController popViewControllerAnimated:YES];
     //            } else {
     //                [ToolOfClass showMessage:responseObject[@"message"]];
     //            }
     //            self.navigationItem.rightBarButtonItem.enabled=YES;
     //        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
     //            self.navigationItem.rightBarButtonItem.enabled=YES;
     //            [ToolOfClass showMessage:@"发送失败!"];
     //        }];
     //
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark textFieldDidBeginEditing

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSInteger size=0;
    int height=isAddImage==YES?173+150:173;
    size=ScreenHeight-height-44*(textField.tag-100+1);
    if (size<300) {
        if (isAddImage) {
            [self.tableview setContentOffset:CGPointMake(0, 300-size) animated:YES];
        }else{
            [self.tableview setContentOffset:CGPointMake(0, 300-size) animated:YES];
        }
        
    }
//    [self.tableview setContentOffset:CGPointMake(0, 70) animated:YES];
//    [self.themeTextFiled becomeFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)sender {
    [self.tableview setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.themeTextFiled becomeFirstResponder];
    [self.themeTextFiled resignFirstResponder];
}


-(void)textFieldChanged:(UITextField*)textField{
    
    NSInteger index=textField.tag-100;
    if (index<_listData.count) {
        [_listData replaceObjectAtIndex:index withObject:textField.text];
    }else{
        [self.tableview reloadData];
    }
//    NSLog(@"%@.tag=%ld",textField.text,textField.tag);
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"VoteTableViewCell";
    VoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
//    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"VoteTableViewCell" owner:self options:nil] lastObject];
//    }
    cell.numLabel.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
    cell.detailTextFiled.text=[self.listData objectAtIndex:indexPath.row];
    cell.detailTextFiled.placeholder=[NSString stringWithFormat:@"%@%ld",NSLocalized(@"vote_input_option"),indexPath.row+1];
    cell.detailTextFiled.tag=100+indexPath.row;
    cell.detailTextFiled.delegate=self;
    [cell.detailTextFiled addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

//返回YES，表示支持单元格的移动
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==self.listData.count-1) {
        return UITableViewCellEditingStyleInsert;
    }else{
        return UITableViewCellEditingStyleDelete;
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete&&self.listData.count>2) {
        //        获取选中删除行索引值
        NSInteger row = [indexPath row];
        //        通过获取的索引值删除数组中的值
        [self.listData removeObjectAtIndex:row];
        //        删除单元格的某一行时，在用动画效果实现删除过程
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [tableView reloadData];
        });
    }
    if(editingStyle==UITableViewCellEditingStyleInsert&&self.listData.count<10)
    {
        NSInteger row = [indexPath row];
        NSArray *insertIndexPath = [NSArray arrayWithObjects:indexPath, nil];
        NSString *mes = [NSString stringWithFormat:@""];
        //        添加单元行的设置的标题
        [self.listData insertObject:mes atIndex:row+1];
        [tableView insertRowsAtIndexPaths:insertIndexPath withRowAnimation:UITableViewRowAnimationRight];
        NSIndexPath *te=[NSIndexPath indexPathForRow:self.listData.count-1 inSection:0];//刷新第一个section的第n行
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationMiddle];
    }  
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    
    UITextField * textField=nil;
    for (UIView *tmpView in self.view.subviews )
    {
        if (tmpView.tag==100+sourceIndexPath.row) {
            textField=(UITextField*)tmpView;
        }
    }
    if (textField!=nil) {
        [self.listData replaceObjectAtIndex:textField.tag-100 withObject:textField.text];
    }
    
    
    [tableView reloadData];
    //    需要的移动行
    NSInteger fromRow = [sourceIndexPath row];
    //    获取移动某处的位置
    NSInteger toRow = [destinationIndexPath row];
    //    从数组中读取需要移动行的数据
    id object = [self.listData objectAtIndex:fromRow];
    //    在数组中移动需要移动的行的数据
    [self.listData removeObjectAtIndex:fromRow];
    //    把需要移动的单元格数据在数组中，移动到想要移动的数据前面
    [self.listData insertObject:object atIndex:toRow];
    
//    [self.listData replaceObjectAtIndex:toRow withObject:object];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  self.headerVeiw;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.footview;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (isAddImage) {
        return 272;
    }else{
        return 82;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    float hi=self.footview.frame.size.height;
//    NSLog(@"self.footview.frame.size.height is%f",hi);
    return 380;
}

#pragma mark   添加图片button事件
/**
 *  添加图片button事件
 *
 *  @param sender 添加图片button
 */
- (IBAction)addPictureButClick:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalized(@"HOME_alert__imgAlbum"),NSLocalized(@"HOME_alert__imgPZ"), nil];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];//self.view];
}

#pragma mark delegate for imagePickerConreoller
// 协议中的方法 ,当用户选中图片时被调用
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//    if (picker.sourceType==UIImagePickerControllerSourceTypePhotoLibrary) {
//        
//        NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
//        NSString *iimageName = [url lastPathComponent];
//        self.imageName = iimageName;
//    } else {
//        self.imageName = @"origin.jpg";
//    }
//    self.image = info[UIImagePickerControllerOriginalImage];
//    self.imageView.image=self.image;
//    isAddImage=YES;
//    
//    [self dismissViewControllerAnimated:NO completion:nil];
//    [self.tableview reloadData];
//}

#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    self.image = info[UIImagePickerControllerOriginalImage];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    /* 此处info 有六个值
     * UIImagePickerControllerMediaType; // an NSString UTTypeImage)
     * UIImagePickerControllerOriginalImage;  // a UIImage 原始图片
     * UIImagePickerControllerEditedImage;    // a UIImage 裁剪后图片
     * UIImagePickerControllerCropRect;       // an NSValue (CGRect)
     * UIImagePickerControllerMediaURL;       // an NSURL
     * UIImagePickerControllerReferenceURL    // an NSURL that references an asset in the AssetsLibrary framework
     * UIImagePickerControllerMediaMetadata    // an NSDictionary containing metadata from a captured photo
     */
    // 保存图片至本地，方法见下文
    [self saveImage:image withName:@"currentImage.png"];
    
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"currentImage.png"];
    
    UIImage *savedImage = [[UIImage alloc] initWithContentsOfFile:fullPath];
    
    [self.imageView setImage:savedImage];
    
    self.imageView.tag = 100;
    
    isAddImage=YES;
    [self.tableview reloadData];
}

#pragma mark - 保存图片至沙盒
- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName
{
    
    NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.5);
    // 获取沙盒目录
    
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
    // 将图片写入文件
    
    [imageData writeToFile:fullPath atomically:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==2){
        [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if (buttonIndex==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        picker.allowsEditing = YES;
    } else if (buttonIndex==1) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

@end
