//
//  VoteViewController.h
//  nc
//
//  Created by docy admin on 6/10/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoteViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *themeTextFiled;
@property (nonatomic, retain) NSNumber * groupId;

@property (nonatomic, retain) UIImage *image; // 用于截图或选择图片后传值
@property (nonatomic, copy) NSString *imageName; // 用于截图或选择图片后图片名字

@end
