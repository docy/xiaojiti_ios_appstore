//
//  MessageFouncationView.h
//  nc
//
//  Created by guanxf on 16/3/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageFouncationView : UIView

@property (weak, nonatomic) IBOutlet UILabel *Localized_file;
@property (weak, nonatomic) IBOutlet UILabel *Localized_map;
@property (weak, nonatomic) IBOutlet UILabel *Localized_vote;
@property (weak, nonatomic) IBOutlet UILabel *Localized_topic;
@property (weak, nonatomic) IBOutlet UILabel *Localized_image;

@property (weak, nonatomic) IBOutlet UILabel *Localized_photo;

@property (weak, nonatomic) IBOutlet UIButton *mapButton;

@end
