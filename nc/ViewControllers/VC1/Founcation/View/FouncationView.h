//
//  FouncationView.h
//  nc
//
//  Created by docy admin on 6/4/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FouncationView : UIView


@property (weak, nonatomic) IBOutlet UILabel *Localized_file;
@property (weak, nonatomic) IBOutlet UILabel *Localized_map;
@property (weak, nonatomic) IBOutlet UILabel *Localized_vote;
@property (weak, nonatomic) IBOutlet UILabel *Localized_topic;
@property (weak, nonatomic) IBOutlet UILabel *Localized_image;

@end
