//
//  MainThemeViewController.m
//  nc
//
//  Created by docy admin on 6/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "MainThemeViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "MessageViewController.h"
#import "MessageModel.h"
#import "UIAlertView+AlertView.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"
#import "NSString+XJTString.h"
#import "ShowPictureViewController.h"
#import "XJTTextView.h"

@interface MainThemeViewController () <UITextViewDelegate>
@property (nonatomic, retain) UIBarButtonItem *sendBarButton;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController; //系统文档控制器

@property (weak, nonatomic) IBOutlet UILabel *Localized_theme;
@property (weak, nonatomic) IBOutlet UILabel *Localized_desc;
@property (weak, nonatomic) IBOutlet UILabel *Localized_sending;

@property (nonatomic, weak) XJTTextView *textView;

@end

@implementation MainThemeViewController
@synthesize titleString,detailString;

- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置导航栏属性
    [self setUpMainThemeViewData];
    
    // // 20160315添加自定义textView
    [self setCustomTextView];
    
    _textView.text = self.titleString; // 20160315 修改界面添加
    [self textViewChange];
    
    
    /*  20160315 隐藏之前的界面以及操作
     
    self.themeTextView.text=self.titleString;
    self.descTextView.text=self.detailString;
    
    if (self.themeTextView.text.length!=0) {
        self.themeLabel.hidden = YES;
    }
    if (self.descTextView.text.length!=0) {
        self.descLabel.hidden = YES;
    }
     
    */
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];  //将分享到本地的文件移动位置
    NSString * Inboxto=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@",titleString]];
    if([fileManager fileExistsAtPath:_FileCopyPath]){
        BOOL boo=[fileManager copyItemAtPath:_FileCopyPath toPath:Inboxto error:nil];
        if (boo) {
            BOOL bo=[fileManager removeItemAtPath:_FileCopyPath error:nil];
            NSLog(@"bobobobo is %d",bo);
            _FileCopyPath=Inboxto;
        }
//        BOOL bo=[fileManager createDirectoryAtPath:Inboxto withIntermediateDirectories:YES attributes:nil error:nil];
    }
}


#pragma mark 20160315 添加自定义的textView
- (void)setCustomTextView{
    XJTTextView *textView = [[XJTTextView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 100)];
    _textView = textView;
    textView.placeholder = NSLocalized(@"Share_your_thoughts");//@"分享你此刻的想法";
    textView.delegate = self;
//    textView.backgroundColor = [UIColor redColor];
    [self.view addSubview:textView];
    [self.view addSubview:_whiteView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewChange) name:UITextViewTextDidChangeNotification object:nil];
}
#pragma mark 20160315 添加监测自定义的textView
- (void)textViewChange{

    if (_textView.text.length) {
        _textView.hidePlaceholder = YES;
    } else {
        _textView.hidePlaceholder = NO;
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setTitleString:(NSString*)titleStr{
    if (titleString==nil) {
        titleString=[[NSString alloc] init];
    }
    titleString=[titleStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

-(void)setFileCopyPath:(NSString*)titleStr{
    if (_FileCopyPath==nil) {
        _FileCopyPath=[[NSString alloc] init];
    }
    _FileCopyPath=[titleStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

// 设置导航栏属性
- (void)setUpMainThemeViewData
{
    
    self.Localized_theme.text = NSLocalized(@"topic_caption");
    self.Localized_desc.text = NSLocalized(@"topic_content");
    self.Localized_sending.text = NSLocalized(@"topic_sending");
    self.themeLabel.text = NSLocalized(@"topic_captionPH");
    self.descLabel.text = NSLocalized(@"topic_contentPH");
    
    
    self.navigationItem.title = NSLocalized(@"topic_title");
    UIBarButtonItem *item = [UIBarButtonItem itemWithTitle:NSLocalized(@"topic_title_right") target:self action:@selector(onSendThemeButtonClick:)];
    self.navigationItem.rightBarButtonItem = item;
    self.sendBarButton = item;
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onMainThemeViewBackBtnClick)];
    
    
    /* 20160315 隐藏之前的界面以及操作
    
    self.themeTextView.delegate = self;
    self.descTextView.delegate = self;
    
     */
     
    if (self.image) {
        
        /*
        CGRect rect = self.imageView.frame;
        CGFloat width = self.image.size.width;
        CGFloat height = self.image.size.height;
        
        if (width>height) {
            rect.size.height = 180*(height/width);
        } else {
            rect.size.width = 180*(width/height);
        }
        
        self.imageView.frame = rect;
         */
        
        self.imageView.image = self.image;
        self.imageView.hidden = NO;
//        UILongPressGestureRecognizer *longPressGesture =
//        [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
         UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        [self.imageView addGestureRecognizer:tap];
        self.imageView.userInteractionEnabled = YES;
    } else {
        self.imageView.hidden = YES;
        self.imageView.image = nil;
    }
    //设置等待view位置大小
    _whiteView.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    _ActivitView.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2-40);
    _whiteLable.center=CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    _whiteView.hidden=YES;
}

- (void)handleLongPress:(UITapGestureRecognizer *)longPressGesture
{
//    if (longPressGesture.state == UIGestureRecognizerStateBegan)
//    {
    
    if (!_isFileCreate) {
//        ShowPictureViewController *showPictureVC = [[ShowPictureViewController alloc] initWithNibName:@"ShowPictureViewController" bundle:[NSBundle mainBundle]];
//        showPictureVC.imageView =self.imageView ;
//        [self presentViewController:showPictureVC animated:YES completion:nil];
    }else{
        QLPreviewController *previewController = [[QLPreviewController alloc] init];
        previewController.dataSource = self;
        previewController.delegate = self;
        [[self navigationController] pushViewController:previewController animated:YES];
    }
        // start previewing the document at the current section index
//        previewController.currentPreviewItemIndex = indexPath.row;


//        NSIndexPath *cellIndexPath = [readTable indexPathForRowAtPoint:[longPressGesture locationInView:readTable]];
//        
//        NSURL *fileURL;
//        if (cellIndexPath.section == 0)
//        {
//            // for section 0, we preview the docs built into our app
//            fileURL = [self.dirArray objectAtIndex:cellIndexPath.row];
//        }
//        else
//        {
//            // for secton 1, we preview the docs found in the Documents folder
//            fileURL = [self.dirArray objectAtIndex:cellIndexPath.row];
//        }
//        self.docInteractionController.URL = fileURL;
//        
//        [self.docInteractionController presentOptionsMenuFromRect:longPressGesture.view.frame
//                                                           inView:longPressGesture.view
//                                                         animated:YES];
//    }
}

- (void)onMainThemeViewBackBtnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 发送主题按钮点击事件
- (void)onSendThemeButtonClick:(UIBarButtonItem *)sender
{
    /* 20160315 隐藏之前的界面以及操作
    
    NSString *title = [NSString stringThrowOffLinesWithString:self.themeTextView.text];
    
     */
    
    NSString *title = [NSString stringThrowOffLinesWithString:_textView.text];
     if (title.length!=0) { // 主题必须填写
        // 创建topic
        _whiteView.hidden=NO;
         
         /* 20160315 隐藏之前的界面以及操作
          
        [self sendTopicMessageWithTitle:title]; // 之前的发送主题包括所有的主题
          
        */
         
         [self sendTopicWithTitle:title sender:sender]; // 目前发送主题不包括普通的主题（只包括带附件的，如地图，图片，文件）
         
        sender.enabled = NO;
    } else {
        [ToolOfClass showMessage:NSLocalized(@"topic_caption_unBlank")];
    }
}

// 20160315添加 目前发送主题不包括普通的主题（只包括带附件的，如地图，图片，文件）
- (void)sendTopicWithTitle:(NSString *)title  sender:(UIBarButtonItem *)sender{
    sender.enabled = NO;
    [self.view endEditing:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"groupId"] = self.groupId;
    parameter[@"title"] = title;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    
    if (self.imageView.hidden==NO) { // 发表图片子话题
        NSString *postStr = nil;
        NSData *imageData = UIImageJPEGRepresentation(self.image, 1);
        if (self.imageName==nil) {
            self.imageName=@"image";
        }
        
        if (self.latitude!=0 && self.longitude!=0) {
            parameter[@"longitude"] = @(self.longitude);
            parameter[@"latitude"] = @(self.latitude);
            postStr = [creatMapTopic copy];
            
        } else {
            postStr = [creatImageTopic copy];
        }
        
        NSData * FileData=nil;
        if (_isFileCreate) {   //发表文件子话题
            postStr = [creatFileTopic copy];
            FileData=[[NSData alloc] initWithContentsOfFile:_FileCopyPath];
        }
        [manager POST:postStr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            if (_isFileCreate) {
                //                [ToolOfClass toolUploadFilePathWithHttpString:postStr fileName:titleString file:nil setAvatarOrLogoPath:nil];
                NSRange range=[_FileCopyPath rangeOfString:@"."];
                NSString * fileType=[_FileCopyPath substringFromIndex:range.location+1];
                [formData appendPartWithFileData:FileData name:@"file" fileName:titleString mimeType:[NSString stringWithFormat:@"application/%@",fileType ]];
            }else{
                [formData appendPartWithFileData:imageData name:@"image" fileName:self.imageName mimeType:@"image/jpg"];
            }
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            sender.enabled = YES;
            if ([responseObject[@"code"] isEqualToNumber:@200]) {// 发送成功
                [ToolOfClass showMessage:NSLocalized(@"vote_send_success")];
                
                self.sendBarButton.enabled = YES;
                
                if ([postStr isEqualToString:creatMapTopic]) {
                    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
                } else {
                    if (_isFileCreate) {
                        if (_isLoacFileOpen) {
                            [self.navigationController popViewControllerAnimated:YES];
                            if (self.backblock) {
                                self.backblock();
                            }
                        }else{
                            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                
                            }];
                        }
                    }else{
                        [self.navigationController popViewControllerAnimated:YES];
                        if (self.backblock) {
                            self.backblock();
                        }
                    }
                }
            } else {
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:NSLocalized(@"vote_send_failure")];
            sender.enabled = YES;
        }];
        
    }

}

// 创建topic 之前的发送主题包括所有的主题
- (void)sendTopicMessageWithTitle:(NSString *)title
{
    self.sendBarButton.enabled = NO;
    [self.themeTextView resignFirstResponder];
    [self.descTextView resignFirstResponder];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"groupId"] = self.groupId;
    parameter[@"title"] = title;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];
    if (self.descTextView.text.length!=0) {
        parameter[@"desc"] = self.descTextView.text;
    }
    
    self.navigationItem.rightBarButtonItem.enabled=NO;
    
    if (self.imageView.hidden==NO) { // 发表图片子话题
        NSString *postStr = nil;
        NSData *imageData = UIImageJPEGRepresentation(self.image, 1);
        if (self.imageName==nil) {
            self.imageName=@"image";
        }
        
        if (self.latitude!=0 && self.longitude!=0) {
            parameter[@"longitude"] = @(self.longitude);
            parameter[@"latitude"] = @(self.latitude);
            postStr = [creatMapTopic copy];
        
        } else {
            postStr = [creatImageTopic copy];
        }
        
        NSData * FileData=nil;
        if (_isFileCreate) {   //发表文件子话题
            postStr = [creatFileTopic copy];
            FileData=[[NSData alloc] initWithContentsOfFile:_FileCopyPath];
        }
        [manager POST:postStr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            if (_isFileCreate) {
//                [ToolOfClass toolUploadFilePathWithHttpString:postStr fileName:titleString file:nil setAvatarOrLogoPath:nil];
                NSRange range=[_FileCopyPath rangeOfString:@"."];
                NSString * fileType=[_FileCopyPath substringFromIndex:range.location+1];
                [formData appendPartWithFileData:FileData name:@"file" fileName:titleString mimeType:[NSString stringWithFormat:@"application/%@",fileType ]];
            }else{
                [formData appendPartWithFileData:imageData name:@"image" fileName:self.imageName mimeType:@"image/jpg"];
            }
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {// 发送成功
                [ToolOfClass showMessage:NSLocalized(@"vote_send_success")];
                
                self.sendBarButton.enabled = YES;
                
                if ([postStr isEqualToString:creatMapTopic]) {
                    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
                } else {
                    if (_isFileCreate) {
                        if (_isLoacFileOpen) {
                            [self.navigationController popViewControllerAnimated:YES];
                            if (self.backblock) {
                                self.backblock();
                            }
                        }else{
                            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                
                            }];
                        }
                    }else{
                        [self.navigationController popViewControllerAnimated:YES];
                        if (self.backblock) {
                            self.backblock();
                        }
                    }
                }
                
                
//                if (self.backblock) {
//                    self.backblock();
//                }
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
            self.navigationItem.rightBarButtonItem.enabled=YES;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [ToolOfClass showMessage:NSLocalized(@"vote_send_failure")];
            self.navigationItem.rightBarButtonItem.enabled=YES;
        }];
        
    } else {

        [manager POST:creatPostTopic parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@200]) {// 发送成功
                [ToolOfClass showMessage:NSLocalized(@"vote_send_success")];
                
                self.sendBarButton.enabled = YES;
                [self.navigationController popViewControllerAnimated:YES];
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
            self.navigationItem.rightBarButtonItem.enabled=YES;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.navigationItem.rightBarButtonItem.enabled=YES;
            [ToolOfClass showMessage:NSLocalized(@"vote_send_failure")];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView.tag==9) {
        if (textView.text.length!=0) {
            self.themeLabel.hidden = YES;
        } else {
            self.themeLabel.hidden = NO;
        }
    } else {
        if (textView.text.length!=0) {
            self.descLabel.hidden = YES;
        } else {
            self.descLabel.hidden = NO;
        }
    }
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
    return self;
}


#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    //    NSInteger numToPreview = 0;
    //
    //	numToPreview = [self.dirArray count];
    //
    //    return numToPreview;
    return 1;//[self.dirArray count];
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
    [previewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"click.png"] forBarMetrics:UIBarMetricsDefaultPrompt];
    
    NSURL *fileURL = nil;
    //    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *path = [[NSString stringWithFormat:@"%@",documentDir] stringByAppendingPathComponent:titleString];
    fileURL = [NSURL fileURLWithPath:path];
    return fileURL;
}


@end
