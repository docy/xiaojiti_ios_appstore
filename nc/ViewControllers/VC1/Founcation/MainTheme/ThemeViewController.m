//
//  ThemeViewController.m
//  nc
//
//  Created by docy admin on 16/3/14.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "ThemeViewController.h"
#import "XJTTextView.h"
#import "UIImage+UIImage.h"
#import "ToolOfClass.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

#import <AFNetworking/AFNetworking.h>


@interface ThemeViewController () <UITextViewDelegate>

@property (nonatomic, weak) XJTTextView *textView;

@end

@implementation ThemeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setAttribute];
}

- (void)setAttribute{
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"主题";
    UIBarButtonItem *item = [UIBarButtonItem itemWithTitle:NSLocalized(@"topic_title_right") target:self action:@selector(onSendButtonClick:)];
    self.navigationItem.rightBarButtonItem = item;
    
    XJTTextView *textView = [[XJTTextView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    _textView = textView;
    textView.placeholder = @"分享你此刻的想法";
    textView.delegate = self;
    [self.view addSubview:textView];
    
    //    textView.textContainerInset = UIEdgeInsetsMake(50, 15, 0, 0);
    
    //    textView.backgroundColor = [UIColor colorWithPatternImage:[UIImage stretchableImageWithName:@"Focus on"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChange) name:UITextViewTextDidChangeNotification object:nil];
}

- (void)onSendButtonClick:(UIBarButtonItem *)sender{
    if (_textView.text.length!=0) { // 主题必须填写
        // 创建topic
        [self sendThemeWithTitle:_textView.text sender:sender];

    } else {
        [ToolOfClass showMessage:NSLocalized(@"topic_caption_unBlank")];
    }
}
- (void)sendThemeWithTitle:(NSString *)title sender:(UIBarButtonItem *)sender{
    sender.enabled = NO;
    [self.view endEditing:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    parameter[@"groupId"] = self.groupId;
    parameter[@"title"] = title;
    parameter[@"clientId"] = [ToolOfClass toolSetUpClientIdWithGroupId:self.groupId];

    
    [manager POST:creatPostTopic parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        sender.enabled = YES;
        if ([responseObject[@"code"] isEqualToNumber:@200]) {// 发送成功
            [ToolOfClass showMessage:NSLocalized(@"vote_send_success")];
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        sender.enabled = YES;
        [ToolOfClass showMessage:NSLocalized(@"vote_send_failure")];
    }];

}


- (void)textChange{

    if (_textView.text.length) {
        _textView.hidePlaceholder = YES;
    } else {
        _textView.hidePlaceholder = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_textView becomeFirstResponder];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
