//
//  MainThemeViewController.h
//  nc
//
//  Created by docy admin on 6/8/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>



typedef void(^thmemBlock)(void);


typedef void (^backBlock)(void);

@interface MainThemeViewController : UIViewController<QLPreviewControllerDataSource,QLPreviewControllerDelegate,UIDocumentInteractionControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextView *themeTextView;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *themeLabel;

@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property (weak, nonatomic) IBOutlet UILabel *whiteLable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivitView;

@property (nonatomic, retain) UIImage *image; // 用于截图或选择图片后传值
@property (nonatomic, copy) NSString *imageName; // 用于截图或选择图片后图片名字
@property (nonatomic, strong) NSString * FileCopyPath; // 用于创建文件子话题的文件路径

@property (nonatomic, copy) NSString *titleString; //
@property (nonatomic, copy) NSString *detailString; //

@property (nonatomic, strong) thmemBlock block;
@property (nonatomic, strong) NSNumber *groupId; // 群组ID

@property (nonatomic, strong) backBlock backblock;

@property (nonatomic, assign) CGFloat latitude; // 纬度
@property (nonatomic, assign) CGFloat longitude; // 经度

@property (nonatomic, assign) BOOL isFileCreate; // 是文件子话题的创建
@property (nonatomic, assign) BOOL isLoacFileOpen;  //本地创建子话题

@end
