//
//  CreatGroupViewController.h
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 添加群组视图控制器
@interface CreatGroupViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *groupNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *groupDescribeTextField;

@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;


@property (weak, nonatomic) IBOutlet UILabel *nameCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *descCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *groupIconImageView;
- (IBAction)selectGroupIcon:(id)sender;



//@property (weak, nonatomic) IBOutlet UISwitch *canJoin;
//
//@property (weak, nonatomic) IBOutlet UISwitch *notifiMode;

@property (weak, nonatomic) IBOutlet UISwitch *privateGroup;




@end
