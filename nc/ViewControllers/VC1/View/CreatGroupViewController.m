//
//  CreatGroupViewController.m
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CreatGroupViewController.h"
#import "AddressListViewController.h"

#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import "NSString+XJTString.h"
#import "APIHeader.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

@interface CreatGroupViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>

@property (nonatomic, retain) UIBarButtonItem *CreatBtnItem;
@property (nonatomic ,copy) NSString *groupLogoName; // 群组头像名
@property (nonatomic ,copy) NSString *logoPath; // logo


@property (weak, nonatomic) IBOutlet UILabel *Localized_avator;
@property (weak, nonatomic) IBOutlet UILabel *Localized_isPrivate;


@end

@implementation CreatGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置属性
    [self setUpCreatGroupViewAttribute];
    [self getUpCreatGroupViewRandomLogo];
}
// 设置基本属性
- (void)setUpCreatGroupViewAttribute
{
    
    self.groupNameTextField.placeholder = NSLocalized(@"group_creat_name_placeH");
    self.descLabel.text = NSLocalized(@"group_creat_des_placeH");
    self.Localized_avator.text = NSLocalized(@"group_creat_imageCover");
    
    self.Localized_isPrivate.text = NSLocalized(@"group_creat_isPrivate");
    
    
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.title = NSLocalized(@"group_creat_title");
    UIBarButtonItem *item = [UIBarButtonItem itemWithTitle:NSLocalized(@"group_creat_title_rightTitle") target:self action:@selector(onCreatGroupViewCreatButtonClick)];
    self.navigationItem.rightBarButtonItem = item;
    self.CreatBtnItem = item;
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onCreatGroupViewNavBackClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onCreatGroupViewNavBackClick)];
    
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
//    [self.groupDescribeTextField addTarget:self action:@selector(CreatGroupViewGroupDescribeTextFieldChanged) forControlEvents:UIControlEventEditingChanged];
    
//    self.groupDescribeTextField.delegate = self;
    
//    [self.descTextView addTarget:self action:@selector(CreatGroupViewGroupDescribeTextFieldChanged) forControlEvents:UIControlEventEditingChanged];
    self.descTextView.delegate = self;
    self.groupNameTextField.delegate = self;
    [self.groupNameTextField addTarget:self action:@selector(CreatGroupViewGroupNameTextFieldChanged) forControlEvents:UIControlEventEditingChanged];
}
- (void)getUpCreatGroupViewRandomLogo
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:getRandomLogoPath,@"group"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] intValue] == 200) {
            self.logoPath = responseObject[@"data"][@"logo"];
            [self.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:responseObject[@"data"][@"logo"]]] placeholderImage:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
// 返回按钮
- (void)onCreatGroupViewNavBackClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// 创建完成按钮的点击事件
- (void)onCreatGroupViewCreatButtonClick
{
    self.CreatBtnItem.enabled = NO;
    [self.groupNameTextField resignFirstResponder];
//    [self.groupDescribeTextField resignFirstResponder];
//    NSString *groupName = [NSString stringThrowOffBlankWithString:self.groupNameTextField.text];
    int countName = [ToolOfClass convertToInt:self.groupNameTextField.text];
    int countDesc = [ToolOfClass convertToInt:self.descTextView.text];
    
    if (countName < 2) {
        [ToolOfClass showMessage:NSLocalized(@"group_creat_name_small")];
        
        self.CreatBtnItem.enabled = YES;
    } else if (countName > 10) {
        
        [ToolOfClass showMessage:NSLocalized(@"group_creat_name_max")];
        
        self.CreatBtnItem.enabled = YES;
    } else if (countDesc > 30) {
        [ToolOfClass showMessage:NSLocalized(@"group_creat_des_max")];
        self.CreatBtnItem.enabled = YES;
    } else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [info objectForKey:@"authToken"];
        parameter[@"name"] = self.groupNameTextField.text;
        parameter[@"category"] = @(self.privateGroup.on?4:1);
//        parameter[@"broadcast"] = @(self.notifiMode.on);
//        parameter[@"joinType"] = @(self.canJoin.on);
        
        if (countDesc>0) {
            parameter[@"desc"] = self.descTextView.text;
        }
        if (self.groupLogoName==nil) { // 上传默认图片
            self.groupLogoName = @"groupAvatar.png";
            parameter[@"logo"] = self.logoPath;
        }
        NSData *imageDate = nil;
        imageDate = UIImageJPEGRepresentation(self.groupIconImageView.image, ImageYaSuo);
        
        [manager POST:creatGroupPath parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {

                [formData appendPartWithFileData:imageDate name:@"logo" fileName:self.groupLogoName mimeType:@"image/png"];

        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                
                self.CreatBtnItem.enabled = YES;
                // 发通知创建群组
                [[NSNotificationCenter defaultCenter] postNotificationName:@"creatGroup" object:nil];
                
                [self.navigationController popToRootViewControllerAnimated:YES];

            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                self.CreatBtnItem.enabled = YES;
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.CreatBtnItem.enabled = YES;
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 选择群组头像
- (IBAction)selectGroupIcon:(id)sender {
    UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
    pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerVC.allowsEditing = YES;
    pickerVC.delegate = self;
    [self presentViewController:pickerVC animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.groupIconImageView.image = info[UIImagePickerControllerEditedImage];
    NSURL *url = info[@"UIImagePickerControllerReferenceURL"];
    NSString *fileName = [url lastPathComponent];
    self.groupLogoName = [fileName copy];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 群组名称检测
- (void)CreatGroupViewGroupNameTextFieldChanged
{
    
    int count = [ToolOfClass convertToInt:self.groupNameTextField.text];
    
//    NSString *nameStr = [NSString stringThrowOffBlankWithString:self.groupNameTextField.text];
    if (count!=0) {
        
        self.nameCountLabel.hidden = NO;
        NSString *countStr = [NSString stringWithFormat:@"%d/10",count];
        if (count>10) {
            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:countStr];
            NSRange range = [countStr rangeOfString:[NSString stringWithFormat:@"%d",count]];
            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
            self.nameCountLabel.attributedText = attributeStr;
        } else {
            self.nameCountLabel.text = [NSString stringWithFormat:@"%d/10",count];
        }
        
    } else {
        self.nameCountLabel.text = @"0/10";
        self.nameCountLabel.hidden = YES;
    }

    
//    NSString *nameStr = [NSString stringThrowOffBlankWithString:self.groupNameTextField.text];
//    if (nameStr.length!=0) {
//        
//        self.nameCountLabel.hidden = NO;
//        NSString *countStr = [NSString stringWithFormat:@"%lu/10",nameStr.length];
//        if (nameStr.length>10) {
//            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:countStr];
//            NSRange range = [countStr rangeOfString:[NSString stringWithFormat:@"%lu",nameStr.length]];
//            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
//            self.nameCountLabel.attributedText = attributeStr;
//        } else {
//            self.nameCountLabel.text = [NSString stringWithFormat:@"%lu/10",nameStr.length];
//        }
//        
//    } else {
//        self.nameCountLabel.text = @"0/10";
//        self.nameCountLabel.hidden = YES;
//    }
    
}

// 群组描述检测
//- (void)CreatGroupViewGroupDescribeTextFieldChanged
//{
//    NSString *descStr = self.groupDescribeTextField.text;
//    if (descStr.length!=0) {
//        NSString *countStr = [NSString stringWithFormat:@"%lu/30",descStr.length];
//        if (descStr.length>30) {
//            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:countStr];
//            NSRange range = [countStr rangeOfString:[NSString stringWithFormat:@"%lu",descStr.length]];
//            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
//            self.descCountLabel.attributedText = attributeStr;
//        } else {
//            self.descCountLabel.text = [NSString stringWithFormat:@"%lu/30",descStr.length];
//        }
//    } else {
//        self.descCountLabel.text = @"0/30";
//    }
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    
    int count = [ToolOfClass convertToInt:textView.text];
    
    if (count!=0) {
        self.descLabel.hidden = YES;
        self.descCountLabel.hidden = NO;
    } else {
        self.descLabel.hidden = NO;
        self.descCountLabel.hidden = YES;
    }
//    NSString *descStr = textView.text;
    if (count!=0) {
        NSString *countStr = [NSString stringWithFormat:@"%d/30",count];
        if (count>30) {
            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:countStr];
            NSRange range = [countStr rangeOfString:[NSString stringWithFormat:@"%d",count]];
            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
            self.descCountLabel.attributedText = attributeStr;
        } else {
            self.descCountLabel.text = [NSString stringWithFormat:@"%d/30",count];
        }
    } else {
        self.descCountLabel.text = @"0/30";
    }
    
    
    
    
//    if (textView.text.length!=0) {
//        self.descLabel.hidden = YES;
//        self.descCountLabel.hidden = NO;
//    } else {
//        self.descLabel.hidden = NO;
//        self.descCountLabel.hidden = YES;
//    }
//    NSString *descStr = textView.text;
//    if (descStr.length!=0) {
//        NSString *countStr = [NSString stringWithFormat:@"%lu/30",descStr.length];
//        if (descStr.length>30) {
//            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:countStr];
//            NSRange range = [countStr rangeOfString:[NSString stringWithFormat:@"%lu",descStr.length]];
//            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
//            self.descCountLabel.attributedText = attributeStr;
//        } else {
//            self.descCountLabel.text = [NSString stringWithFormat:@"%lu/30",descStr.length];
//        }
//    } else {
//        self.descCountLabel.text = @"0/30";
//    }
    
}


@end
