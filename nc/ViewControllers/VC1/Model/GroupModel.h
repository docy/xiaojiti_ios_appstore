//
//  GroupModel.h
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "CommonModel.h"

// 群组model

@class UIImage;
@interface GroupModel : CommonModel

@property (nonatomic, retain) NSNumber *id; // 群组ID
@property (nonatomic, copy) NSString *name; // 群组名称
@property (nonatomic, copy) NSString *desc; // 群组描述
@property (nonatomic, copy) NSString *logo; // 群组logo
@property (nonatomic, copy) NSString *updatedAt; // 更新时间
@property (nonatomic, copy) NSString *joinType; // 成员加入类型
@property (nonatomic, retain) NSArray *messages; // 消息数组
@property (nonatomic, retain) NSNumber *unreadCount; // 未读消息个数
@property (nonatomic, retain) NSNumber *userCount; // 群组成员数

@property (nonatomic, retain) NSNumber *creator; // 群组的创建者
@property (nonatomic, retain) NSNumber *category; // 群组类别
@property (nonatomic, retain) NSNumber *broadcast; // 广播

@property (nonatomic, retain) NSNumber *topicCount; // 子话题个数
@property (nonatomic, retain) NSNumber *activity; // 活跃度

@end
