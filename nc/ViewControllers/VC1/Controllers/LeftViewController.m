//
//  LeftViewController.m
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "LeftViewController.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"

#import "AFNetWorkNotReachableView.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "MessageViewController.h"
#import "CreatGroupViewController.h"
#import "addGroupViewController.h"
#import "DirectChatViewController.h"
#import "GroupDetailViewController.h"

#import "OpenNetWorkViewController.h"
#import "GroupModel.h"
#import "GroupTableViewCell.h"
#import "ToolOfClass.h"
#import <AFNetworking/AFNetworking.h>
#import "MJRefresh.h"
#import "UIImageView+WebCache.h"
#import "UISegmentedControl+XJTSegmentedControl.h"
#import "GroupDisbandGroupViewController.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "UIAlertView+AlertView.h"
#import "CanJoinTableViewCell.h"
#import "UIColor+Hex.h"

#define SearchBarHeight 40

@interface LeftViewController ()<UINavigationBarDelegate,UIAlertViewDelegate,MessageViewControllerDelegate,UISearchControllerDelegate,UISearchResultsUpdating,UITabBarControllerDelegate>{
    UILabel * weiduNum;         //segm 群聊未读数提示
    UILabel * weiduNum2;        //segm 私聊未读数提示
    int PrvGrpUnreadCount;      //私聊未读数记录
    int GrpUnreadCount;         //群聊未读数记录
    NSNumber *_joinGroupId;     //加入组id
    UISegmentedControl *segContro;
    BOOL seachBarIsShow;
}

@property(nonatomic, retain) UIImageView * bgImage;
@property (nonatomic, retain) NSTimer *timer;

@property (nonatomic, retain) NSNumber * willInGroupId;
@property (nonatomic, assign) BOOL  isPrivateGroup;
@property (nonatomic, retain) UIBarButtonItem *leftBarItem;
@property (nonatomic, retain) UIBarButtonItem *rightBarItem;
@property (nonatomic, retain) UIBarButtonItem *creatItem;
@property (nonatomic, retain) NSNumber *deleGroupId;
//@property (nonatomic, retain) NSNumber *PrivId; // 11.11

@property (nonatomic, retain) NSMutableArray *groupSearchDaList; // 搜索数组未加入
@property (nonatomic, retain) NSMutableArray *groupSearchDList1; // 搜索数组已加入
@property (nonatomic, strong) UISearchController *searchController; // 实现搜索

@end

@implementation LeftViewController

#pragma mark MessageViewControllerDelegate

- (void)messageViewSetNewGroupName:(NSString *)newName groupId:(NSNumber *)groupId{
    for (int i = 0; i < _groupData.count; i++) {
        GroupModel *model = _groupData[i];
        if ([model.id isEqualToNumber:groupId]) {
            model.name = newName;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
    }
}

-(NSMutableArray*)groupDaList{
    if (_groupDaList==nil) {
        _groupDaList=[[NSMutableArray alloc] init];
    }
    return _groupDaList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.groupData = [NSMutableArray array];
    self.PrivateGroupData = [NSMutableArray array];
    self.groupDaList=[NSMutableArray array];
    
    if ([DEFAULTS boolForKey:@"firstLaunch"]) {
        [UIImageView imageViewWithBgImage:isEnglish?@"Newbie guide_2English version":@"Newbie guide_2"];
    }
    
    self.groupSearchDaList = [NSMutableArray array];
    self.groupSearchDList1 = [NSMutableArray array];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.delegate=self;
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
//    self.searchController.searchBar.backgroundColor=[UIColor blueColor];
    self.searchController.searchBar.layer.borderColor=[[UIColor colorWithHex:0xEEEEEE] CGColor];
    self.searchController.searchBar.layer.borderWidth=0.5;
    self.searchController.searchBar.barTintColor=[UIColor colorWithHex:0xEEEEEE];
    [self.searchController.searchBar sizeToFit];

//    self.navigationItem.titleView = self.searchController.searchBar;
    
    // 获取可添加群组列表
    [self getCanJionGroupList];
    [self setUpLeftViewControllerData];
    [self getUpLeftViewUserGroupList];
    [self setUpLeftViewMJRefresh];
    
    self.tabBarController.delegate=self;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.searchController.active) {
        self.searchController.active = NO;
        [self.searchController.searchBar removeFromSuperview];
    }
}

// 添加群组
- (void)addGroupViewJoinGroup
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *path = [NSString stringWithFormat:joinGroupPath,_joinGroupId];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    
    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [ToolOfClass showMessage:NSLocalized(@"group_search_jion_success")];
            [self getCanJionGroupList];
            [self getUpLeftViewUserGroupList];
            // 消除搜索控制器
            
            if (self.searchController.active) {
                self.searchController.active = NO;
                [self.searchController.searchBar removeFromSuperview];
                [self onLeftViewSearchButtonClick];
            }
            
//            [self.searchController dismissViewControllerAnimated:YES completion:nil];
//            [self.searchController.view removeFromSuperview];

        } else {
            //            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
// 获取可添加群组列表
- (void)getCanJionGroupList
{
    [self.groupDaList removeAllObjects];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    [manager GET:canJionGroupPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
            for (NSDictionary *dict in responseObject[@"data"]) {
                GroupModel *model = [[GroupModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.groupDaList addObject:model];
            }
//            [_refreshView stopAnimation];
            
            if (self.groupDaList.count==0) {
                //                self.tableView.backgroundView = [UIImageView imageViewWithImage:@"Create a group"];
//                self.tableView.backgroundView = isEnglish?_nullBgView_en:_nullBgView;//[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Create a group"]];
            } else {
                self.tableView.backgroundView = nil;
            }
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
    }];
}


// 返回按钮的点击事件
- (void)ViewBackButtonClick{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
}

#pragma mark 设置属性
- (void)setUpLeftViewControllerData{

    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
    
    self.deleGroupId = @(-1);
//    self.PrivId = @(-1);
    self.willInGroupId = @(-1);
    self.leftBarItem = [UIBarButtonItem itemWithIcon:@"home" target:self action:@selector(ViewBackButtonClick)];
    self.navigationItem.leftBarButtonItem = self.leftBarItem;

    self.rightBarItem = [UIBarButtonItem itemWithIcon:@"navigation_search" target:self action:@selector(onLeftViewSearchButtonClick)];
    
    self.creatItem = [UIBarButtonItem itemWithIcon:@"navigation_add" target:self action:@selector(onCreatGroupButtonClick)];
    
    self.navigationItem.rightBarButtonItems = @[self.rightBarItem,self.creatItem];
    
    
    
    
//    self.navigationItem.leftBarButtonItem =[UIBarButtonItem itemWithTarget:self action:@selector(ViewBackButtonClick)];
//    [UIBarButtonItem itemWithTarget:self action:@selector(onLeftViewAddGroupButtonClick)];
    [self.navigationItem setHidesBackButton:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewRefreshAgainInBadNetWork) name:@"netWorkIsUnAvailable" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewInGoodNetWork) name:@"netWorkIsAvailable" object:nil];
    // 为添加群组注册观察者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewCreatGroup) name:@"creatGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewLogoutGroup:) name:@"logoutGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLeftViewNewMessage:) name:@"newMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(huidaoQianTai) name:@"huidaoQianTai" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewLogoutGroup:) name:disbandGroup object:nil]; // 解散群组
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LeftViewConveyGroupSuccess:) name:conveyGroupOwer object:nil]; // 转让群主成功
    
    //从通知进入时调用
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StartingFromTheNotification:) name:@"StartingFromTheNotification" object:nil];
    // 切换公司通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpLeftViewUserGroupList) name:@"SwitchCurrentCompany" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCanJionGroupList) name:@"SwitchCurrentCompany" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeUnReadConutPriGroup:) name:@"removeUnReadConutPriGroup" object:nil];
    self.isPrivateGroup = NO;
    segContro = [UISegmentedControl segmentedControlWithItems:@[NSLocalized(@"group_list_title_group"),NSLocalized(@"group_list_title_priChat")] target:self action:@selector(onSegClick:)];
    
    UIView * viewSeg=[[UIView alloc] initWithFrame:segContro.frame];
    [viewSeg addSubview:segContro];
    CGRect frame=segContro.frame;
    
    weiduNum=[[UILabel alloc] initWithFrame:CGRectMake(frame.size.width/3, 4, 6, 6)];
    weiduNum.backgroundColor=[UIColor redColor];
    weiduNum.layer.cornerRadius=3;
    weiduNum.clipsToBounds=YES;
    [viewSeg addSubview:weiduNum];
    
    weiduNum2=[[UILabel alloc] initWithFrame:CGRectMake(frame.size.width/2+frame.size.width/3, 4, 6, 6)];
    weiduNum2.backgroundColor=[UIColor redColor];
    weiduNum2.layer.cornerRadius=3;
    weiduNum2.clipsToBounds=YES;
    [viewSeg addSubview:weiduNum2];
    
    weiduNum.hidden=YES;
    weiduNum2.hidden=YES;
    
    PrvGrpUnreadCount=0;
    GrpUnreadCount=0;
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//    self.navigationItem.titleView = viewSeg;
    self.navigationItem.title = NSLocalized(@"group_list_title_group");
    
}

- (id)bgImage{
    if (_bgImage == nil) {
        
        _bgImage = [[[NSBundle mainBundle] loadNibNamed:isEnglish?@"DirectMessagesNullView_en":@"DirectMessagesNullView" owner:self options:nil] lastObject]; //[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Direct messages"]];
        _bgImage.contentMode=UIViewContentModeScaleAspectFill;
    }
    
    return _bgImage;
}

- (void)onSegClick:(UISegmentedControl *)seg{
    if (seg.selectedSegmentIndex==0) {
        self.isPrivateGroup = NO;
        self.navigationItem.rightBarButtonItems = @[self.rightBarItem,self.creatItem];
        
//        if (self.groupData.count == 0) {
//            self.tableView.backgroundView = self.bgImage;
//        } else {
//            self.tableView.backgroundView = nil;
//            self.bgImage = nil;
//        }
        self.tableView.backgroundView = nil;
        
        
    } else {
        self.navigationItem.rightBarButtonItems = nil;
        self.isPrivateGroup = YES;
        if (self.tableView.tableHeaderView!=nil) {
            self.tableView.tableHeaderView=nil;
        }
        if (self.PrivateGroupData.count == 0) {
            self.tableView.backgroundView = self.bgImage;
        } else {
            self.tableView.backgroundView = nil;
            self.bgImage = nil;
        }
        
    }
    [self.tableView reloadData];
    
}

//回到前台调用
-(void)huidaoQianTai{
    [self getCanJionGroupList];
    [self getUpLeftViewUserGroupList];
}

/**
 *  通过通知进入app 加载指定群组详情
 *
 *  @param notification 通知详情
 */

-(void)StartingFromTheNotification:(NSNotification*)notification{
    
//    NSDictionary * dic= [notification userInfo];
//    NSLog(@"%s---%@",__func__,dic);
    // 消除搜索控制器
    [self.searchController dismissViewControllerAnimated:YES completion:nil];
    [self.searchController.view removeFromSuperview];
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    // 之前收到消息自动跳转到相应的小组中，后废弃  1124日 处理 先为添加文件子话题相关功能服务
//    double delayInSecondsd = 0.3;
//    dispatch_time_t popTimee = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSecondsd * NSEC_PER_SEC));
//    dispatch_after(popTimee, dispatch_get_main_queue(), ^(void){
//    
//        int indexrow=-1;
//        for (int i=0; i<_groupData.count+_PrivateGroupData.count; i++) {
//            GroupModel *mode=nil;
//            int k=i;
//            if (i<_groupData.count) {
//                mode=[self.groupData objectAtIndex:k];
//            }else{
//                k=i-(int)_groupData.count;
//                mode=[self.PrivateGroupData objectAtIndex:k];
//            }
//            
//            if ([mode.id isEqualToNumber:dic[@"groupId"]]) {
//                indexrow=0;
//                if ([mode.category isEqualToNumber:@3]) {
//                    segContro.selectedSegmentIndex=1;
//                }else{
//                    segContro.selectedSegmentIndex=0;
//                }
//                [self onSegClick:segContro];
//                break;
//            }
//        }
//        if (indexrow!=-1) {
//            
//        }
//    });
//    
//    double delayInSeconds = 0.6;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
////        int indexrow=-1;
////        for (int i=0; i<_groupData.count; i++) {
////            GroupModel *model = self.groupData[i];
////            if ([model.id isEqualToNumber:dic[@"groupId"]]) {
////                indexrow=0;
////            }
////        }
////        if (indexrow!=-1) {
//            NSIndexPath * indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//            [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
////        }
//    });
}

- (void)setUpLeftViewMJRefresh{
    __block LeftViewController *wearSelf = self;
    [self.tableView addHeaderWithCallback:^{
        [wearSelf getUpLeftViewUserGroupList];
    }];
}

#pragma mark 通知

- (void)LeftViewConveyGroupSuccess:(NSNotification *)not{
    for (GroupModel *model in self.groupData) {
        
        if ([self.willInGroupId isEqualToNumber:model.id]) {
            
            model.creator = (NSNumber *)not.object;
            break;
        }
        [UIView animateWithDuration:1.0 animations:^{
            
        }];
    }
}

- (void)LeftViewRefreshAgainInBadNetWork{
//    NSLog(@"%s",object_getClassName(self));
    NetWorkBadView *badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
    badView.contentLabel.text = NSLocalized(@"NetWorkBadView_des");
    [badView.OpenNetWorkBtn addTarget:self action:@selector(onOpenNetWorkBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableHeaderView = badView;
}

- (void)LeftViewInGoodNetWork{
    if (self.tableView.tableHeaderView!=nil) {
        self.tableView.tableHeaderView = self.searchController.searchBar;
    }
    if (self.groupData.count==0) {
        [self getUpLeftViewUserGroupList];
    }
}

- (void)onOpenNetWorkBtnClick{
    OpenNetWorkViewController *openVC = [[OpenNetWorkViewController alloc] init];
    openVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:openVC animated:YES];
}

- (void)LeftViewCreatGroup{
    [self getUpLeftViewUserGroupList];
    [self getCanJionGroupList];
}

- (void)LeftViewLogoutGroup:(NSNotification *)notification{
//    [self getUpLeftViewUserGroupList];
    for (NSInteger i=0; i<self.groupData.count; i++) {
        GroupModel *model = self.groupData[i];
        if ([model.id isEqualToNumber:notification.object]) {
            [self.groupData removeObjectAtIndex:i];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (void)getLeftViewNewMessage:(NSNotification *)notification{
    NSArray *array = notification.object;
    NSDictionary * dict=array[0];
    
    if ([dict[@"type"] isEqualToNumber:@5]) { // 踢人message
        if ([dict[@"userId"] isEqualToNumber:USER_ID]) {
            for (int i=0; i<_groupData.count; i++) {
                GroupModel *model = self.groupData[i];
                if ([model.id isEqualToNumber:dict[@"groupId"]]) {
                    
                    if (model.unreadCount.intValue>0) {
                        GrpUnreadCount-=model.unreadCount.intValue;
                    }
                    if (GrpUnreadCount>0) {
                        weiduNum.hidden=NO;
                    }else{
                        weiduNum.hidden=YES;
                    }
                    [self.groupData removeObject:model];
                    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:i inSection:0];
                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    if ([_willInGroupId isEqualToNumber:dict[@"groupId"]]) {
//                        UIAlertView *alert = [UIAlertView alertViewWithTitle:@"你已经被移出该小组" target:self];
                        UIAlertView *alert = [UIAlertView alertViewWithTitle:NSLocalized(@"group_list_logout_0") target:self];
                        alert.tag = 10;
                    } else {
//                        [UIAlertView alertViewWithTitle:[NSString stringWithFormat:@"你已经被移出%@小组",model.name]];
                        [UIAlertView alertViewWithTitle:[NSString stringWithFormat:@"%@%@%@",NSLocalized(@"group_list_logout_1"),model.name,NSLocalized(@"group_list_title_group")]];
                    }
                    break;
                }
            }
            return;
        }
        return;
    }
    
    if ([dict[@"type"] isEqualToNumber:@4]) { // 自己退出小组，不再接收消息
        if ([dict[@"userId"] isEqualToNumber:USER_ID]) {
            return;
        }
    }

    
    GroupModel *modeee = nil; //存储groupId 对应的那组数据
    // 遍历2组数据  找到groupId   区分是不是私信组的消息
    for (int i=0; i<_groupData.count+_PrivateGroupData.count; i++) {
        GroupModel *mode=nil;
        int k=i;
        if (i<_groupData.count) {
            mode=[self.groupData objectAtIndex:k];
        }else{
            k=i-(int)_groupData.count;
            mode=[self.PrivateGroupData objectAtIndex:k];
        }
        
        if ([mode.id isEqualToNumber:dict[@"groupId"]]) {
            modeee = mode;
            /**
             *  11.10更改
             */
            if (!((NSNumber*)dict[@"groupId"]==_willInGroupId)) { // 未选中小组的未读消息增加
                if (![mode.category isEqualToNumber:@3]) { //私信或者群组
                    
                    GrpUnreadCount++;
                }else{
                    PrvGrpUnreadCount++;
                }
            }
            /**
             *  11.10更改
             */
//            if (![mode.category isEqualToNumber:@3]) { //私信或者群组
//                GrpUnreadCount++;
//            }else{
//                PrvGrpUnreadCount++;
//            }
            break;
        }
    }

    NSInteger count = self.groupData.count+self.PrivateGroupData.count;
    int i = 0;
    for (i=0;i<count;i++) {

        GroupModel *mode = nil;
        
        int k=i;
        
        if (i<_groupData.count) {
            mode=[self.groupData objectAtIndex:i];
        }else{
            k=i-(int)_groupData.count;
            mode=[self.PrivateGroupData objectAtIndex:k];
        }
        
        if (mode.id.intValue ==[(NSNumber*)dict[@"groupId"] intValue]) { // 刷新message
            mode.messages=array;
            int uc = [mode.unreadCount intValue];  //某一群组的未读数
            uc++;//未读数本地加1
        
            
            /**
             *  11.10更改
             */
            if ((NSNumber*)dict[@"groupId"]==_willInGroupId) {  //当前grpID相同不添加未读
                
//                mode.unreadCount=[NSNumber numberWithInt:0];
//                if (![modeee.category isEqualToNumber:@3]) {
//
//                    GrpUnreadCount--;
//                }else{
//                    PrvGrpUnreadCount--;
//                }
                
            } else {
//                mode.unreadCount=[NSNumber numberWithInt:0];
                
//                if (![modeee.category isEqualToNumber:@3]) {
//                    GrpUnreadCount--;
//                }else{
//                    PrvGrpUnreadCount--;
//                }

                mode.unreadCount=[NSNumber numberWithInt:uc];
            }
            
            
            if (![mode.category isEqualToNumber:@3]) {
                [self.groupData replaceObjectAtIndex:k withObject:mode];
            } else {
                [self.PrivateGroupData replaceObjectAtIndex:k withObject:mode];
            }
            
            if (self.isPrivateGroup && i >= _groupData.count) { // 私信小组
                NSIndexPath *te=[NSIndexPath indexPathForRow:k inSection:0];//刷新第i个section的第0行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
                // 刷新显示在第一行
                if (k!=0) {
                    [self.tableView moveRowAtIndexPath:te toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    [self.PrivateGroupData insertObject:mode atIndex:0];
                    [self.PrivateGroupData removeObjectAtIndex:k+1];
                }
            }
            if (!self.isPrivateGroup && i < _groupData.count) { // 普通小组
                NSIndexPath *te=[NSIndexPath indexPathForRow:i inSection:0];//刷新第i个section的第0行
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
                // 刷新显示在第一行
                if (i!=0) {
                    [self.tableView moveRowAtIndexPath:te toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    [self.groupData insertObject:mode atIndex:0];
                    [self.groupData removeObjectAtIndex:i+1];
                }
            }
//            NSIndexPath *te=[NSIndexPath indexPathForRow:k inSection:0];//刷新第i个section的第0行
//            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:te,nil] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }
    
    if (PrvGrpUnreadCount>0) {
        weiduNum2.hidden=NO;
    }else{
        weiduNum2.hidden=YES;
    }
    if (GrpUnreadCount>0) {
        weiduNum.hidden=NO;
    }else{
        weiduNum.hidden=YES;
    }
    
    
    
    if (i==count) {
//        self.PrivId = dict[@"groupId"];
//        
//        NSLog(@"PrivId==%@",self.PrivId);
        [self getUpLeftViewUserGroupList];
    }
}

- (void)removeUnReadConutPriGroup:(NSNotification *)not{

    for (GroupModel *model in self.PrivateGroupData) {
        if ([not.object isEqualToNumber:model.id]) {
            PrvGrpUnreadCount-=model.unreadCount.intValue;
            model.unreadCount = @0;
            
            if (PrvGrpUnreadCount>0) {
                weiduNum2.hidden=NO;
            } else {
                weiduNum2.hidden=YES;
            }
            [self.tableView reloadData];
            break;
        }
    }
}

#pragma mark 获取数据

- (void)getUpLeftViewUserGroupList{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [manager GET:userGroupsPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [self.groupData removeAllObjects];
            [self.PrivateGroupData removeAllObjects];
            
            PrvGrpUnreadCount=0;
            GrpUnreadCount=0;
            
            for (NSDictionary *dict in responseObject[@"data"]) {
                GroupModel *model = [[GroupModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                if ([model.id isEqualToNumber:_willInGroupId]) {
                    model.unreadCount = @0;
                }
                if ([model.category isEqualToNumber:@3]) { // 私聊群组
                    [self.PrivateGroupData addObject:model];
                    PrvGrpUnreadCount+=[model.unreadCount intValue];
                } else {
                    [self.groupData addObject:model];
                    GrpUnreadCount+=[model.unreadCount intValue];
                }
            }
            
            if (PrvGrpUnreadCount>0) {
                UITabBarItem* item=[self.tabBarController.viewControllers objectAtIndex:1].tabBarItem;
                [item setBadgeValue:[NSString stringWithFormat:@"%d",PrvGrpUnreadCount]];
                weiduNum2.hidden=NO;
            }else{
                weiduNum2.hidden=YES;
            }
            if (GrpUnreadCount>0) {
                weiduNum.hidden=NO;
            }else{
                weiduNum.hidden=YES;
            }
            
            [self.tableView headerEndRefreshing];
            
            [self.tableView reloadData];
            
            if (self.isPrivateGroup) {
                if (self.PrivateGroupData.count == 0) {
                    self.tableView.backgroundView = self.bgImage;
                } else {
                    self.tableView.backgroundView = nil;
                    self.bgImage = nil;
                }
            } else {
//                if (self.groupData.count == 0) {
//                    self.tableView.backgroundView = self.bgImage;
//                } else {
//                    self.tableView.backgroundView = nil;
//                    self.bgImage = nil;
//                }
                
                self.tableView.backgroundView = nil;
            }
            
        }
//        else if([responseObject[@"code"] isEqualToNumber:@3]){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
////            [[NSNotificationCenter defaultCenter] postNotificationName:@"postCompListVC" object:nil];
//        }
        else{
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
        [self.tableView headerEndRefreshing];
    }];
}

#pragma mark 导航栏点击事件

- (void)onLeftViewSearchButtonClick{
    if (self.tableView.tableHeaderView!=nil) {
        self.tableView.tableHeaderView=nil;
//        NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [[self tableView] scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];

    }else{
        self.tableView.tableHeaderView=self.searchController.searchBar;
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
//    if (self.tableView.tableHeaderView.hidden) {
//        self.tableView.tableHeaderView.hidden=NO;
//    }else{
//        self.tableView.tableHeaderView.hidden=YES;
//    }
//    addGroupViewController *addGroupVC = [[addGroupViewController alloc] init];
//    addGroupVC.block = ^() {
//        [self getUpLeftViewUserGroupList];
//    };
//    addGroupVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:addGroupVC animated:YES];
}

- (void)onCreatGroupButtonClick{
    CreatGroupViewController *creatGroupVC = [[CreatGroupViewController alloc] init];
    creatGroupVC.hidesBottomBarWhenPushed = YES;
    if (self.searchController.active) {
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
    }
    [self.navigationController pushViewController:creatGroupVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 加入群组的按钮点击事件
- (void)onAddGroupViewJoinGroupButtonClick:(UIButton *)sender{
    NSString *name = [NSString string];
    for (GroupModel *model in self.groupDaList) {
        if (sender.tag-100==model.id.intValue) {
            name = [model.name copy];
            _joinGroupId = model.id;
        }
    }

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@%@",NSLocalized(@"group_search_jion_sure"),name] message:nil delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"HOME_alert_sure"), nil];
    alert.tag=131;
    [alert show];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==1) {
        if (self.searchController.active) {
            if (_groupSearchDaList.count==0) {
                return 0.0;
            }
        }
        if (_groupDaList.count==0) {
            return 0.0;
        }
        return 28;
    }else{
        return 0.0;
    }
}

//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    if (section==1) {
//        if (self.searchController.active) {
//            return [NSString stringWithFormat:@"未加入小组(%ld)",_groupSearchDaList.count];
//        }else{
//            return [NSString stringWithFormat:@"未加入小组(%ld)",_groupDaList.count];
//        }
//    }else{
//        return nil;
//    }
//}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==1) {
        UIView * view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 28)];
        view.backgroundColor=[UIColor colorWithHex:0xF6F6F6];
        UILabel * titlelabel=[[UILabel alloc] initWithFrame:CGRectMake(14, 4, 200, 20)];
        if (self.searchController.active) {
            if (_groupSearchDaList.count==0) {
                return nil;
            }
            titlelabel.text= [NSString stringWithFormat:@"未加入小组(%ld)",_groupSearchDaList.count];
        }else{
            if (_groupDaList.count==0) {
                return nil;
            }
            titlelabel.text= [NSString stringWithFormat:@"未加入小组(%ld)",_groupDaList.count];
        }
        titlelabel.textColor=[UIColor colorWithHex:0x999999];
//        titlelabel.font=[UIFont fontWithName:@"Helvetica" size:15];
        
        [view addSubview:titlelabel];
        return view;
    }
    return nil;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.isPrivateGroup==NO) {
        if (self.searchController.active) {
            if (_groupSearchDaList.count==0) {
                return 1;
            }else{
                return 2;
            }
        }
        if (self.groupData.count==0) {
            return 1;
        }
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isPrivateGroup==NO) {
        if (self.searchController.active) {
            if (section==0) {
                return self.groupSearchDList1.count;
            }else{
                return self.groupSearchDaList.count;
            }
        }else{
            if (section==0) {
                return self.groupData.count;
            }else{
                return self.groupDaList.count;
            }
        }
    } else {
        return self.PrivateGroupData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellId = @"GroupTableViewCellId";
    static NSString *cellId2 = @"GroupTableViewCellId2";
    if (indexPath.section==1) {  //未加入小组列表
        CanJoinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId2];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"CanJoinTableViewCell" owner:self options:nil] lastObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        GroupModel *model = nil;
        if (self.searchController.active) { // 搜索tableView
            if (indexPath.section==1) {
                model = self.groupSearchDaList[indexPath.row];
            }else{
                model = self.groupSearchDList1[indexPath.row];
            }
        } else {
            model = self.groupDaList[indexPath.row];
        }
        cell.Localized_active.text = NSLocalized(@"group_search_active");
        cell.groupNameLabel.text = model.name;
        cell.groupDescribeLabel.text = model.desc;
        [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
        [cell.joinGroupBtn setTitle:NSLocalized(@"group_search_jionBtn") forState:UIControlStateNormal];
        cell.joinGroupBtn.tag = 100+model.id.intValue;
        [cell.joinGroupBtn addTarget:self action:@selector(onAddGroupViewJoinGroupButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//        cell.userCountLabel.text = [model.userCount.stringValue stringByAppendingString:NSLocalized(@"group_search_userCount")];
        
        cell.userCountLabel.text = [NSLocalized(@"group_search_userCount") stringByAppendingString:model.userCount.stringValue];
        
        cell.topicLabel.text = [NSLocalized(@"group_search_subTopic") stringByAppendingString:model.topicCount==nil?@"0":model.topicCount.stringValue];
        
        CGRect frame = cell.bgHyd.frame;
        
        UIImage *image = [UIImage imageNamed:@"active-selected"];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y+1, image.size.width, image.size.height)];
        imageView.contentMode = UIViewContentModeLeft;
        imageView.clipsToBounds = YES;
        imageView.image = image;
        [cell.contentView addSubview:imageView];
        
        CGFloat width = frame.size.width*model.activity.intValue/10.0;
        CGRect imframe = imageView.frame;
        imframe.size.width = width;
        imageView.frame = imframe;
        return cell;
    }else{
    
        GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"GroupTableViewCell" owner:self options:nil] lastObject];
        }
        cell.joinGroupBtn.hidden = YES;
        GroupModel *model =  nil;
        if (self.isPrivateGroup==NO) {
            if (self.searchController.active) { // 搜索tableView
                if (indexPath.section==1) {
                    model = self.groupSearchDaList[indexPath.row];
                }else{
                    model = self.groupSearchDList1[indexPath.row];
                }
            }else{
                model =  self.groupData[indexPath.row];
            }
        } else {
            model =  self.PrivateGroupData[indexPath.row];
        }

    //    if ([model.id isEqualToNumber:self.PrivId]) {
    //        NSLog(@"22");
    //        PrvGrpUnreadCount -= model.unreadCount.intValue;
    //        model.unreadCount = @0;
    //        if (PrvGrpUnreadCount>0) {
    //            weiduNum2.hidden=NO;
    //        } else {
    //            weiduNum2.hidden=YES;
    //        }
    //        
    ////        self.PrivId = @(-1);
    //    }
        
        cell.groupNameLabel.text = model.name;
        
        if (model.category.intValue == 4) {
            cell.isPrivateGroup.hidden = NO;
        } else {
            cell.isPrivateGroup.hidden = YES;
        }
        
        if ([model.unreadCount isEqualToNumber:@0]) {
            cell.messageRemindView.hidden = YES;
        } else {
            cell.messageRemindView.hidden = NO;
            
            CGRect frame = cell.messageRemindView.frame;
            if (model.unreadCount.intValue>=100) { // 信息大于100，不显示数字
                frame.size.width = 10;
                frame.size.height = 10;
                frame.origin.x = CGRectGetMaxX(cell.groupIconImageView.frame)-7;
                frame.origin.y = CGRectGetMinY(cell.groupIconImageView.frame)-3;
                cell.messageRemindView.layer.cornerRadius = 5;
                cell.messageRemindView.text = @"";
            } else {
                frame.size.width = 18;
                frame.size.height = 13;
                frame.origin.x = CGRectGetMaxX(cell.groupIconImageView.frame)-15;
                frame.origin.y = CGRectGetMinY(cell.groupIconImageView.frame)-2;
                
                cell.messageRemindView.layer.cornerRadius = 7;
                cell.messageRemindView.text = model.unreadCount.stringValue;
            }
            cell.messageRemindView.frame = frame;
        }
        NSDictionary *message = [model.messages lastObject];
        int type = [message[@"type"] intValue];
        if ([model.category isEqualToNumber:@3]) {
            NSString *sender = message[@"sender"];
            if (type == 12) {
                cell.groupDescribeLabel.text = NSLocalized(@"group_list_send_image");
            } else if (type==13) {
                cell.groupDescribeLabel.text = NSLocalized(@"group_list_send_map");
            } else if (type==15) {
                cell.groupDescribeLabel.text = [sender stringByAppendingFormat:@":%@",NSLocalized(@"group_list_send_voice")];
            } else {
                cell.groupDescribeLabel.text = message[@"info"][@"message"];
            }
            
        } else {
            
            
            NSString *sender = message[@"sender"];
            NSString *text = [NSString string];
            if (type==3||type==2) { // 加入群组
                text = [sender stringByAppendingString:NSLocalized(@"group_list_join")];
            } else if (type==4) { // 退出群组
                text = [sender stringByAppendingString:NSLocalized(@"group_list_logout")];
            } else if (type==7) { // 主题
                
                int subType = [message[@"subType"] intValue];
                if (subType==7) {
                    text = [sender stringByAppendingFormat:NSLocalized(@"group_list_send_link")];
                } else if (subType==2) {
                    text = [sender stringByAppendingFormat:NSLocalized(@"group_list_topic_image")];
                } else if (subType==3) {
                    text = [sender stringByAppendingFormat:NSLocalized(@"group_list_topic_map")];
                } else if (subType==4) {
                    text = [sender stringByAppendingFormat:NSLocalized(@"group_list_topic_vote")];
                } else {
    //                text = [sender stringByAppendingFormat:@":%@",message[@"info"][@"desc"]];
                    text = [sender stringByAppendingString:NSLocalized(@"group_list_topic")];
                }
                
            } else if (type==8) { // 评论
                
                int subType = [message[@"subType"] intValue];
                if (subType==2) {
                    text = [sender stringByAppendingFormat:@":%@",NSLocalized(@"group_list_response_image")];
                } else if (subType==5) {
                    text = [sender stringByAppendingFormat:@":%@",NSLocalized(@"group_list_send_voice")];
                } else  {
                   text = [sender stringByAppendingFormat:@":%@",message[@"info"][@"message"]];
                }
            } else if (type==15) {
                text = [sender stringByAppendingFormat:@":%@",NSLocalized(@"group_list_send_voice")];
            } else {
                text = [sender stringByAppendingFormat:@":%@",message[@"info"][@"message"]];
            }
            
            // 自定义text属性(加粗)
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:text];
            NSRange range = [text rangeOfString:sender];
            [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0] range:range];
            cell.groupDescribeLabel.attributedText = str;

        }
        
        cell.updatedAtLabel.text = [ToolOfClass toolGetLocalAllDateFormateWithUTCDate:message[@"updatedAt"]];
        if (model.logo==nil) {
            
        }else{
            [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
        }
       return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return groupCellHeight;
    if (indexPath.section==1||self.searchController.active) {
        return 94;
    }else{
        return 80;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    if (self.searchController.active) {
//        return;
//    }
    
    if (indexPath.section==1) {
        GroupDetailViewController *groupDetailVC = [[GroupDetailViewController alloc] init];
        groupDetailVC.block=^(){
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self addGroupViewJoinGroup];
//            });
            [self getCanJionGroupList];
            [self getUpLeftViewUserGroupList];
        };
        GroupModel *model = nil;
        if (self.searchController.active) { // 搜索tableView
            if (indexPath.section==1) {
                model = self.groupSearchDaList[indexPath.row];
            }else{
                model = self.groupSearchDList1[indexPath.row];
            }
        } else {
            model = self.groupDaList[indexPath.row];
        }
//        GroupModel *model = self.searchController.active?self.groupSearchDaList[indexPath.row]:self.groupDaList[indexPath.row];
        groupDetailVC.groupId = model.id;
        groupDetailVC.SuperViewStr=@"chakan";
        //    groupDetailVC.groupUsers = self.groupUsers;
        if (self.tableView.tableHeaderView!=nil) {
            self.tableView.tableHeaderView=nil;
        }
        [self.navigationController pushViewController:groupDetailVC animated:YES];

    }else{
        GroupModel *model = nil;
        
        if (self.isPrivateGroup==NO) { // 普通小组
            model = self.groupData[indexPath.row];
            MessageViewController *messageVC = [[MessageViewController alloc] init];
            messageVC.hidesBottomBarWhenPushed = YES;
            messageVC.groupId = model.id;
            _willInGroupId=model.id;
            messageVC.groupName = model.name;
            messageVC.unreadCount = model.unreadCount;
            messageVC.broadcast = model.broadcast.intValue;
            messageVC.groupCreator = model.creator;
            GrpUnreadCount-=[model.unreadCount intValue];
            messageVC.Namedelegate = self;
            messageVC.cellBlock = ^(void) {
                model.unreadCount = @0;
                _willInGroupId = @(-1);
                [self.tableView reloadData];
            };
            if (self.tableView.tableHeaderView!=nil) {
                self.tableView.tableHeaderView=nil;
            }
            [self.navigationController pushViewController:messageVC animated:YES];
        } else { // 私信
            model = self.PrivateGroupData[indexPath.row];
            DirectChatViewController *directVC = [[DirectChatViewController alloc] init];
            directVC.groupId = model.id;
            _willInGroupId=model.id;
            directVC.groupName = model.name;
            PrvGrpUnreadCount-=[model.unreadCount intValue];
            directVC.cellBlock = ^(void) {
                model.unreadCount = @0;
                _willInGroupId = @(-1);
                [self.tableView reloadData];
            };
            directVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:directVC animated:YES];
        }
        
        if (PrvGrpUnreadCount>0) {
            weiduNum2.hidden=NO;
        }else{
            weiduNum2.hidden=YES;
        }
        if (GrpUnreadCount>0) {
            weiduNum.hidden=NO;
        }else{
            weiduNum.hidden=YES;
        }
    
    }
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        return UITableViewCellEditingStyleNone;
    }else{
        return UITableViewCellEditingStyleDelete;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalized(@"group_list_delChat") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        GroupModel *model = self.PrivateGroupData[indexPath.row];
        
        self.deleGroupId = model.id;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"accessToken"] = [ToolOfClass authToken];
        [manager POST:[NSString stringWithFormat:DeleteDirectChat,model.id] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"code"] isEqualToNumber:@200]) {
                [ToolOfClass showMessage:NSLocalized(@"group_list_delChat_success")];
                
                if (model.unreadCount.intValue>0) {
                    PrvGrpUnreadCount-=model.unreadCount.intValue;
                }
                
                [self.PrivateGroupData removeObject:model];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//                [self.tableView reloadData];
                if (self.PrivateGroupData.count == 0) {
                    self.tableView.backgroundView = self.bgImage;
                } else {
                    self.tableView.backgroundView = nil;
                    self.bgImage = nil;
                }
                if (PrvGrpUnreadCount>0) {
                    weiduNum2.hidden=NO;
                }else{
                    weiduNum2.hidden=YES;
                }
                
            } else {
//                [ToolOfClass showMessage:responseObject[@"message"]];
                [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }];
    deleteAction.backgroundColor = [UIColor colorWithRed:197/255.0 green:196/255.0 blue:201/255.0 alpha:1];
    
    UITableViewRowAction *logOutAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalized(@"group_list_logout") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        GroupModel *model = self.groupData[indexPath.row];
        
        self.deleGroupId = model.id;
        
        NSNumber * num=[[NSUserDefaults standardUserDefaults] objectForKey:@"id"];
        
        if (model.creator==nil) {
            [UIAlertView alertViewWithTitle:NSLocalized(@"group_list_noMaster")];
            return;
        }
        
        if ([num isEqualToNumber:model.creator]) {
            
            [UIAlertView alertViewWithTitle:NSLocalized(@"group_list_logout") subTitle:
             NSLocalized(@"group_list_isMaster") target:self];
        } else {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
            parameter[@"accessToken"] = [ToolOfClass authToken];
            [manager POST:[NSString stringWithFormat:logoutGroupPath,model.id] parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if ([responseObject[@"code"] isEqualToNumber:@200]) {
                    
                    [ToolOfClass showMessage:NSLocalized(@"group_list_logout_success")];
                    
                    if (model.unreadCount.intValue>0) {
                        GrpUnreadCount-=model.unreadCount.intValue;
                    }
                    
                    [self.groupData removeObject:model];
                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    if (self.groupData.count == 0) {
                        self.tableView.backgroundView = nil;
                    }
                    
                    if (GrpUnreadCount>0) {
                        weiduNum.hidden=NO;
                    }else{
                        weiduNum.hidden=YES;
                    }
                    
                } else {
//                    [ToolOfClass showMessage:responseObject[@"message"]];
                    [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
            }];
        }
        
    }];
    logOutAction.backgroundColor = [UIColor colorWithRed:197/255.0 green:196/255.0 blue:201/255.0 alpha:1];
    
    return self.isPrivateGroup==YES? @[deleteAction]:@[logOutAction];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.tabBarController.tabBar.hidden = NO;
    
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)navigationBar:(UINavigationBar *)navigationBar didPushItem:(UINavigationItem *)item{
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.translucent = YES;
//    [self.navigationController.navigationBar setAlpha:1.0];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==131) {
        if (buttonIndex==1) { // 确定按钮
            [self addGroupViewJoinGroup];
        }
    }else if (alertView.tag==10) { // 被踢出小组
        // 消除搜索控制器
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
        [self.searchController.view removeFromSuperview];
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        if (buttonIndex==1) {
            
            for (GroupModel *model in self.groupData) {
                
                if ([self.deleGroupId isEqualToNumber:model.id]) {
                    GroupDetailViewController *detail = [[GroupDetailViewController alloc] init];
                    detail.groupId = model.id;
                    [self.navigationController pushViewController:detail animated:YES];
                    break;
                }
            }
        }
    }
}

#pragma mark UISearchControllerDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self.groupSearchDaList removeAllObjects];
    [self.groupSearchDList1 removeAllObjects];
    for (GroupModel *model in self.groupDaList) {
        //        BOOL range = [model.name hasPrefix:searchString];
        NSRange range1 = [model.name rangeOfString:searchString];
        if (range1.location != NSNotFound) {
            [self.groupSearchDaList addObject:model];
        }
    }
    for (GroupModel *model in self.groupData) {
        //        BOOL range = [model.name hasPrefix:searchString];
        NSRange range1 = [model.name rangeOfString:searchString];
        if (range1.location != NSNotFound) {
            [self.groupSearchDList1 addObject:model];
        }
    }
    [self.tableView reloadData];
}

- (void)willPresentSearchController:(UISearchController *)searchController{

}

- (void)didPresentSearchController:(UISearchController *)searchController{
    UIButton *cancelButton;
    UIView *topView = self.searchController.searchBar.subviews[0];
    for (UIView *subView in topView.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            cancelButton = (UIButton*)subView;
        }
    }
    if (cancelButton) {
        //Set the new title of the cancel button
//        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor colorWithHex:0x48C1A8] forState:UIControlStateNormal];
        cancelButton.titleLabel.font = [UIFont fontWithName:@"Heiti SC" size:15];
    }
}

// 搜索框开始编辑时触发方法
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self presentViewController:self.searchController animated:YES completion:nil];
}

//tabBarControllerDelegate   解决搜索状态切换tabbar 时  状态栏消失问题
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *) viewController {
    if (self.searchController.active) {
        self.searchController.active = NO;
        [self.searchController.searchBar removeFromSuperview];
    }
}

@end
