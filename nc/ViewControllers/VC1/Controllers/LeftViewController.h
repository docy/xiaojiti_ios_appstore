//
//  LeftViewController.h
//  nc
//
//  Created by docy admin on 6/18/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

// 群组视图控制器
@interface LeftViewController : UITableViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *groupData;
@property (nonatomic, strong) NSMutableArray *PrivateGroupData; // 私聊群组

@property (nonatomic,retain) NSMutableArray *groupDaList; // 可添加群组数组

@end
