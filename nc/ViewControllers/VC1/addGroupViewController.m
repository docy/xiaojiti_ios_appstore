//
//  addGroupViewController.m
//  nc
//
//  Created by docy admin on 7/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import "addGroupViewController.h"

#import <AFNetworking/AFNetworking.h>
#import "GroupModel.h"
#import "GroupTableViewCell.h"

#import "CanJoinTableViewCell.h"

#import "UIImageView+WebCache.h"
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "ToolOfClass.h"
#import "UIAlertView+AlertView.h"
#import "GroupDetailViewController.h"
#import "APIHeader.h"
#import "CreatGroupViewController.h"
#import "UIImageView+XJTImageView.h"

@interface addGroupViewController () <UIAlertViewDelegate,UISearchControllerDelegate,UISearchBarDelegate,UISearchResultsUpdating>
{
    NSNumber *_joinGroupId;
    ToolOfClass *_refreshView;
}

@property (nonatomic, retain) NSMutableArray *groupSearchDataList; // 搜索数组
@property (nonatomic, strong) UISearchController *searchController; // 实现搜索
@property (strong, nonatomic) IBOutlet UIView *nullBgView;
@property (strong, nonatomic) IBOutlet UIView *nullBgView_en;

@end


@implementation addGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置基本属性
    [self setUpAddGroupViewData];
    // 获取可添加群组列表
    [self getCanJionGroupList];
}

// 设置基本属性
- (void)setUpAddGroupViewData
{
    
    
    self.groupDataList = [NSMutableArray array];
    _joinGroupId = [[NSNumber alloc] init];
//    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(onNavLeftButtonClick)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemLeftItemWithTarget:self action:@selector(onNavLeftButtonClick)];
    self.navigationItem.backBarButtonItem = [UIBarButtonItem itemWithTarget:nil action:nil];
    _refreshView = [[ToolOfClass alloc] init];
    [_refreshView startAnimationInView:self.view];
    
    self.groupSearchDataList = [NSMutableArray array];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    [self.searchController.searchBar sizeToFit];
    self.navigationItem.titleView = self.searchController.searchBar;
 
//    NetWorkBadView *badView = [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
//    [badView.OpenNetWorkBtn addTarget:self action:@selector(onCreatGroupBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    badView.backgroundColor = [UIColor whiteColor];
//    badView.imageView.image = [UIImage imageNamed:@"invitation friends"];
//    badView.contentLabel.text = @"创建小组";
//    self.tableView.tableHeaderView = badView;
    
}

//- (void)onCreatGroupBtnClick{
//    CreatGroupViewController *creatGroup = [[CreatGroupViewController alloc] initWithNibName:@"CreatGroupViewController" bundle:[NSBundle mainBundle]];
//    [self.navigationController pushViewController:creatGroup animated:YES];
//}

- (void)onNavLeftButtonClick
{
    // 消除搜索控制器
    if (self.searchController.active) {
        [self.searchController dismissViewControllerAnimated:YES completion:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

// 添加群组
- (void)addGroupViewJoinGroup
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *path = [NSString stringWithFormat:joinGroupPath,_joinGroupId];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];

    [manager POST:path parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            [ToolOfClass showMessage:NSLocalized(@"group_search_jion_success")];
            if (self.block) {
                self.block();
            }
            // 消除搜索控制器
            [self.searchController dismissViewControllerAnimated:YES completion:nil];
            [self.searchController.view removeFromSuperview];
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
//            [ToolOfClass showMessage:responseObject[@"message"]];
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
// 获取可添加群组列表
- (void)getCanJionGroupList
{
    [self.groupDataList removeAllObjects];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    [manager GET:canJionGroupPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            
            for (NSDictionary *dict in responseObject[@"data"]) {
                GroupModel *model = [[GroupModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.groupDataList addObject:model];
            }
            [_refreshView stopAnimation];
            
            if (self.groupDataList.count==0) {
//                self.tableView.backgroundView = [UIImageView imageViewWithImage:@"Create a group"];
                self.tableView.backgroundView = isEnglish?_nullBgView_en:_nullBgView;//[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Create a group"]];
            } else {
                self.tableView.backgroundView = nil;
            }
            [self.tableView reloadData];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ToolOfClass showMessage:NSLocalized(@"HOME_show_failure")];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (self.searchController.active) {
        return self.groupSearchDataList.count;
    }
    return self.groupDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *cellId = @"GroupTableViewCellId";
//    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
//    if (!cell) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:@"GroupTableViewCell" owner:self options:nil] lastObject];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    cell.messageRemindView.hidden = YES;
//    cell.isPrivateGroup.hidden = YES;
//    
//    GroupModel *model = nil;
//    if (self.searchController.active) { // 搜索tableView
//        model = self.groupSearchDataList[indexPath.row];
//    } else {
//        model = self.groupDataList[indexPath.row];
//    }
//    
//    cell.groupNameLabel.text = model.name;
//    cell.groupDescribeLabel.text = model.desc;
//    [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
//    
//    cell.joinGroupBtn.tag = 100+model.id.intValue;
//    [cell.joinGroupBtn addTarget:self action:@selector(onAddGroupViewJoinGroupButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    static NSString *cellId = @"GroupTableViewCellId";
    CanJoinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CanJoinTableViewCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    GroupModel *model = nil;
    if (self.searchController.active) { // 搜索tableView
        model = self.groupSearchDataList[indexPath.row];
    } else {
        model = self.groupDataList[indexPath.row];
    }
    cell.Localized_active.text = NSLocalized(@"group_search_active");
    cell.groupNameLabel.text = model.name;
    cell.groupDescribeLabel.text = model.desc;
    [cell.groupIconImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logo]] placeholderImage:nil];
    [cell.joinGroupBtn setTitle:NSLocalized(@"group_search_jionBtn") forState:UIControlStateNormal];
    cell.joinGroupBtn.tag = 100+model.id.intValue;
    [cell.joinGroupBtn addTarget:self action:@selector(onAddGroupViewJoinGroupButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.userCountLabel.text = [model.userCount.stringValue stringByAppendingString:NSLocalized(@"group_search_userCount")];

    cell.topicLabel.text = [NSLocalized(@"group_search_subTopic") stringByAppendingString:model.topicCount==nil?@"0":model.topicCount.stringValue];
    
    CGRect frame = cell.bgHyd.frame;

    UIImage *image = [UIImage imageNamed:@"active-selected"];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, image.size.width, image.size.height)];
    imageView.contentMode = UIViewContentModeLeft;
    imageView.clipsToBounds = YES;
    imageView.image = image;
    [cell.contentView addSubview:imageView];
    
    CGFloat width = frame.size.width*model.activity.intValue/10.0;
    CGRect imframe = imageView.frame;
    imframe.size.width = width;
    imageView.frame = imframe;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return groupCellHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupDetailViewController *groupDetailVC = [[GroupDetailViewController alloc] init];
    
    GroupModel *model = self.searchController.active?self.groupSearchDataList[indexPath.row]:self.groupDataList[indexPath.row];
    
    groupDetailVC.groupId = model.id;
    groupDetailVC.SuperViewStr=@"chakan";
//    groupDetailVC.groupUsers = self.groupUsers;
    [self.navigationController pushViewController:groupDetailVC animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (self.searchController.active) { // 搜索tableView
        
        return [NSString stringWithFormat:NSLocalized(@"group_search_count"),self.groupSearchDataList.count];
    } else {
        return nil;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//        return 20;
//}

// 加入群组的按钮点击事件
- (void)onAddGroupViewJoinGroupButtonClick:(UIButton *)sender{
    NSString *name = [NSString string];
    for (GroupModel *model in self.groupDataList) {
        if (sender.tag-100==model.id.intValue) {
            name = [model.name copy];
            _joinGroupId = model.id;
        }
    }
    [UIAlertView alertViewWithTitle:[NSString stringWithFormat:@"%@%@",NSLocalized(@"group_search_jion_sure"),name] target:self];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) { // 确定按钮
        [self addGroupViewJoinGroup];
    }
}

#pragma mark UISearchControllerDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = self.searchController.searchBar.text;
    [self.groupSearchDataList removeAllObjects];
    for (GroupModel *model in self.groupDataList) {
//        BOOL range = [model.name hasPrefix:searchString];
        NSRange range1 = [model.name rangeOfString:searchString];
        if (range1.location != NSNotFound) {
            [self.groupSearchDataList addObject:model];
        }
    }
    [self.tableView reloadData];
}

// 搜索框开始编辑时触发方法
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self presentViewController:self.searchController animated:YES completion:nil];
}

@end
