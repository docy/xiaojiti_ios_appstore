//
//  addGroupViewController.h
//  nc
//
//  Created by docy admin on 7/2/15.
//  Copyright (c) 2015 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
// 添加群组视图控制器

typedef void(^joinGroupBlock) (void);

@interface addGroupViewController : UITableViewController

@property (nonatomic,retain) NSMutableArray *groupDataList; // 可添加群组数组

@property (nonatomic,strong) joinGroupBlock block;

@end
