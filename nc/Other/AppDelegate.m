//
//  AppDelegate.m
//  nc
//
//  Created by jianghuan on 15/3/30.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import "AppDelegate.h"

#import "SLKTextViewController.h"
#import "MessageViewController.h"
#import "XJTNavigationController.h"
#import "RegisterInviteToJoinCompanyViewController.h"
#import "LoginViewController.h"
#import "Login_ViewController.h"
#import "UITabBarController+CustomTabBarController.h"
#import <AFNetworking/AFNetworking.h>
#import "ToolOfClass.h"

#import "AFNetWorkNotReachableView.h"

#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "MobClick.h"
#import "MobClickSocialAnalytics.h"

//#import "UMSocialQQHandler.h"
#import "XJTTabBarController.h"
#import "CompanyListViewController.h"
#import "LeadingViewController.h"
#import "Socket_IO_Client_Swift-Swift.h"
#import "AllGroupViewController.h"
#import "AFNHttpRequest.h"
#import "UIViewController+HOMECategory.h"
#import "usernamePW_ViewController.h"

@interface AppDelegate () <UIAlertViewDelegate>{
//    XJTTabBarController* tabbarC;
//    CompanyListViewController * company;
    BOOL NetworkReachability;
}

@property(nonatomic, retain)XJTTabBarController* tabbarC;
@property(nonatomic, retain)XJTNavigationController * company;
@property(nonatomic, retain)XJTNavigationController * loginVC;

@property (nonatomic, retain) NSTimer *timer;

@end

@implementation AppDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/xiao-ji-ti/id1056523959?l=zh&ls=1&mt=8"]];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
 //   [NSThread sleepForTimeInterval:1.0];//.sleepForTimeInterval(1.0)//延长1秒
    NSLog(@"launchOptions is %@",launchOptions);
    // 获取错误吗列表
    [self getErrorCodeList];

    // 升级提示
    [[UIViewController alloc] GETHttpRequest:Versions parameters:nil success:^(id obj) {
        if ([obj[@"code"] intValue] == 0) {
            NSString *newVersion = obj[@"data"][@"ios"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy/MM/dd"];
            NSString *currentDate = [dateFormatter stringFromDate:[NSDate date]];
            if (![newVersion isEqualToString:CURRENT_VERSION]) {
                NSString *lastDate = [DEFAULTS objectForKey:LAET_DATE];
                if (![lastDate isEqualToString:currentDate]) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalized(@"update_title") message:NSLocalized(@"update_message") delegate:self cancelButtonTitle:NSLocalized(@"HOME_alert_cancel") otherButtonTitles:NSLocalized(@"update_update"), nil];
                    [alert show];
                }
            }
            [DEFAULTS setObject:currentDate forKey:LAET_DATE];
            
        } else {
            
        }
        
    } failure:^(NSString *localizedDescription) {
        
    }];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //解决iOS9.1 rootviewcontroller 没设置引起的闪退问题
    UIViewController * rootvc=[[UIViewController alloc] init];
    UIImageView * imgv=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Defauit"]];
    imgv.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [rootvc.view addSubview:imgv];
    self.window.rootViewController=rootvc;
    
//    UIViewController *emptyView = [[UIViewController alloc] initWithNibName:nil bundle:nil];
//    self. window .rootViewController = emptyView;
    
    //友盟分享
//    [UMSocialData setAppKey:@"55cd7986e0f55ae569002591"];  //添加key 小集体AppStore
                            //  5639b1c7e0f55a6cec0028bb
    [UMSocialData setAppKey:UM_key];  //添加key  小集体企业版key
//    [UMSocialWechatHandler setWXAppId:@"wx24ac62df19be7a51" appSecret:@"d4624c36b6795d1d99dcf0547af5443d" url:@"http://www.docy.co"];  //微信id 企业版
    [UMSocialWechatHandler setWXAppId:@"wx5640274decb96d97" appSecret:@"70bf1f9249b55ae18544509ee1135b54" url:@"http://www.docy.co"];  //微信id  AppStore
    //友盟统计
    [MobClick startWithAppkey:UM_key reportPolicy:BATCH   channelId:@""];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    
    NSLog(@"------%@-------",NSHomeDirectory());
    
    // iOS8 下需要使用新的 API
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationType myTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:myTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
    
    // App 是用户点击推送消息启动
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo) {
        NSLog(@"从消息启动:%@",userInfo);
    }
    
#if TARGET_IPHONE_SIMULATOR
    Byte dt[32] = {0xc6, 0x1e, 0x5a, 0x13, 0x2d, 0x04, 0x83, 0x82, 0x12, 0x4c, 0x26, 0xcd, 0x0c, 0x16, 0xf6, 0x7c, 0x74, 0x78, 0xb3, 0x5f, 0x6b, 0x37, 0x0a, 0x42, 0x4f, 0xe7, 0x97, 0xdc, 0x9f, 0x3a, 0x54, 0x10};
    [self application:application didRegisterForRemoteNotificationsWithDeviceToken:[NSData dataWithBytes:dt length:32]];
#endif
    //角标清0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    /*
     // 测试本地通知
     [self performSelector:@selector(testLocalNotifi) withObject:nil afterDelay:1.0];
     */
    
//    NSLog(@"device =======%@",[[UIDevice currentDevice]name]);
    
    // 注册通知(退出登录回调)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutSocketDisonnect) name:@"logout" object:nil];
    // 切换公司，重新连接socket
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SwitchCurrentCompanySocketDisonnect) name:@"SwitchCurrentCompany" object:nil];
    // 登录成功回调
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AppDelegateSocketConnect) name:@"LoginSuccessful" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SwitchCurrentCompanySocketDisonnect) name:@"LoginSuccessful" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoginSuccessful) name:@"LoginSuccessful" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socketCloseConencted) name:@"socketCloseConencted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socket_Conencted) name:@"socket_Conencted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WXLoginSuccessful) name:@"WXLoginSuccessful" object:nil];
    
//   选择公司后推出tabbarVC
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postTabbarVC) name:@"postTabbarVC" object:nil];
    //   返回公司列表
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postCompListVC) name:@"postCompListVC" object:nil];
    //   返回登录界面
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postLogoutVC) name:@"postLogoutVC" object:nil];
    //   push文件子话题创建流程界面
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushShareFileToAllGroupVC:) name:@"pushShareFileToAllGroupVC" object:nil];
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isWeiXinLogin"] isEqualToString:@"YES"]) {
//        self.window.rootViewController = self.loginVC;
//    }
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];
    }
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
        
//        if ([currentVersion isEqualToString:lastVersion]) {
//            
//        } else {
//            self.window.rootViewController = [[LeadingViewController alloc] init];
//            // 存储新版本
//            [defaults setObject:currentVersion forKey:CFBundleVersion];
//            [defaults synchronize];
//        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isLogout"] isEqualToString:@"NO"]) { // 没有退出登录，直接进入主界面
//            [self loginInBack];
//            self.window.rootViewController =self.company;
            
            [self pushToken];
            // 登录成功之后切换控制器，token必须每次都是新的
            self.window.rootViewController =self.company;
            
//            if ([data[@"currentCompany"] isEqual:[NSNull null]]) {
//                [self setUserCurrentCompanyWith_companyId:@3];
//                [info setObject:@3 forKey:@"currentCompany"];
//            }
            
            [self AppDelegateSocketConnect];
            
        } else { // 退出登录，需要显示登录页面
            self.window.rootViewController =self.loginVC;
        }
    } else { // 引导页
        self.window.rootViewController = [[LeadingViewController alloc] init];
    }
    
    [self isNetWorkReachable];
    [self.window makeKeyAndVisible];
    
    return YES;
}

-(void)pushShareFileToAllGroupVC:(NSNotification*)notfi{
    AllGroupViewController * allgroupVC=[[AllGroupViewController alloc] initWithNibName:@"AllGroupViewController" bundle:nil];
    allgroupVC.isShearPush=YES;
    allgroupVC.groupId=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentCompany"];
    NSLog(@"-----%@",notfi.userInfo);
    allgroupVC.userStr=notfi.userInfo[@"ursStr"];
    XJTNavigationController * navAllGroup=[[XJTNavigationController alloc] initWithRootViewController:allgroupVC];
    [self.window.rootViewController presentViewController:navAllGroup animated:YES completion:^{
        
    }];
}

-(void)postTabbarVC{
    self.window.rootViewController =self.tabbarC;
}

-(void)postCompListVC{
    self.window.rootViewController =self.company;
}

-(void)postLogoutVC{
    _loginVC=nil;
    self.window.rootViewController =self.loginVC;
}


-(XJTTabBarController*)tabbarC{
    if (!_tabbarC) {
        _tabbarC=[[XJTTabBarController alloc] init];
    }
    return _tabbarC;
}

-(XJTNavigationController*)company{

    if (!_company) {
        CompanyListViewController * compl= [[CompanyListViewController alloc] init];
        _company=[[XJTNavigationController alloc] initWithRootViewController:compl];
    }
    return _company;
}

-(XJTNavigationController*)loginVC{
    if (!_loginVC) {
//        LoginViewController * compl= [[LoginViewController alloc] init];
        Login_ViewController * compl= [[Login_ViewController alloc] init];
        _loginVC=[[XJTNavigationController alloc] initWithRootViewController:compl];
    }
    return _loginVC;
}

-(SocketIOClient*)socketClient{
    if (!_socketClient) {
        _socketClient=[[SocketIOClient alloc] initWithSocketURL:socketServerPath options:nil];
    }
    return _socketClient;
 }

#pragma mark socket连接
- (void)AppDelegateSocketConnect{
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSString *authToken = [info objectForKey:@"authToken"];
    // 创建对象
//    self.socketClient = nil;
//    SocketIOClient *_socketClient = [[SocketIOClient alloc] initWithSocketURL:socketServerPath options:nil];
    // 监听连接
    [self.socketClient on:@"connect" callback:^(NSArray * data , SocketAckEmitter * ack) {
        [_socketClient off:@"authenticated"];
    }];
    
    // 监听连接
    [_socketClient on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {//^(NSArray * data, void (^ack)(NSArray *)) {
        [_socketClient off:@"authenticated"];
        // 监听token
        [_socketClient on:@"authenticated" callback:^(NSArray* authorData, SocketAckEmitter* ack) {//^(NSArray * authorData, void (^ack)(NSArray *)) {
            [_socketClient off:@"message"];
            [_socketClient on:@"message" callback:^(NSArray* message, SocketAckEmitter* ack) {//^(NSArray * message, void (^ack)(NSArray *)) {
                
                NSLog(@"socketMessage%@",message);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"newMessage" object:message];// 发通知刷新数据
            }];
        }];
        // 发送token
        [_socketClient emit:@"authenticate" withItems:@[@{@"token":authToken}]];
    }];
    // 监听错误
    [_socketClient on:@"error" callback:^(NSArray* error, SocketAckEmitter* ack) {//^(NSArray * error, void (^ack)(NSArray*)) {
        NSLog(@"error:%@",error);
        
        if ([error[0] isEqualToString:@"The Internet connection appears to be offline."]) {
            //            if (!self.socketClient.connected) {
            //                [self socketBeginConencted];
            //            }
            [self socketBeginConencted];
        }
    }];
    
//    self.socketClient = _socketClient;
    // 建立连接
    [self socketBeginConencted];
}

-(void)logoutRequest{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [manager POST:logoutPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"message==%@",responseObject[@"message"]);
        if ([responseObject[@"code"] isEqualToNumber:@200]) { // 退出程序
//            [info setObject:@"YES" forKey:@"isLogout"];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"postLoginVC" object:nil];
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

- (void)timerBeginConnectSocket{
    
//    if (self.socketClient.connected) {
//        [self.timer invalidate];
//        self.timer = nil;
//    } else {
//        self.socketClient = nil;
//         [self AppDelegateSocketConnect];
//    }
}

- (void)socketBeginConencted{
    // 建立连接
    if (self.socketClient.status==SocketIOClientStatusNotConnected||self.socketClient.status==SocketIOClientStatusClosed ) {
        [self.socketClient connect];
//        [self AppDelegateSocketConnect];
    }
}

- (void)socket_Conencted{
    // 建立连接
    if (self.socketClient.status==SocketIOClientStatusNotConnected||self.socketClient.status==SocketIOClientStatusClosed ) {
        //        [self.socketClient connect];
        [self AppDelegateSocketConnect];
    }
}

- (void)socketCloseConencted{
    // 断开连接
    if (self.socketClient.status==SocketIOClientStatusConnected) {
        [self.socketClient disconnect];
    }
}

#pragma mark 通知回调

- (void)LoginSuccessful{
    self.socketClient = nil;
    [_socketClient disconnect];
    [self AppDelegateSocketConnect];
}

- (void)WXLoginSuccessful{
    usernamePW_ViewController * usernamePWVC=[[usernamePW_ViewController alloc] initWithNibName:@"usernamePW_ViewController" bundle:nil];
    [self.window.rootViewController presentViewController:usernamePWVC animated:YES completion:^{
        
    }];
//    self.socketClient = nil;
//    [_socketClient disconnect];
//    [self AppDelegateSocketConnect];
}

- (void)SwitchCurrentCompanySocketDisonnect{
//    if (self.socketClient.connected) {
//        [self.socketClient disconnectWithFast:YES];
//    }
//    [self socketBeginConencted];
}
- (void)logoutSocketDisonnect{
//    [self.socketClient disconnectWithFast:YES];
    self.socketClient = nil;
    [self showLoginView];
}

-(void)showLoginView{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postLogoutVC" object:nil];
//    LoginViewController *loginVC = [LoginViewController new];
//    self.window.rootViewController = [[XJTNavigationController alloc] initWithRootViewController:loginVC];
}

#pragma mark 网络监测
- (BOOL)isNetWorkReachable{

    __block BOOL isAvalible;
    AFNetworkReachabilityManager *afNetworkReachabilityManager = [AFNetworkReachabilityManager sharedManager];
    [afNetworkReachabilityManager startMonitoring];  //开启网络监视器；
    
    [afNetworkReachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:{
                NSLog(@"网络不通");
                NetworkReachability=NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"netWorkIsUnAvailable" object:nil];
                
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                NSLog(@"网络通过WIFI连接");
                NetworkReachability=YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"netWorkIsAvailable" object:nil];
//                if (self.socketClient.connected==NO) {
//                    [self socketBeginConencted];
//                }
                if (self.socketClient.status==SocketIOClientStatusNotConnected||self.socketClient.status==SocketIOClientStatusClosed ) {
                    [self.socketClient connect];
                }
                
                break;
            }
                
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                NSLog(@"网络通过流量连接");
                NetworkReachability=YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"netWorkIsAvailable" object:nil];
                
//                if (self.socketClient.connected==NO) {
//                    [self socketBeginConencted];
//                }
                if (self.socketClient.status==SocketIOClientStatusNotConnected||self.socketClient.status==SocketIOClientStatusClosed ) {
                    [self.socketClient connect];
                }
                break;
            }
            default:
                NetworkReachability=NO;
                break;
        }
        
    }];
    return [AFNetworkReachabilityManager sharedManager].isReachable;
//    return isAvalible;
}

-(void)socketStatusIsConnect{
    if (self.socketClient.status==SocketIOClientStatusNotConnected||self.socketClient.status==SocketIOClientStatusClosed ) {
        [self.socketClient connect];
    }
}

- (void)loginInBack{

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isLogout"] isEqualToString:@"NO"]) { // 没有退出登录，直接进入主界面
        [self logoutRequest];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    
    // 发送登录请求
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = [info objectForKey:@"name"];
    parameter[@"password"] = [info objectForKey:@"password"];
    [manager POST:LoginPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"code"] isEqualToNumber:@200] ) {
            
            NSDictionary *data = responseObject[@"data"];
            // 存储用户信息
            [info setObject:data[@"authToken"] forKey:@"authToken"];
            [self pushToken];
            // 登录成功之后切换控制器，token必须每次都是新的
            self.window.rootViewController =self.company;
            
            if ([data[@"currentCompany"] isEqual:[NSNull null]]) {
                [self setUserCurrentCompanyWith_companyId:@3];
                [info setObject:@3 forKey:@"currentCompany"];
            }
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self postLogoutVC];
    }];
}

//服务器端设置当前用户ID对应的pushToken
-(void)pushToken{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    if ([info objectForKey:@"deviceToken"]==nil) {
        return;
    }
    parameter[@"pushToken"]=[info objectForKey:@"deviceToken"];//[BPush getChannelId];
    parameter[@"os"]=@"ios";
    NSLog(@"parameter is ：%@",parameter);
    [manager POST:SetPushToken parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        if ([responseObject[@"code"] isEqualToNumber:@1]) {
//            NSLog(@"error is %@",responseObject[@"error"]);
//        }else
        if ([responseObject[@"code"] isEqualToNumber:@200]) {
            NSLog(@"Dic is %@",responseObject);
            
        } else {
            [NSString stringGetErrorCodeWithCode:responseObject[@"code"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error is %@",error);
    }];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"userInfo：%@",userInfo);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartingFromTheNotification" object:self userInfo:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

//设置当前公司 解决退出或解散当前公司时 服务器当前公司为空问题
// 设置当前公司
- (void)setUserCurrentCompanyWith_companyId:(NSNumber *)_companyId
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    
    NSString *str = [NSString stringWithFormat:setCurrentCompany,_companyId];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 200) {
            
        }else{
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}


// 在 iOS8 系统中，还需要添加这个方法。通过新的 API 注册推送服务
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"deviceToken:%@",deviceToken);
    NSUserDefaults * info=[NSUserDefaults standardUserDefaults];
    [info setObject:deviceToken forKey:@"deviceToken"];
    [self pushToken];
    
}

// 当 DeviceToken 获取失败时，系统会回调此方法
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"DeviceToken 获取失败，原因：%@",error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // App 收到推送的通知
    NSLog(@"userInfo：%@",userInfo);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"userInfo：%@",notification);
    if(application.applicationState == UIApplicationStateActive)
    {//前台
        
    }
    else{//后台
        
    }
}

//微信回调
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  [UMSocialSnsService handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url  sourceApplication:(NSString *)sourceApplication  annotation:(id)annotation
{
    if ([url.description hasPrefix:@"file://"]) {
        //你的处理逻辑
        if (url != nil) {
            NSString *path = [url absoluteString];
            NSMutableString *string = [[NSMutableString alloc] initWithString:path];
            if ([path hasPrefix:@"file://"]) {
                [string replaceOccurrencesOfString:@"file://" withString:@"" options:NSCaseInsensitiveSearch  range:NSMakeRange(0, path.length)];
            }
            
            NSMutableDictionary * userInfo=[[NSMutableDictionary alloc] init];
            userInfo[@"ursStr"]=string;
            userInfo[@"groupId"]=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentCompany"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:self userInfo:userInfo];
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"StartingFromTheNotification" object:self userInfo:userInfo];
//                [self pushShareFileToAllGroupVC];
            });
            
            double delayInSecondss = 1.0;
            dispatch_time_t popTime2 = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSecondss * NSEC_PER_SEC));
            dispatch_after(popTime2, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"pushShareFileToAllGroupVC" object:self userInfo:userInfo];
//                [self pushShareFileToAllGroupVC];
            });
            
        }
        return YES;
    }else {
        return [UMSocialSnsService handleOpenURL:url];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"socketCloseConencted" object:nil];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    //进入前台时调用此函数
    [[NSNotificationCenter defaultCenter] postNotificationName:@"huidaoQianTai" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"socket_Conencted" object:nil];
//    if (!self.socketClient.connected) {
//        [self.socketClient connect];
//    }
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"socketCloseConencted" object:nil];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark 获取错误吗列表
- (void)getErrorCodeList{
    
//    NSData *data = [NSData dataWithContentsOfFile:path options:NSDataReadingMappedIfSafe error:nil];
//    self.errorCodeDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
//    [self.errorCodeDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//        NSLog(@"key = %@ and obj = %@", key, obj);
//    }];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:errorCodes parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *path=[paths objectAtIndex:0];
        NSString *Json_path=[path stringByAppendingPathComponent:@"errors.json"];

        if (operation.response.statusCode==200) {
    
            NSData *data = (NSData *)responseObject;
            BOOL is = [data writeToFile:Json_path atomically:YES];

        } else {
            NSFileManager *fileM = [NSFileManager defaultManager];
            BOOL isExist = [fileM fileExistsAtPath:Json_path];
            if (isExist==0) { // 文件不存在
                NSString *error_path = [[NSBundle mainBundle] pathForResource:@"errors" ofType:@"json"];
                NSData *error_data = [NSData dataWithContentsOfFile:error_path options:NSDataReadingMappedIfSafe error:nil];
                [error_data writeToFile:Json_path atomically:YES];
            }
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
//    NSString *currentLanguage = currentLanguage;
//    NSLog(@"errorCodeDict＝%@",self.errorCodeDict);
}

- (NSDictionary *)errorCodeDict{
    if (_errorCodeDict==nil) {
        NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *path=[paths objectAtIndex:0];
        NSString *Json_path=[path stringByAppendingPathComponent:@"errors.json"];

        _errorCodeDict = [NSDictionary dictionaryWithContentsOfFile:Json_path];
        if (_errorCodeDict==nil) {
            NSData *data = [NSData dataWithContentsOfFile:Json_path options:NSDataReadingMappedIfSafe error:nil];
            _errorCodeDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        }
        
    }
    return _errorCodeDict;
}

@end
