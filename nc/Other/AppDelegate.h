//
//  AppDelegate.h
//  nc
//
//  Created by jianghuan on 15/3/30.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Socket_IO_Client_Swift/Socket_IO_Client_Swift-Swift.h>
@class CompanyModel;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain)SocketIOClient *socketClient; // socket连接

@property (nonatomic, retain) NSNumber *tempCopId; // 当前公司的ID，切换公司的时候用

@property (nonatomic, retain) CompanyModel *tempCopModel; // 当前公司的信息
@property (nonatomic, retain) NSDictionary *errorCodeDict; // 错误吗字典
//@property (nonatomic, assign) BOOL isLocalCode; // 判断是否是本地错误吗。默认为NO
-(void)socketStatusIsConnect; //确保socket 意外断了能 重连

- (void)AppDelegateSocketConnect;  //注册后第一次登陆需要 初始化socket参数调用

@end

