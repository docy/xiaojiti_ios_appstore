//
//  CompanyListTVCell.h
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import <UIKit/UIKit.h>

@interface CompanyListTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *companyImageView;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *userCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
