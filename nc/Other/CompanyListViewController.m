//
//  CompanyListViewController.m
//  
//
//  Created by guanxf on 15/10/19.
//
//

#import "CompanyListViewController.h"
#import "CompanyNameViewController.h"
#import "CompanyModel.h"
#import "CompanyModel.h"
#import <AFNetworking/AFNetworking.h>
#import "UIBarButtonItem+XJTBarButtonItem.h"
#import "CompanyTableViewCell.h"
#import "CompanyListTVCell.h"
#import "ToolOfClass.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "CreatGroupViewController.h"
#import "RegisterCreatCompanyViewController.h"

@interface CompanyListViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSNumber *_companyId; // 公司ID，设置当前公司用
}

@property (nonatomic, retain) NSMutableArray *companyList;      // 已加入公司列表
@property (nonatomic, retain) NSMutableArray *companyCanJoinList;      // 未加入且能公司列表
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CompanyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    _companyList=[[NSMutableArray alloc] init];
    _companyCanJoinList=[[NSMutableArray alloc] init];
    // 设置基本属性
    [self setUpCompanyNameViewData];
    
    [self getCanJoinCompanyList];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden=YES;
    [self setUsersCompanyList];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
}

-(void)viewWillDisappear:(BOOL)animated

{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

// 设置基本属性
- (void)setUpCompanyNameViewData
{
    self.navigationController.navigationBar.hidden=YES;
    self.navigationItem.title = @"公司名称";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(onVotifyButtonClick)];
    self.navigationItem.leftBarButtonItem = nil;
    
    UIBarButtonItem *item =[[UIBarButtonItem alloc] init];
    UIImage* image = [UIImage imageNamed:@"navigation_back"];
    [item setBackButtonBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(0, image.size.width, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [item setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = item;
}

- (void)onLeftViewAddGroupButtonClick{
    RegisterCreatCompanyViewController *creatCompanyVC = [[RegisterCreatCompanyViewController alloc] init];
//    creatCompanyVC.isPersonalViewPush = YES;
    creatCompanyVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:creatCompanyVC animated:YES];
    
//    CreatGroupViewController *creatGroupVC = [[CreatGroupViewController alloc] init];
//    creatGroupVC.navigationController.navigationBar.hidden=NO;
//    creatGroupVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:creatGroupVC animated:YES];
}

// 获取用户的公司列表
- (void)setUsersCompanyList
{
    [self.companyList removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"] forKey:@"accessToken"];
    [manager GET:userCompanyListPath parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                CompanyModel *model = [[CompanyModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                if ([self.companyName isEqualToString:model.name]) {
                    model.isCurrentCpy = YES;
                } else {
                    model.isCurrentCpy = NO;
                }
                [self.companyList addObject:model];
            }
            
            /**
             *  遍历数组找到当前公司把他放在第一个显示
             */
            
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            NSNumber *comid=[info objectForKey:@"currentCompany"];
            
            for (int i=0; i<_companyList.count; i++) {
                CompanyModel *model=_companyList[i];
                if ([comid isEqualToNumber:model.id]) {
                    if (i!=0) {
                        [_companyList exchangeObjectAtIndex:i withObjectAtIndex:0];
                        break;
                    }
                }
            }
            [self.tableView reloadData];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}


//所有能加入的公司列表
- (void)getCanJoinCompanyList
{
    [_companyCanJoinList removeAllObjects];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    [manager GET:canJoinCompanyList parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"canJoinCompanyList:%@",responseObject);
        if ([responseObject[@"code"] isEqualToNumber:@0]) {
            for (NSDictionary *dict in responseObject[@"data"]) {
                CompanyModel *model = [[CompanyModel alloc] init];
                [model setValuesForKeysWithDictionary:dict];
                [self.companyCanJoinList addObject:model];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

#pragma mark - Table view data source


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (iPhone6Plus) {
        return  736/2;
    }
    if (iPhone6) {
        return  667.0/2.0;
    }
    return 277;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.companyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellId";
    CompanyListTVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CompanyListTVCell" owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    CompanyModel *model = self.companyList[indexPath.row];
    cell.companyName.text = model.name;
    
    cell.creatorLabel.text=[NSString stringWithFormat:@"创建者：%@",model.creator];
    cell.userCountLabel.text=[NSString stringWithFormat:@"人数：%@",model.userCount];
    if (model.desc==nil) {
        cell.descriptionLabel.text=@"世界这么大  我就不看了  根本看不完  别被累趴下";
    }else{
        cell.descriptionLabel.text=model.desc;
    }
    if (model.logoUrlOrigin!=nil) {
        [cell.companyImageView sd_setImageWithURL:[NSURL URLWithString:[iconPath stringByAppendingString:model.logoUrlOrigin]] placeholderImage:nil];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CompanyModel *model = self.companyList[indexPath.row];
    _companyId = model.id;
    [self setUserCurrentCompany];
}

// cell的选择按钮点击事件
- (void)onSetCurrentCompanyBtnClick:(UIButton *)sender
{
    if (sender.selected) {
        return;
    }
    
    sender.selected = !sender.selected;
    for (NSInteger i = 0; i < self.companyList.count; i++) {
        CompanyModel *model = self.companyList[i];
        if (sender.tag-800==i) {
            model.isCurrentCpy = YES;
        } else {
            model.isCurrentCpy = NO;
        }
        
    }
    [self.tableView reloadData];
    
    CompanyModel *model = self.companyList[sender.tag-800];
    _companyId = model.id;
    //  发送设置为当前公司
    [self setUserCurrentCompany];
}
// 设置当前公司
- (void)setUserCurrentCompany
{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:@"正在载入集体..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    
    NSString *str = [NSString stringWithFormat:setCurrentCompany,_companyId];
    [manager POST:str parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] intValue] == 0) {
            NSUserDefaults *info = [NSUserDefaults standardUserDefaults];
            [info setObject:_companyId forKey:@"currentCompany"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SwitchCurrentCompany" object:nil];
            double delayInSeconds = 0.6;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [SVProgressHUD dismiss];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postTabbarVC" object:nil];
            });
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"添加集体失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }];
}

- (IBAction)addButtonClick:(id)sender {
    [self onLeftViewAddGroupButtonClick];
}

- (IBAction)searchCompanyList:(id)sender {
    CompanyNameViewController * compNameListVC=[[CompanyNameViewController alloc] initWithNibName:@"CompanyNameViewController" bundle:nil];
    [self.navigationController pushViewController:compNameListVC animated:YES];
}

@end
