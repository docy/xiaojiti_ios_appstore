//
//  APIHeader.h
//  nc
//
//  Created by docy admin on 15/8/25.
//  Copyright (c) 2015年 cn.dossi. All rights reserved.
//

#ifndef nc_APIHeader_h
#define nc_APIHeader_h

// 正式服务器 http://docy.datacanvas.io
//测试服务器 http://im.datacanvas.io

#define SearchBarHeight 40
#define groupCellHeight 100
#define badNetWorkViewHeight 44

#define NSLocalized(text) NSLocalizedString(text, nil) //多语言支持

#define currentLanguage [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0] //当前语言

#define isEnglish ([currentLanguage isEqualToString:@"en"]||[currentLanguage isEqualToString:@"en-CN"]) //当前语言是否是英文

#define ScreenWidth [[UIScreen mainScreen] bounds].size.width//获取屏幕宽度
#define ScreenHeight [[UIScreen mainScreen] bounds].size.height//获取屏幕高度

#define errorCodes @"http://docy.datacanvas.io/assets/errors.json" // 获取错误吗
#define iconPath @"http://docy.datacanvas.io" // 域名，用于拼接用户头像
#define LoginPath  @"http://docy.datacanvas.io/v2/users/login" // 登录APIhttp://54.223.82.102///@"http://docy.datacanvas.io/v2/users/login" // 登录API
//#define LoginPath @"http://54.223.82.102/v2/users/login" // 登录API
#define WEIXIN_LoginPath @"http://docy.datacanvas.io/v2/users/weixinlogin"   //微信登陆

#define SetPushToken @"http://docy.datacanvas.io/v2/users/setpushToken" //设置百度pushToken
#define socketServerPath @"http://docy.datacanvas.io" // 服务器连接

#define getidentifyCodePath @"http://docy.datacanvas.io/v2/sms/signup" // 获取验证码
//#define identifyCodePath @"http://docy.datacanvas.io/v2/sms/signupSms" // 获取验证码
#define registerPath @"http://docy.datacanvas.io/v2/users/regist" // 注册请求
#define registCheck @"http://docy.datacanvas.io/v2/users/registCheck" // 注册信息检测是否符合要求

// 公司
#define canJoinCompanyList @"http://docy.datacanvas.io/v2/companies/canJoin" // 公司列表
#define CurrentCompanyPath @"http://docy.datacanvas.io/v2/users/currentCompany" // 获取当前公司
#define setCurrentCompany @"http://docy.datacanvas.io/v2/users/currentCompany/%@" // 设置当前公司
#define joinCompany @"http://docy.datacanvas.io/v2/users/company/%@" // 添加公司
#define creatCompanyPath @"http://docy.datacanvas.io/v2/companies" // 创建公司API
#define leaveCompanyPath @"http://docy.datacanvas.io/v2/companies/%@/leave"  //退出公司
#define delectCompanyPath @"http://docy.datacanvas.io/v2/companies/del/%@"  //解散公司
#define GetUserList @"http://docy.datacanvas.io/v2/companies/%@/users"  //获取公司用户列表
#define GetCompanyInfo  @"http://docy.datacanvas.io/v2/companies/%d"     //获取公司info
#define UpdataCompanyInfo  @"http://docy.datacanvas.io/v2/companies/%@"     //更新公司info
#define JoinCompanyInv  @"http://docy.datacanvas.io/v2/companies/join/%@" //通过邀请码加入公司
#define userCompanyListPath @"http://docy.datacanvas.io/v2/users/companies" // 获取用户公司列表
#define companyOwnerTransfer @"http://docy.datacanvas.io/v2/companies/%@/transfer" //公司权限转让
#define CompanyInviteCode  @"http://docy.datacanvas.io/v2/companies/%d/invite"
#define addCompany         @"http://docy.datacanvas.io/v2/users/company/" // 添加公司
#define allCompanyGroups   @"http://docy.datacanvas.io/v2/companies/%@/groups" // 获取当前公司所有小组列表
#define creatCompany @"http://docy.datacanvas.io/v2/companies" //  创建公司
#define randomLogo @"http://docy.datacanvas.io/v2/randomLogo?type=company"   //获得随机图片


// 群组
#define groupDetailInfoPath @"http://docy.datacanvas.io/v2/groups/%@" // 获取群组详细信息
#define getSpecifyGroupUsersListPath @"http://docy.datacanvas.io/v2/groups/%@/users" // 获取群组用户列表
#define logoutGroupPath @"http://docy.datacanvas.io/v2/groups/%@/leave" // 退出群组
#define setLogoPath @"http://docy.datacanvas.io/v2/groups/%@/logo" // 设置群组头像
#define userGroupsPath @"http://docy.datacanvas.io/v2/groups" // 获取群组列表(已加入)
#define creatGroupPath @"http://docy.datacanvas.io/v2/groups" // 创建群组
#define canJionGroupPath @"http://docy.datacanvas.io/v2/groups/canJoin" // 获取群组列表(未加入)
#define joinGroupPath @"http://docy.datacanvas.io/v2/groups/%@/join" // 加入群组
#define UpdateGroupInfo @"http://docy.datacanvas.io/v2/groups/%@" // 更改群组信息
#define getMessageListPath @"http://docy.datacanvas.io/v2/groups/%@/messages?page=%d" // 获取所有消息列表
#define getSpecifyGroupUsersListPath @"http://docy.datacanvas.io/v2/groups/%@/users" // 获取群组用户列表
#define ResetUnread @"http://docy.datacanvas.io/v2/groups/%@/resetunread"  //退出详情列表
#define MarkAudioReaded @"http://docy.datacanvas.io/v2/events/readAudio/%@"   //标记语音消息已读

/**
 * 个人资料
 */
#define personalInfoPath @"http://docy.datacanvas.io/v2/users/" // 获取个人信息
#define logoutPath @"http://docy.datacanvas.io/v2/users/logout" // 退出登录
#define setAvatarPath @"http://docy.datacanvas.io/v2/users/avatar" // 设置个人头像
#define uploadFilePath @"http://docy.datacanvas.io/v2/files/upload" // 在上传头像之前获取path
#define UpdateUserInfo @"http://docy.datacanvas.io/v2/users/update" // 更改用户信息
#define GetUserInfo @"http://docy.datacanvas.io/v2/users/%@"  //获取用户信息 支持用户id 或者是用户名


#define sendMessagePath @"http://docy.datacanvas.io/v2/events/message" // 发送消息


#define addCommentToTopicPath @"http://docy.datacanvas.io/v2/topics/%@/comment" // 为某一个topic添加评论
#define getTopicCommentsPath @"http://docy.datacanvas.io/v2/topics/%@/comment" // 获取topic评论列表
#define GetTopicDetail @"http://docy.datacanvas.io/v2/topics/%@" // 获取topic comment

#define getImageBeforeRegisterPath @"http://docy.datacanvas.io/v2/directrandomLogo?type=%@" // 在注册前获取图片
#define getRandomLogoPath @"http://docy.datacanvas.io/v2/randomLogo?type=%@" // 在注册前获取图片

#define addFavorite @"http://docy.datacanvas.io/v2/users/favorite/%@" // 关注
#define getFavorites @"http://docy.datacanvas.io/v2/users/favorites/?page=%d" // 获取关注

#define addTopicsVote @"http://docy.datacanvas.io/v2/topics/vote"     //创建投票子话题接口
#define sendVoteRes @"http://docy.datacanvas.io/v2/topics/vote/%@"     //创建投票子话题接口
#define sendCloseVote @"http://docy.datacanvas.io/v2/topics/vote/%@/close"   //关闭子话题接口

#define sendImage @"http://docy.datacanvas.io/v2/images"  //上传图片接口 投票图片

#define iPhone6Plus ScreenWidth==414.0?YES:NO
#define iPhone6 ScreenWidth==375.0?YES:NO

// 创建主题
#define creatPostTopic @"http://docy.datacanvas.io/v2/topics/post" // 创建主题
#define creatFileTopic @"http://docy.datacanvas.io/v2/topics/file"   // 创建文件子话题
#define creatImageTopic @"http://docy.datacanvas.io/v2/topics/image" // 创建图片子话题
#define creatMapTopic @"http://docy.datacanvas.io/v2/topics/map" // 创建地图子话题
#define deleteTopic @"http://docy.datacanvas.io/v2/topics/del/%@" // 删除子话题
#define getSubTopicListPath @"http://docy.datacanvas.io/v2/topics/?groupId=%@&page=%d" // 获取子话题列表
#define getSubTopicList @"http://docy.datacanvas.io/v2/topics"   //子话题列表接口
#define topicForward @"http://docy.datacanvas.io/v2/topics/%@/forward"   // 主题流转
/**
 *  私聊
 */

#define directChat @"http://docy.datacanvas.io/v2/groups/directChat" 
#define directChatSendImage @"http://docy.datacanvas.io/v2/events/image"
#define directChatSendAudio @"http://docy.datacanvas.io/v2/events/audio"

#define GroupOwnerTransfer @"http://docy.datacanvas.io/v2/groups/%@/transfer/%@"
#define GroupOwnerDisbandGroup @"http://docy.datacanvas.io/v2/groups/delete/%@"
#define SetGroupPrivate @"http://docy.datacanvas.io/v2/groups/%@/setPrivate"
#define SetGroupBroadcast @"http://docy.datacanvas.io/v2/groups/%@/broadcast" // 设置广播群


#define DeleteDirectChat @"http://docy.datacanvas.io/v2/groups/directchat/del/%@" // 删除私聊会话

#define ResetPassword @"http://docy.datacanvas.io/v2/users/resetPassword" // 忘记密码
#define ResetPasswordSms @"http://docy.datacanvas.io/v2/sms/forgotPassword" // 忘记密码验证码
#define ResetPhoneSms @"http://docy.datacanvas.io/v2/sms/updatePhone"//更新手机号

#define InviteJoinGroup @"http://docy.datacanvas.io/v2/groups/%@/invite" // 邀请加入群组

// 通知name名称
#define GetGroupNotificationLevel @"http://docy.datacanvas.io/v2/groups/%@/notification" // 获取通知级别 
#define conveyGroupOwer @"conveyGroupOwer" // 转换群主
#define disbandGroup @"disbandGroup" // 解散小组
//#define CFBundleShortVersionString @"CFBundleShortVersionString"
#define CFBundleVersion @"CFBundleVersion"

// 颜色值
#define MainColor [UIColor colorWithRed:72/255.0 green:193/255.0 blue:168/255.0 alpha:1]
#define CustomColor(a,b,c) [UIColor colorWithRed:a/255.0 green:b/255.0 blue:c/255.0 alpha:1]
#define COLOR_WHITE [UIColor whiteColor]

// 用户的ID
#define USER_ID [[NSUserDefaults standardUserDefaults] objectForKey:@"id"]
#define DEFAULTS [NSUserDefaults standardUserDefaults]
#define CURRENT_VERSION [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]
#define LAET_DATE @"lastDate" // 当前时间，用于升级问题

#define APP_DELEGATE (AppDelegate *)[[UIApplication sharedApplication] delegate] // appdelegate

// 弹框提示
#define ALERT_HOME(title,msg)\
{\
UIAlertView *HomeAlert=[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil]; \
[HomeAlert show]; \
}

// 图片压缩值
#define ImageYaSuo 0.1

#define UM_key @"5639b1c7e0f55a6cec0028bb"

//AppStore 5639b1c7e0f55a6cec0028bb
//企业版 55cd5a62e0f55ae5dd0051f6

// 群主踢人
#define Group_KickUser @"http://docy.datacanvas.io/v2/groups/%@/kick"

//检测版本更新接口
#define Versions @"http://docy.datacanvas.io/v2/versions"

#define durationTime 600       //录音超时（ms）

/**
 *  ============ 服务器端的消息分类 ===============
 */
//以下是普通消息类
#define TYPE_MSG = 1;
#define TYPE_SOUND = 15;

//以下是控制类
#define TYPE_CREATE = 2;
#define TYPE_JOIN = 3;
#define TYPE_LEAVE = 4;
#define TYPE_KICK = 5;

//以下是topic主题类及其子类
#define TYPE_TOPIC = 7;
#define SUBTYPE_FILE = 1;
#define SUBTYPE_IMAGE = 2;
#define SUBTYPE_MAP = 3;
#define SUBTYPE_VOTE = 4;
#define SUBTYPE_TEXT = 5;
#define SUBTYPE_VOTE_CLOSE = 6;
#define SUBTYPE_LINK = 7;

//以下是comment评论类及其子类
#define TYPE_TOPIC_COMMANT = 8;
#define SUBTYPE_TEXT_COMMENT = 1;
#define SUBTYPE_IMAGE_COMMENT = 2;
#define SUBTYPE_LINK_COMMENT = 4;
#define SUBTYPE_SOUND_COMMENT = 5;

//以下是私信专有图片及地图类
#define TYPE_TOPIC_IMAGE_DIRECTROOM = 12;
#define TYPE_TOPIC_MAP_DIRECTROOM = 13;



#endif
