//
//  ShareViewController.m
//  SystemShare
//
//  Created by guanxf on 16/3/28.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "ShareViewController.h"
#import "ListTableViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AFNetworking/AFNetworking.h>
#import <UIKit/UIAlertView.h>
#import <UIKit/UINavigationBar.h>
#import "APIHeader.h"
@interface ShareViewController ()<UITextViewDelegate>{
    NSString * urlString;
    NSString * conTextString;
    UIAlertController * alertvc;
}

@property(nonatomic,retain) NSNumber* groupId;
@property(nonatomic,retain) NSString* groupName;
@property(nonatomic,strong) UIImage* image;
@property(nonatomic,retain) NSString* imageName;
@property (nonatomic, strong) NSURL * FileCopyPath; // 用于创建文件子话题的文件路径


@end

@implementation ShareViewController

//#define creatPostTopic @"http://docy.datacanvas.io/v2/topics/post" // 创建主题
//#define creatImageTopic @"http://docy.datacanvas.io/v2/topics/image" // 创建图片子话题
//#define creatFileTopic @"http://docy.datacanvas.io/v2/topics/file"   // 创建文件子话题

- (void)viewDidLoad
{
    
    NSExtensionItem * imageItem = [self.extensionContext.inputItems firstObject];
    
    NSItemProvider * imageItemProvider = [[imageItem attachments] firstObject];
    
    self.textView.delegate=self;
    
    self.navigationItem.title=@"小集体";
    
    if (!urlString) {
        urlString=[[NSString alloc] init];
    }
    
    if (!conTextString) {
        conTextString=[[NSString alloc] init];
    }
    
    [self fetchItemDataAtBackground];
    
    if([imageItemProvider hasItemConformingToTypeIdentifier:(NSString*)kUTTypeURL])
    {
        NSLog(@"----kUTTypeURL is :%@",kUTTypeURL);
        [imageItemProvider loadItemForTypeIdentifier:(NSString*)kUTTypeURL options:nil completionHandler:^(NSURL* imageUrl, NSError *error) {
            //在这儿做自己的工作
            urlString=imageUrl.absoluteString;
            
        }];
    }
    
    //app group路径
    NSURL *containerURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:@"group.docy.co"];
    NSLog(@"app group:\n%@",containerURL.path);
    
    //打印可执行文件路径
    NSLog(@"bundle:\n%@",[[NSBundle mainBundle] bundlePath]);
    
    //打印documents
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSLog(@"documents:\n%@",path);
    
}

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
//    NSLog(@"------textView   is:%@",self.textView.text);
//    NSLog(@"------contentText   is:%@",self.contentText);
    conTextString=self.contentText;
    
//    self.textView.text=[NSString stringWithFormat:@"%@\n%@",conTextString,urlString];
    
    NSExtensionItem * imageItem = [self.extensionContext.inputItems firstObject];
    if(!imageItem)
    {
        return NO;
    }
    NSItemProvider * imageItemProvider = [[imageItem attachments] firstObject];
    if(!imageItemProvider)
    {
        return NO;
    }
    if([imageItemProvider hasItemConformingToTypeIdentifier:@"public.url"]&&self.contentText)
    {
        NSLog(@"self.contentText is:%@",self.contentText);
        return YES;
    }
    
    return YES;
}

- (void)didSelectPost {
    
    if (_groupId==NULL) {
        return;
    }
    
    NSExtensionItem * imageItem = [self.extensionContext.inputItems firstObject];
    NSLog(@"self.extensionContext.inputItems is %@",self.extensionContext.inputItems);
    //完成一些自己的操作 保存，添加 http请求
    
    NSExtensionItem * outputItem = [imageItem copy];
    
    outputItem.attributedContentText = [[NSAttributedString alloc] initWithString:self.contentText attributes:nil];
    
    NSArray * outPutitems= @[outputItem];
    
//    NSLog(@"userinfo is %@",outputItem.userInfo);
    conTextString=self.contentText;
    [self.extensionContext completeRequestReturningItems:outPutitems completionHandler:nil];
    NSString* str=nil;
    if (conTextString!=NULL&&![urlString isEqualToString:@""]) {
        str=[NSString stringWithFormat:@"%@\n%@",conTextString,urlString];
    }else{
        if (conTextString==NULL) {
            str= [NSString stringWithFormat:@"%@",urlString];
        }else{
            str= [NSString stringWithFormat:@"%@",conTextString];
        }
    }
    
    [self sendTopicMessageWithTitle:str];
    
//    [self postDataWithTitle:str];
    
}

- (void)fetchItemDataAtBackground{

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0),^{
        NSArray*inputItems=self.extensionContext.inputItems;
        NSExtensionItem*item=inputItems.firstObject;//无论多少数据，实际上只有一个NSExtensionItem对象
        for (NSItemProvider *provider in item.attachments) {
        //completionHandler是异步运行的
            NSString*dataType=provider.registeredTypeIdentifiers.firstObject;//实际上一个NSItemProvider里也只有一种数据类型
            
            if ([dataType isEqualToString:@"public.image"]) {
                [provider loadItemForTypeIdentifier:dataType options:nil completionHandler:^(UIImage *image, NSError *error){
                    self.image=image;
                    //collect image...
                }];
            }else if ([dataType isEqualToString:@"public.png"]) {
                [provider loadItemForTypeIdentifier:dataType options:nil completionHandler:^(UIImage *image, NSError *error){
                    self.image=image;
                    //collect image...
                }];
            }else if ([dataType isEqualToString:@"public.jpeg"]) {
                [provider loadItemForTypeIdentifier:dataType options:nil completionHandler:^(UIImage *image, NSError *error){
                    self.image=image;
                    //collect image...
                }];
            }else if ([dataType isEqualToString:@"public.plain-text"]){
                [provider loadItemForTypeIdentifier:dataType options:nil completionHandler:^(NSString *contentText, NSError *error){
                    //collect image...
                }];
            }else if ([dataType isEqualToString:@"public.url"]){
                [provider loadItemForTypeIdentifier:dataType options:nil completionHandler:^(NSURL *url, NSError *error){
                urlString=url.absoluteString;
                    //collect url...
                }];
            }else if ([dataType isEqualToString:@"public.movie"]){
                [provider loadItemForTypeIdentifier:dataType options:nil completionHandler:^(NSURL * FilelUrl, NSError *error){
                    self.FileCopyPath=FilelUrl;//[FilelUrl absoluteString];
                    NSLog(@"movie is %@",FilelUrl);
                    //collect url...
                }];
            }else if ([dataType isEqualToString:@"com.apple.quicktime-movie"]){
                [provider loadItemForTypeIdentifier:dataType options:nil completionHandler:^(NSURL * FilelUrl, NSError *error){
                    self.FileCopyPath=FilelUrl;//[FilelUrl absoluteString];
                    //collect url...
                }];
            }
            

            NSLog(@"don't support data type: %@", dataType);
        }
    });
}

- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    SLComposeSheetConfigurationItem * item =[[SLComposeSheetConfigurationItem alloc]init];
    item.title=@"发送给";
    item.value=_groupName!=NULL?_groupName:@"请选择小组";
    item.valuePending = NO;
    item.tapHandler = ^(void)
    {
        ListTableViewController * listVC = [[ListTableViewController alloc] init];
        listVC.databack=^(NSDictionary * dic){//groupId
            NSLog(@"groupid is :%@",dic[@"groupId"]);
            _groupId=dic[@"groupId"];
            _groupName=dic[@"groupName"];
            [self reloadConfigurationItems];
        };
        [self pushConfigurationViewController:listVC];
        
    };
    SLComposeSheetConfigurationItem * item2 =[[SLComposeSheetConfigurationItem alloc]init];
    item2.title=@"选择小组";
    item2.value=@"iOS问题反馈";
    item2.valuePending = NO;
    item2.tapHandler = ^(void)
    {
        ListTableViewController * listVC = [[ListTableViewController alloc] init];
        [self pushConfigurationViewController:listVC];
        
    };
    return @[item];
}

-(NSString *)mytoolSetUpClientIdWithGroupId:(NSNumber *)groupId{
    NSString *clientId = nil;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSDate *data = [NSDate date];
    clientId = [NSString stringWithFormat:@"%@_%@_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"id"],groupId,[formatter stringFromDate:data]];
    return clientId;
}

// 创建topic 之前的发送主题包括所有的主题
- (void)sendTopicMessageWithTitle:(NSString *)title
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *info = [[NSUserDefaults alloc] initWithSuiteName:@"group.docy.co"];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    parameter[@"groupId"] = self.groupId;
    parameter[@"title"] = title;
    parameter[@"clientId"] = [self mytoolSetUpClientIdWithGroupId:self.groupId];
    
    if (self.imageName==nil) {
        self.imageName=@"image";
    }
    
    NSString * titleString=@"file";
    
    NSString *postStr = nil;
    NSData *imageData = nil;
    NSData * FileData=nil;
    if (_image!=NULL) {
        imageData=UIImageJPEGRepresentation(self.image, 0.8);
        postStr = [creatImageTopic copy];
    }else{
        if (_FileCopyPath!=NULL) {   //发表文件子话题
            postStr = [creatFileTopic copy];
            FileData=[[NSData alloc] initWithContentsOfFile:_FileCopyPath];
        }else{
            postStr = [creatPostTopic copy];
        }
    }
    [manager POST:postStr parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (_image!=NULL) {
            //                [ToolOfClass toolUploadFilePathWithHttpString:postStr fileName:titleString file:nil setAvatarOrLogoPath:nil];
            [formData appendPartWithFileData:imageData name:@"image" fileName:self.imageName mimeType:@"image/jpg"];
        }else{
            if (_FileCopyPath!=NULL) {   //发表文件子话题
                NSRange range=[_FileCopyPath.absoluteString rangeOfString:@"."];
                NSString * fileType=[_FileCopyPath.absoluteString substringFromIndex:range.location+1];
                [formData appendPartWithFileData:FileData name:@"file" fileName:[NSString stringWithFormat:@"%@.%@",titleString,fileType] mimeType:[NSString stringWithFormat:@"application/%@",fileType ]];
            }
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"code"] isEqualToNumber:@200]) {// 发送成功
            NSLog(@"-----分享上传成功！");
        }
        self.navigationItem.rightBarButtonItem.enabled=YES;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            [ToolOfClass showMessage:NSLocalized(@"vote_send_failure")];
//            self.navigationItem.rightBarButtonItem.enabled=YES;
    }];
    
}

-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textViewDidChange is :%@",textView.text);
    if (textView.text.length==0) {
        self.navigationItem.rightBarButtonItem.enabled=NO;
//        self.navigationController.navigationBar.backgroundColor=[UIColor redColor];
    }else{
        self.navigationItem.rightBarButtonItem.enabled=YES;
//        self.navigationController.navigationBar.backgroundColor=[UIColor blueColor];
    }
}


@end
