//
//  ListTableViewController.h
//  nc
//
//  Created by guanxf on 16/3/28.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^dataBack)(NSDictionary*);

@interface ListTableViewController : UITableViewController

@property(nonatomic,strong) dataBack databack;

@end
