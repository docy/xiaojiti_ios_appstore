//
//  ListTableViewController.m
//  nc
//
//  Created by guanxf on 16/3/28.
//  Copyright © 2016年 cn.dossi. All rights reserved.
//

#import "ListTableViewController.h"
//#import <AFNetworking/AFNetworking.h>
#import "APIHeader.h"

@interface ListTableViewController (){
    NSURLSession * mySession;
}

@property (nonatomic, strong) NSMutableArray *groupData;
@property (nonatomic, strong) NSMutableArray *PrivateGroupData; // 私聊群组

@end

@implementation ListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self getGroupMyData];
    self.tableView.backgroundColor=[UIColor clearColor];
//    [self getUpLeftViewUserGroupList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _groupData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * cellID=@"reuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    NSDictionary *model=[_groupData objectAtIndex:indexPath.row];
    cell.textLabel.text=[NSString stringWithFormat:@" %@ ",model[@"name"]];
    cell.tag=[model[@"id"] intValue];
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * indexDic=[_groupData objectAtIndex:indexPath.row];
    NSDictionary * dic=[[NSDictionary alloc] initWithObjectsAndKeys:indexDic[@"id"],@"groupId",indexDic[@"name"],@"groupName", nil];
    self.databack(dic);
    [self.navigationController popViewControllerAnimated:YES];
}

#define ShareUserGroupsPath @"http://docy.datacanvas.io/v2/groups" // 获取群组列表(已加入)


-(void)getGroupMyData{
    //GET请求, 也可以给服务器发送信息, 也有参数(微博用户名,用户id)
    //1.构造URL, 参数直接拼接在url连接后
    NSUserDefaults *info = [[NSUserDefaults alloc] initWithSuiteName:@"group.docy.co"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?accessToken=%@",ShareUserGroupsPath,[info objectForKey:@"authToken"]]];//@"http://news-at.zhihu.com/api/3/news/4602734"];
    
    //2.构造Request
    //把get请求的请求头保存在request里
    //NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 参数
    // (1)url
    // (2)缓存策略
    // (3)超时的时间, 经过120秒之后就放弃这次请求
    //NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:120];
    //NSURLRequest 不可变,不能动态的添加请求头信息
    
    //可变的对象
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //(1)设置请求方式
    [request setHTTPMethod:@"GET"];
    
    //(2)超时时间
    [request setTimeoutInterval:120];
    
    //(3)缓存策略
    [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
    

    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = [info objectForKey:@"authToken"];
    NSLog(@"accessToken is :%@",[info objectForKey:@"authToken"]);
    //(4)设置请求头其他内容
//    [request setValue:[info objectForKey:@"authToken"] forHTTPHeaderField:@"accessToken"];
//    [request addValue:[info objectForKey:@"authToken"] forHTTPHeaderField:@"accessToken"];
//    [request setAllHTTPHeaderFields:parameter];
    
    
    //3.构造Session
    NSURLSession *session = [NSURLSession sharedSession];
    
    //4.构造要执行的任务task
    /**
     * task
     *
     * @param data 返回的数据
     * @param response 响应头
     * @param error 错误信息
     *
     */
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error == nil) {
            
//            NSString *dataStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSLog(@"data: %@", dataStr);
            
            [self.groupData removeAllObjects];
            
            NSDictionary * responseObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];

            for (NSDictionary *dict in responseObject[@"data"]) {
                
                if ([dict[@"category"] isEqualToNumber:@3]) { // 私聊群组
                    
                } else {
                    [_groupData addObject:dict];
                }
            }
            
            [self.tableView reloadData];
            
        }
    }];
    
    //5.
    [task resume];
}

-(NSMutableArray*)groupData{
    if (!_groupData) {
        _groupData=[[NSMutableArray alloc] init];
    }
    return _groupData;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
